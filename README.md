###Zadania związane z oprogramowaniem i badaniem stanowiska zrobotyzowanego montażu teleskopu###
Zespoły: 

**WE** - S. Brock (SB), D. Janiszewski (DJ), K. Nowopolski (KN)

**WI** - 
     
					
### 1. Modelowanie i konfiguracja napędu bezpośredniego  - WE
1. studium literatury 
1. opracowanie modelu matematycznego napędu bezpośredniego 
1. implementacja modelu w środowisku symulacyjnym 
1. identyfikacja 

### 2. Synteza algorytmów sterowania dla napędu jednej osi  - WE
1. Studium literatury i dotychczasowych rozwiązań - SB, DJ
1. Synteza praw sterowania dla sterowania napędem 
1. Oprogramowanie narzędziowe
     1. Rejestrator i narzędzia w Matlabie do odczytu danych - SB, III.2017 *(procedury rejestracji danych typu integer, float i double - do 4 rejestrowanych kanałów)*
     1. Generator sygnałowy - SB, III.2017 *(sygnały trójkąt, sinus, piła, trapez, s-shape; dostępne także 1 i 2 pochodna sygnałów)*
     1. Filtr dolnoprzepustowy - SB, IV.2017 *( filtr 1-szego i 2-giego stopnia )*
     1. Organizacja systemu przerwań w stanowisku osi - DJ, I.2017
     1. Odczyt położenia z głowic BISS - DJ, I.2017
     1. Fuzja pozycji z głowic - DJ
     1. Odczyt prądów fazowych - DJ, II.2017  
     1. Generowanie SV-PWM dla przekształtnika - DJ, I.2017
     1. Wyznaczanie i filtracja prędkości - SB, III.2017 *(uzupełnianie brakujących odczytów położenia, filtracja FIR eliminująca szum)*
1. Implementacja algorytmu sterowania na mikrokontrolerze - obwód regulacji momentu
     1. Implementacja szablonu przekształceń Parka/Clark - DJ, I. 2017
     1. Implementacja szablonu bazowych regulatorów PID - DJ, I.2017
     1. Implementacja szablonu regulatora PI-2DOF - DJ, III.2017
     1. Implementacja predykcyjnego sterowania prądem - 
1. Implementacja algorytmu sterowania na mikrokontrolerze - obwód regulacji położenia/prędkości
     1. Implementacja regulatora położenia/prędkości typu PI - SB
     1. Implementacja regulatora położenia/prędkości typu ADRC - SB
     1. Implementacja układu modelu wzorcowego, z redukcją wybranych częstotliwości - SB
     1. Impelementacja cyfrowych filtrów w torze sprzężenia zwrotnego - DJ, SB
     1. Implementacja regulatora położenia/prędkości/pradu  typu MPC - DJ
     1. Kompensacja momentów pasożytniczych - SB
     1. Kompensacja tarcia - SB 
1. Interfejs wymiany danych z układem sterowania montażem
     1. Opracowanie modelu reprezentacji napędu osi w warstwie nadrzędnej - DJ
     1. Integracja z nadrzędnymi układami sterowania - DJ, SB


### 3. Eksperymentalna weryfikacja wyników dla napędu jednej osi  - WE
1. opracowanie metod weryfikacji i schematu doświadczeń 
1. przeprowadzenie eksperymentów 
1. strojenie parametrów sterowników, optymalizacja 
					
					
### 4. Modelowanie i konfiguracja zrobotyzowanego montażu teleskopu  - WI
1. studium literatury 
1. opracowanie modelu matematycznego montażu (równania dynamiki i kinematyki) 
1. implementacja modelu w środowisku symulacyjnym 
1. identyfikacja 
					
### 5. Konfiguracja i weryfikacja stanowiska laboratoryjnego: symulator pozycji gwiazdy  - WI
1. studium literatury 
1. opracowanie koncepcji symulatora gwiazdy 
1. dobór komponentów (wyświetlaczy) 
1. złożenie 
1. przygotowanie oprogramowania 
1. zapewnienie kanału komunikacyjnego i interfejsów 
											
### 6. Integracja modułów w jeden system  - WI
1. dobór teleskopu 
1. dobór kamery 
1. złożenie montażu, optyki i symulatora gwiazdy w jedno stanowisko eksperymentalne 
1. oprogramowanie narzędzi konwersji współrzędnych w złączach do współrzędnych astronomicznych 
					
### 7. Opracowanie praw sterowania zrobotyzowanym montażem opartym o geometrię różniczkową  - WI
 1. studium literatury 
 1. przegląd dostępnych rozwiązań w zakresie algorytmów sterowania 
 1. synteza praw sterowania 
 1. implementacja praw sterowania w środowisku symulacyjnym 
					
						
### 8. Eksperymentalna weryfikacja wyników dla zrobotyzowanego montażu teleskopu  - WI, WE
1. opracowanie metod weryfikacji i schematu doświadczeń
1. przeprowadzenie eksperymentów
1. strojenie parametrów sterowników, optymalizacja
					
### 9. Testy polowe układu sterowania napędów dla stabilizacji i śledzenie na niebie w Obserwatorium Astronomicznym Uniwersytetu im. A. Mickiewicza  - WI, WE
1. opracowanie metod weryfikacji i schematu doświadczeń
1. przeprowadzenie eksperymentów
1. strojenie parametrów sterowników, optymalizacja