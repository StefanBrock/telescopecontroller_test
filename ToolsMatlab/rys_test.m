clear all
x1 = read_TI_scope('x1',2);
x2 = read_TI_scope('x2',2);

x1_sm = smooth(x1,20);
x2_sm = smooth(x2,20);



t=(1:length(x1))*1e-4;
Start=round(t(1)/1e-4) ;
Stop =round( t(end)/1e-4); %length(t)

subplot(211)
plot1=plot(t(Start:Stop),x1(Start:Stop),'.b',t(Start:Stop),x1_sm(Start:Stop),'-r');
xlabel('time [s]');
ylabel('Ref q-axis current [A]');
set(plot1(2),'LineWidth',1);
grid on
subplot(212)
plot2=plot(t(Start:Stop),x2(Start:Stop),'b.',t(Start:Stop),x2_sm(Start:Stop),'-r');
xlabel('time [s]');
ylabel('Speed [rad/s]');
set(plot2(2),'LineWidth',1);
grid on

