/*
 * GlobalVariable.h
 *
 *  Created on: Apr 3, 2017
 *      Author: brock
 */





volatile STRUCT_ENABLESIGNALS EnableSignals;
volatile STRUCT_ENABLESIGNALS* pEnableSignals=&EnableSignals;

/// initialize pointer motion control flow
volatile STRUCT_MOTION_CONTROL_FLOW motion_control_flow; ///<defined in Foc_init
volatile STRUCT_MOTION_CONTROL_FLOW *pmotion_control_flow=&motion_control_flow; //trial new structure


volatile int scope_channel01[SCOPE_DATA_DIMENSION]; //memory for scope
volatile int scope_channel02[SCOPE_DATA_DIMENSION];
volatile int scope_channel03[SCOPE_DATA_DIMENSION];
volatile int scope_channel04[SCOPE_DATA_DIMENSION];
volatile int *ipscope_channel01 = (int*) scope_channel01; // pointers to integer
volatile int *ipscope_channel02 = (int*) scope_channel02;
volatile int *ipscope_channel03 = (int*) scope_channel03;
volatile int *ipscope_channel04 = (int*) scope_channel04;
volatile float *fpscope_channel01 = (float*) scope_channel01; // pointers to float
volatile float *fpscope_channel02 = (float*) scope_channel02;
volatile float *fpscope_channel03 = (float*) scope_channel03;
volatile float *fpscope_channel04 = (float*) scope_channel04;
volatile double *dpscope_channel01 = (double*) scope_channel01; // pointers to double
volatile double *dpscope_channel02 = (double*) scope_channel02;
volatile double *dpscope_channel03 = (double*) scope_channel03;
volatile double *dpscope_channel04 = (double*) scope_channel04;
volatile int scope_start = 0;   // switch of start/stop scope
volatile int scope_counter = 0; // scope counter
