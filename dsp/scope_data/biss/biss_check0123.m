
cd ./start50


clear all

fopen("biss1.hex")
biss1u=fread("biss1.hex","uint32");
%plot(1:131071,biss2a(1:131071))
fclose("biss1.hex")

fopen("biss2.hex")
biss2u=fread("biss2.hex","uint32");
fclose("biss2.hex")

fopen("biss3.hex")
biss3u=fread("biss3.hex","uint32");
%plot(1:131071,biss2a(1:131071))
fclose("biss3.hex")

fopen("biss4.hex")
biss4u=fread("biss4.hex","uint32");
fclose("biss4.hex")

figure;
plot(1:131071,biss1u(1:131071),1:131071,biss2u(1:131071),1:131071,biss3u(1:131071),1:131071,biss4u(1:131071))

figure;
subplot(4,1,1)
plot(1:131071,biss1u(1:131071));
subplot(4,1,2)
plot(1:131071,biss2u(1:131071));
subplot(4,1,3)
plot(1:131071,biss3u(1:131071));
subplot(4,1,4)
plot(1:131071,biss4u(1:131071));

%speed



speed1u=diff(biss1u);
speed2u=diff(biss2u);
speed3u=diff(biss3u);
speed4u=diff(biss4u);

speed1u2=speed1u;
speed2u2=speed2u;
speed3u2=speed3u;
speed4u2=speed4u;

overspeed = min(speed1u);
for count = 1:size(speed1u,1)
  if speed1u(count)<overspeed/2 speed1u2(count,1) = speed1u(count)-overspeed;
  end
end

overspeed = min(speed2u);
for count = 1:size(speed2u,1)
  if speed2u(count)<overspeed/2 speed2u2(count,1) = speed2u(count)-overspeed;
  end
end

overspeed = min(speed3u);
for count = 1:size(speed3u,1)
  if speed3u(count)<overspeed/2 speed3u2(count,1) = speed3u(count)-overspeed;
  end
end

overspeed = min(speed4u);
for count = 1:size(speed4u,1)
  if speed4u(count)<overspeed/2 speed4u2(count,1) = speed4u(count)-overspeed;
  end
end



figure; plot(1:131070,speed1u(1:131070),1:131070,speed2u(1:131070),1:131070,speed3u(1:131070),1:131070,speed4u(1:131070));

%figure; plot(1:131070,speed1u2(1:131070),1:131070,speed2u2(1:131070),1:131070,speed3u2(1:131070),1:131070,speed4u2(1:131070));

cd ..