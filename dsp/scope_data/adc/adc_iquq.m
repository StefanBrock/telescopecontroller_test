
%cd ./start50


clear all

fname = "iq2.hex";
ftype = "float32";

fopen(fname)
iq1f=fread(fname,ftype);
fclose(fname)

fname = "uq2.hex";
ftype = "float32";

fopen(fname)
uq1f=fread(fname,ftype);
fclose(fname)


figure;
plot(1:size(uq1f,1),uq1f,'b-',1:size(iq1f,1),iq1f,'r-');

Udc=43; %[V]
F=10e3; %[Hz]
T=1/F;
startplot=79950; %[samples]
stopplot=80300;  %size(uq1f,1);

figure;
subplot(2,1,1)
plot(T*(startplot:stopplot),uq1f(startplot:stopplot)*Udc,'b-')
xlabel("time [s]");
ylabel("q-axis voltage [V]");
subplot(2,1,2);
plot(T*(startplot:stopplot),iq1f(startplot:stopplot),'r-');
xlabel("time [s]");
ylabel("q-axis current [V]");

%figure;
%plot(adcnoise2u(1:200),'b-*');

b=ones(4,1)/4;
a=1;
iq1ff=filtfilt(b,a,iq1f);

figure; plot(startplot:stopplot,iq1ff(startplot:stopplot),'r-');

%FFT
iq1fft=iq1f(1:80000);
L=size(iq1fft,1);
Fs=10e3; %[Hz]
T=1/Fs; %[s]
t = (0:L-1)*T;
f = Fs*(0:(L/2))/L;
Y = fft(iq1fft);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P4 = arg(Y);
P3 = P4(1:L/2+1);

figure;
subplot(2,1,1);
loglog(f,P1) 
title('Single-Sided Amplitude Spectrum of iq(t)')
xlabel('f (Hz)')
ylabel('|iq(f)|')
subplot(2,1,2);
semilogx(f,P3) 
title('Single-Sided Argument Spectrum of iq(t)')
xlabel('f (Hz)')
ylabel('|iq(f)|')
