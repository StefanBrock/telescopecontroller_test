/*
 * SignalGenerator.c
 *
 * Created on: Apr 08, 2017
 *      Author: brock
 */

/*  SWITCH OFF FOR PC SIMULATION */
#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Types.h>
#include <ti/sysbios/BIOS.h>
#include "axis_motion_controller.h"
#include "SignalGenerator.h" ///< Signal Generator definitions
/* ****************************** */

/*  SWITCH OFF FOR REAL SYSYSTEM
#define PI 3.141592653589793
#include "C:\brock\OneDrive\Teleskop\telescopemotioncontroller\dsp\SignalGenerator\SignalGenerator.h" ///< Signal Generator definitions
#include <math.h>
* ***************************** */



double SignalGenerator(STRUCT_SIGNALGENERATOR *lps)
{

  double xx,x,y,abs_amplitude;
  int i;

  if ((lps->calculate) && (lps->shape == SG_SSHAPE))
  {
      lps->calculate=0;
      abs_amplitude = fabs(lps->amplitude);

      lps->i_period = lps->period/2;
      if (abs_amplitude>(lps->derivative*lps->derivative/lps->derivative2))
           y = (lps->period/4) - (lps->derivative/(2*lps->derivative2))-(abs_amplitude/(2*lps->derivative));
      else
           y = (lps->period/4)-sqrt(abs_amplitude/lps->derivative2);
      lps->i_pause = (y>0) ? 2*y : 0.0;
  }

  if ((lps->calculate) && (lps->shape == SG_PROFILE)) // set of SG_PROFILE defaults value
  {
   lps->calculate=0;
    for(i=0;i<SG_PROFILE_MAXNUMBER;i++)
   {
    if (lps->profile[i].time == 0.0) lps->profile[i].time=lps->period;
   }
   for(i=1;i<SG_PROFILE_MAXNUMBER;i++)
   {
    if (lps->profile[i].time < lps->profile[i-1].time) lps->profile[i].time=lps->profile[i-1].time;
   }
  }


  lps->current_time += lps->sampling_time;
  if (lps->current_time > lps->period) lps->current_time -= lps->period;
  if (lps->period <=0.0)
    {
        x = lps->offset;
        lps->output_signal=x;
        return (x);
    }
  switch (lps->shape)
  {
     case SG_CONST: // fixed value
         x = 1.0;
         break;
     case SG_RECTANGLE:
         if (lps->current_time < ((lps->period)/2)) x=-1.0; else x=1.0;
         lps->output_derivative = 0.0;
         break;
     case SG_SINUS:
          x=sinf(((lps->current_time)/(lps->period)) * 2 * PI);
          lps->output_derivative=((2 * PI)/lps->period) *cosf(((lps->current_time)/(lps->period)) * 2 * PI);
          break;
     case SG_TRIANGLE:
         if (lps->current_time < ((lps->period)/2))
         { x=4.0*(lps->current_time)/(lps->period);
           lps->output_derivative = 4.0 /lps->period;
         }
         else
            { x=4.0*((lps->period)-(lps->current_time))/(lps->period);
              lps->output_derivative = -4.0 /lps->period;
            }
         x -=1.0; x = lps->output_signal + lps->output_derivative*lps->sampling_time;
         break;
     case SG_SAW:
         x=2.0*(lps->current_time)/(lps->period);
         lps->output_derivative = 2.0/(lps->period);
		 x -=1.0;
         break;
     case SG_TRAPEZOID:
          if (lps->amplitude == 0.0)
            { x=0; lps->output_derivative = 0.0;
              break; }
          if (lps->current_time<lps->pause/2)
             { x=0.0; lps->output_derivative = 0.0;
               break;}
          if (lps->current_time<lps->period/2)
              { y=(lps->current_time-lps->pause/2)*fabs(lps->derivative/lps->amplitude);
                if (y<1.0)
                   {  x=y;
                      lps->output_derivative = fabs(lps->derivative/lps->amplitude);
                   }
             else { x= 1.0; lps->output_derivative = 0.0;}
             break; }
          if (lps->current_time<(lps->period-lps->pause/2))
             {   y=(lps->period-lps->current_time-lps->pause/2)*fabs(lps->derivative/lps->amplitude);
                 if (y<1.0)
                   {
                    x=y;
                    lps->output_derivative = -fabs(lps->derivative/lps->amplitude);
                   }
               else { x= 1.0; lps->output_derivative = 0.0;}
               break; }
           x=0.0;lps->output_derivative = 0.0;
           break;

     case SG_SSHAPE:
         if (lps->amplitude == 0.0)
            { xx=0; lps->output_derivative2 = 0.0;
              break; }
          if (lps->current_time<lps->i_pause/2)
             { xx=0.0; lps->output_derivative2 = 0.0;
               break;}
          if (lps->current_time<lps->i_period/2)
              { y=(lps->current_time-lps->i_pause/2)*fabs(lps->derivative2/lps->amplitude);
                if (y<fabs(lps->derivative/lps->amplitude))
                   {  xx=y;
                      lps->output_derivative2 = fabs(lps->derivative2/lps->amplitude);
                   }
             else { xx= fabs(lps->derivative/lps->amplitude); lps->output_derivative2 = 0.0;}
             break; }
          if (lps->current_time<(lps->i_period-lps->i_pause/2))
             {   y=(lps->i_period-lps->current_time-lps->i_pause/2)*fabs(lps->derivative2/lps->amplitude);
                 if (y<fabs(lps->derivative/lps->amplitude))
                   {
                    xx=y;
                    lps->output_derivative2 = -fabs(lps->derivative2/lps->amplitude);
                   }
               else { xx= fabs(lps->derivative/lps->amplitude); lps->output_derivative2 = 0.0;}
               break; }
          if (lps->current_time<(lps->i_pause/2+lps->i_period))
             { xx=0.0; lps->output_derivative2 = 0.0;
               break;}
          if (lps->current_time<(lps->i_period/2+lps->i_period))
              { y=(lps->current_time-lps->i_pause/2-lps->i_period)*fabs(lps->derivative2/lps->amplitude);
                if (y<fabs(lps->derivative/lps->amplitude))
                   {  xx=-y;
                      lps->output_derivative2 = -fabs(lps->derivative2/lps->amplitude);
                   }
             else { xx= -fabs(lps->derivative/lps->amplitude); lps->output_derivative2 = 0.0;}
             break; }
        if (lps->current_time<(lps->i_period-lps->i_pause/2+lps->i_period))
             {   y=(lps->i_period-lps->current_time-lps->i_pause/2+lps->i_period)*fabs(lps->derivative2/lps->amplitude);
                 if (y<fabs(lps->derivative/lps->amplitude))
                   {
                    xx=-y;
                    lps->output_derivative2 =  fabs(lps->derivative2/lps->amplitude);
                   }
               else { xx= -fabs(lps->derivative/lps->amplitude); lps->output_derivative2 = 0.0;}
               break; }
           xx=0.0;lps->output_derivative2 = 0.0;
        break;
     case SG_PROFILE:
        for (i=0;i<SG_PROFILE_MAXNUMBER;i++)
        {
          if (lps->current_time<lps->profile[i].time)
             { x=lps->profile[i].value;
               break;
             }
        }
        lps->output_derivative = 0.0;
        if (lps->derivative!=0) // with derivative limit
           {
              if (lps->output_signal<x) // up
              {
                  lps->output_signal+=(fabs(lps->derivative/lps->amplitude)*lps->sampling_time);
                  if (lps->output_signal>x) lps->output_signal=x;
                  lps->output_derivative = fabs(lps->derivative/lps->amplitude);
              }
              if (lps->output_signal>x) // down
              {
                  lps->output_signal-=(fabs(lps->derivative/lps->amplitude)*lps->sampling_time);
                  if (lps->output_signal<x) lps->output_signal=x;
                  lps->output_derivative = -fabs(lps->derivative/lps->amplitude);
              }
              x=lps->output_signal;
           }
        break;
     default:
         x =0.0;
         lps->output_derivative = 0.0;
         lps->output_derivative2 = 0.0;
         break;
  }
  if (lps->shape == SG_SSHAPE){
     lps->output_derivative = xx;
     x = lps->output_signal + xx*lps->sampling_time;
  }


  lps->output_signal=x;
  x *= lps->amplitude;
  x += lps->offset;
  lps->output_derivative *= lps->amplitude;
  lps->output_derivative2 *= lps->amplitude;
  return (x);

}




