/*
 * SignalGenerator.h
 *
 *  Created on: Apr 08, 2017
 *      Author: brock
 */

#define SG_CONST 0
#define SG_RECTANGLE 1
#define SG_SINUS 2
#define SG_TRIANGLE 3
#define SG_SAW 4
#define SG_TRAPEZOID 5       // trapezoid shape, for example: time-optimal speed control with limited acceleration
#define SG_SSHAPE 6          // S- shape, for example: time-optimal position control with limited speed and acceleration
#define SG_PROFILE 7

#define SG_PROFILE_MAXNUMBER 10

typedef struct ProfileElement_type{
   double time;
   double value;
   } PROFILEELEMENT;


 typedef struct SignalGenerator_type{
     double current_time;   // internal variable of Signal Generator channel
     double period;        // period of Signal Generator wave
     double sampling_time;  // sampling time of Signal Generator
     double amplitude;     // amplitude of wave
     double offset;        // offset of wave
     double pause;          // pause for SG_TRAPEZOID
     double derivative;     // derivative for SG_TRAPEZOID, SG_SSHAPE
     PROFILEELEMENT profile[SG_PROFILE_MAXNUMBER]; // table of sets: (value, end-time)
     double derivative2;     // 2nd order derivative for SG_SSSHAPE
     double i_pause;         // internal variable for SG_SSHAPE
     double i_period;        // internal variable for SG_SSHAPE
     double output_signal;  // additional output: main signal;
     double output_derivative; // additional output: derivative of main signal (valid for selected shapes)
     double output_derivative2; // additional output: 2nd derivative of main signal (valid for SG_SSHAPE shape only)
     int shape;    // shape of wave, using defined SG_* constants
     int calculate; // trigger for calculating of new value, valid for SG_SSHAPE only
 } STRUCT_SIGNALGENERATOR;
 //TODO: dynamic profile list can be useful

 double SignalGenerator(STRUCT_SIGNALGENERATOR *lps);



 /******************************************
  * Multi-channel signal generator
  *  The output (type: double ) changes periodical
  *  from (offset-amplitude) to (offset+amplitude)
  *
  * Each channel should be declared as a global (or static) variable:
  *
     STRUCT_SIGNALGENERATOR signalgenerator1;
     STRUCT_SIGNALGENERATOR* psignalgenerator1=&signalgenerator1;
  *
  *
  * Each channel should be initial configured
  *
    psignalgenerator1->current_time = 0.0;     // Internal variable
    psignalgenerator1->sampling_time = 1e-4;   // in seconds
    psignalgenerator1->period = 10.0;          // in seconds
    psignalgenerator1->amplitude = 10.0;       // can be negative
    psignalgenerator1->offset = 0.0;           // can be negative, too
    psignalgenerator1->derivative = 6;         // in (1/s), maximum absolute value of signal derivative  - SG_SSHAPE
    psignalgenerator1->derivative2 = 16;       // in (1/s2), maximum absolute value of signal 2nd derivative -SG_SSHAPE
    psignalgenerator1->pause = 1;              // in seconds, pause between waves - SG_TRAPEZOID only
    psignalgenerator1->shape = SG_SINUS;       // shape of wave, using defined SG_* constants
   *
   *
   * Example of profile initiation. Up to SG_PROFILE_MAXNUMBER segments can be defined
   *
    psignalgenerator1->current_time = 0.0;     // Internal variable
    psignalgenerator1->sampling_time = 1e-4;   // in seconds
    psignalgenerator1->period = 10.0;          // in seconds
    psignalgenerator1->amplitude = 10.0;       // absolute value
    psignalgenerator1->offset = 0.0;           // can be negative, too
    psignalgenerator1->derivative = 6;         // in (1/s), maximum absolute value of signal derivative  - SG_PROFILE
    psignalgenerator1->derivative2 = 0 ;       // not use
    psignalgenerator1->pause = 0;              // not use
    psignalgenerator1->profile[0].time=0.1; psignalgenerator1->profile[0].value=0.2; // for each segment the final time
    psignalgenerator1->profile[1].time=0.3; psignalgenerator1->profile[1].value=0.5; // and the relative amplitude is given
    psignalgenerator1->profile[2].time=0.6; psignalgenerator1->profile[2].value=-0.5;// Finals time should be in incremental order
    psignalgenerator1->profile[3].time=0.7; psignalgenerator1->profile[3].value=0.2; // After last segment the output will be 0.0
    psignalgenerator1->calculate = 1;          // for this wave initialize calculation will be started
    psignalgenerator1->shape = SG_PROFILE;     // shape of wave


   * Each channel should be call periodical with fixed sampling time:
   *
   double sg_out = SignalGenerator(psignalgenerator1);    // main output
   double sg_der = psignalgenerator1->output_derivative;  // derivative of main signal
   *
  */
