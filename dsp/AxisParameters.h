/*
 * AxisParameters.h
 *
 *  Created on: Apr 6, 2017
 *      Author: brock
 */

#pragma once

#define IREFMAX (15.0)
#define POSITION_ELEC_OFFSET (4.05)  ///> set by experiments [rad]

#define IP_CONTROLLER_SPEED_K  (450)   ///> K parameter for IP speed controller [xxx]
#define IP_CONTROLLER_SPEED_KF (3.0e-3)  ///> Kf parameter for IP speed controller [xxx]

#define IP_CONTROLLER_IDIQ_K  (200.0e-3)   ///> K parameter for IP vector current controller [xxx]
#define IP_CONTROLLER_IDIQ_KF (20.0e-3)  ///> Kf parameter for IP vector current controller [xxx]

#define IDREF_STANDSTILL (6.0)        ///> current for identification  purpose [A]
#define SPEED_MAX_ERROR (2.0)        ///> maximum speed, which switch off the drive [rad]


