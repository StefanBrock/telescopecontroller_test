/*
 * GlobalVariableExternal.h
 *
 *  Created on: Apr 3, 2017
 *      Author: brock
 */



extern volatile  STRUCT_ENABLESIGNALS EnableSignals;
extern volatile  STRUCT_ENABLESIGNALS* pEnableSignals;

extern volatile  STRUCT_MOTION_CONTROL_FLOW motion_control_flow; ///<defined in Foc_init
extern volatile  STRUCT_MOTION_CONTROL_FLOW *pmotion_control_flow; //trial new structure

//4-channel Scope
extern volatile int scope_channel01[SCOPE_DATA_DIMENSION]; //memory for scope
extern volatile int scope_channel02[SCOPE_DATA_DIMENSION];
extern volatile int scope_channel03[SCOPE_DATA_DIMENSION];
extern volatile int scope_channel04[SCOPE_DATA_DIMENSION];
extern volatile int *ipscope_channel01; // pointers to integer
extern volatile int *ipscope_channel02;
extern volatile int *ipscope_channel03;
extern volatile int *ipscope_channel04;
extern volatile float *fpscope_channel01; // pointers to float
extern volatile float *fpscope_channel02;
extern volatile float *fpscope_channel03;
extern volatile float *fpscope_channel04;
extern volatile double *dpscope_channel01; // pointers to double
extern volatile double *dpscope_channel02;
extern volatile double *dpscope_channel03;
extern volatile double *dpscope_channel04;
extern volatile int scope_start;   // switch of start/stop scope
extern volatile int scope_counter; // scope counter
