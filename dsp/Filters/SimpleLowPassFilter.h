/*
 * SignalGenerator.h
 *
 *  Created on: April 2, 2017
 *      Author: brock
 */




 typedef struct SimpleLowPassFilter_type{
     double sampling_time;  // sampling time of Simple Low Pass Filter
     int order;            // order: 1 or 2
     double omega;        // cut-off frequency (rad/s)
     double damping;       // damping coefficient (for 2nd order only)
     double input;        // input signel
     double outputs[3];    // actual and old outputs
 } STRUCT_SIMPLELOWPASSFILTER;

 double SimpleLowPassFilter(STRUCT_SIMPLELOWPASSFILTER *lps);


/*
 *
 * SimpleLowPassFilter, discrete equivalent to continuous transfer function,
 * using linear approximation
 * for order 1:
 *   G(s) = (omega)/(s+omega)
 * for order 2:
 *   G(s) = (omega^2)/(s^2+2*damping*omega*s+omega^2)
 * else
 *   G(s) = 1

 * Each filter should be declared as a global (or static) variable:
    STRUCT_SIMPLELOWPASSFILTER filter1;
    STRUCT_SIMPLELOWPASSFILTER* pfilter1=&filter1;

  * Each filter should be initial configured
    pfilter1->order = 2;          // order of filter {1, 2}
    pfilter1->sampling_time = 1e-4; // sampling time in seconds
    pfilter1->omega = 20;           // cut-off frequency (rad/s)
    pfilter1->damping = 1.1;    // damping coefficient (for 2nd order only)
  *
  * Each filter should be call periodical with fixed sampling time:
    pfilter1->input = input_signal;
    double output_signal = SimpleLowPassFilter(pfilter1)
  *
*/
