/*
 * SignalGenerator.c
 *
 * Created on: Mar 27, 2017
 *      Author: brock
 */

/*  SWITCH OFF FOR PC SIMULATION */
#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Types.h>
#include <ti/sysbios/BIOS.h>
#include "axis_motion_controller.h"
#include "SimpleLowPassFilter.h" ///< Simple Low Pass Filter definitions
/* ***************************** */

/*  SWITCH OFF FOR REAL SYSYSTEM
#include "C:\brock\OneDrive\Teleskop\telescopemotioncontroller\dsp\axis\SimpleLowPassFilter.h" ///< Simple Low Pass Filter definitions
#include <math.h>
* ***************************** */



/*
 *
 * SimpleLowPassFilter, discrete equivalent to continuous transfer function,
 * using linear approximation
 * for order 1:
 *   G(s) = (omega)/(s+omega)
 * for order 2:
 *   G(s) = (omega^2)/(s^2+2*damping*omega*s+omega^2)
 * else
 *   G(s) = 1
*/

double SimpleLowPassFilter(STRUCT_SIMPLELOWPASSFILTER *lps)
{
  switch (lps->order)
  {
    case 1:
        lps->outputs[1]=lps->outputs[0];
        lps->outputs[0]=((lps->omega*lps->sampling_time)*lps->input+lps->outputs[1])/(1.0+lps->omega*lps->sampling_time);
        return(lps->outputs[0]);
        break;
    case 2:
        lps->outputs[2]=lps->outputs[1];
        lps->outputs[1]=lps->outputs[0];
        lps->outputs[0]=((lps->omega*lps->sampling_time*lps->omega*lps->sampling_time)*lps->input+
                        (2.0+2.0*lps->damping*lps->omega*lps->sampling_time)*lps->outputs[1]-
                        lps->outputs[2])/
                        (1.0+2.0*lps->damping*lps->omega*lps->sampling_time+lps->omega*lps->sampling_time*lps->omega*lps->sampling_time);
        return(lps->outputs[0]);
        break;
    default:
        return(lps->outputs[0]=lps->input);
   }
} // SimpleLowPassFilter
