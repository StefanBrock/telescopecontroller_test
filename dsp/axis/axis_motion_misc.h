/*
 * axis_motion_misc.h
 *
 *  Created on: Apr 19, 2017
 *      Author: darjan
 */

#ifndef AXIS_AXIS_MOTION_MISC_H_
#define AXIS_AXIS_MOTION_MISC_H_

float ramper(float in, float out, float rampDelta);
//int posArray[8] = { (1.5), (-1.5), (2.5), (-2.5) }, ptrMax = 2, cntr1 = 0,
//        ptr1 = 0, posSlewRate = (0.001);
int refPosGen(int out);



#endif /* AXIS_AXIS_MOTION_MISC_H_ */
