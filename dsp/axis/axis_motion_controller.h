/*
 * foc.h
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */


#ifndef FOC_H_
#define FOC_H_
#include"math_blocks/IQmathLib.h" //pi, rc, rg

#include <xdc/std.h>
#include "../motion_control.h"

// Define the base quantites TODO: not real
#define BASE_VOLTAGE    31.76             // Base peak phase voltage (volt), maximum measurable DC Bus/sqrt(3)
#define BASE_CURRENT    10                // Base peak phase current (amp) , maximum measurable peak current
#define BASE_FREQ       266.66667         // Base electrical frequency (Hz)

#define PI 3.14159265358979                // Pi

//#define PWM_PERIOD        30000 //30us for 33.3KHz
//#define PWM_PERIOD        40000 //40us for 25 KHz - Does not work
//#define PWM_PERIOD        50000 //50us for 20KHz
#define PWM_PERIOD        100000 //100us for 10KHz
#define PWM_CLOCK_CYCLES    PWM_PERIOD/10        // 10000 Number of Clock Cycles to Obtain a 10Khz Freq
                                        // PWM CLOCK=100MHZ / 10000 Counts = 10Khz
#define ISR_FREQUENCY       (1000000.0/PWM_PERIOD)

// Sampling period in sec for 10kz T=0.0001.
//#define  Tsample  (0.001 / ISR_FREQUENCY)


#define BISS_OFFSET_IDENTIFY_DELAY 200

/*------------------------------------------------------------------------------
Following is the list of the Build Level choices.
------------------------------------------------------------------------------*/
#define LEVEL1  1              // Module check out (do not connect the motors)
#define LEVEL2  2              // Verify ADC, park/clarke, calibrate the offset
#define LEVEL3  3           // Verify closed current(torque) loop, QEP and speed meas.
#define LEVEL4  4           // Verify closed speed loop and speed PID
#define LEVEL5  5           // Verify position control with EnDat
#define LEVEL6  6            // The EnDAT FOC code from flash demo
#define LEVEL7    7            // EnDAT + Position control PID
/*------------------------------------------------------------------------------
This line sets the BUILDLEVEL to one of the available choices.
------------------------------------------------------------------------------*/
#ifndef BUILDLEVEL
#define   BUILDLEVEL LEVEL6
#endif

#ifndef BUILDLEVEL
#error  Critical: BUILDLEVEL must be defined !!
#endif  // BUILDLEVEL


#ifndef TRUE
#define FALSE 0
#define TRUE  1
#endif

///interrupts numbers
#define ADC0_GENINT (48u)
#define ADC1_GENINT (147u)
#define GPIOINT4A (138u)
#define ePWM0_TZINT (90u)

void PwmISR(UArg a0);
void BissISR(UArg a0);
void HWISRtripzone(UArg a0);

void FocInit(void);
void FocLoop(void);

void adc_drv_foc_pwm_init();

float ramper(float in, float out, float rampDelta);
Int32 refPosGen(Int32 out);

void motion_read_ADC_samples(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_read_position(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_position(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_average_position(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_speed(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_clarke(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_park(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_ipark(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_control_voltage_drive(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_current_d_pi_controller(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_current_q_pi_controller(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_calibrate_electrical_position(STRUCT_MOTION_CONTROL_FLOW * lpmotion_control_flow);
int motion_identify_currents_bias(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_compute_speed_noise_robust(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_identify_biss_offset(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
int IsNeighborPosition(double posA, double posB); ///< Test if posA is a neighbor to posB on a circle


#endif
