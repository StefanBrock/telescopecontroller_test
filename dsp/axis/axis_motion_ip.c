/**********************************************************/
/*                                                        */
/*  motor_coordinate.h                                    */
/*                                                        */
/*  Jan Deskur              xx.xx.2008                    */
/*  Dariusz Janiszewski     19.12.2006                    */
/*                                                        */
/**********************************************************/

/// Please cite:
/// bibTex:
/*
 @Article{5332332,
 Title                    = {Damping of Torsional Vibrations in High-Dynamic Industrial Drives},
 Author                   = {Muszynski, R. and Deskur, J.},
 Journal                  = {IEEE Transactions on Industrial Electronics},
 Year                     = {2010},
 Month                    = {feb. },
 Number                   = {2},
 Pages                    = {544 -552},
 Volume                   = {57},
 Doi                      = {10.1109/TIE.2009.2036034},
 ISSN                     = {0278-0046},
 }
 */

#include <axis_motion_ip.h>
#include <math.h>
//#include "../motion_control.h"
#include "../AxisParameters.h"

void motion_current_dq_ip_controller(
        STRUCT_MOTION_CONTROL_FLOW* lpmotion_control_flow,
        STRUCT_IP_CONTROLLER* lpip_controller)
{
    //dx  =   i_d - i_d1;// przyrost (pochodna) pradu
    //i_d1 =  i_d;        // przechowanie do nastepnego pomiaru

//    xx =    i_d_ref - i_d1; // uchyb pradu
//    xx *=   regI_Kf_;      // uchyb wzmocniony  (*Ts/T)
//    xx -=   dx;             // minus pochodna
//    xx *= regI_K_;          // wzmocnienie prop. ( 1/A )
//    u_d += xx;              // calka=wyjscie, przed ograniczeniem
    lpip_controller->ud += ((lpmotion_control_flow->id_ref
            - lpmotion_control_flow->id) * (lpip_controller->Kf)
            - (lpmotion_control_flow->id - lpip_controller->id1))
            * lpip_controller->K;
    lpip_controller->id1 = lpmotion_control_flow->id; //last value of id current for next step diff

    lpip_controller->uq += ((lpmotion_control_flow->iq_ref
            - lpmotion_control_flow->iq) * (lpip_controller->Kf)
            - (lpmotion_control_flow->iq - lpip_controller->iq1))
            * lpip_controller->K;
    lpip_controller->iq1 = lpmotion_control_flow->iq; //last value of iq current for next step diff

    //circle limitation -- keep voltage  angle
    float corrector = 1.0
            / fmaxf(sqrt(
                    (lpip_controller->ud) * (lpip_controller->ud)
                            + (lpip_controller->uq) * (lpip_controller->uq)),
                    1.0);

    lpmotion_control_flow->ud_ref = lpip_controller->ud = lpip_controller->ud
            * corrector;
    lpmotion_control_flow->uq_ref = lpip_controller->uq = lpip_controller->uq
            * corrector;

//    lpmotion_control_flow->uq_ref = (lpmotion_control_flow->iq_ref - lpmotion_control_flow->iq) * lpip_controller->K;
//    if (lpmotion_control_flow->uq_ref > 0.8) lpmotion_control_flow->uq_ref =0.8;
//    if (lpmotion_control_flow->uq_ref < -0.8) lpmotion_control_flow->uq_ref =-0.8;
}

void motion_speed_ip_controller(
        STRUCT_MOTION_CONTROL_FLOW* lpmotion_control_flow,
        STRUCT_SPEED_IP_CONTROLLER* lpspeed_controller)
{
    lpspeed_controller->iref += ((lpmotion_control_flow->speed_ref
            - lpmotion_control_flow->speed) * (lpspeed_controller->Kf)
            - (lpmotion_control_flow->speed - lpspeed_controller->speed1))
            * (lpspeed_controller->K);
    lpspeed_controller->speed1 = lpmotion_control_flow->speed;
//  lpspeed_controller->iref = (lpmotion_control_flow->speed_ref-lpmotion_control_flow->speed)
//                                        *(lpspeed_controller->K);
    lpspeed_controller->iref =
            (lpspeed_controller->iref > IREFMAX) ?
                    (IREFMAX) :
                    ((lpspeed_controller->iref < -IREFMAX) ?
                            (-IREFMAX) : (lpspeed_controller->iref));
}

