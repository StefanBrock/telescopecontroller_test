/*
 * axis_motion_pi.c
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * Copyright (c) 2016-2017 Dariusz Janiszewski
 * Copyright (c) 2017 Stefan Brock
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

#include "axis_motion_pi.h"

#define PI_CONTROLLER_IQ_KP (35.0e-3)
#define PI_CONTROLLER_IQ_KI (20.0e-3)
#define PI_CONTROLLER_IQ_UMAX (0.8)

#define PI_CONTROLLER_ID_KP (35.0e-3)
#define PI_CONTROLLER_ID_KI (20.0e-3)
#define PI_CONTROLLER_ID_UMAX (0.9)

/**     Instance PID regulators to regulate the d and q  axis currents, and speed    */
PI_CONTROLLER pi_id; //dj
PI_CONTROLLER pi_iq; //dj
PI_CONTROLLER pi_spd; //dj
PI_CONTROLLER pi_pos = PI_CONTROLLER_DEFAULTS ;

volatile PI_CONTROLLER *p_pi_id;
volatile PI_CONTROLLER *p_pi_iq;
volatile PI_CONTROLLER *p_pi_spd;
volatile PI_CONTROLLER *p_pi_pos;

unsigned short SpeedLoopPrescaler = 10;      // Speed loop prescaler
unsigned short SpeedLoopCount = 1;          // Speed loop counter


void motion_pi_init(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{


    volatile PI_CONTROLLER *p_pi_id;
    volatile PI_CONTROLLER *p_pi_iq;
    volatile PI_CONTROLLER *p_pi_spd;
    volatile PI_CONTROLLER *p_pi_pos;

    // Initialize PI controllers
    PI_CONTROLLER pi = PI_CONTROLLER_DEFAULTS;

    p_pi_id = &pi_id;
    p_pi_iq = &pi_iq;
    p_pi_spd = &pi_spd;
    p_pi_pos = &pi_pos;

    memset((void *) p_pi_id, 0, sizeof(PI_CONTROLLER));
    memset((void *) p_pi_iq, 0, sizeof(PI_CONTROLLER));
    memset((void *) p_pi_spd, 0, sizeof(PI_CONTROLLER));
    memset((void *) p_pi_pos, 0, sizeof(PI_CONTROLLER));

    *p_pi_id = pi;
    *p_pi_iq = pi;
    *p_pi_spd = pi;
    *p_pi_pos = pi;

    /// Initialize the PI module for Id
    p_pi_id->Kp = PI_CONTROLLER_ID_KP;   //1.0); //DJ was 0.3
    p_pi_id->Ki = PI_CONTROLLER_ID_KI;   //T/0.04);
    p_pi_id->Umax = (PI_CONTROLLER_ID_UMAX);   // DJ was (0.9);
    p_pi_id->Umin = (-PI_CONTROLLER_ID_UMAX);   // DJ was (-0.9);
    /// Initialize the PI module for Iq
    p_pi_iq->Kp = PI_CONTROLLER_IQ_KP;   //1.0); //DJ was 0.3
    p_pi_iq->Ki = PI_CONTROLLER_IQ_KI;   //T/0.04);
    p_pi_iq->Umax = (PI_CONTROLLER_IQ_UMAX);   // DJ was (0.95);
    p_pi_iq->Umin = (-PI_CONTROLLER_IQ_UMAX);   // DJ was (-0.95);

    /// Initialize the PI module for speed
    p_pi_spd->Kp = (200.0); //DJ was 10
    p_pi_spd->Ki = (lpmotion_control_flow->Tsample * SpeedLoopPrescaler / 0.2);
    p_pi_spd->Umax = (20.0); //Dj was 1.0
    p_pi_spd->Umin = (-20.0); //Dj was 1.0

    /// Initialize the PI module for position
    p_pi_pos->Kp = (50.0);
    p_pi_pos->Ki = (0.001);          //(T*SpeedLoopPrescaler/0.3);
    p_pi_pos->Umax = (0.05);
    p_pi_pos->Umin = (-0.05);
    ;
}

void motion_current_q_pi_controller(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    pi_iq.Fbk = lpmotion_control_flow->iq;
    pi_iq.Ref = lpmotion_control_flow->iq_ref;
    PI_MACRO(pi_iq);
    lpmotion_control_flow->uq_ref = pi_iq.Out;
}

void motion_current_d_pi_controller(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    pi_id.Ref = lpmotion_control_flow->id_ref;
    pi_id.Fbk = lpmotion_control_flow->id;
    PI_MACRO(pi_id);
    lpmotion_control_flow->ud_ref = pi_id.Out;
}

