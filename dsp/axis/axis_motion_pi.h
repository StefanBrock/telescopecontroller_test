/*
 * axis_motion_pi.h
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * Copyright (c) 2016-2017 Dariusz Janiszewski
 * Copyright (c) 2017 Stefan Brock
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

#ifndef AXIS_AXIS_MOTION_PI_H_
#define AXIS_AXIS_MOTION_PI_H_

#include <string.h>

#include "math_blocks/IQmathLib.h" //pi, rc, rg
#include "math_blocks/pi.h"
#include "../motion_control.h"

#define PI_CONTROLLER_IQ_KP (35.0e-3)
#define PI_CONTROLLER_IQ_KI (20.0e-3)
#define PI_CONTROLLER_IQ_UMAX (0.8)

#define PI_CONTROLLER_ID_KP (35.0e-3)
#define PI_CONTROLLER_ID_KI (20.0e-3)
#define PI_CONTROLLER_ID_UMAX (0.9)

void motion_pi_init(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_current_q_pi_controller(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);
void motion_current_d_pi_controller(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow);


#endif /* AXIS_AXIS_MOTION_PI_H_ */
