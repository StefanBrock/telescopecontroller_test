/*
 * math_blocks.h
 *
 *  Created on: Feb 22, 2017
 *      Author: darjan
 */

#ifndef MATH_BLOCKS_H_
#define MATH_BLOCKS_H_

#include "math_blocks/IQmathLib.h" //pi, rc, rg


///* Motor Control Blocks     */
//#include "dmctype.h"
//#include "park.h"               // Include header for the PARK object
//#include "ipark.h"               // Include header for the IPARK object
//#include "clarke.h"             // Include header for the CLARKE object
//#include "svgen_dq.h"           // Include header for the SVGENDQ object
//#include "rmp_cntl.h"           // Include header for the RMPCNTL object
//#include "rampgen.h"            // Include header for the RAMPGEN object
//#include "speed_fr.h"
//#include "pi.h"


/* Motor Control Blocks     */
#include "math_blocks/dmctype.h"
#include "math_blocks/park.h"               // Include header for the PARK object
#include "math_blocks/ipark.h"               // Include header for the IPARK object
#include "math_blocks/clarke.h"             // Include header for the CLARKE object
#include "math_blocks/svgen_dq.h"           // Include header for the SVGENDQ object
#include "math_blocks/rmp_cntl.h"           // Include header for the RMPCNTL object
#include "math_blocks/rampgen.h"            // Include header for the RAMPGEN object
#include "math_blocks/speed_fr.h"
#include "math_blocks/pi.h"





#endif /* MATH_BLOCKS_H_ */
