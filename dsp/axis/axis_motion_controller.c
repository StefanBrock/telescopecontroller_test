/*
 * axis_motion_controller.c
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * Copyright (c) 2016-2017 Dariusz Janiszewski
 * Copyright (c) 2017 Stefan Brock
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

/* ========================================================================== */
/*                          INCLUDE FILES                                     */
/* ========================================================================== */
#include "axis_motion_ip.h"
#include "axis_motion_pi.h"
#include "axis_motion_controller.h"  /* Main Configurations and Headers */
#include "math_blocks.h"
#include <pwms.h>
#include <epwm.h>
#include <stdio.h>
#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>

#include <xdc/cfg/global.h>
#include <string.h>

#include "am437x.h"

#include "adc.h"
#include "console_utils.h"
#include "gpio.h"
#include "biss_drv.h"

#include "../motion_control.h"
#include "../scope/scope.h" ///< Scope definitions (without variable)
#include "../SignalGenerator/SignalGenerator.h" ///< Signal Generator definitions
#include "../Filters/SimpleLowPassFilter.h" ///< Signal Generator definitions

#include "../GlobalVariableExtern.h"
#include "../AxisParameters.h"

void msdelay(unsigned long long ms);
extern void DebugGPIOinit(void); //GPIO0_14 GPIO0_15

#define ENABLE_DEBUG_FOC_PROFILE_GPIO   ///for debug based on GPIO0_14 GPIO0_15
//#define PROFILE                        // For profiling and debug Purposes

int ElecThetaCalibrateFlag = 0;

double sb_position_regresion = 0.0; //XXX ???

///Reference values
float IdRef = (0.0);    // Id reference [A]
float IqRef = (0.0);   // Iq reference [A]
float IdRef_standstill = IDREF_STANDSTILL; //Id reference for standstill [A]
float SpeedRef = (0.010); // const

//Uint32 cntr = 0, alignCnt = 20000; //DJ???
float IdRef_start = (10.0), IdRef_run = (0.0);

Uint16 wsl = 0;

//currents biases filter
//for motion_identify_currents_bias(...);
float K1 = (0.998); ///< Current Offset filter coefficient K1: 0.05/(Tsample+0.05);
float K2 = (0.001999); ///< Current Offset filter coefficient K2: Tsample/(Tsample+0.05);

//ACD inequality channels
float ScaleFactor = (.28 / .24); //Scale factor for Phase B measured using ADC1

PWMGEN pwm_drv = PWMGEN_DEFAULTS; ///Instance a PWM driver instance

volatile STRUCT_IP_CONTROLLER ip_idiq; ///Instance a IP type Id and Iq controller
volatile STRUCT_IP_CONTROLLER *pip_idiq = &ip_idiq;

volatile STRUCT_SPEED_IP_CONTROLLER ip_speed; ///Instance a IP type speed controller
volatile STRUCT_SPEED_IP_CONTROLLER *pip_speed = &ip_speed;

STRUCT_SIMPLELOWPASSFILTER filter1;     ///Instance of filter1
STRUCT_SIMPLELOWPASSFILTER* pfilter1 = &filter1;

STRUCT_SIMPLELOWPASSFILTER id_filter;   ///Instance a filter of Id current
STRUCT_SIMPLELOWPASSFILTER* pid_filter = &id_filter;

STRUCT_SIMPLELOWPASSFILTER iq_filter;   ///Instance a filter of Iq current
STRUCT_SIMPLELOWPASSFILTER* piq_filter = &iq_filter;

STRUCT_SIMPLELOWPASSFILTER speed_filter;   ///Instance a filter of speed current
STRUCT_SIMPLELOWPASSFILTER* pspeed_filter = &speed_filter;

STRUCT_SIGNALGENERATOR SigGenCurrent; ///Instance a Signal Generator based on "SignalGenerator.h"
STRUCT_SIGNALGENERATOR* pSigGenCurrent = &SigGenCurrent;

/** Instance a few transform objects                            */
CLARKE clarke = CLARKE_DEFAULTS;
PARK park = PARK_DEFAULTS;
IPARK ipark = IPARK_DEFAULTS;
/** Instance a ramp controller to smoothly ramp the frequency    */
RMPCNTL rc = RMPCNTL_DEFAULTS;
/** Instance a ramp generator to simulate an Anglele             */
RAMPGEN rg = RAMPGEN_DEFAULTS;

/** Instance a Space Vector PWM modulator. This modulator generates a, b and c
 phases based on the d and q stationary reference frame inputs    */
SVGENDQ svgen_dq = SVGENDQ_DEFAULTS;

SPEED_MEAS_QEP speed1 = SPEED_MEAS_QEP_DEFAULTS; ///Instance of simple derivative filtered speed calculation

/**
 *  \brief  ======== ADC ISR Hwi (10Khz) =======
 *  \brief  Continue Motion Controller Routine.
 *  \brief  Interrupt done after ADC conversion
 *
 */
void HWISRadc(UArg a0)
{
    pmotion_control_flow->pwmisrcnt++;
#ifdef ENABLE_DEBUG_FOC_PROFILE_GPIO
    GPIOPinWrite(SOC_GPIO0_REG, 14, GPIO_PIN_HIGH);
#endif
    ///ADC interrupt clear
    int adc_basee = CHIPDBBaseAddress(CHIPDB_MOD_ID_ADC1, 0);
    HWREG(adc_basee + ADC0_IRQSTS) = HWREG(adc_basee + ADC0_IRQSTS); //Clear ADC0/1 irg flag
//    HWREG(adc_basee + ADC0_STEPEN) = ADC0_STEPEN_STEP1_MASK; //activate one step after on-shot operation (necessary, not included in description)
    motion_read_ADC_samples(
            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    //activate steps after on-shot operation (necessary, not included in description)
    //XXX: one shot acquisition erases step_enable bits!!!
    HWREG(adc_basee + ADC0_STEPEN) = ( ADC0_STEPEN_STEP1_MASK
#ifndef MINIMAL_ADC_CONFIG
            | ADC0_STEPEN_STEP2_MASK |
            ADC0_STEPEN_STEP3_MASK |
            ADC0_STEPEN_STEP4_MASK
#endif //MINIMAL_ADC_CONFIG
            );
}

/**
 *  \brief  ======== Biss conversion finished ISR Hwi  =======
 *  \brief  Continue...
 *
 */
void HWISRbiss(UArg a0)
{
    pmotion_control_flow->IsrTicker++; // System current time, for all periodic and one-shot task
    pmotion_control_flow->bissisrcnt++;
    motion_read_position((STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);
//    motion_compute_position(
//            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);
    motion_compute_average_position(
            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    motion_identify_biss_offset(
            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    GPIOIntrClear(SOC_GPIO4_REG, 0u, 17);

    //identify currents take first 20000 IsrTicket
    if (motion_identify_currents_bias(
            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow))
        return;

//    motion_calibrate_electrical_position(
//            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    motion_compute_speed_noise_robust(
            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

//  TO DO WYJASNIENIA!!!
//    motion_compute_speed(
//            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    double URef_test = SignalGenerator(pSigGenCurrent);
//    pfilter1->input=test1;
//    double test2 = SimpleLowPassFilter(pfilter1);

//    pid_filter->input=pmotion_control_flow->id;
//    pmotion_control_flow->id = SimpleLowPassFilter(pid_filter);
//    piq_filter->input=pmotion_control_flow->iq;
//    pmotion_control_flow->iq = SimpleLowPassFilter(piq_filter);
//
    pspeed_filter->input = pmotion_control_flow->speed;
    pmotion_control_flow->speed = SimpleLowPassFilter(pspeed_filter);

    if (ElecThetaCalibrateFlag)
        if (scope_counter < (SCOPE_DOUBLE_SIZE - 1))
        {
            scope_counter++;
            *fpscope_channel01++ = pmotion_control_flow->iq_ref;
            *fpscope_channel02++ = pmotion_control_flow->speed;
//      *dpscope_channel03++=pmotion_control_flow->id;
//      *dpscope_channel04++=pmotion_control_flow->ud_ref;
//      *ipscope_channel03++=pmotion_control_flow->position_biss[3];
//      *ipscope_channel02++=sb_position_regresion;
//      *dpscope_channel03++=pmotion_control_flow->speed;
        }

    motion_compute_clarke((STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    motion_compute_park((STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    if (pmotion_control_flow->IsrTicker == 100000u)
        pmotion_control_flow->lsw = 999; //flywheel after 10s

    ///overspeed check in rad/s
    if (fabs(pmotion_control_flow->speed) > SPEED_MAX_ERROR)
        pmotion_control_flow->lsw = 1;

    ///enable TRIPZONE interrupt
    if (pmotion_control_flow->lsw != 999)
        Hwi_enableInterrupt(ePWM0_TZINT);     // enable interrupt

    switch (pmotion_control_flow->lsw)
    {
    case (0): //lsw=0 - off, constant current in Aphase
        pmotion_control_flow->id_ref = IdRef_standstill;
        pmotion_control_flow->iq_ref = IqRef;
        pmotion_control_flow->speed_ref = 0.0;

        break;
    case (1): //lsw=1 - current controller only
        pmotion_control_flow->id_ref = IdRef;
        pmotion_control_flow->iq_ref = IqRef;
        pmotion_control_flow->speed_ref = 0.0;

        break;
    case (2): //lsw=2 - speed controller
        if (ElecThetaCalibrateFlag)
        {
            pmotion_control_flow->speed_ref = URef_test;
            motion_speed_ip_controller(
                    (STRUCT_MOTION_CONTROL_FLOW*) pmotion_control_flow,
                    (STRUCT_SPEED_IP_CONTROLLER*) pip_speed);
            pmotion_control_flow->iq_ref = pip_speed->iref;

            // ElecThetaCalibrate test
//                  if (pmotion_control_flow->IsrTicker < 30000u) pmotion_control_flow->iq_ref=5.0; else pmotion_control_flow->iq_ref=0.0;
//                  pmotion_control_flow->id_ref = 0.0;

//                pmotion_control_flow->iq_ref = URef_test;
//                pmotion_control_flow->id_ref = IdRef;
        }
        else
        {
            pmotion_control_flow->id_ref = 0.0;
            pmotion_control_flow->iq_ref = 0.0;

        }
        break;
    case (999): //lsw=999 - flywheel PWM signals high impedance
//                EPWMTzTripEventEnable(SOC_PWMSS0_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
//                EPWMTzTripEventEnable(SOC_PWMSS1_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
//                EPWMTzTripEventEnable(SOC_PWMSS2_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
//                //EPWMTzTripEventEnable(SOC_PWMSS0_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);
//
//                //HWREG(PWMSS_EPWM_TZCLR) = 0x0004; //TripZone chA artificial activation
//                EPWMTzTripEventEnable(SOC_PWMSS0_REG,EPWM_TZ_EVENT_CYCLE_BY_CYCLE,0);
//                EPWMTzTripEventEnable(SOC_PWMSS1_REG,EPWM_TZ_EVENT_CYCLE_BY_CYCLE,0);
//                EPWMTzTripEventEnable(SOC_PWMSS2_REG,EPWM_TZ_EVENT_CYCLE_BY_CYCLE,0);

        EPWMTzTriggerSwEvent(SOC_PWMSS0_REG, EPWM_TZ_EVENT_ONE_SHOT);
        EPWMTzTriggerSwEvent(SOC_PWMSS1_REG, EPWM_TZ_EVENT_ONE_SHOT);
        EPWMTzTriggerSwEvent(SOC_PWMSS2_REG, EPWM_TZ_EVENT_ONE_SHOT);

        break;
    default:
        pmotion_control_flow->id_ref = IdRef;
        pmotion_control_flow->iq_ref = IqRef;
        pmotion_control_flow->speed_ref = 0.0;

    };

    motion_current_dq_ip_controller(
            (STRUCT_MOTION_CONTROL_FLOW*) pmotion_control_flow,
            (STRUCT_IP_CONTROLLER*) pip_idiq);

//        motion_current_q_pi_controller(
//                (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);
//        motion_current_d_pi_controller(
//                (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

    motion_compute_ipark((STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);
    motion_control_voltage_drive(
            (STRUCT_MOTION_CONTROL_FLOW *) pmotion_control_flow);

}

/**
 *  \brief  ======== Trip Zone Inverter Occurrence ISR Hwi  =======
 *  \brief  Continue...
 *
 */
void HWISRtripzone(UArg a0)
{
    //TODO divide into two groups
    //1. interrupt after lsw=999 conditionally flywheel
    //2. interrupt after inverter fault occurence

    //TODO after that interrupt appears stop any integration tasks!!!
    CONSOLEUtilsPrintf("\n     !!!MOTOR STOPPED!!!   ");
//    EPWMTzEventStatusClear(SOC_PWMSS0_REG, PWMSS_EPWM_TZCLR_OST_MASK); ///clear the trip zone occurrence (for once test)
//    pmotion_control_flow->drv_status = ((spi0_drv_read_controlregister(0x01)&0x7ff)<<15)|(spi0_drv_read_controlregister(0x00)&0x7ff); ///read DRV8301 status

    ///disbale TRIPZONE interrupt
    Hwi_disableInterrupt(ePWM0_TZINT);      // disable tripzone irq

    if (pmotion_control_flow->lsw != 999) //possible fault occurrence
    {
        CONSOLEUtilsPrintf("\n Possible INVERTER FAULT\n");
        pmotion_control_flow->drv_status = ((spi0_drv_read_controlregister(0x01)
                & 0x7ff) << 16) | (spi0_drv_read_controlregister(0x00) & 0x7ff); ///read DRV8301 status
//        pmotion_control_flow->drv_status = ((spi0_drv_read_controlregister(0x01)&0x7ff)<<16)|(spi0_drv_read_controlregister(0x00)&0x7ff); ///read DRV8301 status
        if (!((pmotion_control_flow->drv_status) & (1 << 10))) //TODO any error DATA BASE write
            CONSOLEUtilsPrintf("\nDRV8301 error!!!\n"); //DRV8301 Status Register D10 bit means nFAULT
        if (((pmotion_control_flow->drv_status) & (0x07ff07ff))) //TODO any error DATA BASE write
                    CONSOLEUtilsPrintf("\nDRV8301 not responce!!!\n Possible VDC absent!!!\n\n"); //
        EPWMTzEventStatusClear(SOC_PWMSS0_REG, PWMSS_EPWM_TZCLR_OST_MASK); ///clear the trip zone occurrence (for once test)

        ///enable TRIPZONE interrupt (possible error elimination)
        Hwi_enableInterrupt(ePWM0_TZINT);     // enable interrupt

        pmotion_control_flow->lsw=999; ///for test only to avoid multi SPI read
    }


}

/*
 * \brief    Function to initialize modules of Motor Control.
 *
 * \param    None
 * \return   None
 *
 *  Note:
 */
void motion_controller_init()
{
    Hwi_disableInterrupt(ADC1_GENINT); // disable PWM interrupt
    Hwi_disableInterrupt(GPIOINT4A); // disable BISS (GPIO4_17) interrupt

    pmotion_control_flow->Tsample = (0.001 / ISR_FREQUENCY);

    // Initial value for current reference Signal Generator
    pSigGenCurrent->sampling_time = (0.001 / ISR_FREQUENCY);
    pSigGenCurrent->amplitude = 1e-3;
    pSigGenCurrent->offset = 0;
    pSigGenCurrent->shape = SG_CONST;
    pSigGenCurrent->period = 4;

    // Initial value for Low Pass Filter - example
    pfilter1->order = 2;
    pfilter1->damping = 0.9;
    pfilter1->omega = 100;
    pfilter1->sampling_time = (0.001 / ISR_FREQUENCY);

    // Initial value for Low Pass Filter - id
    pid_filter->order = 2;
    pid_filter->damping = 0.9;
    pid_filter->omega = 1000;
    pid_filter->sampling_time = (0.001 / ISR_FREQUENCY);

    // Initial value for Low Pass Filter - iq
    piq_filter->order = 2;
    piq_filter->damping = 0.9;
    piq_filter->omega = 1000;
    piq_filter->sampling_time = (0.001 / ISR_FREQUENCY);

    // Initial value for Low Pass Filter - speed
    piq_filter->order = 2;
    piq_filter->damping = 0.9;
    piq_filter->omega = 500;
    piq_filter->sampling_time = (0.001 / ISR_FREQUENCY);

    pmotion_control_flow->biss_offset_ready = 0;

    //rc.RampDelayMax=1;

    /// IsrTicker initialize
    pmotion_control_flow->IsrTicker = 0;
    /// Initialize the RAMPGEN module
    rg.StepAngleMax = (BASE_FREQ * pmotion_control_flow->Tsample);

    ///Motor data
    pmotion_control_flow->npp = 12; //no. pole pairs
    pmotion_control_flow->position_elec_offset = POSITION_ELEC_OFFSET;
    ElecThetaCalibrateFlag = 1;

    // Mode of work
    pmotion_control_flow->lsw = 2;

    //IP current controller init

    pip_idiq->K = IP_CONTROLLER_IDIQ_K;
    pip_idiq->Kf = IP_CONTROLLER_IDIQ_KF;

    pip_speed->K = IP_CONTROLLER_SPEED_K;
    pip_speed->Kf = IP_CONTROLLER_SPEED_KF;

//    /// Initialize Low-pass cut-off speed value filter
//    speed1.K1 = (1 / (BASE_FREQ * pmotion_control_flow->Tsample));
//    speed1.K2 = (1 / (1 + pmotion_control_flow->Tsample * 2 * PI * 5));  // Low-pass cut-off frequency
//    speed1.K3 = (1) - speed1.K2;
//    speed1.BaseRpm = 120 * (BASE_FREQ / (2 * pmotion_control_flow->npp));

    ///Clear all old interrupts
    Hwi_clearInterrupt(ADC1_GENINT);    // clear old PWM irq
    Hwi_enableInterrupt(ADC1_GENINT);   // enable PWM interrupt

    Hwi_clearInterrupt(GPIOINT4A);      // clear old BISS (GPIO4_17) irq
    Hwi_enableInterrupt(GPIOINT4A);     // enable BISS (GPIO4_17) interrupt
}

/*
 * \brief    Function for ADC samples read. BISS Data Retrieve.
 *
 *
 * \param    none
 * \return   adc_value[7..0] and into STRUCT_MOTION_CONTROL_FLOW
 *
 *
 */
void motion_read_ADC_samples(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    int adc_basee = CHIPDBBaseAddress(CHIPDB_MOD_ID_ADC1, 0);
    int adc_fifo = HWREG(adc_basee + ADC0_FIFO0DATA); //read only one FIFO value (after that FIFO pointer move -1)
    lpmotion_control_flow->adc_value[1] = (unsigned short) (adc_fifo & 0x0fff); //LSword ACD1
    lpmotion_control_flow->adc_value[0] = (unsigned short) ((adc_fifo >> 16)
            & 0x0fff); //MSword ADC0         break;
#ifndef MINIMAL_ADC_CONFIG
            //TODO: check measurements in four step conversion
            lpmotion_control_flow->adc_value[2] = (unsigned short)(HWREG(adc_basee + ADC0_FIFO0DATA+0x01)&0x0fff);//LSword ACD1
            lpmotion_control_flow->adc_value[3] = (unsigned short)((HWREG(adc_basee + ADC0_FIFO0DATA+0x01)>>16)&0x0fff);//MSword ADC0         break;
            lpmotion_control_flow->adc_value[4] = (unsigned short)(HWREG(adc_basee + ADC0_FIFO0DATA+0x02)&0x0fff);//LSword ACD1
            lpmotion_control_flow->adc_value[5] = (unsigned short)((HWREG(adc_basee + ADC0_FIFO0DATA+0x02)>>16)&0x0fff);//MSword ADC0         break;
            lpmotion_control_flow->adc_value[6] = (unsigned short)(HWREG(adc_basee + ADC0_FIFO0DATA+0x03)&0x0fff);//LSword ACD1
            lpmotion_control_flow->adc_value[7] = (unsigned short)((HWREG(adc_basee + ADC0_FIFO0DATA+0x03)>>16)&0x0fff);//MSword ADC0         break;
#endif
    //measurement current signal flow
    // Ua=Ia/Ra; Ra=0.002 [Ohm]
    // Ua'= Ua*ku; ku=10
    // Ua''= Ua' * 4096/Umax; Umax = 1.8[V]
    // Ia = 180/4096 * Ua''
    //
    lpmotion_control_flow->ia = -180
            * ((float) (0.000244141
                    * (float) (lpmotion_control_flow->adc_value[0])
                    - lpmotion_control_flow->adc_offset[0])); // Phase A curr. //'-' for DRV8301board //[A]
    lpmotion_control_flow->ib = -180
            * ((float) (0.000244141
                    * (float) (lpmotion_control_flow->adc_value[1])
                    - lpmotion_control_flow->adc_offset[1])) * ScaleFactor; // Phase B curr. //'-' for DRV8301board [A]
}

void motion_read_position(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    uint32_t* ptemp = biss_get_angles(gSPI0Obj); //read BISS package 0-status 1-headA 2-headB 3-headC 4-headC

    lpmotion_control_flow->position_biss[0] = -(*(ptemp + 0x01)); //'-' different phase ABC and position counting direction
    lpmotion_control_flow->position_biss[1] = -(*(ptemp + 0x02));
    lpmotion_control_flow->position_biss[2] = -(*(ptemp + 0x03));
    lpmotion_control_flow->position_biss[3] = -(*(ptemp + 0x04));

//    if (lpmotion_control_flow->position_biss[3]==0) //for debug - catch the zero
//    {
//        asm("nop;");
//    }
}

/*
 * \brief    Function for compute mechanical and electrical position based on one head only.
 *
 *
 * \param    pointer to STRUCT_MOTION_CONTROL_FLOW, head - no of head
 * \return   modify STRUCT_MOTION_CONTROL_FLOW->position_mech
 * \return   modify STRUCT_MOTION_CONTROL_FLOW->position_elec
 *
 *
 */
void motion_compute_position_one_head(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow, int head)
{

    lpmotion_control_flow->position_mech =
            (double) ((((double) lpmotion_control_flow->position_biss[head] //data from Ahead
            / (double) 0xffffffff)) * 2 * PI); //position float in <0,2*PI> range
    lpmotion_control_flow->position_elec =
            (double) (((double) ((UInt32) (lpmotion_control_flow->position_biss[head] //data from Ahead
            * 12u)) / (double) (0xffffffff)) * 2 * PI)
                    - lpmotion_control_flow->position_elec_offset;
}

#define MAXIMUM_POSITION_CHANGE 0.01
int IsNeighborPosition(double posA, double posB) ///< Test if posA is a neighbor to posB on a circle
{
    if (fabs(posA - posB) < MAXIMUM_POSITION_CHANGE)
        return (TRUE);
    if ((posA > (2 * PI - MAXIMUM_POSITION_CHANGE))
            && (posB < MAXIMUM_POSITION_CHANGE))
        return (TRUE);
    if ((posB > (2 * PI - MAXIMUM_POSITION_CHANGE))
            && (posA < MAXIMUM_POSITION_CHANGE))
        return (TRUE);
    return (FALSE);
}

void motion_compute_average_position(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    double pos0, pos1, pos2, pos3;
    double head0, head1, head2, head3;
    double sum1, sum2, a, b, pos_reg;

// 8 point linear regression constants
#define REGRESSION_N 8
#define REGRESSION_P 28
#define REGRESSION_Q 336

    if (!(lpmotion_control_flow->biss_offset_ready))
        return;

    // do linear regression

    sum1 = (lpmotion_control_flow->last_position_mech[1]
            - lpmotion_control_flow->last_position_mech[0])
            + (lpmotion_control_flow->last_position_mech[2]
                    - lpmotion_control_flow->last_position_mech[0])
            + (lpmotion_control_flow->last_position_mech[3]
                    - lpmotion_control_flow->last_position_mech[0])
            + (lpmotion_control_flow->last_position_mech[4]
                    - lpmotion_control_flow->last_position_mech[0])
            + (lpmotion_control_flow->last_position_mech[5]
                    - lpmotion_control_flow->last_position_mech[0])
            + (lpmotion_control_flow->last_position_mech[6]
                    - lpmotion_control_flow->last_position_mech[0])
            + (lpmotion_control_flow->last_position_mech[7]
                    - lpmotion_control_flow->last_position_mech[0]);
    sum2 = (lpmotion_control_flow->last_position_mech[1]
            - lpmotion_control_flow->last_position_mech[0])
            + 2
                    * (lpmotion_control_flow->last_position_mech[2]
                            - lpmotion_control_flow->last_position_mech[0])
            + 3
                    * (lpmotion_control_flow->last_position_mech[3]
                            - lpmotion_control_flow->last_position_mech[0])
            + 4
                    * (lpmotion_control_flow->last_position_mech[4]
                            - lpmotion_control_flow->last_position_mech[0])
            + 5
                    * (lpmotion_control_flow->last_position_mech[5]
                            - lpmotion_control_flow->last_position_mech[0])
            + 6
                    * (lpmotion_control_flow->last_position_mech[6]
                            - lpmotion_control_flow->last_position_mech[0])
            + 7
                    * (lpmotion_control_flow->last_position_mech[7]
                            - lpmotion_control_flow->last_position_mech[0]);
    a = (-REGRESSION_N * sum2 + REGRESSION_P * sum1) / REGRESSION_Q;
    b = (sum1 + a * REGRESSION_P) / REGRESSION_N;
    sb_position_regresion = pos_reg =
            lpmotion_control_flow->last_position_mech[0] + a + b;

    head0 = pos0 = (((double) lpmotion_control_flow->position_biss[0]
            / (double) 0xffffffff) * 2 * PI);

    head1 = (((double) lpmotion_control_flow->position_biss[1]
            / (double) 0xffffffff) * 2 * PI);
    pos1 = head1 - lpmotion_control_flow->biss_offset[1];
    head2 = (((double) lpmotion_control_flow->position_biss[2]
            / (double) 0xffffffff) * 2 * PI);
    pos2 = head2 - lpmotion_control_flow->biss_offset[2];
    head3 = (((double) lpmotion_control_flow->position_biss[3]
            / (double) 0xffffffff) * 2 * PI);
    pos3 = head3 - lpmotion_control_flow->biss_offset[3];
    pos1 = (pos1 < 0) ? (pos1 + 2 * PI) : (pos1);
    pos2 = (pos2 < 0) ? (pos2 + 2 * PI) : (pos2);
    pos3 = (pos3 < 0) ? (pos3 + 2 * PI) : (pos3);

    if (IsNeighborPosition(head0,
                           lpmotion_control_flow->last_position_headers[0])
            && IsNeighborPosition(
                    head1, lpmotion_control_flow->last_position_headers[1])
            && IsNeighborPosition(
                    head2, lpmotion_control_flow->last_position_headers[2])
            && IsNeighborPosition(
                    head3, lpmotion_control_flow->last_position_headers[3]))
    {
        lpmotion_control_flow->position_mech = (pos0 + pos1 + pos2 + pos3)
                / 4.0;
        if (!(IsNeighborPosition(lpmotion_control_flow->position_mech,
                                 lpmotion_control_flow->last_position_mech[0])))
            lpmotion_control_flow->position_mech = 2
                    * lpmotion_control_flow->last_position_mech[0]
                    - lpmotion_control_flow->last_position_mech[1];
        lpmotion_control_flow->last_position_headers[0] = head0;
        lpmotion_control_flow->last_position_headers[1] = head1;
        lpmotion_control_flow->last_position_headers[2] = head2;
        lpmotion_control_flow->last_position_headers[3] = head3;
        lpmotion_control_flow->biss_ok = 1;
    }
    else
    {
        lpmotion_control_flow->position_mech = pos_reg;
        lpmotion_control_flow->biss_ok = 0;
    }

    lpmotion_control_flow->position_elec = 12.0
            * lpmotion_control_flow->position_mech
            - lpmotion_control_flow->position_elec_offset;

    lpmotion_control_flow->last_position_mech[7] =
            lpmotion_control_flow->last_position_mech[6];
    lpmotion_control_flow->last_position_mech[6] =
            lpmotion_control_flow->last_position_mech[5];
    lpmotion_control_flow->last_position_mech[5] =
            lpmotion_control_flow->last_position_mech[4];
    lpmotion_control_flow->last_position_mech[4] =
            lpmotion_control_flow->last_position_mech[3];
    lpmotion_control_flow->last_position_mech[3] =
            lpmotion_control_flow->last_position_mech[2];
    lpmotion_control_flow->last_position_mech[2] =
            lpmotion_control_flow->last_position_mech[1];
    lpmotion_control_flow->last_position_mech[1] =
            lpmotion_control_flow->last_position_mech[0];
    lpmotion_control_flow->last_position_mech[0] =
            lpmotion_control_flow->position_mech;
}

void motion_compute_speed_ti(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{   //from TI motion library
//SPEED_MEAS_QEP stspeed = SPEED_MEAS_QEP_DEFAULTS;
//SPEED_MEAS_QEP speed should be defined globally or 2nd pointer (TODO)
    speed1.ElecTheta = (float) lpmotion_control_flow->position_elec / (2 * PI);
    SPEED_FR_MACRO(speed1);
    lpmotion_control_flow->speed = (double) speed1.Speed;
}

void motion_compute_speed_noise_robust(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
#define SPEED_SAMPLING_TIME (0.001/ISR_FREQUENCY)
#define SPEED_FILTER_N 7            // N=1,3, ..7
#define SPEED_FILTER_DENOM 64    // 2 ^ (N-1): 1, 4, 16, 64
//double coeff[]={1};  // for N=1
//double coeff[]={1, 1};  // for N=3
//double coeff[]={1, 3, 2};  // for N=5
    double coeff[] = { 1, 5, 9, 5 };  // for N=7

    double sum = 0.0, term = 0.0;

    int i;
    for (i = 0; i <= SPEED_FILTER_N / 2; i++)
    {
        term = lpmotion_control_flow->last_position_mech[i]
                - lpmotion_control_flow->last_position_mech[SPEED_FILTER_N - i];
        if (term > PI)
            term -= 2 * PI;
        if (term < -PI)
            term += 2 * PI;
        sum += coeff[i] * term;
    }

    lpmotion_control_flow->speed = (sum)
            / (SPEED_FILTER_DENOM * SPEED_SAMPLING_TIME);
}

/**
 * Clarke transformation
 */
void motion_compute_clarke(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    clarke.As = lpmotion_control_flow->ia;
    clarke.Bs = lpmotion_control_flow->ib;
    CLARKE_MACRO(clarke);
    lpmotion_control_flow->ialpha = clarke.Alpha;
    lpmotion_control_flow->ibeta = clarke.Beta;
}
/**
 * Park Transfromation
 */
void motion_compute_park(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    park.Alpha = lpmotion_control_flow->ialpha;
    park.Beta = lpmotion_control_flow->ibeta;
    park.Angle = (float) lpmotion_control_flow->position_elec;
    park.Sine = sinf((float) lpmotion_control_flow->position_elec);
    park.Cosine = cosf((float) lpmotion_control_flow->position_elec);
    PARK_MACRO(park);
    lpmotion_control_flow->id = park.Ds;
    lpmotion_control_flow->iq = park.Qs;
}

/**
 * Inverse Park Transfromation
 */
void motion_compute_ipark(STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    ipark.Sine = park.Sine;
    ipark.Cosine = park.Cosine;
    ipark.Ds = lpmotion_control_flow->ud_ref;
    ipark.Qs = lpmotion_control_flow->uq_ref;
    IPARK_MACRO(ipark);
    lpmotion_control_flow->ualpha_ref = ipark.Alpha;
    lpmotion_control_flow->ubeta_ref = ipark.Beta;
}

/**
 * PWM signal generator (for 3phase voltage)
 */
void motion_control_voltage_drive(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    svgen_dq.Ualpha = lpmotion_control_flow->ualpha_ref;
    svgen_dq.Ubeta = lpmotion_control_flow->ubeta_ref;
    SVGEN_MACRO(svgen_dq);

    pwm_drv.MfuncC1 = (Int16) _IQtoQ15(-svgen_dq.Ta / 1.3); // MfuncC1 is in Q15
    pwm_drv.MfuncC2 = (Int16) _IQtoQ15(-svgen_dq.Tb / 1.3); // MfuncC2 is in Q15
    pwm_drv.MfuncC3 = (Int16) _IQtoQ15(-svgen_dq.Tc / 1.3); // MfuncC3 is in Q15

#ifdef ENABLE_DEBUG_FOC_PROFILE_GPIO
    GPIOPinWrite(SOC_GPIO0_REG, 14, GPIO_PIN_LOW);
#endif
    pwm_drv.update(&pwm_drv);
}

void motion_calibrate_electrical_position(
        STRUCT_MOTION_CONTROL_FLOW * lpmotion_control_flow)
{
    static int calibrate_count;
    if ((ElecThetaCalibrateFlag == 0) && (lpmotion_control_flow->lsw == 2))
    {
        lpmotion_control_flow->speed_ref = 0.0; //TODO change to other way
        if (calibrate_count++ > ISR_FREQUENCY * 1000) ///<after 1000ms
            lpmotion_control_flow->position_elec_offset =
                    lpmotion_control_flow->position_elec, ElecThetaCalibrateFlag =
                    1; ///< TODO: check sign!!!
    }
}

int motion_identify_currents_bias(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    if (lpmotion_control_flow->IsrTicker < 5000) ///< wait for analog state calm
    {
        return 1;
    }

    if ((lpmotion_control_flow->IsrTicker >= 5000)
            && (lpmotion_control_flow->IsrTicker <= 20000)) ///< analog measurements calms
    {

        lpmotion_control_flow->adc_offset[0] = K1
                * lpmotion_control_flow->adc_offset[0]
                + K2 * 0.000244141 * lpmotion_control_flow->adc_value[0];
        lpmotion_control_flow->adc_offset[1] = K1
                * lpmotion_control_flow->adc_offset[1]
                + K2 * 0.000244141 * lpmotion_control_flow->adc_value[1];
        //lpmotion_control_flow->adc_offset[2] = K1*lpmotion_control_flow->adc_offset[2] + K2*0.000244141*lpmotion_control_flow->adc_value[2]; //Ic current

        return 1;
    }
    return 0;
}

void motion_identify_biss_offset(
        STRUCT_MOTION_CONTROL_FLOW *lpmotion_control_flow)
{
    int i;
    if (lpmotion_control_flow->biss_offset_ready)
        return;
    if (lpmotion_control_flow->IsrTicker > BISS_OFFSET_IDENTIFY_DELAY)
    {
        lpmotion_control_flow->biss_offset_ready = 1;
        lpmotion_control_flow->last_position_headers[0] =
                (((double) lpmotion_control_flow->position_biss[0]
                        / (double) 0xffffffff) * 2 * PI);
        lpmotion_control_flow->last_position_headers[1] =
                (((double) lpmotion_control_flow->position_biss[1]
                        / (double) 0xffffffff) * 2 * PI);
        lpmotion_control_flow->last_position_headers[2] =
                (((double) lpmotion_control_flow->position_biss[2]
                        / (double) 0xffffffff) * 2 * PI);
        lpmotion_control_flow->last_position_headers[3] =
                (((double) lpmotion_control_flow->position_biss[3]
                        / (double) 0xffffffff) * 2 * PI);
        lpmotion_control_flow->biss_offset[0] = 0.0;
        lpmotion_control_flow->biss_offset[1] =
                lpmotion_control_flow->last_position_headers[1]
                        - lpmotion_control_flow->last_position_headers[0];
        if (lpmotion_control_flow->biss_offset[1] < 0.0)
            lpmotion_control_flow->biss_offset[1] += 2 * PI;
        lpmotion_control_flow->biss_offset[2] =
                lpmotion_control_flow->last_position_headers[2]
                        - lpmotion_control_flow->last_position_headers[0];
        if (lpmotion_control_flow->biss_offset[2] < 0.0)
            lpmotion_control_flow->biss_offset[2] += 2 * PI;
        lpmotion_control_flow->biss_offset[3] =
                lpmotion_control_flow->last_position_headers[3]
                        - lpmotion_control_flow->last_position_headers[0];
        if (lpmotion_control_flow->biss_offset[3] < 0.0)
            lpmotion_control_flow->biss_offset[3] += 2 * PI;
        for (i = 0; i < 8; i++)
            lpmotion_control_flow->last_position_mech[i] =
                    lpmotion_control_flow->last_position_headers[0];
    }
}

/**
 *
 */
void adc_drv_foc_pwm_init(void) //move to HAL???
{
    // ADC init
    ADCInitialization(); ///from adc.c
    CONSOLEUtilsPrintf("\nADC ready...\n");

#ifdef ENABLE_DEBUG_FOC_PROFILE_GPIO
    // configure debug GPIOs
    DebugGPIOinit(); ///from pwms.c
    CONSOLEUtilsPrintf("DebugGPIO (0_14, 0_15) initialized...\n");
#endif

    DRV8313init();  ///inverter driver init from pwms.c
    CONSOLEUtilsPrintf("DRV8313 ready...\n");

    spi0_drv_init();    ///inverter driver spi init from HAL.c
    CONSOLEUtilsPrintf("DRV8313 parameters update...\n");

    motion_controller_init();
    CONSOLEUtilsPrintf("FOC initialized...\n");

    pwm_drv.PeriodMax = PWM_PERIOD / 10;
    Drv_PWM012_Init(&pwm_drv);  ///PWM generator init from pwms.c
    CONSOLEUtilsPrintf("PWM initialized...\n");

    spi0_biss_init();   ///BISS SPI channel init from HAL.c
    CONSOLEUtilsPrintf("BISS initialized...\n");

    CONSOLEUtilsPrintf("Motor ready to run\n");
}
