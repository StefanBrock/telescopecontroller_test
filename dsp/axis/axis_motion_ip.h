/*
 * ip.h
 *
 *  Created on: Mar 11, 2017
 *      Author: Dariusz.Janiszewski (at) put.poznan.pl
 */

#ifndef AXIS_AXIS_MOTION_IP_H_
#define AXIS_AXIS_MOTION_IP_H_

/// Please cite:
/// bibTex:
/*
 @Article{5332332,
 Title                    = {Damping of Torsional Vibrations in High-Dynamic Industrial Drives},
 Author                   = {Muszynski, R. and Deskur, J.},
 Journal                  = {IEEE Transactions on Industrial Electronics},
 Year                     = {2010},
 Month                    = {feb. },
 Number                   = {2},
 Pages                    = {544 -552},
 Volume                   = {57},
 Doi                      = {10.1109/TIE.2009.2036034},
 ISSN                     = {0278-0046},
 }
 */

#include "../motion_control.h"

typedef struct
{
    float K;    // Parameter: proportional loop gain =0.5 * pi*Lq*msi_rKmax/(U_DC*Tdly_i)
    float Kf;   // Parameter: integral gain (Ts/T)  =0.25*U_DC*regI_K_*Ts/(Lq*msi_rKmax)=(pi/8)*(Ts/Tdly_i)

    float id1;  // Data: storage last current differential
    float ud;   //temporary output
    float iq1;  // Data: storage last current differential
    float uq;   //temporary output

} STRUCT_IP_CONTROLLER;


typedef struct
{
    double K;    // Parameter: proportional loop gain
    double Kf;   // Parameter: integral gain (Ts/T)
    double speed1;  // Data: storage last speed
    float iref;   //temporary output
} STRUCT_SPEED_IP_CONTROLLER;


void motion_current_dq_ip_controller(
        STRUCT_MOTION_CONTROL_FLOW* lpmotion_control_flow,
        STRUCT_IP_CONTROLLER* lpip_controller);

void motion_speed_ip_controller(
    STRUCT_MOTION_CONTROL_FLOW* lpmotion_control_flow,
    STRUCT_SPEED_IP_CONTROLLER* lpspeed_controller);

#endif /* AXIS_AXIS_MOTION_IP_H_ */
