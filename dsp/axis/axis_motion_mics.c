// ============================================
// slew programmable ramper
// ============================================

#include "axis_motion_misc.h"

float ramper(float in, float out, float rampDelta)
{
    float err;

    err = in - out;
    if (err > rampDelta)
        return (out + rampDelta);
    else if (err < -rampDelta)
        return (out - rampDelta);
    else
        return (in);
}

// ============================================
// position reference generation test module
// ============================================
int posArray[8] = { (1.5), (-1.5), (2.5), (-2.5) }, ptrMax = 2, cntr1 = 0,
        ptr1 = 0, posSlewRate = (0.001);
int refPosGen(int out)
{
    int in = posArray[ptr1];

    out = ramper(in, out, posSlewRate);

    if (in == out)
        if (++cntr1 > 1000)
        {
            cntr1 = 0;
            if (++ptr1 >= ptrMax)
                ptr1 = 0;
        }
    return (out);
}
