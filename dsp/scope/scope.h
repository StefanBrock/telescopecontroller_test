/*
 * scope.h
 *
 *  Created on: Mar 10, 2017
 *      Author: brock
 */

#define SCOPE_DATA_DIMENSION (0x40000u)    // size of each scope channel
#define SCOPE_INT_SIZE SCOPE_DATA_DIMENSION
#define SCOPE_FLOAT_SIZE (SCOPE_DATA_DIMENSION*sizeof(int)/sizeof(float))
#define SCOPE_DOUBLE_SIZE (SCOPE_DATA_DIMENSION*sizeof(int)/sizeof(double))


