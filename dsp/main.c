/**
 * main.c
 *
 * Copyright (c) 2016, Poznan University of Technology
 * Copyright (c) 2016-2017, Dariusz Janiszewski
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Types.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/timers/dmtimer/Timer.h>
#include <ti/sysbios/family/arm/a8/Mmu.h> ///< for MMU

#include "HAL.h"

#include <console_utils.h>
#include "board.h" ///<system board ID rev
#include "soc.h" ///<procesor core ID
#include "board_support.h" ///<IDK board led, flash, etc
#include "os_drivers/osdrv_mmu.h"

#include "motion_control.h"
#include "../scope/scope.h" ///< Scope definitions (with variable)
#include "../SignalGenerator/SignalGenerator.h" ///< Signal Generator definitions
#include "GlobalVariable.h"

#define ENABLE_THREAD_SAFETY

// interrupt routine functions
extern void HWISRadc(UArg a0);
extern void HWISRbiss(UArg a0);

#define SYS_MMU_BUFFERABLE      1
#define SYS_MMU_CACHEABLE       2
#define SYS_MMU_SHAREABLE       4
#define SYS_MMU_NO_EXECUTE      8

SYS_MMU_ENTRY applMmuEntries[] = { { (void *) 0x44D00000, SYS_MMU_BUFFERABLE }, //PRCM - Non Cacheable | Bufferable : Device
        { (void *) 0x44E00000, SYS_MMU_BUFFERABLE }, //Clock Module, PRM, GPIO0, UART0, I2C0, - Non Cacheable | Bufferable : Device
        { (void *) 0x48000000, SYS_MMU_BUFFERABLE }, //UART1,UART2,I2C1,McSPI0,McASP0 CFG,McASP1 CFG,DMTIMER,GPIO1 -Non Cacheable | Bufferable : Device
        { (void *) 0x48100000, 0 }, //I2C2,McSPI1,UART3,UART4,UART5, GPIO2,GPIO3,MMC1 - Non bufferable| Non Cacheable : Strongly Ordered
        { (void *) 0x48300000, SYS_MMU_BUFFERABLE }, //PWM - Non Cacheable | Bufferable : Device
        { (void *) 0x4A000000, SYS_MMU_BUFFERABLE }, //L4 FAST CFG- Non Cacheable | Bufferable : Device
        { (void *) 0x54400000, SYS_MMU_SHAREABLE | SYS_MMU_BUFFERABLE }, //PRU-ICSS0/1 -Non Cacheable | Bufferable : Shared Device
        { (void *) 0xFFFFFFFF, 0xFFFFFFFF } };

// global variables
volatile uint8_t hvs_keyboard = 0x00; //external SPI keyboard
volatile uint8_t hvs_ledbar = 0x00; //external I2C LED bar

volatile i2cCfgObj_t *gLCDscreen;
#define LCDbuffer_size 80
volatile char LCDbuffer_temp[LCDbuffer_size];
volatile char (*pLCDbuffer_temp)[LCDbuffer_size];

//SYSBIOS variables
Clock_Params clockParams;
Clock_Handle clock100m;

extern volatile STRUCT_MOTION_CONTROL_FLOW *pmotion_control_flow; //trial new structure

/**
 *  ======== primary task taskFxn ========
 */
Void taskFxn(UArg a0, UArg a1)
{
    Hwi_Params hwi_params;
    Error_Block eb;

    System_printf("enter taskFxn()\n");

    BOARDInit(NULL);

    /** Initialize the UART console */
    CONSOLEUtilsInit();

    /** Select the console type based on compile time check */
    CONSOLEUtilsSetType(CONSOLE_UTILS_TYPE_UART);
    /** System description to console print*/
    CONSOLEUtilsPrintf("\n Telescope axis drive ");
//    CONSOLEUtilsPrintf(IND_SDK_VERSION);
    CONSOLEUtilsPrintf(" darjan & Brock (C), 2017 \n");
    CONSOLEUtilsPrintf("\n\rDevice name \t: ");
    CONSOLEUtilsPrintf(SOCGetSocFamilyName());
    CONSOLEUtilsPrintf("\n\rChip Revision \t: ");
    CONSOLEUtilsPrintf(Board_getChipRevision());
    CONSOLEUtilsPrintf("\n\rARM Clock rate \t: ");
    CONSOLEUtilsPrintf("%d MHz", Board_getArmClockRate());
    CONSOLEUtilsPrintf(
            "\n\rSYS/BIOS Standalone One Axis Motor Control Application running on modified ");
    CONSOLEUtilsPrintf(BOARDGetBoardName());

    //IDK original pinmux init
    pin_mux_init();

    //IDK hardware init
    board_init(
    BOARD_LED_DIGOUT |
    BOARD_TRICOLOR0_GREEN |
    BOARD_TRICOLOR0_RED |
    BOARD_HVS_DIGIN);

    gpio4_init();

    pwmss012init();

    //LED blinking
    int i;
    for (i = 0; i < 7; i++)
    {
        Board_setDigOutput(0x01 << i);
        Board_setTriColorLED(BOARD_TRICOLOR0_GREEN, 0);
        Board_setTriColorLED(BOARD_TRICOLOR0_RED, 1);
        Task_sleep(100);
        Board_setTriColorLED(BOARD_TRICOLOR0_GREEN, 1);
        Board_setTriColorLED(BOARD_TRICOLOR0_RED, 0);
        Task_sleep(100);
    }

    Board_enableGPIOClock(0xFF); //DJ ??

//Hardware Interrupt routine declaration PwmISR
//ADC1 generate interrupt after conversion
    Hwi_Params_init(&hwi_params);
    hwi_params.priority = 2;
    hwi_params.enableInt = 1;
    Error_init(&eb);
    //i=Hwi_create(epwm_intr, PwmISR, &hwi_params, &eb);    //previously based on PWM interrupt
    Hwi_create(ADC1_GENINT, HWISRadc, &hwi_params, &eb); //wait for simultaneously ADC0 and ADC1 end of conversion

//Hardware Interrupt routine declaration BissISR
//BISS-C CPLD generate interrupt after conversion
    Hwi_Params_init(&hwi_params);
    hwi_params.priority = 3;
    hwi_params.enableInt = 1;
    Error_init(&eb);
    Hwi_create(GPIOINT4A, HWISRbiss, &hwi_params, &eb);

//Hardware Interrupt routine declaration HWISRtripzone
//Trip Zone PWM generate interrupt inverter fault occurrence
    Hwi_Params_init(&hwi_params);
    hwi_params.priority = 4;
    hwi_params.enableInt = 1;
    Error_init(&eb);
    Hwi_create(ePWM0_TZINT, HWISRtripzone, &hwi_params, &eb);


    pLCDbuffer_temp = malloc(sizeof(char) * LCDbuffer_size); //LCDbuffer for short messages

    i2c_lcd_init();

    spi0_biss_init();

    adc_drv_foc_pwm_init();

    Clock_start(clock100m); //start periodic clock and tasks

    CONSOLEUtilsPrintf(
            "\nSpeed & Position displayed are based on BISS feedback \n(if encoder is not connected, they are invalid)\n\n");

    while (1)
    {
        char c;
        int x = -1, x_ok = 1;
        int y = -1, y_ok = 1, speed_ok = 1, position_ok = 1;
        int speed = 0, position = 0;

        //DRV read register1 tests DJ
        //unsigned short drv_register1 = spi0_drv_read_controlregister(0x02);

//        CONSOLEUtilsPrintf("press enter to configure...\n");
//        CONSOLEUtilsScanf("%c", &c);

        CONSOLEUtilsPrintf("\nspeed: %f mrad/s\t",
                           pmotion_control_flow->speed * 1000.0);
        CONSOLEUtilsPrintf("position: %f degree\t",
                           pmotion_control_flow->position_mech * (180 / PI));
        CONSOLEUtilsPrintf("PWM status: %d \t", pEnableSignals->EnablePWM);
        CONSOLEUtilsPrintf("LSW status: %d \t\n", pmotion_control_flow->lsw);
        DelayOSAL_microSeconds(100);

//        CONSOLEUtilsPrintf(
//                "enter msw (0 - speed control, 1 - position control): ");
//
//        if ((CONSOLEUtilsScanf("%d\n", &x) < 0) || (x != 0 && x != 1))
//        {
//            CONSOLEUtilsPrintf(
//                    "WARNING: invalid selection, previous value will be retained\n");
//            x_ok = 0;
//        }
//
//        CONSOLEUtilsPrintf(
//                "enter lsw (0 - off, 1 - open speed loop / position control, 2 - closed speed loop / position control): ");
//
//        if ((CONSOLEUtilsScanf("%d\n", &y) < 0) || (y != 0 && y != 1 && y != 2))
//        {
//            CONSOLEUtilsPrintf(
//                    "WARNING: invalid selection, previous value will be retained\n");
//            y_ok = 0;
//        }
//
//        CONSOLEUtilsPrintf(
//                "enter reference speed in rad/s (applicable only for speed control): ");
//
//        if ((CONSOLEUtilsScanf("%d\n", &speed) < 0) || (speed > 400)
//                || (speed < -400))
//        {
//            CONSOLEUtilsPrintf(
//                    "WARNING: invalid selection, previous value will be retained\n");
//            speed_ok = 0;
//        }
//
//        CONSOLEUtilsPrintf(
//                "enter reference position in angle (applicable only for position control): ");
//
//        if ((CONSOLEUtilsScanf("%d\n", &position) < 0) || (position < 0)
//                || (position > 360))
//        {
//            CONSOLEUtilsPrintf(
//                    "WARNING: invalid selection, previous value will be retained\n");
//            position_ok = 0;
//        }
//
//        if (x_ok)
//            pmotion_control_flow->msw = (unsigned) x;
//        if (y_ok)
//            pmotion_control_flow->lsw = (unsigned short) y;
//        if (speed_ok)
//            pmotion_control_flow->speed_ref = (double) speed;
//        if (position_ok)
//            pmotion_control_flow->position_ref = (float) position / 360;

//        pmotion_control_flow->drv_status = (spi0_drv_read_controlregister(0x00)
//                << 16) | (spi0_drv_read_controlregister(0x01));

    }

    System_printf("exit taskFxn()\n");
}

// HMI handler
Void clockHandler(UArg a0, UArg a1)
{
    Swi_disable(); //DJ???

    Board_getDigInput((uint8_t*) &hvs_keyboard);

    //TODO
//    pmotion_control_flow->drv_status = (spi0_drv_read_controlregister(0x00)<<16)|(spi0_drv_read_controlregister(0x01)) ;

    sprintf((char*) pLCDbuffer_temp,
            "position[hex]:  %08x        position[deg]:  %016.12f ",
            (unsigned int) pmotion_control_flow->position_biss[0],
            ((double) pmotion_control_flow->position_mech * (180 / PI)));

//// LCD TODO avoid DelayOSAL!!!
//    //LCD 16x4 handled
//    Board_initLCDscreen((i2cCfgObj_t *) gLCDscreen); //TODO some other settings influence
//    //Board_setLCDscreenClear((i2cCfgObj_t *) gLCDscreen);
//    Task_sleep(50);
//    //DelayOSAL_microSeconds(50);
//    //Board_setLCDscreenHome((i2cCfgObj_t *) gLCDscreen);
//    Board_setLCDscreenCmd((i2cCfgObj_t *) gLCDscreen, 0x02);
//    Task_sleep(5); //home
//    Board_setLCDscreenPrintf((i2cCfgObj_t *) gLCDscreen,
//                             (uint8_t*) pLCDbuffer_temp);

    Board_setDigOutput(hvs_keyboard); //output LEDbar
    Swi_enable();
}

/*
 *  ======== main ========
 */
Int main()
{
    Task_Handle task;
    Task_Params taskParams;
    Error_Block eb;

    ///Memory Management Unit Entries table init
    SDKMMUInit(applMmuEntries);   // needed first

    //free();

    System_printf("enter main()\n");

///create task taskFxn
    Task_Params_init(&taskParams);
    taskParams.priority = 3;
    Error_init(&eb);
    task = Task_create(taskFxn, &taskParams, &eb);
    if (task == NULL)
    {
        System_printf("Task_create() failed!\n");
        BIOS_exit(0);
    }

///Create periodic task
    Clock_Params_init(&clockParams);
    clockParams.period = 1000; /* every 1000 Clock ticks */
    clockParams.startFlag = FALSE; /* no start immediately */
    Error_init(&eb);
    clock100m = Clock_create((Clock_FuncPtr) clockHandler, 1000, &clockParams,
                             &eb); //1000 means first shot 1000ms after creation
    if (clock100m == NULL)
    {
        System_printf("Clock_create() failed!\n");
        BIOS_exit(0);
    }

    BIOS_start(); /* does not return */

    System_printf("End of operation\n");
    while (1)
    {
    }
    return (0);
}
