/*
 * motion_control.h
 *
 *  Created on: Feb 2, 2017
 *      Author: Dariusz Janiszewski
 */

#ifndef  MOTION_CONTROL_H_
#define MOTION_CONTROL_H_
/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */
typedef struct mem_motion_control_flow_type{ /*! A test struct */
    float Tsample;  //Sampling period in sec for 10kz
    unsigned int    IsrTicker;    ///< System current time, for all periodic and one-shot task
    unsigned int    in_control_loop; ///<if n0 then in control loop (between ADC aqusition and PWM update
    ///analog measurement data handle
    unsigned short  adc_value[8];   ///<16bit ADC values
    float  adc_offset[8];  ///<16bit ADC offsets (float or uint)
    float ia, ib, ic;   ///< measured phase currents in a-b-c stationary coordination system
    float ialpha, ibeta;    ///<phase currents in &alpha;-&beta; stationary coordination system
    ///mechanical measurement data handle
    unsigned int position_biss[4];  ///<full position messages from 4 heads (increments)


    double biss_offset[4];
    int   biss_offset_ready;

    unsigned int biss_ok; ///< 1 if BISS was read in last sampling time
    double last_position_mech[8]; ///< last 8 absolute position in rad <0..2*PI>
    double position_ref;
    double position_mech; ///< real mechanical position  in rad <0..2*PI>
    double position_elec; ///< real electric position in rad
    double position_elec_offset; ///< difference between real and electrical
    double last_position_headers[4];
    double speed; ///< speed in rad/s
    ///Control loop handle
        ///type of loop
        ///FOC, MPC, ... XXX: to discussion
        unsigned int    msw;    ///< TI: 0 - Speed control, non-zero - Position control
        unsigned short    lsw;  ///< TI: 0- lock the rotor in Aphase of the motor 1- close the current loop 2- close the speed loop
    float iq, id;   //measured feedback
    float iq_ref, id_ref;
    float uq_ref, ud_ref;
    float ualpha_ref, ubeta_ref;
    float PWM_Ta, PWM_Tb, PWM_Tc;

    //reference quantities
    double position_mech_ref; ///< rad
    double speed_ref; ///< rad/s


    //motor parameters
    char npp; //number pole pairs XXX: maybe in #define
        //motor
        float Rs, Ld, Lq;
        float J, B;

    //errors
    unsigned int errors; //TODO: struct of errors in future
    unsigned int biss_errors; ///< errors occurred during BISS handle
    unsigned int drv_status; ///<status DRV

    //DEBUG
    unsigned int pwmisrcnt;
    unsigned int bissisrcnt;

}STRUCT_MOTION_CONTROL_FLOW;


typedef struct EnableSignals_type{
    int EnablePWM;  // if 0 all PWM signals will be disables
}STRUCT_ENABLESIGNALS;




#endif /* MOTION_CONTROL_H_ */
