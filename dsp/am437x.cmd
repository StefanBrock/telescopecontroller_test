 MEMORY
{
    SRAM (RWX) : org = 0x402f0400, len = 0x10000-0x400
}
/*
 SECTIONS
{

  .bss : { *(.bss) } > EXT_RAM
  .vecs : { *(.vecs) } > EXT_RAM
  .stack : { *(.stack) } > EXT_RAM

   ti.sysbios.family.arm.a8.mmuTableSection : { *(ti.sysbios.family.arm.a8.mmuTableSection) } > EXT_RAM
  .data : { *(.data) } > EXT_RAM
   .tiesc_appreload_code :  { ./EcatStack/tiesc_appreload.o } > EXT_RAM
   app_reload_code_size = SIZEOF(.tiesc_appreload_code);
   app_reload_code_start = ADDR(.tiesc_appreload_code);
   app_reload_code_run = LOADADDR(.tiesc_appreload_code);

 }
 SECTIONS
{
  .stack : { *(.stack) } > OCMCRAM
  .bss : { *(.bss) } > OCMCRAM
  .data : { *(.data) } > OCMCRAM
  //.rodata : {*(.rodata)} > OCMCRAM
  .vecs : {*(.vecs)} > OCMCRAM
 }*/

