/*
 * HAL.h
 *
 *  Created on: Dec 9, 2016
 *      Author: darjan
 */

#ifndef HAL_HAL_H_
#define HAL_HAL_H_

#include "../axis/axis_motion_controller.h"
#include "board_LCD.h" 	//hd47780 4x16 via SPI

#include "board_mcspi.h"
#include "board_mux.h"
#include "am43xx_pinmux.h"
#include "am437x.h"
#include "gpio.h"
#include "stdio.h"

#include "biss_drv.h"

void pin_mux_init(void);

void gpio4_init(void);

void pwmss012init(void);


void spi0_biss_init(void);

void spi0_drv_init(void);

unsigned short spi0_drv_read_controlregister(unsigned short address_register);

void i2c_lcd_init(void);

void Board_MCSPICycle32(mcspiCfgObj_t *pCfgMcspi, uint32_t *buff, uint32_t len);
void Board_MCSPICycle16(mcspiCfgObj_t *pCfgMcspi, uint16_t *buff, uint32_t len);


int32_t PINMUXPerConfig(uint32_t ctrlModBase, pinmuxPerCfg_t* pInstanceData, uint16_t* pParam1);

static uint8_t gTxBuffer[32U]; //Transmit buffer to hold data
static uint8_t gRxBuffer[32U]; //Receive buffer to hold data

/** \brief MCSPI default configuration */
static const mcspiCfgObj_t MCSPI_DEFAULT =
{
    0U,                                      /* instNum.*/ //SPI0--> =0 ???
	0U,                                      /* instAddr.*/ //address of control register eg SOC_MCSPI0_REG
    48000000U,                               /* inClk.*/
    10000000U,                                /* outClk.*/
    0U,                                      /* channelNum.*/ //for different CS
    0U,                                      /* dataLength.*/
    gTxBuffer,                              /* pTx.*/
    gRxBuffer,                              /* rTx.*/
    {
        MCSPI_CH_MULTI,                     /* channel.*/
        MCSPI_TRANSFER_MODE_TX_RX,           /* txRxMode.*/
        MCSPI_DATA_LINE_COMM_MODE_7,         /* pinMode. */
        MCSPI_CLK_MODE_2,                    /* clkMode.*/
        8U,                                  /* wordLength.*/
        MCSPI_CS_POL_LOW,                    /* csPolarity.*/
        TRUE,                                /* txFifoCfg.*/
        FALSE,                                /* rxFifoCfg.*/
        MCSPI_INTR_TX_EMPTY(0U) | \
        MCSPI_INTR_RX_FULL(0U)               /* interrupt.*/
    },
    {
        INTC_TRIG_HIGH_LEVEL,                /* trigType.*/
        0U,                                  /* intrLine.*/
        10U,                                 /* intrPriority.*/
        FALSE,                               /* isIntrSecure.*/
        NULL,                                /* pTxBuf.*/
        NULL,                                /* pRxBuf.*/
        NULL                                 /* pFnIntrHandler.*/
    },
    {
        TRUE,                                /* csFlag.*/
        MCSPI_MODE_MASTER,                   /* modeFlag.*/
		MCSPI_INTERRUPT_MODE                 /* comFlag.*/
    }
};
mcspiCfgObj_t *gSPI0Obj;

/** \brief I2C default configuration */
static const i2cCfgObj_t I2C_CFG_DEFAULT =      //from board_support.c
    { 0U, /* instId */
            0U, /* instAddr */
            /* The instance Id and baseAddress shall be updated by ChipDB */
            0x0U, /* slaveAddr */
            {
                    100U, /* busSpeed */
                    I2C_ADDRMODE_7BIT, /* addrMode */
                    I2C_OPMODE_POLLING, /* opMode */
                    0U, /* rxThreshold */
                    0U /* txThreshold */
            }, /* i2cAppCfg */
            {
                    INTC_TRIG_HIGH_LEVEL, /* trigType */
                    0U, /* intrLine */
                    /* The interrupt line shall be updated using ChipDB */
                    0x20U, /* intrPriority */
                    FALSE, /* isIntrSecure */
                    NULL, /* pFnIntrhandler */
            } /* i2cIntrCfg  */
    };
volatile i2cCfgObj_t *gLCDscreen;


//from DRV8301_SPI.h //ControlSUITE

// DRV8301 SPI Input Data bit definitions:
struct  DRV8301_SPI_WRITE_WORD_BITS {       // bit    	description
   uint16_t DATA:11;          				// 10:0		FIFO reset
   uint16_t ADDRESS:4;        				// 14:11	Enhancement enable
   uint16_t R_W:1;          					// 15		R/W
};

union DRV8301_SPI_WRITE_WORD_REG {
   uint16_t               				all;
   struct DRV8301_SPI_WRITE_WORD_BITS	bit;
};

// DRV8301 SPI Status Reister 1 bit definitions:
struct  DRV8301_STATUS_REG_1_BITS {		// bit    	description
   uint16_t FETLC_OC:1;					// 0		Phase C, low-side FET OC
   uint16_t FETHC_OC:1;					// 1		Phase C, high-side FET OC
   uint16_t FETLB_OC:1;					// 2		Phase B, low-side FET OC
   uint16_t FETHB_OC:1;					// 3		Phase B, high-side FET OC
   uint16_t FETLA_OC:1;					// 4		Phase A, low-side FET OC
   uint16_t FETHA_OC:1;					// 5		Phase A, high-side FET OC
   uint16_t OTW:1;	        			// 6		Over-temperature warning
   uint16_t OTSD:1;	          			// 7		Over-temperature shut down
   uint16_t PVDD_UV:1;					// 8		PVDD < 6V
   uint16_t GVDD_UV:1;					// 9		GVDD < 8V
   uint16_t FAULT:1;						// 10		FAULTn pin is asserted
   uint16_t Reserved:5;					// 15:11
};

union DRV8301_STATUS_REG_1 {
   uint16_t               			all;
   struct DRV8301_STATUS_REG_1_BITS	bit;
};

// DRV8301 SPI Status Reister 2 bit definitions:
struct  DRV8301_STATUS_REG_2_BITS {		// bit    	description
   uint16_t DEVICE_ID:4;					// 3:0		Device ID
   uint16_t Reserved1:3;					// 6:4
   uint16_t GVDD_OV:1;					// 7		GVDD > 16V
   uint16_t Reserved2:8;					// 15:8
};

union DRV8301_STATUS_REG_2 {
   uint16_t               			all;
   struct DRV8301_STATUS_REG_2_BITS	bit;
};

// DRV8301 SPI Control Reister 1 bit definitions:
struct  DRV8301_CONTROL_REG_1_BITS {	// bit    	description
   uint16_t GATE_CURRENT:2;				// 1:0		Gate driver peak current, 1.7A (00b), 0.7A (01b), 0.25A (10b), Reserved (11b)
   uint16_t GATE_RESET:1;        			// 2		Reset all latched faults (1), Normal Mode (0)
   uint16_t PWM_MODE:1;          			// 3		Three (1) or six (0) pwm inputs
   uint16_t OC_MODE:2;					// 5:4		over-current mode, current limit (00b), latched shut down (01b), Report only (10b), OC disabled (11b)
   uint16_t OC_ADJ_SET:5;					// 10:6		Set Vds trip point for OC see the table in the datasheet
   uint16_t Reserved:5;					// 15:11
};

union DRV8301_CONTROL_REG_1 {
   uint16_t               				all;
   struct DRV8301_CONTROL_REG_1_BITS	bit;
};

// DRV8301 SPI Control Reister 2 bit definitions:
struct  DRV8301_CONTROL_REG_2_BITS {	// bit    	description
   uint16_t OCTW_SET:2;					// 1:0		Report OT and OC (00b), Report OT (01b), Report OC (10b), Reserved (11b)
   uint16_t GAIN:2;        				// 3:2		Gain of shunt amplifier, 10 (00b), 20 (01b), 40 (10b), 80 (11b)
   uint16_t DC_CAL_CH1:1;        			// 4		Shunt amplifier inputs shorted and disconnected from load (1) or shunt amplifier inputs connected to load (0)
   uint16_t DC_CAL_CH2:1;        			// 5		Shunt amplifier inputs shorted and disconnected from load (1) or shunt amplifier inputs connected to load (0)
   uint16_t OC_TOFF:1;					// 6		Normal CBC operation (0), off time control during OC (1)
   uint16_t Reserved:9;					// 15:7
};

union DRV8301_CONTROL_REG_2 {
   uint16_t               				all;
   struct DRV8301_CONTROL_REG_2_BITS	bit;
};

union DRV8301_STATUS_REG_1 DRV8301_stat_reg1;
union DRV8301_STATUS_REG_2 DRV8301_stat_reg2;
union DRV8301_CONTROL_REG_1 DRV8301_cntrl_reg1;
union DRV8301_CONTROL_REG_2 DRV8301_cntrl_reg2;

/***************************************************************************************************/
//defines
/***************************************************************************************************/
//DRV8301 Register Addresses
#define STAT_REG_1_ADDR		0x00
#define STAT_REG_2_ADDR		0x01
#define CNTRL_REG_1_ADDR	0x02
#define CNTRL_REG_2_ADDR	0x03



#endif /* HAL_HAL_H_ */
