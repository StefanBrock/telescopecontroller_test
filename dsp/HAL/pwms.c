/*
 * pwms.c
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

/* ==================================================================================
File name:       foc_pwm.c

Description:   This file contains source for the PWM  drivers for the AM437x

Target: AM437x

====================================================================================*/

/*
 * modified by dariusz . janiszewski (at) put . poznan . pl
 *
 */

#include "types.h"
#include "hw_types.h"
#include <epwm.h>                // Sitara APIs Definition Library

#include <am437x.h>
#include <pwms.h>
#include "hw_cm_per.h"

//#include "osdrv_epwm.h"
#include "gpio.h"
#include "os_drivers/osdrv_osal.h"
#include "os_drivers/osdrv_epwm.h"

void PWMSSModuleClkConfig(unsigned int instanceNum);
void PWMSSClockEnable(unsigned int instance);

#define ENABLE_PWM_SYNC    /* sync b/w PWM's */

#define CLOCK_DIV_VAL        1    // Clock Pre scaler.
#define TimeBasePhase        0
#define DBcnt                100

#define SOC_EPWM_MODULE_FREQ                 100



void Drv_PWM345_Init(PWMGEN *p)
{
        Int32 Tm;
        Tm = (Int32)p->PeriodMax;

        PowerPWM();
        epwmAqActionCfg_t gCfg;
        gCfg.zeroAction = EPWM_AQ_ACTION_DONOTHING;
        gCfg.cmpADownAction = EPWM_AQ_ACTION_LOW;
        gCfg.cmpAUpAction = EPWM_AQ_ACTION_HIGH;
        gCfg.cmpBDownAction = EPWM_AQ_ACTION_DONOTHING;
        gCfg.cmpBUpAction = EPWM_AQ_ACTION_DONOTHING;
        gCfg.prdAction = EPWM_AQ_ACTION_DONOTHING;

        /************ EPWM 3 *****************
         * Init Timer-Base Control Register
         * Configure the clock frequency     */
        EPWMTbTimebaseClkCfg(SOC_PWMSS3_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL ,
                                SOC_EPWM_MODULE_FREQ);

        /* Configure the period of the output waveform             */
        EPWMTbPwmFreqCfg(SOC_PWMSS3_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL*1000000,
                                Tm,
                                EPWM_TB_COUNTER_DIR_UP_DOWN,
                                EPWM_SHADOW_REG_CTRL_DISABLE);


#ifdef ENABLE_PWM_SYNC
 #ifdef SYNC_ECAT_PWM
        /* ECAT SYNC has to be connected to PWM3 SYNCI in h/w     */
        /* Route PWM3 syncout to PWM4,5 synci                     */
        EPWMTbSetSyncOutMode(SOC_PWMSS3_REG, EPWM_TB_SYNC_OUT_EVT_SYNCIN);
        /* Enable PWM3 synci to sync with EtherCAT sync           */
        EPWMTbSyncEnable(SOC_PWMSS3_REG, TimeBasePhase,
                                EPWM_TB_CNT_DIR_AFT_SYNC_UP);
 #else
        /* Disable syncin for PWM3                                */
        EPWMTbSyncDisable(SOC_PWMSS3_REG);
        /* Enable syncout to sync PWM4,5 with PWM3                */
        EPWMTbSetSyncOutMode(SOC_PWMSS3_REG, EPWM_TB_SYNC_OUT_EVT_CNT_EQ_ZERO);
 #endif
#else
        /* Disable synchronization                                */
        EPWMTbSyncDisable(SOC_PWMSS3_REG);
#endif

        /* Configure the emulation behaviour                    */
        EPWMTbSetEmulationMode(SOC_PWMSS3_REG, EPWM_TB_EMU_MODE_FREE_RUN);

        //************ EPWM 4 *****************
        /* Configure the clock frequency                         */
        EPWMTbTimebaseClkCfg(SOC_PWMSS1_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL ,
                                SOC_EPWM_MODULE_FREQ);

        /* Configure the period of the output waveform             */
        EPWMTbPwmFreqCfg(SOC_PWMSS4_REG,
                            SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL*1000000,
                            Tm,
                            EPWM_TB_COUNTER_DIR_UP_DOWN,
                            EPWM_SHADOW_REG_CTRL_DISABLE);

#ifdef ENABLE_PWM_SYNC
        /* Enable synchronization                                */
        EPWMTbSyncEnable(SOC_PWMSS4_REG, TimeBasePhase,
                                EPWM_TB_CNT_DIR_AFT_SYNC_UP);
#endif

        EPWMTbSetEmulationMode(SOC_PWMSS4_REG, EPWM_TB_EMU_MODE_FREE_RUN);

        //************ EPWM 5 *****************
        /* Configure the clock frequency                         */
        EPWMTbTimebaseClkCfg(SOC_PWMSS5_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL ,
                                SOC_EPWM_MODULE_FREQ);

        /* Configure the period of the output waveform             */
        EPWMTbPwmFreqCfg(SOC_PWMSS5_REG,
                            SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL*1000000,
                            Tm,
                            EPWM_TB_COUNTER_DIR_UP_DOWN,
                            EPWM_SHADOW_REG_CTRL_DISABLE);
#ifdef ENABLE_PWM_SYNC
        /* Enable synchronization                                */
        EPWMTbSyncEnable(SOC_PWMSS5_REG, TimeBasePhase,
                                EPWM_TB_CNT_DIR_AFT_SYNC_UP);
#endif

        /* Configure the emulation behaviour                    */
        EPWMTbSetEmulationMode(SOC_PWMSS5_REG, EPWM_TB_EMU_MODE_FREE_RUN);

        // // Init Compare Control Register for EPWM1-EPWM3
        /* Load Compare A value */
        EPWMCounterComparatorCfg(SOC_PWMSS3_REG, EPWM_CC_CMP_A, (Tm/4), EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
        /* Load Compare A value */
        EPWMCounterComparatorCfg(SOC_PWMSS4_REG, EPWM_CC_CMP_A, (Tm/4), EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);

        /* Load Compare A value */
        EPWMCounterComparatorCfg(SOC_PWMSS5_REG, EPWM_CC_CMP_A, (Tm/4), EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);

        // // Init Action Qualifier Output A and B Register for EPWM1-EPWM3
        /* Configure Action qualifier                             */
        EPWMAqActionOnOutputCfg(SOC_PWMSS3_REG, EPWM_OUTPUT_CH_A, &gCfg);

        EPWMAqActionOnOutputCfg(SOC_PWMSS4_REG, EPWM_OUTPUT_CH_A, &gCfg);

        EPWMAqActionOnOutputCfg(SOC_PWMSS5_REG, EPWM_OUTPUT_CH_A, &gCfg);


        // // Disable Dead Band Modulation for PWM3-5
        /* Dead band sub-module Mode Selection                             */
        EPWMDeadbandBypass(SOC_PWMSS3_REG);
        /* Dead band sub-module Mode Selection                             */
        EPWMDeadbandBypass(SOC_PWMSS4_REG);
        /* Dead band sub-module Mode Selection                             */
        EPWMDeadbandBypass(SOC_PWMSS5_REG);

        // // Init PWM Chopper Control Register for EPWM1-EPWM3
        /* Disable Chopper sub-module                             */
        EPWMChopperEnable(SOC_PWMSS3_REG, 0);
        EPWMChopperEnable(SOC_PWMSS4_REG, 0);
        EPWMChopperEnable(SOC_PWMSS5_REG, 0);
        // // Init Trip Zone Select Register
        /* Disable trip events                                     */
        EPWMTzTripEventDisable(SOC_PWMSS3_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
        EPWMTzTripEventDisable(SOC_PWMSS3_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);
        EPWMTzTripEventDisable(SOC_PWMSS4_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
        EPWMTzTripEventDisable(SOC_PWMSS4_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);

        EPWMTzTripEventDisable(SOC_PWMSS3_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
        EPWMTzTripEventDisable(SOC_PWMSS3_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);
        /* Event trigger                                         */
        /* Generate interrupt every 1st occurrence of the event     */
        EPWMEtIntrCfg(SOC_PWMSS3_REG, EPWM_ET_INTR_EVT_CNT_EQ_PRD, EPWM_ET_INTR_PERIOD_FIRST_EVT);
        /* Generate event when CTR = 0                          */
        /* Clear any Interrupt event                             */
        EPWMEtIntrClear(SOC_PWMSS3_REG);
        /* Enable interrupt                                     */
        EPWMEtIntrEnable(SOC_PWMSS3_REG);
        /* Disable High resolution capability                     */
        EPWMHighResolutionDisable(SOC_PWMSS3_REG);


}

void Drv_PWM012_Init(PWMGEN *p)
{
        Int32 Tm;
        Tm = (Int32)p->PeriodMax;

        PowerPWM();
        epwmAqActionCfg_t gCfg;
        gCfg.zeroAction = EPWM_AQ_ACTION_DONOTHING;
        gCfg.cmpADownAction = EPWM_AQ_ACTION_LOW;
        gCfg.cmpAUpAction = EPWM_AQ_ACTION_HIGH;
        gCfg.cmpBDownAction = EPWM_AQ_ACTION_DONOTHING;
        gCfg.cmpBUpAction = EPWM_AQ_ACTION_DONOTHING;
        gCfg.prdAction = EPWM_AQ_ACTION_DONOTHING;

        /************ EPWM 0 *****************
         * Init Timer-Base Control Register
         * Configure the clock frequency     */
        EPWMTbTimebaseClkCfg(SOC_PWMSS0_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL ,
                                SOC_EPWM_MODULE_FREQ);

        /* Configure the period of the output waveform             */
        EPWMTbPwmFreqCfg(SOC_PWMSS0_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL*1000000,
                                Tm,
                                EPWM_TB_COUNTER_DIR_UP_DOWN,
                                EPWM_SHADOW_REG_CTRL_DISABLE);


#ifdef ENABLE_PWM_SYNC
 #ifdef SYNC_ECAT_PWM
        /* ECAT SYNC has to be connected to PWM3 SYNCI in h/w     */
        /* Route PWM3 syncout to PWM4,5 synci                     */
        EPWMTbSetSyncOutMode(SOC_PWMSS0_REG, EPWM_TB_SYNC_OUT_EVT_SYNCIN);
        /* Enable PWM3 synci to sync with EtherCAT sync           */
        EPWMTbSyncEnable(SOC_PWMSS0_REG, TimeBasePhase,
                                EPWM_TB_CNT_DIR_AFT_SYNC_UP);
 #else
        /* Disable syncin for PWM0                                */
        EPWMTbSyncDisable(SOC_PWMSS0_REG);
        /* Enable syncout to sync PWM4,5 with PWM3                */
        EPWMTbSetSyncOutMode(SOC_PWMSS0_REG, EPWM_TB_SYNC_OUT_EVT_CNT_EQ_ZERO);
 #endif
#else
        /* Disable synchronization                                */
        EPWMTbSyncDisable(SOC_PWMSS0_REG);
#endif

        /* Configure the emulation behaviour                    */
        EPWMTbSetEmulationMode(SOC_PWMSS0_REG, EPWM_TB_EMU_MODE_FREE_RUN);

        //************ EPWM 1 *****************
        /* Configure the clock frequency                         */
        EPWMTbTimebaseClkCfg(SOC_PWMSS1_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL ,
                                SOC_EPWM_MODULE_FREQ);

        /* Configure the period of the output waveform             */
        EPWMTbPwmFreqCfg(SOC_PWMSS1_REG,
                            SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL*1000000,
                            Tm,
                            EPWM_TB_COUNTER_DIR_UP_DOWN,
                            EPWM_SHADOW_REG_CTRL_DISABLE);

#ifdef ENABLE_PWM_SYNC
        /* Enable synchronization                                */
        EPWMTbSyncEnable(SOC_PWMSS1_REG, TimeBasePhase,
                                EPWM_TB_CNT_DIR_AFT_SYNC_UP);
#endif

        EPWMTbSetEmulationMode(SOC_PWMSS1_REG, EPWM_TB_EMU_MODE_FREE_RUN);

        //************ EPWM 2 *****************
        /* Configure the clock frequency                         */
        EPWMTbTimebaseClkCfg(SOC_PWMSS2_REG,
                                SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL ,
                                SOC_EPWM_MODULE_FREQ);

        /* Configure the period of the output waveform             */
        EPWMTbPwmFreqCfg(SOC_PWMSS2_REG,
                            SOC_EPWM_MODULE_FREQ/CLOCK_DIV_VAL*1000000,
                            Tm,
                            EPWM_TB_COUNTER_DIR_UP_DOWN,
                            EPWM_SHADOW_REG_CTRL_DISABLE);
#ifdef ENABLE_PWM_SYNC
        /* Enable synchronization                                */
        EPWMTbSyncEnable(SOC_PWMSS2_REG, TimeBasePhase,
                                EPWM_TB_CNT_DIR_AFT_SYNC_UP);
#endif

        /* Configure the emulation behaviour                    */
        EPWMTbSetEmulationMode(SOC_PWMSS2_REG, EPWM_TB_EMU_MODE_FREE_RUN);

        // // Init Compare Control Register for EPWM1-EPWM3
        /* Load Compare A value */
        EPWMCounterComparatorCfg(SOC_PWMSS0_REG, EPWM_CC_CMP_A, (Tm/4), EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
        /* Load Compare A value */
        EPWMCounterComparatorCfg(SOC_PWMSS1_REG, EPWM_CC_CMP_A, (Tm/4), EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);

        /* Load Compare A value */
        EPWMCounterComparatorCfg(SOC_PWMSS2_REG, EPWM_CC_CMP_A, (Tm/4), EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);

        // // Init Action Qualifier Output A and B Register for EPWM1-EPWM3
        /* Configure Action qualifier                             */
        EPWMAqActionOnOutputCfg(SOC_PWMSS0_REG, EPWM_OUTPUT_CH_A, &gCfg);

        EPWMAqActionOnOutputCfg(SOC_PWMSS1_REG, EPWM_OUTPUT_CH_A, &gCfg);

        EPWMAqActionOnOutputCfg(SOC_PWMSS2_REG, EPWM_OUTPUT_CH_A, &gCfg);


        // Enable Dead Band Modulation for PWM0-2
        epwmDeadbandCfg_t gDeadbandCfg;
        gDeadbandCfg.inputMode = EPWM_DB_IN_MODE_A_RED_A_FED;
        gDeadbandCfg.outputMode = EPWM_DB_OUT_MODE_A_RED_B_FED;
        gDeadbandCfg.polaritySelect = EPWM_DB_POL_SEL_ACTV_HIGH_COMPLEMENTARY;
        gDeadbandCfg.risingEdgeDelay = 100u; //for c20069 is 1.66us -> for 10kHz = 100u
        gDeadbandCfg.fallingEdgeDelay = 100u; //for c20069 is 1.66us -> for 10kHz = 100u

        EPWMDeadbandCfg(SOC_PWMSS0_REG, &gDeadbandCfg);
        EPWMDeadbandCfg(SOC_PWMSS1_REG, &gDeadbandCfg);
        EPWMDeadbandCfg(SOC_PWMSS2_REG, &gDeadbandCfg);


        // // Init PWM Chopper Control Register for EPWM1-EPWM3
        /* Disable Chopper sub-module                             */
        EPWMChopperEnable(SOC_PWMSS0_REG, 0);
        EPWMChopperEnable(SOC_PWMSS1_REG, 0);
        EPWMChopperEnable(SOC_PWMSS2_REG, 0);


        //TODO DJ: make tripzone interrupt and tripzone handle (with DRV8301 error/fault inquiry)
        // // Init Trip Zone Select Register
        /* Disable trip events                                     */
        EPWMTzTripEventEnable(SOC_PWMSS0_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
        //EPWMTzTripEventEnable(SOC_PWMSS0_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);
        EPWMTzTripEventEnable(SOC_PWMSS1_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
        //EPWMTzTripEventEnable(SOC_PWMSS1_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);
        EPWMTzTripEventEnable(SOC_PWMSS2_REG, EPWM_TZ_EVENT_ONE_SHOT, 0);
        //EPWMTzTripEventEnable(SOC_PWMSS2_REG, EPWM_TZ_EVENT_CYCLE_BY_CYCLE, 0);

        //EPWMTzTriggerTripAction --all channel high impedance

        EPWMTzIntrEnable(SOC_PWMSS0_REG, EPWM_TZ_EVENT_ONE_SHOT);

        /* Event trigger                                         */
        /* Generate interrupt every 1st occurrence of the event     */
        EPWMEtIntrCfg(SOC_PWMSS0_REG, EPWM_ET_INTR_EVT_CNT_EQ_PRD, EPWM_ET_INTR_PERIOD_FIRST_EVT);
        /* Generate event when CTR = 0                          */
        /* Clear any Interrupt event                             */
        EPWMEtIntrClear(SOC_PWMSS0_REG);
        /* Enable interrupt                                     */
        EPWMEtIntrEnable(SOC_PWMSS0_REG);
        /* Disable High resolution capability                     */
        EPWMHighResolutionDisable(SOC_PWMSS0_REG);
}

//inline void Drv_PWM_ClearInt(PRUICSS_Handle pruIcssHandle)
//{
//    /* clear PRU event in INTC of ICSS0 */
//    HWREG(((PRUICSS_HwAttrs *)(pruIcssHandle->hwAttrs))->baseAddr + 0x20000 + 0x24) = 18;
//}


void Drv_PWM345_Update(PWMGEN *p)
{
        Int16 MPeriod;
        Int32 Tmp;
// Compute the timer period (Q0) from the period modulation input (Q15)
        Tmp = (Int32)p->PeriodMax*(Int32)p->MfuncPeriod;           // Q15 = Q0*Q15
        MPeriod = (Int16)(Tmp>>16) + (Int16)(p->PeriodMax>>1);     // Q0 = (Q15->Q0)/2 + (Q0/2)
        HWREGH(SOC_PWMSS3_EPWM_REG + PWMSS_EPWM_TBPRD) = MPeriod;
        HWREGH(SOC_PWMSS4_EPWM_REG + PWMSS_EPWM_TBPRD) = MPeriod;
        HWREGH(SOC_PWMSS5_EPWM_REG + PWMSS_EPWM_TBPRD) = MPeriod;

// Compute the compare A (Q0) from the related duty cycle ratio (Q15)
        Tmp = (Int32)MPeriod*(Int32)p->MfuncC1;                    // Q15 = Q0*Q15
        p->PWM1out = (Int16)(Tmp>>16) + (Int16)(MPeriod>>1);   // Q0 = (Q15->Q0)/2 + (Q0/2)
// Compute the compare B (Q0) from the related duty cycle ratio (Q15)
        Tmp = (Int32)MPeriod*(Int32)p->MfuncC2;                   // Q15 = Q0*Q15
        p->PWM2out= (Int16)(Tmp>>16) + (Int16)(MPeriod>>1);  // Q0 = (Q15->Q0)/2 + (Q0/2)
// Compute the compare C (Q0) from the related duty cycle ratio (Q15)
        Tmp = (Int32)MPeriod*(Int32)p->MfuncC3;                   // Q15 = Q0*Q15
        p->PWM3out= (Int16)(Tmp>>16) + (Int16)(MPeriod>>1);  // Q0 = (Q15->Q0)/2 + (Q0/2)

        EPWMCounterComparatorCfg(SOC_PWMSS3_REG, EPWM_CC_CMP_A, p->PWM1out, EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
        EPWMCounterComparatorCfg(SOC_PWMSS4_REG, EPWM_CC_CMP_A, p->PWM2out, EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
        EPWMCounterComparatorCfg(SOC_PWMSS5_REG, EPWM_CC_CMP_A, p->PWM3out, EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
}

void Drv_PWM012_Update(PWMGEN *p)
{
        Int16 MPeriod;
        Int32 Tmp;
// Compute the timer period (Q0) from the period modulation input (Q15)
        Tmp = (Int32)p->PeriodMax*(Int32)p->MfuncPeriod;           // Q15 = Q0*Q15
        MPeriod = (Int16)(Tmp>>16) + (Int16)(p->PeriodMax>>1);     // Q0 = (Q15->Q0)/2 + (Q0/2)
        HWREGH(SOC_PWMSS0_EPWM_REG + PWMSS_EPWM_TBPRD) = MPeriod;
        HWREGH(SOC_PWMSS1_EPWM_REG + PWMSS_EPWM_TBPRD) = MPeriod;
        HWREGH(SOC_PWMSS2_EPWM_REG + PWMSS_EPWM_TBPRD) = MPeriod;

// Compute the compare A (Q0) from the related duty cycle ratio (Q15)
        Tmp = (Int32)MPeriod*(Int32)p->MfuncC1;                    // Q15 = Q0*Q15
        p->PWM1out = (Int16)(Tmp>>16) + (Int16)(MPeriod>>1);   // Q0 = (Q15->Q0)/2 + (Q0/2)
// Compute the compare B (Q0) from the related duty cycle ratio (Q15)
        Tmp = (Int32)MPeriod*(Int32)p->MfuncC2;                   // Q15 = Q0*Q15
        p->PWM2out= (Int16)(Tmp>>16) + (Int16)(MPeriod>>1);  // Q0 = (Q15->Q0)/2 + (Q0/2)
// Compute the compare C (Q0) from the related duty cycle ratio (Q15)
        Tmp = (Int32)MPeriod*(Int32)p->MfuncC3;                   // Q15 = Q0*Q15
        p->PWM3out= (Int16)(Tmp>>16) + (Int16)(MPeriod>>1);  // Q0 = (Q15->Q0)/2 + (Q0/2)

        EPWMCounterComparatorCfg(SOC_PWMSS0_REG, EPWM_CC_CMP_A, p->PWM1out, EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
        EPWMCounterComparatorCfg(SOC_PWMSS1_REG, EPWM_CC_CMP_A, p->PWM2out, EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
        EPWMCounterComparatorCfg(SOC_PWMSS2_REG, EPWM_CC_CMP_A, p->PWM3out, EPWM_SHADOW_REG_CTRL_ENABLE, EPWM_CC_CMP_LOAD_MODE_CNT_EQ_ZERO, 1);
}

/* configure GPIOs to enable DRV8313 on AM437x IDK */
void DRV8313init(void)
{
    // The following pads control the DRV8313:
    // CCDC0_DATA5 / GPIO4_27: EN1
    // UART3_TXD / GPIO5_3: EN2
    // UART3_RTSN / GPIO5_1: EN3
    // ccdc1_vd / GPIO4_10: nSLEEP - same as Alpha
    // gpmc_ad10 GPIO0_26: nFAULT
    // ccdc1_data8 / GPIO4_8: nRESET
    // gpmc_wait0 / GPIO0_30 - CAM_MTR_EN - Set high to disable camera input on muxed pins
    // configure GPIOs as output
    //HWREG(0x44E07134) &= ~((1<<26) | (1<<30));    // GPIO0_26 /GPIO0_30
    GPIOSetDirMode(SOC_GPIO0_REG, 30, GPIO_DIRECTION_OUTPUT);
    GPIOSetDirMode(SOC_GPIO5_REG, 1, GPIO_DIRECTION_OUTPUT);
    GPIOSetDirMode(SOC_GPIO5_REG, 3, GPIO_DIRECTION_OUTPUT);
    GPIOSetDirMode(SOC_GPIO4_REG, 8, GPIO_DIRECTION_OUTPUT);
    GPIOSetDirMode(SOC_GPIO4_REG, 10, GPIO_DIRECTION_OUTPUT);
    GPIOSetDirMode(SOC_GPIO4_REG, 27, GPIO_DIRECTION_OUTPUT);

    GPIOPinWrite(SOC_GPIO0_REG, 30, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO4_REG, 8, GPIO_PIN_HIGH);

    DelayOSAL_milliSeconds(1);

    GPIOPinWrite(SOC_GPIO5_REG, 1, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO5_REG, 3, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO4_REG, 8, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO4_REG, 10, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO4_REG, 27, GPIO_PIN_LOW);

    DelayOSAL_milliSeconds(1);

    GPIOPinWrite(SOC_GPIO5_REG, 1, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO5_REG, 3, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO4_REG, 8, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO4_REG, 10, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO4_REG, 27, GPIO_PIN_HIGH);

}

void DebugGPIOinit(void)
{
    // Debug GPIO for timing measurement
    // L21: uart1_txd / AM437X_PROFI_TXD / gpio0_15/ J15 pin 45
    // K21: uart1_rxd / AM437X_PROFI_RXD / gpio0_14 / J15 pin 47
    // configure GPIOs as output
    GPIOSetDirMode(SOC_GPIO0_REG, 14, GPIO_DIRECTION_OUTPUT);
    GPIOSetDirMode(SOC_GPIO0_REG, 15, GPIO_DIRECTION_OUTPUT);
    // set GPIOs high
    GPIOPinWrite(SOC_GPIO0_REG, 14, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO0_REG, 15, GPIO_PIN_HIGH);
}
