/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_led.h"
#include "board_i2c.h"
#include "gpio.h"

#include "os_drivers/osdrv_osal.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/** \brief LED device instance number */
#define I2C_LED_INST_NUM         (0U)
/** \brief Number of GPIOs used for TRI color LED */
#define NO_OF_GPIO               (5U)


#define LCD_I2C_DEVICE			0x27	/**< Change this to the address of your expander */
#define LCD_LINES			4	/**< Enter the number of lines of your display here */
#define LCD_ROWS			16	/**< Enter the number of rows of your display here */

#define LCD_LINE1			0	/**< This should be 0x00 on all displays */
#define LCD_LINE2			40	/**< Change this to the address for line 2 on your display */
#define LCD_LINE3			LCD_LINE1+LCD_ROWS	/**< Change this to the address for line 3 on your display */
#define LCD_LINE4			LCD_LINE2+LCD_ROWS	/**< Change this to the address for line 4 on your display */

//LCD I2C
#define LCD_D4_PIN			4	/**< LCD-Pin D4 is connected to P4 on the PCF8574 */
#define LCD_D5_PIN			5	/**< LCD-Pin D5 is connected to P5 on the PCF8574 */
#define LCD_D6_PIN			6	/**< LCD-Pin D6 is connected to P6 on the PCF8574 */
#define LCD_D7_PIN			7	/**< LCD-Pin D7 is connected to P7 on the PCF8574 */
#define LCD_RS_PIN			0	/**< LCD-Pin RS is connected to P0 on the PCF8574 */
#define LCD_RW_PIN			1	/**< LCD-Pin RW is connected to P1 on the PCF8574 */
//#define LCD_EMPTY_PIN			6	/**< this pin is not connected */
#define LCD_BL_PIN			3	/**< this pin is not connected */
#define LCD_E_PIN			2	/**< LCD-Pin E is connected to P7 on the PCF8574 */


#define LCD_D0				(1 << LCD_D4_PIN)	/**< bit 0 in 1st lower nibble */
#define LCD_D1				(1 << LCD_D5_PIN)	/**< bit 1 in 1st lower nibble */
#define LCD_D2				(1 << LCD_D6_PIN)	/**< bit 2 in 1st lower nibble */
#define LCD_D3				(1 << LCD_D7_PIN)	/**< bit 3 in 1st lower nibble */

#define LCD_D4				(1 << LCD_D4_PIN)	/**< bit 4 in 2nd lower nibble */
#define LCD_D5				(1 << LCD_D5_PIN)	/**< bit 5 in 2nd lower nibble */
#define LCD_D6				(1 << LCD_D6_PIN)	/**< bit 6 in 2nd lower nibble */
#define LCD_D7				(1 << LCD_D7_PIN)	/**< bit 7 in 2nd lower nibble */

#define LCD_RS				(1 << LCD_RS_PIN)	/**< RS-bit in 1st and 2nd higher nibble */
#define LCD_RW				(1 << LCD_RW_PIN)	/**< RW-bit in 1st and 2nd higher nibble */
//#define LCD_EMPTY			(1 << LCD_EMPTY_PIN)	/**< empty-bit in 1st and 2nd higher nibble */
#define LCD_BL				(1 << LCD_BL_PIN)	/**< empty-bit in 1st and 2nd higher nibble */
#define LCD_E				(1 << LCD_E_PIN)	/**< E-bit in 1st and 2nd higher nibble */

int LCD_backlight = 1;

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int32_t Board_initLCDscreen(i2cCfgObj_t *pI2cObj)
{
    int32_t status = E_FAIL;
    uint32_t  i2cInstNum;
    uint32_t  slaveAddr;

    uint32_t  instAddr;

    status = Board_initPlatform(CHIPDB_MOD_ID_I2C, (uint32_t)DEVICE_ID_I2C_LED,
                                I2C_LED_INST_NUM,
                                &i2cInstNum, &slaveAddr, &instAddr);
    pI2cObj->i2cInstNum = i2cInstNum;
    pI2cObj->slaveAddr = slaveAddr;
    pI2cObj->i2cBaseAddr  = instAddr;

    /* Configure i2c bus speed*/
    i2cUtilsInitParams_t pUserParams;
    pUserParams.addrMode = pI2cObj->i2cDevCfg.addrMode;
    pUserParams.busSpeed = (pI2cObj->i2cDevCfg.busSpeed);
    pUserParams.operMode = pI2cObj->i2cDevCfg.opMode;
    pUserParams.rxThreshold = pI2cObj->i2cDevCfg.rxThreshold;
    pUserParams.txThreshold = pI2cObj->i2cDevCfg.txThreshold;
    status = I2CUtilsInit(pI2cObj->i2cInstNum, &pUserParams);


    return status;

}

/*
 *  Board_setLCDscreenOut - Send one value to parallel LCD port (higer nibble valid)
 *  LCDscreen_val - value to be applied
 *      Board_setLCDscreenOut(gLCDscreen, 0x61); //char a 0x61
        Board_setLCDscreenOut(gLCDscreen, 0x11); //char a
 */
void Board_setLCDscreenOut(i2cCfgObj_t *pI2cCtrlCfg, uint8_t LCDscreen_val)
{
    uint8_t out_buff[3];// = { 0x55, 0xbb };
    if (LCD_backlight)
    {
    	out_buff[0]=(LCDscreen_val | LCD_BL | LCD_E) & ~(LCD_RW);
    	out_buff[1]=(LCDscreen_val | LCD_BL  | LCD_E) & ~(LCD_RW);
    	out_buff[2]=((LCDscreen_val | LCD_BL)  & ~(LCD_E)) & ~(LCD_RW) ;
    }
    else
    {
    	out_buff[0]=(LCDscreen_val | LCD_E) & ~(LCD_RW);
    	out_buff[1]=(LCDscreen_val | LCD_E) & ~(LCD_RW);
    	out_buff[2]=(LCDscreen_val & ~(LCD_E)) & ~(LCD_RW) ;
    }
    i2cUtilsTxRxParams_t i2cObj;
    i2cObj.slaveAddr = LCD_I2C_DEVICE;
    i2cObj.offsetSize = 0;
    i2cObj.bufLen = 3;
    i2cObj.pBuffer = (uint8_t *)&out_buff[0];
    Board_i2cUtilsWrite(pI2cCtrlCfg->i2cInstNum, &i2cObj, 1000);
}

void Board_setLCDscreenCmd(i2cCfgObj_t *pI2cCtrlCfg, uint8_t LCDscreen_cmd)
{
    uint8_t out_buff[5];
    uint8_t LCDscreen_cmdL = (LCDscreen_cmd&0x0f)<<4;
    uint8_t LCDscreen_cmdH = (LCDscreen_cmd&0xf0);

    if (LCD_backlight)
    {
    	out_buff[0]=(LCDscreen_cmdH | LCD_BL | LCD_E) & (~LCD_RW);
    	out_buff[1]=(LCDscreen_cmdH | LCD_BL | LCD_E) & (~LCD_RW);
    	out_buff[2]=((LCDscreen_cmdH | LCD_BL) & (~LCD_E)) & (~LCD_RW) ;
    	out_buff[3]=(LCDscreen_cmdL | LCD_BL | LCD_E) & (~LCD_RW);
    	out_buff[4]=((LCDscreen_cmdL | LCD_BL) & (~LCD_E)) & (~LCD_RW) ;
    }
    else
    {
    	out_buff[0]=(LCDscreen_cmdH | LCD_E) & (~LCD_RW);
    	out_buff[1]=(LCDscreen_cmdH | LCD_E) & (~LCD_RW);
    	out_buff[2]=((LCDscreen_cmdH) & (~LCD_E)) & (~LCD_RW) ;
    	out_buff[3]=(LCDscreen_cmdL | LCD_E) & (~LCD_RW);
    	out_buff[4]=((LCDscreen_cmdL) & (~LCD_E)) & (~LCD_RW) ;

    }

    i2cUtilsTxRxParams_t i2cObj;
    i2cObj.slaveAddr = LCD_I2C_DEVICE;
    i2cObj.offsetSize = 0; //1
    i2cObj.bufLen = 5;
    i2cObj.pBuffer = (uint8_t *)&out_buff[0];
    Board_i2cUtilsWrite(pI2cCtrlCfg->i2cInstNum, &i2cObj, 1000);
}


void Board_setLCDscreenChar(i2cCfgObj_t *pI2cCtrlCfg, uint8_t LCDscreen_char)
{
    uint8_t out_buff[5];
    uint8_t LCDscreen_charL = (LCDscreen_char&0x0f)<<4;
    uint8_t LCDscreen_charH = (LCDscreen_char&0xf0);

    if (LCD_backlight)
    {
    	out_buff[0]=(LCDscreen_charH | LCD_BL | LCD_RS | LCD_E) & (~LCD_RW);
    	out_buff[1]=(LCDscreen_charH | LCD_BL | LCD_RS | LCD_E) & (~LCD_RW);
    	out_buff[2]=((LCDscreen_charH | LCD_BL  | LCD_RS) & (~LCD_E)) & (~LCD_RW) ;
    	out_buff[3]=(LCDscreen_charL | LCD_BL  | LCD_RS | LCD_E) & (~LCD_RW);
    	out_buff[4]=((LCDscreen_charL | LCD_BL  | LCD_RS) & (~LCD_E)) & (~LCD_RW) ;
    }
    else
    {
    	out_buff[0]=(LCDscreen_charH | LCD_RS | LCD_E) & (~LCD_RW);
    	out_buff[1]=(LCDscreen_charH | LCD_RS | LCD_E) & (~LCD_RW);
    	out_buff[2]=((LCDscreen_charH | LCD_RS) & (~LCD_E)) & (~LCD_RW) ;
    	out_buff[3]=(LCDscreen_charL | LCD_RS | LCD_E) & (~LCD_RW);
    	out_buff[4]=((LCDscreen_charL | LCD_RS) & (~LCD_E)) & (~LCD_RW) ;

    }

    i2cUtilsTxRxParams_t i2cObj;
    i2cObj.slaveAddr = LCD_I2C_DEVICE;//pI2cCtrlCfg->i2cBaseAddr;
    i2cObj.offsetSize = 0;
    i2cObj.bufLen = 5;
    i2cObj.pBuffer = (uint8_t *)&out_buff[0];
    Board_i2cUtilsWrite(pI2cCtrlCfg->i2cInstNum, &i2cObj, 1000);
    DelayOSAL_microSeconds(50);
}

void Board_setLCDscreenPrintf(i2cCfgObj_t *pI2cCtrlCfg, uint8_t *LCDscreen_buff)
{
	uint8_t cnt8_tmp;
	uint8_t position;
	uint8_t bufftoprint[0x50]="                                                                                ";
	//Board_setLCDscreenCmd(pI2cCtrlCfg, 0x01); //clear display

	for (cnt8_tmp=0; LCDscreen_buff[cnt8_tmp] != 0x00; cnt8_tmp++)
	{
		//position=cnt8_tmp;
		if (cnt8_tmp<LCD_ROWS) position = cnt8_tmp+LCD_LINE1;
		else if (cnt8_tmp<LCD_ROWS*2) position = cnt8_tmp +LCD_LINE2-LCD_ROWS;
		else if (cnt8_tmp<LCD_ROWS*3) position = cnt8_tmp +LCD_LINE3-LCD_ROWS*2;
		else if (cnt8_tmp<LCD_ROWS*4) position = cnt8_tmp +LCD_LINE4-LCD_ROWS*3;

		bufftoprint[position]=LCDscreen_buff[cnt8_tmp];
	}
	for (cnt8_tmp=0; cnt8_tmp<0x4f; cnt8_tmp++)
	{
		Board_setLCDscreenChar(pI2cCtrlCfg, bufftoprint[cnt8_tmp]);
	}
}

void Board_setLCDscreenClear(i2cCfgObj_t *pI2cCtrlCfg)
{
    Board_setLCDscreenCmd(pI2cCtrlCfg, 0x01); //clear
    DelayOSAL_milliSeconds(5);
}

void Board_setLCDscreenHome(i2cCfgObj_t *pI2cCtrlCfg)
{
    Board_setLCDscreenCmd(pI2cCtrlCfg, 0x02); //home
    DelayOSAL_milliSeconds(5); //1.52ms necessery
}


void Board_setLCDscreenInit(i2cCfgObj_t *pI2cCtrlCfg)
{
        Board_setLCDscreenOut(pI2cCtrlCfg, 0x30); //init
        DelayOSAL_milliSeconds(5);
        Board_setLCDscreenOut(pI2cCtrlCfg, 0x30); //init
        DelayOSAL_milliSeconds(1);
        Board_setLCDscreenOut(pI2cCtrlCfg, 0x30); //init
        DelayOSAL_milliSeconds(1);
        Board_setLCDscreenOut(pI2cCtrlCfg, 0x20); //4 bit mode
        DelayOSAL_milliSeconds(1);
        Board_setLCDscreenCmd(pI2cCtrlCfg, 0x28);//N=1 (2line), F=0 (5x8)
        Board_setLCDscreenCmd(pI2cCtrlCfg, 0x0c); //display on, cursorr off, blink off,
        Board_setLCDscreenCmd(pI2cCtrlCfg, 0x01); //clear
        DelayOSAL_milliSeconds(1);
}

/* -------------------------------------------------------------------------- */
/*                 Internal Function Definitions                              */
/* -------------------------------------------------------------------------- */
