/*
 * biss_drv.c
 *
 *  Created on: Dec 13, 2016
 *      Author: Dariusz Janiszewski
 */

#include "biss_drv.h"


// (uint32_t *)pCfgMcspi->pRx --encoder data values, first is control int
uint32_t* biss_get_angles(mcspiCfgObj_t *pCfgMcspi)
{
    //spi0_biss_init();
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_HIGH); //SPI: 1 - BISS, 0 - DRV8301
    //DelayOSAL_microSeconds(5);
    volatile int iii;
    for(iii=0;iii<100;iii++)
        {
        ;
        }
    Board_MCSPICycle32(pCfgMcspi,(uint32_t *)pCfgMcspi->pRx, pCfgMcspi->dataLength);
    //DelayOSAL_microSeconds(5);
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_LOW); //SPI: 1 - BISS, 0 - DRV8301
    return (uint32_t *)pCfgMcspi->pRx; //first 32bit control values, next 4 times data

}

