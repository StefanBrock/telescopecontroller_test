/**
 * @file    board_support.c
 *
 */
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef BOARD_SUPPORT_C_
#define BOARD_SUPPORT_C_

#include "board_i2c.h"
#include "board_support.h"
#include "board_gpio.h"
#include "board_led.h"
#include "board_mcspi.h"
#ifdef AM335X_FAMILY_BUILD
    #include "board_spiflash.h"
    #include "board_lcd.h"
    #include "board_rotaryswitch.h"
    #include "hw_control_am335x.h"
#elif defined (AM43XX_FAMILY_BUILD)
    #include "board_qspi.h"
    #include "hw_control_am43xx.h"
#endif

#include "osdrv_osal.h"

/**********************************************************************
 ************************** Local Definitions *************************
 **********************************************************************/
#ifdef AM43XX_FAMILY_BUILD
static char *chip_ver_name[] =
{
    "AM437x [PG1.1]",
    "AM437x [PG1.2]",
    "Unknown"
};
#elif defined (AM335X_FAMILY_BUILD)
static char *chip_ver_name[] =
{
    "AM335x [PG1.1]",
    "AM335x [PG2.0]",
    "AM335x [PG2.1]",
    "Unknown"
};
#endif
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/** \brief Application default configuration */
static const i2cCfgObj_t I2C_CFG_DEFAULT =
{
    0U,                             /* instId */
    0U,                             /* instAddr */
    /* The instance Id and baseAddress shall be updated by ChipDB */
    0x0U,                          /* slaveAddr */
    {
        100U,                       /* busSpeed */
        I2C_ADDRMODE_7BIT,          /* addrMode */
        I2C_OPMODE_POLLING,         /* opMode */
        0U,                         /* rxThreshold */
        0U                          /* txThreshold */
    },  /* i2cAppCfg */
    {
        INTC_TRIG_HIGH_LEVEL,       /* trigType */
        0U,                         /* intrLine */
        /* The interrupt line shall be updated using ChipDB */
        0x20U,                      /* intrPriority */
        FALSE,                      /* isIntrSecure */
        NULL,                       /* pFnIntrhandler */
    }  /* i2cIntrCfg  */
};

/* Transmit buffer to hold data of 1 page. */
static uint8_t gTxBuffer[260U];
/* Receive buffer to hold data of 1 page. */
static uint8_t gRxBuffer[260U];
/** \brief Application default configuration */
static const mcspiCfgObj_t MCSPI_DEFAULT =
{
    0U,                                      /* instNum.*/
    0U,                                      /* instAddr.*/
    48000000U,                               /* inClk.*/
    1000000U,                                /* outClk.*/
    0U,                                      /* channelNum.*/
    0U,                                      /* dataLength.*/
    gTxBuffer,                              /* pTx.*/
    gRxBuffer,                              /* rTx.*/
    {
        MCSPI_CH_MULTI,                     /* channel.*/
        MCSPI_TRANSFER_MODE_TX_RX,           /* txRxMode.*/
        MCSPI_DATA_LINE_COMM_MODE_7,         /* pinMode. */
        MCSPI_CLK_MODE_2,                    /* clkMode.*/
        8U,                                  /* wordLength.*/
        MCSPI_CS_POL_LOW,                    /* csPolarity.*/
        TRUE,                                /* txFifoCfg.*/
        FALSE,                                /* rxFifoCfg.*/
        MCSPI_INTR_TX_EMPTY(0U) | \
        MCSPI_INTR_RX_FULL(0U)               /* interrupt.*/
    },
    {
        INTC_TRIG_HIGH_LEVEL,                /* trigType.*/
        0U,                                  /* intrLine.*/
        10U,                                 /* intrPriority.*/
        FALSE,                               /* isIntrSecure.*/
        NULL,                                /* pTxBuf.*/
        NULL,                                /* pRxBuf.*/
        NULL                                 /* pFnIntrHandler.*/
    },
    {
        TRUE,                                /* csFlag.*/
        MCSPI_MODE_MASTER,                   /* modeFlag.*/
        MCSPI_INTERRUPT_MODE                 /* comFlag.*/
    }
};
#ifdef AM43XX_FAMILY_BUILD
static const qspiObj_t QSPIOBJ_DEFAULT =
{
    0U,  /* chipSelect */
    0U,  /* instNum */
    0U,  /* instAddr */
    DEVICE_ID_INVALID,  /* devId */
    0U,  /* length*/
    {
        {
            0U, /* chipSelect */
            0U, /* memMapBaseAddr */
            0U, /* cfgBaseAddr */
            QSPI_LIB_READ_TYPE_QUAD, /* qspiLibReadType */
            QSPI_LIB_TX_MODE_MEMORY_MAPPED, /* qspiLibTxMode */
        },
        {
            (64U * MEM_SIZE_MB),   /* deviceSize */
            0x19U,/* flashDevId */
            0xC2,/* flashMfgId */
            (4U * MEM_SIZE_KB),   /* sectorSize */
            (64U * MEM_SIZE_KB),   /* blockSize */
        }
    }
};
#endif
/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/
/** \brief Flag to make sure CDCE correction only done once */
static uint8_t cdceClockCorrectionDone = 0;
/** \brief Saves the detected boardType */
static boardId_t boardType;
/** \brief  Global structure I2C LED */
i2cCfgObj_t *gLedDigOut;
/** \brief  Global structure for GPIO LED Red U-48 */
gpioPinObj_t *gpioLedRed0;
/** \brief  Global structure for GPIO LED Green U-48 */
gpioPinObj_t *gpioLedGreen0;
/** \brief  Global structure for GPIO LED Red U-48 */
gpioPinObj_t *gpioLedYellow0;
/** \brief  Global structure for GPIO LED Red U-48 */
gpioPinObj_t *gpioLedRed1;
/** \brief  Global structure for GPIO LED Red U-48 */
gpioPinObj_t *gpioLedGreen1;
/** \brief  Global structure for GPIO LED Red U-48 */
gpioPinObj_t *gpioLedYellow1;
/** \brief  Global structure for McSPI instance related data */
mcspiCfgObj_t *gHvsDigIn;
#ifdef AM335X_FAMILY_BUILD
    /** \brief  Global structure for McSPI instance related data */
    mcspiCfgObj_t *gSPIFlashObj;
    /** \brief  Global variable for storing LCD Init status */
    int8_t lcdInitStatus;
    /** \brief  Global variable for storing Rotary switch Init status */
    int8_t rotarySwInitStatus;
#elif defined(AM43XX_FAMILY_BUILD)
    /** \brief  Global structure for QSPI instance related data */
    qspiObj_t *gQSPIFlashObj;
#endif

#ifdef ENABLE_THREAD_SAFETY
    /* Semaphore locks used for thread safety */
    /** \brief semaphore lock for McSPI access */
    void *mcspiLock;
    /** \brief semaphore lock for QSPI Utils access */
    void *qspiLock;
    /** \brief semaphore lock for I2C Utils access */
    void *i2cLock;
#endif
/**********************************************************************
 ************************** Platform Functions ************************
 **********************************************************************/
int8_t board_init(uint32_t modules)
{
    int8_t retVal = E_FAIL;
    boardType = BOARDGetId();

    // Increased the frequency of CDCE913 clock generator for PHYs from 24.999 MHz to 25.000 MHz by making XCSEL to 0 pF.
    if((0 == cdceClockCorrectionDone) && (BOARD_ICEV2 == boardType
                                          || BOARD_IDKEVM == boardType))
    {
#ifdef ENABLE_THREAD_SAFETY

        if(NULL == i2cLock)
        {
            SemOSAL_Params semParams;
            SemOSAL_Params_init(&semParams);
            semParams.mode = SemOSAL_Mode_BINARY;

            i2cLock = SemOSAL_create(1, &semParams);
        }

#endif
        /* Configure i2c bus speed*/
        i2cUtilsInitParams_t pUserParams;
        pUserParams.addrMode = I2C_ADDRMODE_7BIT;
        pUserParams.busSpeed = 100U;
        pUserParams.operMode = I2C_OPMODE_POLLING;
        pUserParams.rxThreshold = 0U;
        pUserParams.txThreshold = 0U;
        I2CUtilsInit(0, &pUserParams);

        uint8_t out_buff[2] = { 0x85, 0x00 };   //Byte write 00 to XCSEL register - offset=05h
        i2cUtilsTxRxParams_t i2cObj;
        i2cObj.slaveAddr = 0x65;                // CDCE913 Slave receive address
        i2cObj.pOffset = (uint8_t *)&out_buff[0];
        i2cObj.offsetSize = 1;
        i2cObj.bufLen = 1;
        i2cObj.pBuffer = (uint8_t *)&out_buff[1];
        Board_i2cUtilsWrite(0, &i2cObj, 1000);
        cdceClockCorrectionDone = 1;
    }

    if(modules & BOARD_LED_DIGOUT)
    {
#ifdef ENABLE_THREAD_SAFETY

        if(NULL == i2cLock)
        {
            SemOSAL_Params semParams;
            SemOSAL_Params_init(&semParams);
            semParams.mode = SemOSAL_Mode_BINARY;

            i2cLock = SemOSAL_create(1, &semParams);
        }

#endif
        gLedDigOut = (i2cCfgObj_t *)malloc(sizeof(i2cCfgObj_t));
        *gLedDigOut = I2C_CFG_DEFAULT;
        retVal = Board_initDigOut(gLedDigOut);
    }

    if(modules & BOARD_TRICOLOR0_RED)
    {
        gpioLedRed0 = (gpioPinObj_t *)malloc(sizeof(gpioPinObj_t));
        retVal = Board_initTriColorLED(gpioLedRed0, TRI_COLOR0_RED);
    }

    if(modules & BOARD_TRICOLOR0_GREEN)
    {
        gpioLedGreen0 = (gpioPinObj_t *)malloc(sizeof(gpioPinObj_t));
        retVal = Board_initTriColorLED(gpioLedGreen0, TRI_COLOR0_GREEN);
    }

    if(modules & BOARD_TRICOLOR0_YELLOW)
    {
        gpioLedYellow0 = (gpioPinObj_t *)malloc(sizeof(gpioPinObj_t));
        retVal = Board_initTriColorLED(gpioLedYellow0, TRI_COLOR0_YELLOW);
    }

    if(modules & BOARD_TRICOLOR1_RED)
    {
        gpioLedRed1 = (gpioPinObj_t *)malloc(sizeof(gpioPinObj_t));
        retVal = Board_initTriColorLED(gpioLedRed1, TRI_COLOR1_RED);
    }

    if(modules & BOARD_TRICOLOR1_GREEN)
    {
        gpioLedGreen1 = (gpioPinObj_t *)malloc(sizeof(gpioPinObj_t));
        retVal = Board_initTriColorLED(gpioLedGreen1, TRI_COLOR1_GREEN);
    }

    if(modules & BOARD_TRICOLOR1_YELLOW)
    {
        gpioLedYellow1 = (gpioPinObj_t *)malloc(sizeof(gpioPinObj_t));
        retVal = Board_initTriColorLED(gpioLedYellow1, TRI_COLOR1_YELLOW);
    }

    if(modules & BOARD_HVS_DIGIN)
    {
        gHvsDigIn = (mcspiCfgObj_t *)malloc(sizeof(mcspiCfgObj_t));
        *gHvsDigIn = MCSPI_DEFAULT;
#ifdef AM335X_FAMILY_BUILD
        gHvsDigIn->pCfg.channel = MCSPI_CH_SINGLE;
        gHvsDigIn->pCfg.pinMode = MCSPI_DATA_LINE_COMM_MODE_1;
#endif
#ifdef ENABLE_THREAD_SAFETY

        if(NULL == mcspiLock)
        {
            SemOSAL_Params semParams;
            SemOSAL_Params_init(&semParams);
            semParams.mode = SemOSAL_Mode_BINARY;

            mcspiLock = SemOSAL_create(1, &semParams);
        }

#endif
        retVal = Board_initHVSDigIn(gHvsDigIn);
    }

    if(modules & BOARD_FLASH_MEMORY)
    {
#ifdef AM335X_FAMILY_BUILD
#ifdef ENABLE_THREAD_SAFETY

        if(NULL == mcspiLock)
        {
            SemOSAL_Params semParams;
            SemOSAL_Params_init(&semParams);
            semParams.mode = SemOSAL_Mode_BINARY;

            mcspiLock = SemOSAL_create(1, &semParams);
        }

#endif
        gSPIFlashObj = (mcspiCfgObj_t *)malloc(sizeof(mcspiCfgObj_t));
        *gSPIFlashObj = MCSPI_DEFAULT;
        retVal = Board_initSpiFlash(gSPIFlashObj);
#elif  defined(AM43XX_FAMILY_BUILD)
#ifdef ENABLE_THREAD_SAFETY
        SemOSAL_Params semParams;
        SemOSAL_Params_init(&semParams);
        semParams.mode = SemOSAL_Mode_BINARY;

        qspiLock = SemOSAL_create(1, &semParams);
#endif
        gQSPIFlashObj = (qspiObj_t *)malloc(sizeof(qspiObj_t));
        qspiLibInfo_t qspiLibInfo;
        *gQSPIFlashObj = QSPIOBJ_DEFAULT;
        qspiFlashDeviceData_t *flashDeviceData;

        retVal = Board_initQSPI(gQSPIFlashObj);

        if(S_PASS == retVal)
        {
            qspiLibInfo = gQSPIFlashObj->qspiLibInfo;
            qspiLibInfo.qspiLibCtrlInfo.cfgBaseAddr = SOC_QSPI_ADDRSP0_REG;
            /* Set the QSPI memory mapped base Address from chibdb*/
            qspiLibInfo.qspiLibCtrlInfo.memMapBaseAddr = gQSPIFlashObj->instAddr;
            qspiLibInfo.qspiLibCtrlInfo.chipSelect = gQSPIFlashObj->chipSelect;
            /* Initialise the QPSI controller */
            QSPILibInit(&qspiLibInfo);
        }

        qspiLibInfo.qspiLibCtrlInfo.qspiLibTxMode = QSPI_LIB_TX_MODE_MEMORY_MAPPED;
        qspiLibInfo.qspiLibCtrlInfo.qspiLibReadType = QSPI_LIB_READ_TYPE_QUAD;
        flashDeviceData = QSPIFlashGetDeviceData(gQSPIFlashObj->devId);
        /* Enter quad mode if Quad read mode is selected */
        QSPILibQuadEnable(&qspiLibInfo, flashDeviceData);

        gQSPIFlashObj->qspiLibInfo = qspiLibInfo;
#endif
    }

#ifdef AM335X_FAMILY_BUILD

    if(modules & BOARD_LCD_DISPLAY)
    {
#ifdef ENABLE_THREAD_SAFETY

        if(NULL == i2cLock)
        {
            SemOSAL_Params semParams;
            SemOSAL_Params_init(&semParams);
            semParams.mode = SemOSAL_Mode_BINARY;

            i2cLock = SemOSAL_create(1, &semParams);
        }

#endif
        lcdInitStatus = Board_initLCD();
        retVal = lcdInitStatus;
    }

    if(modules & BOARD_ROTARY_SWITCH)
    {
        rotarySwInitStatus = Board_initRotarySwitch();
    }

#endif

    return retVal;
}

int8_t Board_setDigOutput(uint8_t ledVal)
{
    int8_t retVal = E_FAIL;

    if(NULL != gLedDigOut)
    {
        Board_setDigOut(gLedDigOut, ledVal);
        retVal = S_PASS;
    }

    return retVal;
}

int8_t Board_setTriColorLED(uint32_t ledType, uint8_t value)
{
    int8_t retVal = E_FAIL;

    if(ledType & BOARD_TRICOLOR0_RED)
    {
        if(NULL != gpioLedRed0)
        {
            Board_setTriColor(gpioLedRed0, value);
            retVal = S_PASS;
        }
    }

    else if(ledType & BOARD_TRICOLOR0_GREEN)
    {
        if(NULL != gpioLedGreen0)
        {
            Board_setTriColor(gpioLedGreen0, value);
            retVal = S_PASS;
        }
    }

    else if(ledType & BOARD_TRICOLOR0_YELLOW)
    {
        if(NULL != gpioLedYellow0)
        {
            Board_setTriColor(gpioLedYellow0, value);
            retVal = S_PASS;
        }
    }

    else if(ledType & BOARD_TRICOLOR1_RED)
    {
        if(NULL != gpioLedRed1)
        {
            Board_setTriColor(gpioLedRed1, value);
            retVal = S_PASS;
        }
    }

    else if(ledType & BOARD_TRICOLOR1_GREEN)
    {
        if(NULL != gpioLedGreen1)
        {
            Board_setTriColor(gpioLedGreen1, value);
            retVal = S_PASS;
        }
    }

    else if(ledType & BOARD_TRICOLOR1_YELLOW)
    {
        if(NULL != gpioLedYellow1)
        {
            Board_setTriColor(gpioLedYellow1, value);
            retVal = S_PASS;
        }
    }

    return retVal;
}

int8_t Board_getDigInput(uint8_t *value)
{
    int8_t retVal = E_FAIL;

    if(NULL != gHvsDigIn)
    {
#ifdef ENABLE_THREAD_SAFETY

        if(OSAL_TIMEOUT == SemOSAL_pend(mcspiLock, DEFAULT_TIMEOUT))
        {
            return E_FAIL;
        }

#endif
        Board_getHVSDigIn(gHvsDigIn);

        /* Limitation on ICEv2, only 7 inputs available */
        if(boardType == BOARD_ICEV2)
        {
            (gHvsDigIn->pRx[0]) = (gHvsDigIn->pRx[0]) & 0x7F;
        }

        *value = gHvsDigIn->pRx[0];
#ifdef ENABLE_THREAD_SAFETY
        SemOSAL_post(mcspiLock);
#endif
        retVal = S_PASS;
    }

    return retVal;
}

int8_t Board_readFlashStorage(uint32_t offset, uint32_t length,
                              uint8_t *buffer)
{
    int8_t retVal = E_FAIL;
#ifdef AM335X_FAMILY_BUILD

    if(NULL != gSPIFlashObj)
    {
#ifdef ENABLE_THREAD_SAFETY

        if(OSAL_TIMEOUT == SemOSAL_pend(mcspiLock, DEFAULT_TIMEOUT))
        {
            return E_FAIL;
        }

#endif
        gSPIFlashObj->dataLength = length;

        retVal = Board_readSpiFlash(offset, buffer, gSPIFlashObj);

        if(retVal == E_FAIL)
        {
#ifdef ENABLE_THREAD_SAFETY
            SemOSAL_post(mcspiLock);
#endif
            return retVal;
        }

        else
        {
            retVal = S_PASS;
        }

#ifdef ENABLE_THREAD_SAFETY
        SemOSAL_post(mcspiLock);
#endif
    }

#elif defined(AM43XX_FAMILY_BUILD)

    if(NULL != gQSPIFlashObj)
    {
#ifdef ENABLE_THREAD_SAFETY

        if(OSAL_TIMEOUT == SemOSAL_pend(mcspiLock, DEFAULT_TIMEOUT))
        {
            return E_FAIL;
        }

#endif
        retVal = QSPILibRead(&(gQSPIFlashObj->qspiLibInfo), offset,
                             (uint32_t)buffer, length);
#ifdef ENABLE_THREAD_SAFETY
        SemOSAL_post(mcspiLock);
#endif
    }

#endif
    return retVal;
}

int8_t Board_writeFlashStorage(uint32_t offset, uint32_t length,
                               uint8_t *buffer, uint8_t eraseFlag)
{
    int8_t retVal = E_FAIL;
#ifdef AM335X_FAMILY_BUILD

    if(NULL != gSPIFlashObj)
    {
#ifdef ENABLE_THREAD_SAFETY

        if(OSAL_TIMEOUT == SemOSAL_pend(mcspiLock, DEFAULT_TIMEOUT))
        {
            return E_FAIL;
        }

#endif
        gSPIFlashObj->dataLength = length;
        gSPIFlashObj->pTx = buffer;
        retVal = Board_writeSpiFlash(offset, eraseFlag, gSPIFlashObj);
#ifdef ENABLE_THREAD_SAFETY
        SemOSAL_post(mcspiLock);
#endif
    }

#elif defined(AM43XX_FAMILY_BUILD)

    if(NULL != gQSPIFlashObj)
    {
        if(1 == eraseFlag)
        {
            uint32_t blockNumber = 0;
            int32_t tempLength = 0;
            uint32_t i = 0;
#ifdef ENABLE_THREAD_SAFETY

            if(OSAL_TIMEOUT == SemOSAL_pend(mcspiLock, DEFAULT_TIMEOUT))
            {
                return E_FAIL;
            }

#endif
            tempLength = length;
            blockNumber = (offset) /
                          gQSPIFlashObj->qspiLibInfo.qspiLibFlashInfo.blockSize;

            /* Erase 64k(blockSize) blocks. Repeat for all required blocks */
            while(tempLength > 0)
            {
                QSPILibBlockErase(&(gQSPIFlashObj->qspiLibInfo), blockNumber + i);
                tempLength = tempLength - gQSPIFlashObj->qspiLibInfo.qspiLibFlashInfo.blockSize;
                i++;
            }

#ifdef ENABLE_THREAD_SAFETY
            SemOSAL_post(mcspiLock);
#endif
        }

#ifdef ENABLE_THREAD_SAFETY

        if(OSAL_TIMEOUT == SemOSAL_pend(mcspiLock, DEFAULT_TIMEOUT))
        {
            return E_FAIL;
        }

#endif
        retVal = QSPILibWrite(&(gQSPIFlashObj->qspiLibInfo), offset,
                              (uint32_t)buffer, length);
#ifdef ENABLE_THREAD_SAFETY
        SemOSAL_post(mcspiLock);
#endif
    }

#endif
    return retVal;
}

#ifdef AM335X_FAMILY_BUILD
int8_t Board_setLCDString(uint8_t *lcdString, uint8_t lineNumber)
{
    if(S_PASS != lcdInitStatus)
    {
        return E_FAIL;
    }

    Board_showStringLCD(1, lcdString, lineNumber, 0);
    return S_PASS;
}

int8_t Board_clearLCDString()
{
    if(S_PASS != lcdInitStatus)
    {
        return E_FAIL;
    }

    Board_fillRAMLCD(0x00);
    return S_PASS;
}

int8_t Board_setLCDScroll(uint8_t enable)
{
    if(S_PASS != lcdInitStatus)
    {
        return E_FAIL;
    }

    if(0 == enable)
    {
        Board_deactivateScrollLCD();
    }

    else if(1 == enable)
    {
        Board_continuousScrollLCD(0x01, 0x01, Max_Column, 0x00, 0x00, 0x01, 0x06,
                                  0x01);
    }

    return S_PASS;
}

int8_t Board_getRotarySwitchPosition(uint8_t *position)
{
    int8_t retVal = E_FAIL;

    if(S_PASS != rotarySwInitStatus)
    {
        return E_FAIL;
    }

    retVal = Board_readRotarySwitch(position);
    return retVal;
}
#endif

int Board_getArmClockRate()
{
    //Return ARM frequency
    uint32_t clkin, dpllMult, dpllDiv;
#ifdef AM43XX_FAMILY_BUILD
    uint32_t clksel = *((uint32_t *)(SOC_CM_WKUP_REG +
                                     PRCM_CM_CLKSEL_DPLL_MPU));
    uint32_t ctrlStats = CTRL_STS;
#elif defined (AM335X_FAMILY_BUILD)
    uint32_t clksel = *((uint32_t *)(SOC_CM_WKUP_REGS +
                                     CM_WKUP_CM_CLKSEL_DPLL_MPU));
    uint32_t ctrlStats = CONTROL_STATUS;
#endif
    dpllMult = (clksel >> 8) & 0x3FF;
    dpllDiv = clksel & 0x7f;
    uint32_t controlMod = CHIPDBBaseAddress(CHIPDB_MOD_ID_CONTROL_MODULE, 0u);
    unsigned osclk_freq = *((uint32_t *)(controlMod + ctrlStats));;
    osclk_freq = (osclk_freq & 0x00C00000) >> 22;

    /* Reference GetInputClockFrequency (GEL files) */
    if(osclk_freq == 0)
#ifdef AM43XX_FAMILY_BUILD
        clkin = 96;

#elif defined (AM335X_FAMILY_BUILD)
        clkin = 19;
#endif
    else if(osclk_freq == 1)
    {
        clkin = 24;
    }

    else if(osclk_freq == 2)
    {
        clkin = 25;
    }

    else if(osclk_freq == 3)
    {
        clkin = 26;
    }

    return ((dpllMult / (dpllDiv + 1)) * clkin);

}

char *Board_getChipRevision()
{
    //Return Device name - in String format
    uint32_t controlMod = CHIPDBBaseAddress(CHIPDB_MOD_ID_CONTROL_MODULE, 0u);
#ifdef AM43XX_FAMILY_BUILD
    uint32_t ctrlId = CTRL_DEVICE_ID;
#elif defined (AM335X_FAMILY_BUILD)
    uint32_t ctrlId = CONTROL_DEVICE_ID;
#endif
    uint32_t temp = *((uint32_t *)(controlMod + ctrlId));
    temp = (temp >> 28) & 0xF;

    switch(temp)
    {
#ifdef AM335X_FAMILY_BUILD

        case 0:
#endif
        case 1:
        case 2:
#ifdef AM335X_FAMILY_BUILD
            return chip_ver_name[temp];
#elif defined(AM43XX_FAMILY_BUILD)
            return chip_ver_name[temp - 1];
#endif

        default :
            return chip_ver_name[(sizeof(chip_ver_name) / sizeof(uint8_t *)) - 1];
    }
}
#endif /* #ifndef BOARD_SUPPORT_C_ */
