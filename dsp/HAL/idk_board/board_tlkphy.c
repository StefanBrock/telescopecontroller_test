/**
 * \file board_tlkphy.c
 * \brief Contains ICSS PHY (TLK) Specific APIs
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "stdlib.h"

#include "board_tlkphy.h"
#include "board_gpio.h"
#include "device.h"
#include "osdrv_osal.h"
#include "board.h"
#include "gpio.h"

/**Number of ports for which Workaround will be applied*/
#define NUM_PORTS 2

/**GPIO Pin configuration structure. Used for PHY reset*/
extern gpioPinObj_t GPIOPINOBJ_DEFAULT;
/**Task Handle for the TLK thread which swaps MDI/MDI-X bits for link*/
void *tlkTask;
/**
* @internal
* @brief API to Swap the MDIO/MDIX setting if Link is down and Phy is in forced mode
*
*       This API is used by TLK Task to do Software MDI/X. This Functin checks for Link status.Incase down,it checks for
*       Autonegotiation status. If the PHY is in forced mode, MDI/X configuration of the PHY is swapped until
*       link is detected
*
* @param mdioBaseAddress MDIO Base Address
* @param phyNum PHY Address of the Port
*
* @retval none
*/
void Board_tlkMDIXSwap(uint32_t mdioBaseAddress, uint32_t phyNum);
/**
* @internal
* @brief Main Task which implements MDI/MDI-X for the PHY in Software
*
*       This Task Does the software MDI/X for the TLK PHY. Once created the task  will constantly
*       Check for Link UP. Incase Link is down, the taks will check whether the PHY is in forced mode.
*       If in Forced mode, the AutoMDIX is disabled and  Software MDI/X is done. It will continuosuly swap
*       MDI/MDIX bit until the Link is established.
*
* @param a0     [IN] Argument passed (MDIO Base Address)
* @param a1     [IN] Argument passed (None)
*
*  @retval none
*/
void Board_tlkMDIXTask(UArg a0, UArg a1);
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

/**
* @internal
* @brief API to Swap the MDIO/MDIX setting if Link is down and Phy is in forced mode
*
*       This API is used by TLK Task to do Software MDI/X. This Functin checks for Link status.Incase down,it checks for
*       Autonegotiation status. If the PHY is in forced mode, MDI/X configuration of the PHY is swapped until
*       link is detected
*
* @param mdioBaseAddress MDIO Base Address
* @param phyNum PHY Address of the Port
*
* @retval none
*/
void Board_tlkMDIXSwap(uint32_t mdioBaseAddress, uint32_t phyNum)
{
    if(!MDIO_getPhyLinkStatus(mdioBaseAddress, phyNum))
    {
        if(!MDIO_getAutoNegStat(mdioBaseAddress, phyNum, NULL))
        {
            if(PHY_ENABLE_FORCE_MDIX == Board_getPhyMDIXStat(mdioBaseAddress, phyNum,
                    NULL))
            {
                Board_setPhyMDIX(mdioBaseAddress, phyNum, PHY_ENABLE_FORCE_MDI, NULL);
            }

            else if(PHY_ENABLE_FORCE_MDI == Board_getPhyMDIXStat(mdioBaseAddress, phyNum,
                    NULL))
            {
                Board_setPhyMDIX(mdioBaseAddress, phyNum, PHY_ENABLE_FORCE_MDIX, NULL);
            }

            else
            {
                Board_setPhyMDIX(mdioBaseAddress, phyNum, PHY_ENABLE_FORCE_MDI, NULL);
            }
        }
    }
}

/**
* @brief Fix for issue where the partner link change from 10 half to 100 was not detected by TLK PHY
*
*        The Link doesnot comeup in TLK PHY when the partner configuration is changed from 10 half to
*        100 full/hallf. The Fix is to write value 0x5668 to NLP register
*
*       MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] PHY Address of the Port
*
*  @retval none
*/
void Board_phyNLPFix(uint32_t mdioBaseAddress, uint32_t phyNum)
{
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_REGCR_REG,
                  EXT_REG_ADDRESS_ACCESS);
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_ADDR_REG, NLP_DET_REG);
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_REGCR_REG,
                  EXT_REG_DATA_NORMAL_ACCESS);
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_ADDR_REG,
                  NLP_DET_CONFIG_REGVAL);
}
/**
* @internal
* @brief Main Task which implements MDI/MDI-X for the TLK PHY in Software
*
*       This Task Does the software MDI/X for the TLK PHY. Once created the task  will constantly
*       Check for LInk UP. In case link is down, the task will check whether the PHY is in forced mode.
*       In this case the AutoMDIX is disabled and  Software MDI/X is done. It will continuosuly swap
*       MDI/MDIX bit until the Link is established.
*
*       The NLP Fix is also enabled in the task
*
* @param a0     [IN] Argument passed (MDIO Base Address)
* @param a1     [IN] Argument passed (None)
*
*  @retval none
*/
void Board_tlkMDIXTask(UArg a0, UArg a1)
{
    uint8_t phyNum[NUM_PORTS] = {0};
    uint32_t i = 0;
    uint32_t *mdioBaseAddress = (uint32_t *)a0;
    TaskOSAL_sleep(MDIX_FIX_TASKSLEEP_TICK);

    for(i = 0; i < NUM_PORTS; i++)
    {
        phyNum[i] = BOARDGetDeviceDataModId(DEVICE_ID_ENET_PHY_MII, i);
        Board_phyNLPFix(*mdioBaseAddress, phyNum[i]);
    }

    while(1)
    {
        for(i = 0; i < NUM_PORTS; i++)
        {
            Board_tlkMDIXSwap(*mdioBaseAddress, phyNum[i]);
            TaskOSAL_sleep(MDIX_FIX_TASKSLEEP_TICK);
        }
    }
}

/**
* @brief Initializes the workarounds for TLK issues. This API should be called to make the TLK PHY
*           work in Forced Mode.
*
*       This API implements the TLK specific workarounds.
*       1) AutoMDIX workaround
*        A task is created which will constantly check whether the PHY is in forced mode, In this case
*        the AutoMDIX is disabled and Software MDI/X is done.
*       2)NLP Fix
*        Fix for issue where the Link is not detected when the partner configuration is changed from 10 half to
*        100 full/Half
*
*        MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN] MDIO Base Address
*
*  @retval none
*/
void Board_phyMDIXFixInit(uint32_t mdioBaseAddress)
{

    uint8_t taskname[] = "tlkMDIXTask";
    uint32_t *baseAddress = (uint32_t *)malloc(sizeof(uint32_t));
    * baseAddress = mdioBaseAddress;
    tlkTask = TaskOSAL_create(3, taskname, 0x1000, baseAddress,
                              Board_tlkMDIXTask);
}
/**
* @brief Shutdown the TLK issue workaround
*
*       Deletes the TLK issue workaround task
*
*
*  @retval none
*/
void Board_phyMDIXFixDeInit()
{
    TaskOSAL_delete(&tlkTask);
}

/**
* @brief Function to disable AutoMDIX
*
*       MDIO init shall be done before using this function.The Semaphore handle need to be passed if the Thread safe MDIO read is used,
*       Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval none
*/
void Board_disablePhyAutoMDIX(uint32_t mdioBaseAddress, uint32_t phyNum,
                              MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregVal;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, &phyregVal,
                 mdioSemhandle);
    //clear the auto bit
    phyregVal &= ~(TLKPHY_AUTOMDIX_ENABLE);
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, phyregVal);
}
/**
* @brief Enable AutoMDIX for the particular Port
*
*       MDIO init shall be done before using this function.The Semaphore handle need to be passed if the Thread safe MDIO read is used,
*       Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval none
*/
void Board_enablePhyAutoMDIX(uint32_t mdioBaseAddress, uint32_t phyNum,
                             MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= TLKPHY_AUTOMDIX_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, phyregval);
}

/**
* @brief Function to get the MDI/X status of a particular PHY
*
*       MDIO init shall be done before using this function.The Semaphore handle need to be passed if the Thread safe MDIO read is used,
*       Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval PHY_ENABLE_AUTO_MDIX if PHY is in Auto MDIX
*          PHY_ENABLE_FORCE_MDIX if PHY is in Forced MDIX
*          PHY_ENABLE_FORCE_MDI if PHY is in Forced MDI
*/
uint8_t Board_getPhyMDIXStat(uint32_t mdioBaseAddress, uint32_t phyNum,
                             MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval;

    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, &phyregval,
                 mdioSemhandle);

    if(phyregval & TLKPHY_AUTOMDIX_ENABLE)
    {
        return PHY_ENABLE_AUTO_MDIX;
    }

    else if(phyregval & TLKPHY_FORCEMDIX_ENABLE)
    {
        return PHY_ENABLE_FORCE_MDIX;
    }

    else
    {
        return PHY_ENABLE_FORCE_MDI;
    }
}

/**
* @brief Function to Configure MDI/X Mode of PHY
*
*       API to enable MDI/MDIX or AutoMDIX mode
*       MDIO init shall be done before using this function.The Semaphore handle need to be passed if the Thread safe MDIO read is used,
*       Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdiState           [IN] MDI/MDIX mode to be set
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is usedp
*
* @retval none
*/
void Board_setPhyMDIX(uint32_t mdioBaseAddress, uint32_t phyNum,
                      uint8_t mdiState, MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval;

    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, &phyregval,
                 mdioSemhandle);

    switch(mdiState)
    {
        case PHY_ENABLE_FORCE_MDI:
            phyregval &= ~(TLKPHY_AUTOMDIX_ENABLE);
            phyregval &= ~(TLKPHY_FORCEMDIX_ENABLE);
            break;

        case PHY_ENABLE_FORCE_MDIX:
            phyregval &= ~(TLKPHY_AUTOMDIX_ENABLE);
            phyregval |= TLKPHY_FORCEMDIX_ENABLE;
            break;

        case PHY_ENABLE_AUTO_MDIX:
        default:
            phyregval |= TLKPHY_AUTOMDIX_ENABLE;
            break;
    }

    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, phyregval);
}

/**
* @brief Function to Enable Power Saving modes of the PHY
*
*       This function should be called before using the Power save modes of the PHY.
*       MDIO init shall be done before using this function.The Semaphore handle need to be passed if the Thread safe MDIO read is used,
*       Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyEnablePowerSaveMode(uint32_t mdioBaseAddress, uint32_t phyNum,
                                  MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval;

    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYSCR_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= TLKPHY_PSMODE_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_PHYSCR_REG, phyregval);
}

/**
* @brief Function to force Power Saving mode in PHY
*
*       Following modes are supported
*       PHY_POWERMODE_NORMAL        Normal opearion with PHY fully functional
*       PHY_POWERMODE_DOWN          Shuts down all internal circuitry except SMI functionality
*       PHY_POWERMODE_ACTIVE_SLEEP  Shuts down all internal circuitry except SMI and energy detect functionalities.
*                                   In this mode the PHY sends NLP every 1.4 Sec to wake up link-partner.
*                                   Automatic power-up is done when link partner is detected
*       PHY_POWERMODE_PASSIVE_SLEEP Shuts down all internal circuitry except SMI and energy detect functionalities.
*                                   Automatic power-up is done when link partner is detected
*
*       Power Save mode should be enabled to use the Power Saving feaures. MDIO init shall be done before using this function
*       The Semaphore handle need to be passed if the Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param phyPowerMode       [IN] Mode to be set
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_setPhyPowerSaveMode(uint32_t mdioBaseAddress, uint32_t phyNum,
                               uint8_t phyPowerMode, MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval;

    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYSCR_REG, &phyregval,
                 mdioSemhandle);

    switch(phyPowerMode)
    {
        case PHY_POWERMODE_NORMAL:
            phyregval &= (~(TLKPHY_PSMODE_BIT1) & ~(TLKPHY_PSMODE_BIT2));
            break;

        case PHY_POWERMODE_DOWN:
            phyregval &= ~(TLKPHY_PSMODE_BIT2);
            phyregval |= TLKPHY_PSMODE_BIT1;
            break;

        case PHY_POWERMODE_ACTIVE_SLEEP:
            phyregval &= ~(TLKPHY_PSMODE_BIT1);
            phyregval |= TLKPHY_PSMODE_BIT2;
            break;

        case PHY_POWERMODE_PASSIVE_SLEEP:
            phyregval |= (TLKPHY_PSMODE_BIT2 | TLKPHY_PSMODE_BIT1);

        default:
            phyregval &= (~(TLKPHY_PSMODE_BIT1) & ~(TLKPHY_PSMODE_BIT2));
            break;
    }

    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_PHYSCR_REG, phyregval);
}


/**
* @brief Function to Configure EthernetLED Blink rate
*
*       Function can be used to configure the PHY LED Blink rate.Valid values incase of TLK PHYs are
*       500ms,200ms,100ms and 50 ms. MDIO init shall be done before using this function.The Semaphore handle need to be passed
*       if the Thread safe MDIO read is used,Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param val                [IN] Supported rate mode
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyLedBlinkConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                             uint16_t val, MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_LEDCR_REG, &phyregval,
                 mdioSemhandle);

    switch(val)
    {
        case LED_BLINK_500:
            phyregval &= 0xF9FF;
            phyregval |= 0x0600;
            break;

        case LED_BLINK_200:
            phyregval &= 0xF9FF;
            phyregval |= 0x0400;
            break;

        case LED_BLINK_100:
            phyregval &= 0xF9FF;
            phyregval |= 0x0200;
            break;

        case LED_BLINK_50:
            phyregval &= 0xF9FF;
            break;
    }

    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_LEDCR_REG, phyregval);
}

/**
* @brief Function to enable Fast RXDV Detection
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyFastRXDVDetEnable(uint32_t mdioBaseAddress, uint32_t phyNum,
                                MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR1_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= FASTRXDV_DET_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR1_REG, phyregval);
}

/**
* @brief Function to enable Fast Link Down Detection
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyFastLinkDownDetEnable(uint32_t mdioBaseAddress, uint32_t phyNum,
                                    uint8_t val, MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR3_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= val;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR3_REG, phyregval);
}
/**
* @brief Function to enable Extended Full Duplex ability
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyExtFDEnable(uint32_t mdioBaseAddress, uint32_t phyNum,
                          MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= EXT_FD_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, phyregval);
}
/**
* @brief Function to enable Enhanced LED Link Functionality
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyEnhLEDLinkEnable(uint32_t mdioBaseAddress, uint32_t phyNum,
                               MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= ENH_LEDLINK_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, phyregval);
}
/**
* @brief Function to enable ODD Nibble detection
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void Board_phyODDNibbleDetEnable(uint32_t mdioBaseAddress, uint32_t phyNum,
                                 MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= ODDNIBBLE_DET_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, phyregval);
}
/**
* @brief Enables Detection of Receive Symbol Error During IDLE State
*
*       Function is used to enable Receive Symbol Error During IDLE State
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used

* @retval none
*/
void Board_phyRxErrIdleEnable(uint32_t mdioBaseAddress, uint32_t phyNum,
                              MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= RXERROR_IDLE_ENABLE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR2_REG, phyregval);
}
/**
* @brief Function to get the PHY Speed and duplexity
*
*       This API returns the Speed and Duplexity Configuration of the PHY
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval PHY_CONFIG_10FD 10 Mbps and Full duplex
*          PHY_CONFIG_10HD 10 Mbps and Half duplex
*          PHY_CONFIG_100FD 100 Mbps and Full duplex
*          PHY_CONFIG_100HD 100 Mbps and Half duplex
*/
uint8_t Board_getPhyConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                           MDIOSEM_Handle mdioSemhandle)
{
    uint16_t regStatus = 0;;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYSTS_REG, &regStatus,
                 mdioSemhandle);

    if(regStatus & TLK_SPEED_STATUS)    /*Speed is 10*/
    {
        if(regStatus & TLK_DUPLEX_STATUS)
        {
            return PHY_CONFIG_10FD;
        }

        else
        {
            return PHY_CONFIG_10HD;
        }
    }

    else/*Speed is 100*/
    {
        if(regStatus & TLK_DUPLEX_STATUS)
        {
            return PHY_CONFIG_100FD;
        }

        else
        {
            return PHY_CONFIG_100HD;
        }
    }
}

/**
*  @brief Routine to Reset PHYs
*
*       Function to reset the PHYs
*
*
*  @retval none
*
*/
void Board_phyReset()
{
    gpioPinObj_t gpioPhyReset = GPIOPINOBJ_DEFAULT;
    Board_initGPIO(DEVICE_ID_RESET_PHY, 6, &gpioPhyReset);
    GPIOPinWrite(gpioPhyReset.instAddr, gpioPhyReset.pinNum , 0);
    DelayOSAL_microSeconds(300);
    GPIOPinWrite(gpioPhyReset.instAddr, gpioPhyReset.pinNum , 1);
}
