/**
 * board_led.c
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_led.h"
#include "board_i2c.h"
#include "gpio.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/** \brief LED device instance number */
#define I2C_LED_INST_NUM         (0U)
/** \brief Number of GPIOs used for TRI color LED */
#define NO_OF_GPIO               (5U)
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int32_t Board_initDigOut(i2cCfgObj_t *pI2cObj)
{
    int32_t status = E_FAIL;
    uint32_t  i2cInstNum;
    uint32_t  slaveAddr;

    uint32_t  instAddr;

    status = Board_initPlatform(CHIPDB_MOD_ID_I2C, (uint32_t)DEVICE_ID_I2C_LED,
                                I2C_LED_INST_NUM,
                                &i2cInstNum, &slaveAddr, &instAddr);
    pI2cObj->i2cInstNum = i2cInstNum;
    pI2cObj->slaveAddr = slaveAddr;
    pI2cObj->i2cBaseAddr  = instAddr;

    /* Configure i2c bus speed*/
    i2cUtilsInitParams_t pUserParams;
    pUserParams.addrMode = pI2cObj->i2cDevCfg.addrMode;
    pUserParams.busSpeed = (pI2cObj->i2cDevCfg.busSpeed);
    pUserParams.operMode = pI2cObj->i2cDevCfg.opMode;
    pUserParams.rxThreshold = pI2cObj->i2cDevCfg.rxThreshold;
    pUserParams.txThreshold = pI2cObj->i2cDevCfg.txThreshold;
    status = I2CUtilsInit(pI2cObj->i2cInstNum, &pUserParams);

    return status;

}

int32_t Board_initTriColorLED(gpioPinObj_t *pGpioObj, uint32_t instance)
{
    int32_t status = E_FAIL;

    Board_initGPIO(DEVICE_ID_LED, instance, pGpioObj);
    status = S_PASS;
    return(status);

}

/*
 *  I2CSetLed - Sets multiple LEDs at a single shot
 *  led_val - led value to be applied
 */
void Board_setDigOut(i2cCfgObj_t *pI2cCtrlCfg, uint8_t led_val)
{
    uint8_t out_buff[2] = { 0x44 };
    out_buff[1] = led_val;

    i2cUtilsTxRxParams_t i2cObj;
    i2cObj.slaveAddr = pI2cCtrlCfg->slaveAddr;
    i2cObj.pOffset = (uint8_t *)&out_buff[0];
    i2cObj.offsetSize = 1;
    i2cObj.bufLen = 1;
    i2cObj.pBuffer = (uint8_t *)&out_buff[1];
    Board_i2cUtilsWrite(pI2cCtrlCfg->i2cInstNum, &i2cObj, 1000);
}


void Board_setTriColor(gpioPinObj_t *pGPIOObj, uint8_t val)
{
    GPIOPinWrite(pGPIOObj->instAddr, pGPIOObj->pinNum , val);
}
/* -------------------------------------------------------------------------- */
/*                 Internal Function Definitions                              */
/* -------------------------------------------------------------------------- */
