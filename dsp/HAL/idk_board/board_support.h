/**
 * @file    board_support.h
 *
 *
 */
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef BOARD_SUPPORT_H_
#define BOARD_SUPPORT_H_

#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Error.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/hal/Hwi.h>
#include <xdc/cfg/global.h>
#include "stdlib.h"

/* Starterware Headers */
#include "error.h"
#ifdef __cplusplus
extern "C" {
#endif

/** @def BOARD_DEFAULT
 *       DEFAULT value used to set other options
 */
#define BOARD_DEFAULT                           (0u)
/** @def BOARD_FLASH_MEMORY
 *       Macro for configuring Flash memory
 */
#define BOARD_FLASH_MEMORY                      (1u << BOARD_DEFAULT)
/** @def BOARD_LED_DIGOUT
 *       Macro for configuring Digital output
 */
#define BOARD_LED_DIGOUT                        (1u << (BOARD_DEFAULT + 1u))
/** @def BOARD_HVS_DIGIN
 *       Macro for configuring Digital input
 */
#define BOARD_HVS_DIGIN                         (1u << (BOARD_DEFAULT + 2u))
/** @def BOARD_ROTARY_SWITCH
 *       Macro for configuring Rotary switch
 */
#define BOARD_ROTARY_SWITCH                     (1u << (BOARD_DEFAULT + 3u))
/** @def BOARD_TRICOLOR0_RED
 *       Macro for configuring Tri color0 Red
 */
#define BOARD_TRICOLOR0_RED                     (1u << (BOARD_DEFAULT + 4u))
/** @def BOARD_TRICOLOR0_GREEN
 *       Macro for configuring Tri color0 green
 */
#define BOARD_TRICOLOR0_GREEN                   (1u << (BOARD_DEFAULT + 5u))
/** @def BOARD_TRICOLOR0_YELLOW
 *       Macro for configuring Tri color0 yellow
 */
#define BOARD_TRICOLOR0_YELLOW                  (1u << (BOARD_DEFAULT + 6u))
/** @def BOARD_TRICOLOR1_RED
 *       Macro for configuring Tri color1 red
 */
#define BOARD_TRICOLOR1_RED                     (1u << (BOARD_DEFAULT + 7u))
/** @def BOARD_TRICOLOR1_GREEN
 *       Macro for configuring Tri color1 green
 */
#define BOARD_TRICOLOR1_GREEN                   (1u << (BOARD_DEFAULT + 8u))
/** @def BOARD_TRICOLOR1_YELLOW
 *       Macro for configuring Tri color1 yellow
 */
#define BOARD_TRICOLOR1_YELLOW                  (1u << (BOARD_DEFAULT + 9u))
/** @def BOARD_LCD_DISPLAY
 *       Macro for configuring LCD Display
 */
#define BOARD_LCD_DISPLAY                       (1u << (BOARD_DEFAULT + 10u))

/** @def DEFAULT_TIMEOUT
 *       Default timeout for semaphore locks
 */
#define DEFAULT_TIMEOUT                         1000
/**********************************************************************
 ************************** Local Definitions *************************
 **********************************************************************/


/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/

/**********************************************************************
 ************************** Platform Functions ************************
 **********************************************************************/

/**
 *  @b Description
 *  @n
 *      The function is to init specified board modules
 *
 *  @param[in]  modules
 *      Modules that has to be init. Valid values BOARD_XX macros
 *
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t board_init(uint32_t modules);

/**
 *  @b Description
 *  @n
 *      The function is used to set the Digital output LEDs
 *
 *  @param[in]  ledVal
 *      Value to be written. 8-bit mapped to 8 DO LEDs
 *
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_setDigOutput(uint8_t ledVal);

/**
 *  @b Description
 *  @n
 *      The function is used to set the TRI Color LEDs
 *
 *  @param[in]  ledType
 *      LED Type to set/reset Eg: TRI_COLOR0_RED Note : Only one LED at a time
 *
 *  @param[in]  value
 *      0/1 - set/reset
 *
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_setTriColorLED(uint32_t ledType, uint8_t value);

/**
 *  @b Description
 *  @n
 *      The function is used to read the Digital inputs
 *
 *  @param[out]  value
 *      Value of the Digital Inputs
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_getDigInput(uint8_t *value);

/**
 *  @b Description
 *  @n
 *      This API reads permanant data from non-volatile storage.
 *
 *  @param[in]  offset
 *      Offset of Data to be read
 *  @param[in]  length
 *      length of data to be read
 *  @param[out]  buffer
 *      Data is written to this mem location
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_readFlashStorage(uint32_t offset, uint32_t length,
                              uint8_t *buffer);


/**
 *  @b Description
 *  @n
 *      This API writes permanant data to non-volatile storage.
 *
 *  @param[in]  offset
 *      Offset of Data to be written
 *  @param[in]  length
 *      length of data to be written
 *  @param[out]  buffer
 *      Data buffer to be written
 *  @param[in] eraseFlag
 *      If set to 1, this API will handling erasing the flash before writing
 *      If set to 0, application should handle this scenario
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_writeFlashStorage(uint32_t offset, uint32_t length,
                               uint8_t *buffer, uint8_t eraseFlag);

/**
 *  @b Description
 *  @n
 *      This API displays 'lcdString' on the LCD Display
 *
 *  @param[in]  lcdString
 *              String to be displayed
 *  @param      lineNumber
 *              The string to be displayed in line 1 or line 2(valid values 0 and 1)
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_setLCDString(uint8_t *lcdString, uint8_t lineNumber);

/**
 *  @b Description
 *  @n
 *      This API clears the LCD display
 *
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_clearLCDString();

/**
 *  @b Description
 *  @n
 *      This API  starts/stops LCD scrolling
 *
 *  @param  enable
 *      0 - Disable scrolling, 1 - enable scrolling
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_setLCDScroll(uint8_t enable);


/**
 *  @b Description
 *  @n
 *      This API reads Rotary switch position
 *
 *  @param[out]  position
 *      Rotary switch value
 *  @retval
 *      S_PASS on success, E_FAIL on failure
 */
int8_t Board_getRotarySwitchPosition(uint8_t *position);

/**
 *  @b Description
 *  @n
 *      Gets the Chip revision as per Errata info. Derived from DEVREV field in Device ID register
 *
 *  @retval
 *      return the version string
 */
char *Board_getChipRevision();

/**
 *  @b Description
 *  @n
 *      Gets ARM clock rate
 *
 *  @retval
 *      ARM frequency
 */
int Board_getArmClockRate();

#ifdef __cplusplus
}
#endif
#endif /* #ifndef BOARD_SUPPORT_H_ */
