/**
 * board_gpio.c
 *
*/

/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include "board_gpio.h"
#include "gpio.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/** \brief LED device instance number */
#define GPIO_LED_INST_NUM     (0U)
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
gpioPinObj_t GPIOPINOBJ_DEFAULT =
{
    0U,  /* pin number */
    0U,  /* Instance number */
    0U,  /* Instance Address */
    {
        GPIO_DIRECTION_OUTPUT, /* dir */
        FALSE,  /* debounceEnable */
        0U,     /* debounceTime */
        FALSE,  /* intrEnable */
        0U,     /* intrType */
        0U,     /* intrLine */
        FALSE,  /* wakeEnable */
        0U,     /* wakeLine */
    } /* gpioAppPinCfg_t */
};
/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */


/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

/* -------------------------------------------------------------------------- */
/*                 Internal Function Definitions                              */
/* -------------------------------------------------------------------------- */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */



/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
void Board_enableGPIOClock(uint8_t instance)
{
#ifdef AM43XX_FAMILY_BUILD

    /* TODO - cleanup*/
    if(instance & GPIO_INSTANCE_0)
    {

        HWREG(SOC_CM_WKUP_REG + PRCM_CM_WKUP_GPIO0_CLKCTRL) |= 0x2;
    }

    if(instance & GPIO_INSTANCE_1)
    {
        HWREG(SOC_CM_PER_REG + PRCM_CM_PER_GPIO1_CLKCTRL)      |=  0x2;
    }

    if(instance & GPIO_INSTANCE_2)
    {
        HWREG(SOC_CM_PER_REG + PRCM_CM_PER_GPIO2_CLKCTRL)      |=  0x2;
    }

    if(instance & GPIO_INSTANCE_3)
    {
        HWREG(SOC_CM_PER_REG + PRCM_CM_PER_GPIO3_CLKCTRL)      |=  0x2;
    }

    if(instance & GPIO_INSTANCE_4)
    {
        HWREG(SOC_CM_PER_REG + PRCM_CM_PER_GPIO4_CLKCTRL)      |=  0x2;
    }

    if(instance & GPIO_INSTANCE_5)
    {
        HWREG(SOC_CM_PER_REG + PRCM_CM_PER_GPIO5_CLKCTRL)      |=  0x2;
    }

#endif
}

void Board_initGPIO(uint32_t deviceID, uint32_t instance,
                    gpioPinObj_t *pGpioObj)
{
    uint32_t  gpioInstNum;
    uint32_t  pinNum;
    uint32_t  instAddr;
    Board_initPlatform(CHIPDB_MOD_ID_GPIO, deviceID, instance,
                       &gpioInstNum, &pinNum, &instAddr);
    pGpioObj->instNum = gpioInstNum;
    pGpioObj->pinNum  = pinNum;
    pGpioObj->instAddr = instAddr;

    /* Enabling the GPIO module. */
    GPIOModuleEnable(pGpioObj->instAddr, TRUE);

    /* GPIO pin characteristics configuration */
    Board_GPIOPinConfig(pGpioObj->instAddr, pGpioObj->pinNum, &pGpioObj->pinCfg);
}

void Board_GPIOPinConfig(uint32_t baseAddr,
                         uint32_t pinNum,
                         gpioPinCfg_t *pGpioPinConfig)
{
    /* Configure Pin as an Input/Output Pin. */
    GPIOSetDirMode(baseAddr, pinNum, pGpioPinConfig->dir);

    /* Enable Debouncing feature for the GPIO Pin. */
    GPIODebounceFuncEnable(baseAddr, pinNum, pGpioPinConfig->debounceEnable);

    if(TRUE == pGpioPinConfig->debounceEnable)
    {
        /*
         * Configure the Debouncing Time for all the input pins of
         * the selected GPIO instance.
         */
        GPIOSetDebounceTime(baseAddr, pGpioPinConfig->debounceTime);
    }

    if(FALSE == pGpioPinConfig->intrEnable)
    {
        /* Enable interrupt for the specified GPIO Pin. */
        GPIOIntrDisable(baseAddr, pGpioPinConfig->intrLine, pinNum);
    }

    else
    {
        /*
         * Configure interrupt generation on detection of a logic HIGH or
         * LOW levels or a rising or a falling edge.
         */
        GPIOSetIntrType(baseAddr, pinNum, pGpioPinConfig->intrType);

        /* Enable interrupt for the specified GPIO Pin. */
        GPIOIntrEnable(baseAddr, pGpioPinConfig->intrLine, pinNum);
    }

    if(FALSE == pGpioPinConfig->wakeEnable)
    {
        /* Enable wakeup generation for GPIO Module */
        GPIOGlobalWakeupDisable(baseAddr);

        /* Configure input GPIO Pin to wakeup */
        GPIOWakeupIntrDisable(baseAddr, pGpioPinConfig->intrLine, pinNum);
    }

    else
    {
        /* Configure input GPIO Pin to wakeup */
        GPIOWakeupIntrEnable(baseAddr, pGpioPinConfig->intrLine, pinNum);
    }
}
