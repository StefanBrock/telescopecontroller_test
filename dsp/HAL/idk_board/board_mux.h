/**
 * \file   board_i2c.h
 *
 *  \brief
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

#ifndef _BOARD_MUX_H
#define _BOARD_MUX_H

#ifdef __cplusplus
extern "C" {
#endif
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "types.h"
#include "chipdb_defs.h"
#include "pinmux.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/**
 * @brief Structure which holds the Pinmux configuration details
 *
 */
typedef struct _mux_config_test
{
    /** CHIPDB ID of the Module to be configured. Values available in chipdb_defs.h*/
    chipdbModuleID_t moduleid;
    /** Particular Instance number of the Module*/
    uint32_t instnum;
    /** Subdevice that needs to be configured. Assigning 0 will configure all the pins under the CHIPDB Module*/
    uint32_t subdevice;
} PINMUX_config;

/* ========================================================================== */
/*                            Global Variables Declarations                   */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */
/**
*
* @brief API that does Pinmux Configuration
*
*       This API does the Pinmux configuration as specified in the PINMUX_config structure.

*       Starterware Pinmux module is used by the API for configuration.Here the Pinmux tool is used to generate the Pinmux file
*       (separate file for Boards) which contains required pin configuration details. Here the user needs to pass the PINMUX_config structure
*       The user needs to pass the CHIPDB Id of the Module, Module instance number and the subdevice through the structure.These information
*       can be obtained from the following starterware files
*       Starterware_Root/board/SOCNAME/SOCNAME_BOARDNAME_pinmuxdata.c
*       Starterware_Root/include/SOCNAME/chipdb_defs.h
*
* @param muxConfig PinMux Configuration structure
*
* @retval none
*/
void Board_pinMuxConfig(PINMUX_config muxConfig[]);

#ifdef __cplusplus
}
#endif


#endif      //_BOARD_ROTARYSWITCH_H

