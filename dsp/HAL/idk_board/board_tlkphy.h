/**
 * \file board_tlkphy.h
 * \brief
 *
 *
*/

/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef _BOARD_TLKPHY_H_
#define _BOARD_TLKPHY_H_


#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include "board_phy.h"

#define MDIX_FIX_TASKSLEEP_TICK       2500

#define TLKPHY_PHYCR_REG                          (0x19)
#define TLKPHY_LEDCR_REG                          (0x18)
#define TLKPHY_PHYSCR_REG                         (0x11)
#define TLKPHY_PHYSTS_REG                         (0x10)


#define TLKPHY_CR1_REG                            (0x9)
#define TLKPHY_CR2_REG                            (0xA)
#define TLKPHY_CR3_REG                            (0xB)
#define TLK105_EXT_MLEDCR_REG                     (0x0025)
/**
* @def MII_PSMODE_ENABLE
*
*/
#define TLKPHY_PSMODE_ENABLE   (1<<14)
/**
* @def MII_PSMODE_BIT1
*
*/
#define TLKPHY_PSMODE_BIT1   (1<<12)
/**
* @def MII_PSMODE_BIT2
*
*/
#define TLKPHY_PSMODE_BIT2   (1<<13)

#define TLKPHY_AUTOMDIX_ENABLE                       (1u<<15)
#define TLKPHY_FORCEMDIX_ENABLE                      (1u<<14)


#define TLK_SPEED_STATUS                     (1<<1)
#define TLK_DUPLEX_STATUS                    (1<<2)

/**
* @def MII_PSMODE_ENABLE
*
*/
#define TLKPHY_PSMODE_ENABLE   (1<<14)
/**
* @def MII_PSMODE_BIT1
*
*/
#define TLKPHY_PSMODE_BIT1   (1<<12)
/**
* @def MII_PSMODE_BIT2
*
*/
#define TLKPHY_PSMODE_BIT2   (1<<13)


/**
* @def NLP_DET_REG
*      NLP Parallel Detect Configuration register
*/
#define NLP_DET_REG   0x0020
/**
* @def NLP_DET_CONFIG_REGVAL
*      NLP Parallel Detect Configuration Value
*/
#define NLP_DET_CONFIG_REGVAL  0x5668

/**
* @def EXT_REG_ADDRESS_ACCESS
*      Extended reg address access value
*/
#define EXT_REG_ADDRESS_ACCESS 0x001F
/**
* @def EXT_REG_DATA_NORMAL_ACCESS
*      Extended reg data access value
*/
#define EXT_REG_DATA_NORMAL_ACCESS 0x401F


/**
* @def TLKPHY_REGCR_REG
*      Value reg for accessing extended registers
*/
#define TLKPHY_REGCR_REG  0x000D
/**
* @def TLKPHY_ADDR_REG
*      ADDR reg for accessing extended registers
*/
#define TLKPHY_ADDR_REG   0x000E


#endif
