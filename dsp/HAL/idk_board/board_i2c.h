/**
 * \file   board_i2c.h
 *
 *  \brief
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

#ifndef _BOARD_I2C_H
#define _BOARD_I2C_H

#ifdef __cplusplus
extern "C" {
#endif
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "types.h"
#include "i2c_utils.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables Declarations                   */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */
/**
 *  \brief   API Calls I2CUtilsWrite. Added i2cLock for thread safety
 *
 *  \param   instId        Instance number of the I2C controller
 *  \param   pTxParams     pointer to a structure containing parameters
 *                         required for Write operation like
 *                         - Slave Address
 *                         - Data Count
 *  \param   timeoutValue  time-out value in milli-seconds for the Read/Write
 *                         operation to avoid Blocking on Read Write Call,
 *                         the value to be passed by the application.
 *
 *  \retval  S_PASS on success.
 *  \retval  E_TIMEOUT        when timeout occurs during Read operation.
 *  \retval  E_INST_NOT_SUPP  on invalid instance number.
 *  \retval  E_INST_NOT_UP    when the instance is not initialized.
 *  \retval  E_I2C_NAK_ERR    when master does not receive slave Acknowledge.
 *  \retval  E_BUF_OVERRUN    When the data to be written is more than what
 *                            the Transmit buffer can handle.
 *
 *  \note   The maximum data transfer length is 128 bytes. This is
 *          because the I2C data buffer has a maximum declared size
 *          of 128 bytes.
 */
int32_t Board_i2cUtilsWrite(uint32_t instId, i2cUtilsTxRxParams_t *pTxParams,
                            uint32_t timeoutValue);


#ifdef __cplusplus
}
#endif


#endif      //_BOARD_ROTARYSWITCH_H

