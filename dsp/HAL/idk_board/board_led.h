/**
 * board_led.h
 *
 *
*/

/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef BOARD_LED_H_
#define BOARD_LED_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_platform.h"
#include "board_gpio.h"
#include "i2c_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */
/* Red  */
#define TRI_COLOR0_RED      0
/* Green  */
#define TRI_COLOR0_GREEN    1
/* Yellow  */
#define TRI_COLOR0_YELLOW   2
/* Red  */
#define TRI_COLOR1_RED  3
/* Green - U49 */
#define TRI_COLOR1_GREEN    4
/* Yellow - U49 */
#define TRI_COLOR1_YELLOW   5

/** \brief Macro representing RED0 LED */
#define TRI_COLOR0_RED_BIT  (0x1 << TRI_COLOR0_RED)
/** \brief Macro representing GREEN0 LED */
#define TRI_COLOR0_GREEN_BIT    (0x1 << TRI_COLOR0_GREEN)
/** \brief Macro representing YELLOW0 LED */
#define TRI_COLOR0_YELLOW_BIT   (0x1 << TRI_COLOR0_YELLOW)
/** \brief Macro representing RED1 LED */
#define TRI_COLOR1_RED_BIT  (0x1 << TRI_COLOR1_RED)
/** \brief Macro representing GREEN1 LED */
#define TRI_COLOR1_GREEN_BIT    (0x1 << TRI_COLOR1_GREEN)
/** \brief Macro representing YELLOW1 LED */
#define TRI_COLOR1_YELLOW_BIT   (0x1 << TRI_COLOR1_YELLOW)
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/** \brief Structure holding i2c data transfer parameters. */
typedef struct i2cTxRxParams
{
    uint32_t            baseAddr;
    /**< i2c instance  base address. */
    uint32_t            slaveAddr;
    /**< i2c device slave address */
    uint32_t            bufLen;
    /**< Length of the buffer passed */
    uint8_t             *pBuf;
    /**< pointer to a buffer to hold the data */
    uint32_t            txThreshold;
    /**< Transmit FIFO threshold configuration. */
    uint32_t            rxThreshold;
    /**< Receive FIFO threshold configuration. */
    int32_t             statusFlag;
    /**< Status flag to indicate the Transfer Status */
} i2cTxRxParams_t;

/**
 *  \brief Enumerates the types of Operational modes supported
 *         for I2C Operations.
 */
typedef enum i2cOpMode
{
    I2C_OPMODE_MIN =  0U,
    /**< Min Operational mode type of the I2C device */
    I2C_OPMODE_POLLING = I2C_OPMODE_MIN,
    /**< Polling mode of the I2C device */
    I2C_OPMODE_INTERRUPT,
    /**< Interrupt mode of I2C device */
    I2C_OPMODE_DMA,
    /**< DMA mode of I2C device */
    I2C_OPMODE_MAX = I2C_OPMODE_DMA,
    /**< Max Operational mode type of the I2C device */
} i2cOpMode_t;

/**
 *  @brief I2C intr config
 */
typedef struct i2cIntrCfgObj
{
    intcTrigType_t intrTrigType;
    /**< Type of interrupt Edge/Level. */
    uint32_t intrLine;
    /**< Interrupt line number. */
    uint32_t intrPriority;
    /**< Interrupt priority level. */
    uint32_t isIntrSecure;
    /**< Secure Interrupt or not */
    void (*pFnIntrHandler)(uint32_t intrId, uint32_t cpuId, void *pUserParam);
    /**< Function pointer to Interrupt handler which needs to be
     *  registered with the interrupt Controller.- Application needs
     *  to initialize this. If set to null then polling Mode is assumed
     */
} i2cIntrCfgObj_t;

/** \brief Structure holding I2C device configuration parameters. */
typedef struct i2cDevCfg
{
    uint32_t            busSpeed;
    /**< I2C Bus Speed in kHz. */
    i2cAddrMode_t       addrMode;
    /**< Addressing mode to be configured for the device. */
    i2cOpMode_t      opMode;
    /**< Operational mode to be configured for the device. */
    uint32_t            txThreshold;
    /**< Transmit FIFO threshold configuration. */
    uint32_t            rxThreshold;
    /**< Receive FIFO threshold configuration. */
} i2cDevCfg_t;

/** \brief Structure holding I2C device Object parameters. */
typedef struct i2cCfgObj
{
    uint32_t       i2cInstNum;
    /**< i2c instance number. */
    uint32_t       i2cBaseAddr;
    /**< i2c instance base address. */
    uint32_t       slaveAddr;
    /**< Slave Address of the Part to be accessed */
    i2cDevCfg_t i2cDevCfg;
    /**< I2C device configuration params structure */
    i2cIntrCfgObj_t i2cIntrCfg;
    /**< I2C interrupt Configuration Structure */
} i2cCfgObj_t;

int32_t Board_initDigOut(i2cCfgObj_t *pI2cObj);
void Board_setDigOut(i2cCfgObj_t *pI2cCtrlCfg, uint8_t led_val);
int32_t Board_initTriColorLED(gpioPinObj_t *pGpioObj, uint32_t instance);
void Board_setTriColor(gpioPinObj_t *pGPIOObj, uint8_t val);

#endif /* BOARD_LED_H_ */
