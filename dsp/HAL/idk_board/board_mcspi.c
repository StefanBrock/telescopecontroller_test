/**
 * board_mcspi.c
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_mcspi.h"
#include "board_gpio.h"
#include "gpio.h"
#include "hw_types.h"
#include "mcspi.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/** \brief LED device instance number */
#define MCSPI_HVS_INST_NUM     (0U)

#define SPI_INSTANCE_COUNT  2

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
volatile static unsigned int spiInitCount[SPI_INSTANCE_COUNT] = {0};

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */
/**
 * \brief Initalizes the GPIO required for HVS882
 *
 * \param None
 *
 *
 * \return
 *
 **/
int32_t Board_initHVSGPIO();

/**
 * \brief Steps done in a cycle of McSPI Read/Write
 *
 * \param
 *
 *
 * \return
 *
 **/
//void MCSPICycle(mcspiCfgObj_t *pCfgMcspi, uint32_t len);
void Board_MCSPICycle(mcspiCfgObj_t *pCfgMcspi, uint8_t *buff, uint32_t len);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
static gpioPinObj_t gGpioObj;

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int32_t Board_initMCSPI(mcspiCfgObj_t *pMcspiObj)
{
    int32_t status = S_PASS;
    //boardType =BOARD_ICEV2;
    spiInitCount[pMcspiObj->instNum]++;

    if(spiInitCount[pMcspiObj->instNum] == 1)
    {
        /* Setup SPI peripheral */
        /* Enable the clocks for McSPI1 module.*/
        MCSPIClkConfig(pMcspiObj->instAddr,
                       pMcspiObj->channelNum,
                       pMcspiObj->inClk,
                       pMcspiObj->outClk,
                       pMcspiObj->pCfg.clkMode);

        /* Reset the McSPI instance.*/
        MCSPIReset(pMcspiObj->instAddr);
        /* Enable chip select pin.*/
        MCSPICsEnable(pMcspiObj->instAddr, TRUE);

    }

    /* Enable master mode of operation.*/
    MCSPIModeConfig(pMcspiObj->instAddr,
                    pMcspiObj->channelNum,
                    pMcspiObj->pFlag.modeFlag,
                    pMcspiObj->pCfg.channel,
                    pMcspiObj->pCfg.txRxMode,
                    pMcspiObj->pCfg.pinMode);

    /*
    ** Default granularity is used. Also as per my understanding clock mode
    ** 0 is proper.
    */
    MCSPIClkConfig(pMcspiObj->instAddr,
                   pMcspiObj->channelNum,
                   pMcspiObj->inClk,
                   pMcspiObj->outClk,
                   pMcspiObj->pCfg.clkMode);
    /* Configure the word length.*/
    MCSPISetWordLength(pMcspiObj->instAddr,
                       pMcspiObj->channelNum,
                       pMcspiObj->pCfg.wordLength);

    /* Set polarity of SPIEN to low.*/
    MCSPISetCsPol(pMcspiObj->instAddr,
                  pMcspiObj->channelNum,
                  pMcspiObj->pCfg.csPolarity);

    MCSPIRxFifoEnable(pMcspiObj->instAddr,
                      pMcspiObj->channelNum,
                      pMcspiObj->pCfg.rxFifoCfg);
    return status;
}

int32_t Board_initHVSDigIn(mcspiCfgObj_t *pMcspiObj)
{
    int32_t status = S_PASS;
    uint32_t  instAddr;
    uint32_t mcspiInstNum;
    uint32_t mcspiCs;

    status = Board_initPlatform(CHIPDB_MOD_ID_MCSPI, (uint32_t)DEVICE_ID_HVS882,
                                MCSPI_HVS_INST_NUM,
                                &mcspiInstNum, &mcspiCs, &instAddr);

    pMcspiObj->instNum = mcspiInstNum;
    pMcspiObj->channelNum  = mcspiCs;
    pMcspiObj->instAddr = instAddr;

    Board_initHVSGPIO();

    GPIOSetDirMode(gGpioObj.instAddr, gGpioObj.pinNum, GPIO_DIRECTION_OUTPUT);
    GPIOPinWrite(gGpioObj.instAddr, gGpioObj.pinNum, 1);

    Board_initMCSPI(pMcspiObj);

    return status;
}




void Board_getHVSDigIn(mcspiCfgObj_t *pCfgMcspi)
{
    uint8_t spiHvsBuf;
    GPIOPinWrite(gGpioObj.instAddr, gGpioObj.pinNum, 0);
    GPIOPinWrite(gGpioObj.instAddr, gGpioObj.pinNum, 1);

    pCfgMcspi->dataLength = 1;
    //*(pCfgMcspi->pTx) = 0;
    spiHvsBuf = 0;
    Board_MCSPICycle(pCfgMcspi, &spiHvsBuf, pCfgMcspi->dataLength);
    pCfgMcspi->pRx[0] = spiHvsBuf;

}

void Board_readMCSPI(mcspiCfgObj_t *pCfgMcspi, uint8_t *buff, uint32_t len)
{
    Board_MCSPICycle(pCfgMcspi, buff, len);
}

void Board_writeMCSPI(mcspiCfgObj_t *pCfgMcspi,  uint8_t *buff, uint32_t len)
{
    Board_MCSPICycle(pCfgMcspi, buff, len);
}
/* -------------------------------------------------------------------------- */
/*                 Internal Function Definitions                              */
/* -------------------------------------------------------------------------- */
int32_t Board_initHVSGPIO()
{
    int32_t status = E_FAIL;
    uint32_t  gpioInstNum;
    uint32_t  pinNum;
    uint32_t  instAddr;
    //gpioPinObj_t *pGpioObj = (gpioPinObj_t *)malloc(sizeof(pGpioObj));
    status = Board_initPlatform(CHIPDB_MOD_ID_GPIO, (uint32_t)DEVICE_ID_HVS882,
                                1,
                                &gpioInstNum, &pinNum, &instAddr);
    gGpioObj.instNum = gpioInstNum;
    gGpioObj.pinNum  = pinNum;
    gGpioObj.instAddr = instAddr;

    /* Enabling the GPIO module. */
    GPIOModuleEnable(gGpioObj.instAddr, TRUE);

    /* GPIO pin characteristics configuration */
    Board_GPIOPinConfig(gGpioObj.instAddr, gGpioObj.pinNum, &gGpioObj.pinCfg);

    //free(pGpioObj);
    status = S_PASS;
    return(status);

}

void Board_MCSPICycle(mcspiCfgObj_t *pCfgMcspi, uint8_t *buff, uint32_t len)
{
    //unsigned int length = 1;
    volatile int loop;
    uint32_t itr = 0;
    /* Enable the MCSPI channel for communication.*/
    MCSPIChEnable(pCfgMcspi->instAddr, pCfgMcspi->channelNum, TRUE);

    /* SPIEN line is forced to low state.*/
    MCSPICsAssert(pCfgMcspi->instAddr, pCfgMcspi->channelNum);

    //while(length) {
    for(itr = 0; itr < len; itr++)
    {
        loop = 1000;

        /* Wait for transmit empty */
        while(((MCSPIChStatus(pCfgMcspi->instAddr,
                              pCfgMcspi->channelNum) & MCSPI_CHSTAT_TXS_MASK) == 0)
                && (loop--));    //check if TX buffer is empty

        if(loop == 0)
        {
            return;
        }

        /* Program the data to be transmitted. */
        MCSPITransmitData(pCfgMcspi->instAddr,
                          pCfgMcspi->channelNum,
                          buff[itr]);

        loop = 1000;
        // check if RX buffer

        while(((MCSPIChStatus(pCfgMcspi->instAddr,
                              pCfgMcspi->channelNum) & MCSPI_CHSTAT_RXS_MASK) == 0)
                && (loop--));    // check if RX buffer is full

        if(loop == 0)
        {
            return;
        }

        //rx_data_hvs = (unsigned char)(MCSPIReceiveData(pCfgMcspi->instAddr, pCfgMcspi->channelNum) & 0xff);
        //(pCfgMcspi->pRx[itr]) = (uint8_t)(MCSPIReceiveData(pCfgMcspi->instAddr, pCfgMcspi->channelNum) & 0xff);
        buff[itr] = (uint8_t)(MCSPIReceiveData(pCfgMcspi->instAddr,
                                               pCfgMcspi->channelNum) & 0xff);
    }

    /* Force SPIEN line to the inactive state.*/
    MCSPICsDeAssert(pCfgMcspi->instAddr, pCfgMcspi->channelNum);
    /* Disable the McSPI channel.*/
    MCSPIChEnable(pCfgMcspi->instAddr, pCfgMcspi->channelNum, FALSE);
}
