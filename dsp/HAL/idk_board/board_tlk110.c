/**
 * \file board_tlk110.c
 * \brief Contains ICSS PHY (TLK 110) Specific APIs
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_tlkphy.h"
#include "device.h"

/**
* @brief Function to do S/w Strap configuration of TLK110
*
* @param mdioBaseAddress MDIO Base Address
* @param phyNum Phy address of the port
* @param mdioSemhandle Semaphore handle if thread safe MDIO access is used

* @retval none
*/
void Board_phySwStrapConfigDone(uint32_t mdioBaseAddress, uint32_t phyNum,
                                MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_CR1_REG, &phyregval,
                 mdioSemhandle);
    phyregval |= SWSTRAP_CONFIG_DONE;
    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_CR1_REG, phyregval);
}

/**
* @brief Function does the  LED Configuration of TLK 110
*
* @param mdioBaseAddress MDIO Base Address
* @param phyNum Phy address of the port
* @param mode Led Config mode
* @param mdioSemhandle Semaphore handle if thread safe MDIO access is used

* @retval none
*/
void Board_phyLedConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                        uint8_t mode, MDIOSEM_Handle mdioSemhandle)
{
    uint16_t phyregval = 0;
    MDIO_regRead(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, &phyregval,
                 mdioSemhandle);

    switch(mode)
    {
        case LED_CFG_MODE1:
            phyregval |= 0x0020; //Set LED_CFG[0] to 1
            break;

        case LED_CFG_MODE2:
            phyregval &= 0xFFCF; //Set LED_CFG[0],LED_CFG[1] to 0
            break;

        case LED_CFG_MODE3:
            phyregval &= 0xFFDF; //Set LED_CFG[0] to 0
            phyregval |= 0x0040; //Set LED_CFG[1] to 1
            break;
    }

    MDIO_regWrite(mdioBaseAddress, phyNum, TLKPHY_PHYCR_REG, phyregval);
}


