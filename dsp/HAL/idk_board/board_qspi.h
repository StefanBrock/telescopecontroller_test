/**
 * \file   board_qspi.h
 *
 *  \brief
 *
*/

/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

#ifndef BOARD_QSPI_H_
#define BOARD_QSPI_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_platform.h"
#include "qspi_flash.h"
#include "qspi_lib.h"


#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/** \brief QSPI device instance number */
#define QSPI_DEV_INST_NUM    (0U)

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**< /brief Structure holdig the QSPI instance related data for nor flash */
typedef struct qspiObj
{
    uint32_t chipSelect;
    /**< QSPI chip select number */
    uint32_t instNum;
    /**< QSPI instance number */
    uint32_t instAddr;
    /**< QSPI instance address */
    uint32_t devId;
    /**< QSPI device ID*/
    uint32_t length;
    /**< Length of data to be transferred */
    qspiLibInfo_t qspiLibInfo;
    /**< configurable parameters for the qspi flash device and controller */
} qspiObj_t;

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 * \brief   QSPI IP configuration initialization api.
 *
 * \param  devInstNum Device instance number to get the information for. Used when multiple devices are present on a board.
 *
 * \retval S_PASS App init successful
 * \retval E_FAIL App init failed
 */
int32_t Board_initQSPI(qspiObj_t *pQspiObj);



#ifdef __cplusplus
}
#endif

#endif /* #ifndef BOARD_QSPI_H_ */

