/**
 * board_gpio.h
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef BOARD_GPIO_H_
#define BOARD_GPIO_H_
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#ifdef AM43XX_FAMILY_BUILD
    #include "am437x.h"
#elif defined AM335X_FAMILY_BUILD
    #include "soc_am335x.h"
#endif
#include "types.h"
#include "hw_types.h"
#include "hw_cm_per.h"
#include "hw_cm_wkup.h"
#include "board_platform.h"

/* ========================================================================== */
/*                             Macros                                         */
/* ========================================================================== */


#define GPIO_INSTANCE_0     0x1
#define GPIO_INSTANCE_1     0x2
#define GPIO_INSTANCE_2     0x4
#define GPIO_INSTANCE_3     0x8
#define GPIO_INSTANCE_4     0x10
#define GPIO_INSTANCE_5     0x20
#define GPIO_INSTANCE_ALL   0xFF
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/** \brief Structure holding gpio pin configuration parameters. */
typedef struct gpioPinCfg
{
    uint32_t dir;
    /**< pin direction . */
    uint32_t debounceEnable;
    /**< Enable/disable debounce feature. */
    uint32_t debounceTime;
    /**< deboune timing control. */
    uint32_t intrEnable;
    /**< Interrupt enable/disable. */
    uint32_t intrType;
    /**< Interrupt type edge or level . */
    uint32_t intrLine;
    /**< Interrupt line number. */
    uint32_t wakeEnable;
    /**< Wake up enable/disable control. */
    uint32_t wakeLine;
    /**< Wakeup line number. */
} gpioPinCfg_t;

/** @brief Structure holding the GPIO pin object data structure. */
typedef struct gpioPinObj
{
    uint32_t     pinNum;
    /**< GPIO pin number. */
    uint32_t     instNum;
    /**< GPIO instance number. */
    uint32_t     instAddr;
    /**< GPIO instance address. */
    gpioPinCfg_t pinCfg;
    /**< GPIO pin configuration structure.*/
} gpioPinObj_t;

/**
 * \brief   This API implements initializes the GPIO module
 *
 * \param   deviceID        Device ID of the GPIO instance(ex : DEVICE_ID_LED)
 * \param   instance        Instance number in the device list
 * \param   pGpioObj        Holds the configuratoin of GPIO object
 */
void Board_initGPIO(uint32_t deviceID, uint32_t instance,
                    gpioPinObj_t *pGpioObj);

/**
 * \brief   This API implements counter based delay.
 *
 * \param   baseAddr        Base address of GPIO instance used.
 * \param   pinNum          GPIO pin number
 * \param   pGpioPinConfig  pointer to the pin configuration data structure.
 */
void Board_GPIOPinConfig(uint32_t baseAddr,
                         uint32_t pinNum,
                         gpioPinCfg_t *pGpioPinConfig);

void Board_enableGPIOClock(uint8_t instance);
#endif /* OSDRV_GPIO_H_ */
