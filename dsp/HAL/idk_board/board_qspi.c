/**
 * board_qspi.c
 *
*/

/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_qspi.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
int32_t Board_initQSPI(qspiObj_t *pQspiObj)
{
    int32_t status = S_PASS;

    /* Start Board info */
    /* Check which QSPI device is present on this board */
    uint32_t *qspiFlashDeviceIdList;
    uint32_t devIndex = 0U;
    uint32_t qspiInstNum;
    uint32_t chipSelect;
    uint32_t instAddr;
    qspiFlashDeviceIdList = QSPIFlashGetDeviceIdList();

    do
    {
        if(TRUE == BOARDIsDevicePresent(qspiFlashDeviceIdList[devIndex]))
        {
            /* QSPI Flash device found */
            pQspiObj->devId = qspiFlashDeviceIdList[devIndex];
            break;
        }

        devIndex++;
    }
    while(DEVICE_ID_INVALID != qspiFlashDeviceIdList[devIndex]);

    if(DEVICE_ID_INVALID == qspiFlashDeviceIdList[devIndex])
    {
        //CONSOLEUtilsPrintf("No QSPI device found \n");
    }
    else
    {
        status = Board_initPlatform(CHIPDB_MOD_ID_QSPI, pQspiObj->devId,
                                    QSPI_DEV_INST_NUM,
                                    &qspiInstNum, &chipSelect, &instAddr);
        pQspiObj->instNum = qspiInstNum;
        pQspiObj->chipSelect  = chipSelect;
        pQspiObj->instAddr = instAddr;
    }

    return status;
}

/* -------------------------------------------------------------------------- */
/*                 Internal Function Definitions                              */
/* -------------------------------------------------------------------------- */

