/*
 * pwms.h
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * Copyright (c) 2016-2017 Dariusz Janiszewski
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */


/* ==================================================================================
File name:        pwms.h

Description: Header file containing data type and object definitions and
			 initializers. Also contains prototypes for the functions in pwms.C.

Target: AM437x

====================================================================================*/


#ifndef __Drv_PWM_H__
#define __Drv_PWM_H__

#include <xdc/std.h>
																 
/*-----------------------------------------------------------------------------
Define the structure of the PWM Driver Object 
-----------------------------------------------------------------------------*/
typedef struct {   
        Int16 PeriodMax;     // Parameter: PWM Half-Period in CPU clock cycles (Q0)
        Int16 MfuncPeriod;    // Input: Period scaler (Q15) 
        Int16 MfuncC1;        // Input: EPWM1 A&B Duty cycle ratio (Q15)
        Int16 MfuncC2;        // Input: EPWM2 A&B Duty cycle ratio (Q15) 
        Int16 MfuncC3;        // Input: EPWM3 A&B Duty cycle ratio (Q15)
		Int16 PWM1out;
		Int16 PWM2out;
		Int16 PWM3out;
        void (*init)();       // Pointer to the init function 
        void (*update)();     // Pointer to the update function 
        } PWMGEN ;    

/*-----------------------------------------------------------------------------
Define a PWMGEN_handle
-----------------------------------------------------------------------------*/
typedef PWMGEN *PWMGEN_handle;

/*------------------------------------------------------------------------------
Default Initializers for the DRV PWMGEN Object 
------------------------------------------------------------------------------*/
#define Drv_FC_PWM_GEN    {1000,   \
                              0x1, \
                              0x4000, \
                              0x4000, \
                              0x4000, \
							  0x4000, \
							  0x4000, \
							  0x4000, \
                             (void (*)(Uint32))Drv_PWM012_Init,  \
                             (void (*)(Uint32))Drv_PWM012_Update \
                             }

#define PWMGEN_DEFAULTS 	Drv_FC_PWM_GEN
/*------------------------------------------------------------------------------
 Prototypes for the functions
------------------------------------------------------------------------------*/
void Drv_PWM345_Init(PWMGEN_handle);
void Drv_PWM012_Init(PWMGEN_handle);
void Drv_PWM345_Update(PWMGEN_handle);
void Drv_PWM012_Update(PWMGEN_handle);

void DRV8313init(void);

#endif  // __Drv_PWM_H__

