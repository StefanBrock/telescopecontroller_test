/*
 * adc.h
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

/**
 *  /File  adc.h
 *
 ============================================================================ */

#ifndef ADC_H_
#define ADC_H_

//define if one step measurements (pair of one variable for ADC0/1)
#define MINIMAL_ADC_CONFIG

/* Include files */
#include "types.h"
#include "hw_types.h"
#include "chipdb.h"
#include "chipdb_defs.h"
#include "hw_tsc_adc_ss.h"
#include "hw_cm_wkup.h"
#include "stdio.h"

#define ADC_SIMULTANEOUS_SAMPLING


 /* 1.8V is ADC Maximum voltage
  * ADC is 12 bit resolution
  * 1.8/4096 = 0.000439
  *
  *  */
#define ADC_MULTIPLIER    0.000439f

 /* 4 wire mode enable */
#define TSC_ADC_SS_CTRL_AFE_PEN_CTRL_4WIRE		0x01

/* Touch screen X Positive and Negative input channel No */
#define TSC_ADC_SS_STEPCONFIG1_SEL_INP_SWC_X	0x00000000
#define TSC_ADC_SS_STEPCONFIG1_SEL_INM_SWM_X	0x00000001

/* Touch screen Y Positive and Negative input channel No */
#define TSC_ADC_SS_STEPCONFIG2_SEL_INP_SWC_Y	0x00000000
#define TSC_ADC_SS_STEPCONFIG2_SEL_INM_SWM_Y	0x00000001

/* Positive Reference selection of X */
#define TSC_ADC_SS_STEPCONFIG1_SEL_RFP_SWC_X   0x00000000

/* Negative Reference selection of X */
#define TSC_ADC_SS_STEPCONFIG1_SEL_RFM_SWC_X   0x00000000

/* Positive Reference selection of Y */
#define TSC_ADC_SS_STEPCONFIG2_SEL_RFP_SWC_Y   0x00000000

/* Negative Reference selection of Y */
#define TSC_ADC_SS_STEPCONFIG2_SEL_RFM_SWC_Y   0x00000000

#define TSC_ADC_SS_STEPCONFIG1_DIFF_CNTRL_ENA	0x1
#define TSC_ADC_SS_STEPCONFIG2_DIFF_CNTRL_ENA	0x1

/* Driver function prototypes */

/* Initialization */
 signed short ADCInitialization (void);

/* Read ADC Data from the controller */
signed short ADCDataRead (unsigned short *data);

#endif /* ADC_H_ */
