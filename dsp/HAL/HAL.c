

#include "HAL.h"

int32_t PINMUXPerConfig(uint32_t ctrlModBase, pinmuxPerCfg_t* pInstanceData, uint16_t* pParam1);


// global variables

//volatile struct biss_position acc_position; //actual position for BISS interfeace tests

//volatile uint8_t hvs_keyboard = 0x00; //external SPI keyboard
//volatile uint8_t hvs_ledbar = 0x00;	//external I2C LED bar

#define LCDbuffer_size 80
volatile char LCDbuffer_temp[LCDbuffer_size];
volatile char (*pLCDbuffer_temp)[LCDbuffer_size];


PINMUX_config am437xIdkMux[] =
{
    {CHIPDB_MOD_ID_GPIO, 0, 14}, /*GPIO 0 14 */
    {CHIPDB_MOD_ID_GPIO, 0, 15}, /*GPIO 0 15 */
    {CHIPDB_MOD_ID_GPIO, 0, 12}, /*GPIO 0 12 */
    {CHIPDB_MOD_ID_GPIO, 0, 13}, /*GPIO 0 13 */
    {CHIPDB_MOD_ID_GPIO, 0, 30}, /*GPIO 0 30 */
    {CHIPDB_MOD_ID_GPIO, 0, 26}, /*GPIO 0 26 */
    {CHIPDB_MOD_ID_GPIO, 4, 8}, /*GPIO 4 8 */
    {CHIPDB_MOD_ID_GPIO, 4, 10}, /*GPIO 4 10 */
    //{CHIPDB_MOD_ID_PWMSS, 0, 0}, /* */ //DJ why PWMSS0?
    //{CHIPDB_MOD_ID_PWMSS, 3, 0}, /* */
    //{CHIPDB_MOD_ID_PWMSS, 4, 0}, /* */
    //{CHIPDB_MOD_ID_PWMSS, 5, 0}, /* */
    {CHIPDB_MOD_ID_GPIO, 4, 27}, /*GPIO 4 27 */
    {CHIPDB_MOD_ID_GPIO, 5, 3}, /*GPIO 5 3 */
    {CHIPDB_MOD_ID_GPIO, 5, 1}, /*GPIO 5 1 */
    {CHIPDB_MOD_ID_ADC0, 0, 0}, /*ext_hw_trigger */
    {CHIPDB_MOD_ID_GPIO, 5, 12},/*GPIO 5 12 */ //print_mnb GPIO_5_12/EnDAT nRX

    {CHIPDB_MOD_ID_I2C, 2, 0}, //DJ I2C
    {CHIPDB_MOD_ID_MCSPI, 1, 0}, //DJ SPI
    {CHIPDB_MOD_ID_GPIO, 3, 1}, //GPIO3_1 --> industrial inputs load
    {0xFFFFFFFF, 0, 0}
};


//from PINmux cloud
//SPIO0
static pinmuxPerCfg_t gMcspi0PinCfg[] =
{
    {
       /* MySPI1 -> spi0_sclk -> P23 */
       PIN_SPI0_SCLK, 0, \
       ( \
           PIN_MODE(0) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MySPI1 -> spi0_d0 -> T22 */
       PIN_SPI0_D0, 0, \
       ( \
           PIN_MODE(0) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MySPI1 -> spi0_d1 -> T21 */
       PIN_SPI0_D1, 0, \
       ( \
           PIN_MODE(0) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
	{
	       /* MySPI2 -> spi0_cs0 -> T20 */
	       PIN_SPI0_CS0, 0, \
	       ( \
	           PIN_MODE(0) | \
	           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
	           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
	       ) \
	    },
    {PINMUX_INVALID_PIN}
};

static pinmuxPerCfg_t gdjGpio4PinCfg[] =
{
    {
       /* MyGPIO4 -> gpio4[17] -> AE22 */
       PIN_CAM1_DATA3, 17, \
       ( \
           PIN_MODE(7) | \
           ((PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_PULL_UD_DIS & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyGPIO4 -> gpio4[19] -> AE23 */
       PIN_CAM1_DATA5, 19, \
       ( \
           PIN_MODE(7) | \
           ((PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_PULL_UD_DIS & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyGPIO4 -> gpio4[21] -> AE24 */
       PIN_CAM1_DATA7, 21, \
       ( \
           PIN_MODE(7) | \
           ((PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_PULL_UD_DIS & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {PINMUX_INVALID_PIN}
};


//from pinmux

static pinmuxPerCfg_t gPwmss0PinCfg[] =
{
    {
       /* MyeHRPWM0 -> ehrpwm0A -> AD25 */
       PIN_CAM1_HD, (uint16_t)PINMUX_SS_PWMSS_EHRPWM0, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM0 -> ehrpwm0B -> AC23 */
       PIN_CAM1_VD, (uint16_t)PINMUX_SS_PWMSS_EHRPWM0, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM0 -> ehrpwm0_tripzone_input -> P24 */
       PIN_SPI4_D1, (uint16_t)PINMUX_SS_PWMSS_EHRPWM0, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM0 -> ehrpwm0_synci -> P25 */
       PIN_SPI4_SCLK, (uint16_t)PINMUX_SS_PWMSS_EHRPWM0, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM0 -> ehrpwm0_synco -> AE18 */
       PIN_CAM0_DATA0, (uint16_t)PINMUX_SS_PWMSS_EHRPWM0, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {PINMUX_INVALID_PIN}
};

static pinmuxPerCfg_t gPwmss1PinCfg[] =
{
    {
       /* MyeHRPWM1 -> ehrpwm1A -> AE20 */
       PIN_CAM0_DATA6, (uint16_t)PINMUX_SS_PWMSS_EHRPWM1, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM1 -> ehrpwm1B -> AD20 */
       PIN_CAM0_DATA7, (uint16_t)PINMUX_SS_PWMSS_EHRPWM1, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM1 -> ehrpwm1_tripzone_input -> P20 */
       PIN_SPI2_D1, (uint16_t)PINMUX_SS_PWMSS_EHRPWM1, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {PINMUX_INVALID_PIN}
};

static pinmuxPerCfg_t gPwmss2PinCfg[] =
{
    {
       /* MyeHRPWM2 -> ehrpwm2A -> B10 */
       PIN_GPMC_AD8, (uint16_t)PINMUX_SS_PWMSS_EHRPWM2, \
       ( \
           PIN_MODE(4) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM2 -> ehrpwm2B -> A10 */
       PIN_GPMC_AD9, (uint16_t)PINMUX_SS_PWMSS_EHRPWM2, \
       ( \
           PIN_MODE(4) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_RX_ACTIVE & ~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {
       /* MyeHRPWM2 -> ehrpwm2_tripzone_input -> T23 */
       PIN_SPI2_CS0, (uint16_t)PINMUX_SS_PWMSS_EHRPWM2, \
       ( \
           PIN_MODE(6) | \
           ((PIN_PULL_UD_DIS | PIN_PULL_UP_EN | PIN_RX_ACTIVE | PIN_DS_VALUE_OVERRIDE_EN | PIN_DS_OP_DIS | PIN_DS_PULL_UP_EN) & \
           (~PIN_DS_OP_VAL_1 & ~PIN_DS_PULL_UD_EN & ~PIN_WAKE_UP_EN))
       ) \
    },
    {PINMUX_INVALID_PIN}
};

static pinmuxModuleCfg_t gPwmssPinCfg[] =
{
    {0, TRUE, gPwmss0PinCfg},
    {1, TRUE, gPwmss1PinCfg},
    {2, TRUE, gPwmss2PinCfg},
    {CHIPDB_INVALID_INSTANCE_NUM}
};

void pin_mux_init(void) //Original from /opt/ti/pdk_am437x_1_0_5/packages/ti/board/src/idkAM437x
{
    Board_pinMuxConfig(am437xIdkMux);
}


void gpio4_init(void)
{
    PRCMModuleEnable(CHIPDB_MOD_ID_GPIO, 4, FALSE);

	PINMUXPerConfig(SOC_CONTROL_MODULE_REG, gdjGpio4PinCfg,  (uint16_t*) 17);
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG, gdjGpio4PinCfg, 19);
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG, gdjGpio4PinCfg, 21);

    GPIOSetDirMode(SOC_GPIO4_REG, 17, GPIO_DIRECTION_INPUT); //busy BISS interface
    GPIOSetIntrType(SOC_GPIO4_REG,17,GPIO_INTR_MASK_FALL_EDGE); //falling edge after BISS conversion
    GPIOIntrEnable(SOC_GPIO4_REG,0u,17); //interrupt line 0


    GPIOSetDirMode(SOC_GPIO4_REG, 19, GPIO_DIRECTION_OUTPUT); ///< GPIO4_19 - free pin connected to CPLD
    GPIOPinWrite(SOC_GPIO4_REG, 19, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO4_REG, 19, GPIO_PIN_LOW);

    GPIOSetDirMode(SOC_GPIO4_REG, 21, GPIO_DIRECTION_OUTPUT); //SPI: 1 - BISS, 0 - DRV8301
}

void pwmss012init(void)
{
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG,gPwmss0PinCfg,0);
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG,gPwmss1PinCfg,0);
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG,gPwmss2PinCfg,0);
}

void spi0_biss_init(void)
{
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG, gMcspi0PinCfg, NULL);

	gSPI0Obj = (mcspiCfgObj_t *)malloc(sizeof(mcspiCfgObj_t));
	*gSPI0Obj = MCSPI_DEFAULT;
	gSPI0Obj->instAddr=SOC_MCSPI0_REG; //SOC_MCSPI0_REG
    gSPI0Obj->instNum = 0;
    gSPI0Obj->outClk=8000000U; //DJ maximum frequency without data mistake (latch the position in CPLD)
    gSPI0Obj->channelNum  = 0; //CS0
	gSPI0Obj->pCfg.wordLength = 32U; //32bits of data

	PRCMModuleEnable(CHIPDB_MOD_ID_MCSPI, gSPI0Obj->instNum, 0u);

	Board_initMCSPI((mcspiCfgObj_t *)gSPI0Obj);
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_HIGH); //SPI: 1 - BISS, 0 - DRV8301

    gSPI0Obj->dataLength = 6;
}

void spi0_drv_init(void)
{
	PINMUXPerConfig(SOC_CONTROL_MODULE_REG, gMcspi0PinCfg, NULL);

	gSPI0Obj = (mcspiCfgObj_t *)malloc(sizeof(mcspiCfgObj_t));
	*gSPI0Obj = MCSPI_DEFAULT;
	gSPI0Obj->instAddr=SOC_MCSPI0_REG; //SOC_MCSPI0_REG
    gSPI0Obj->instNum = 0;
    gSPI0Obj->outClk=5000000U;
    gSPI0Obj->channelNum  = 0; //CS0
	gSPI0Obj->pCfg.wordLength = 16U; //16bits of data
	gSPI0Obj->pCfg.clkMode=MCSPI_CLK_MODE_1;

	PRCMModuleEnable(CHIPDB_MOD_ID_MCSPI, gSPI0Obj->instNum, 0u);

	Board_initMCSPI((mcspiCfgObj_t *)gSPI0Obj);

    gSPI0Obj->dataLength = 1;


    uint16_t read_drv_status = 0;

    		DRV8301_cntrl_reg1.bit.GATE_CURRENT = 0;		// full current 1.7A
//			DRV8301_cntrl_reg1.bit.GATE_CURRENT = 1;		// med current 0.7A
//			DRV8301_cntrl_reg1.bit.GATE_CURRENT = 2;		// min current 0.25A
    		DRV8301_cntrl_reg1.bit.GATE_RESET = 0;			// Normal Mode
    		DRV8301_cntrl_reg1.bit.PWM_MODE = 0;			// 0 - six independant PWMs
//    		DRV8301_cntrl_reg1.bit.PWM_MODE = 1;			// 1 - 3PWM mode
//			DRV8301_cntrl_reg1.bit.OC_MODE = 0;				// current limiting when OC detected
    		DRV8301_cntrl_reg1.bit.OC_MODE = 1;				// latched OC shutdown
//			DRV8301_cntrl_reg1.bit.OC_MODE = 2;				// Report on OCTWn pin and SPI reg only, no shut-down
//			DRV8301_cntrl_reg1.bit.OC_MODE = 3;				// OC protection disabled
			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 0;			// OC @ Vds=0.060V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 4;			// OC @ Vds=0.097V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 6;			// OC @ Vds=0.123V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 9;			// OC @ Vds=0.175V
//    		DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 15;			// OC @ Vds=0.358V //originally
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 16;			// OC @ Vds=0.403V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 17;			// OC @ Vds=0.454V
//			DRV8301_cntrl_reg1.bit.OC_ADJ_SET = 18;			// OC @ Vds=0.511V
    		DRV8301_cntrl_reg1.bit.Reserved = 0;

//			DRV8301_cntrl_reg2.bit.OCTW_SET = 0;			// report OT and OC
    		DRV8301_cntrl_reg2.bit.OCTW_SET = 1;			// report OT only
    		DRV8301_cntrl_reg2.bit.GAIN = 0;				// CS amplifier gain = 10
//			DRV8301_cntrl_reg2.bit.GAIN = 1;				// CS amplifier gain = 20
//			DRV8301_cntrl_reg2.bit.GAIN = 2;				// CS amplifier gain = 40
//			DRV8301_cntrl_reg2.bit.GAIN = 3;				// CS amplifier gain = 80
    		DRV8301_cntrl_reg2.bit.DC_CAL_CH1 = 0;			// not in CS calibrate mode
    		DRV8301_cntrl_reg2.bit.DC_CAL_CH2 = 0;			// not in CS calibrate mode
    		DRV8301_cntrl_reg2.bit.OC_TOFF = 0;				// normal mode
    		DRV8301_cntrl_reg2.bit.Reserved = 0;

    		union DRV8301_SPI_WRITE_WORD_REG w;

    		w.bit.R_W = 0;							//we are initiating a write
    		w.bit.ADDRESS = CNTRL_REG_1_ADDR;				//load the address
    		w.bit.DATA = DRV8301_cntrl_reg1.all;						//data to be written;

    		//*(uint32_t *)gSPI0Obj->pRx = w.all;

    	    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_LOW); //SPI: 1 - BISS, 0 - DRV8301
    		Board_MCSPICycle16(gSPI0Obj, &w.all, 1u);
    	    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_HIGH); //SPI: 1 - BISS, 0 - DRV8301


//            w.bit.R_W = 0;                          //we are initiating a write
//            w.bit.ADDRESS = CNTRL_REG_1_ADDR;               //load the address
//            w.bit.DATA = DRV8301_cntrl_reg1.all;                        //data to be written;
//
//            GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_LOW); //SPI: 1 - BISS, 0 - DRV8301
//            Board_MCSPICycle16(gSPI0Obj, &w.all, 1u);
//            GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_HIGH); //SPI: 1 - BISS, 0 - DRV8301


    	    spi0_biss_init();
 }

///address register 0x00-0x03
unsigned short spi0_drv_read_controlregister(unsigned short address_register)
{

    gSPI0Obj = (mcspiCfgObj_t *) malloc(sizeof(mcspiCfgObj_t));
    *gSPI0Obj = MCSPI_DEFAULT;
    gSPI0Obj->instAddr = SOC_MCSPI0_REG; //SOC_MCSPI0_REG
    gSPI0Obj->instNum = 0;
    gSPI0Obj->outClk = 5000000U;
    gSPI0Obj->channelNum = 0; //CS0
    gSPI0Obj->pCfg.wordLength = 16U; //16bits of data
    gSPI0Obj->pCfg.clkMode = MCSPI_CLK_MODE_1;

    PRCMModuleEnable(CHIPDB_MOD_ID_MCSPI, gSPI0Obj->instNum, 0u);

    Board_initMCSPI((mcspiCfgObj_t *) gSPI0Obj);

    gSPI0Obj->dataLength = 1;

    union DRV8301_SPI_WRITE_WORD_REG w;

    //valid output data should be valid in second polling
    w.bit.R_W = 1;                          //we are initiating a read
    w.bit.ADDRESS = address_register;               //load the address
    w.bit.DATA = 0x0000;                        //any data to be written
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_LOW); //SPI: 1 - BISS, 0 - DRV8301
    Board_MCSPICycle16(gSPI0Obj, &w.all, gSPI0Obj->dataLength);
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_HIGH); //SPI: 1 - BISS, 0 - DRV8301


    w.bit.R_W = 1;                          //we are initiating a read
    w.bit.ADDRESS = address_register;               //load the address
    w.bit.DATA = 0x0000;                        //any data to be written
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_LOW); //SPI: 1 - BISS, 0 - DRV8301
    Board_MCSPICycle16(gSPI0Obj, &w.all, gSPI0Obj->dataLength);
    GPIOPinWrite(SOC_GPIO4_REG, 21, GPIO_PIN_HIGH); //SPI: 1 - BISS, 0 - DRV8301

    spi0_biss_init(); //return to BISS SPI parameters

    return (unsigned short) w.all;
}

void i2c_lcd_init(void)
{
	//LCDbuffer
	pLCDbuffer_temp = malloc(sizeof(char) * LCDbuffer_size);

	//LCDscreen sw/hw init
	gLCDscreen = (i2cCfgObj_t *) malloc(sizeof(i2cCfgObj_t));
	*gLCDscreen = I2C_CFG_DEFAULT;
	Board_initLCDscreen((i2cCfgObj_t *)gLCDscreen);
	Board_setLCDscreenInit((i2cCfgObj_t *)gLCDscreen);
	sprintf((char*) pLCDbuffer_temp,"LCD init...");
	//LCD 16x4 handled
	Board_setLCDscreenHome((i2cCfgObj_t *)gLCDscreen);
	Board_setLCDscreenPrintf((i2cCfgObj_t *)gLCDscreen,(uint8_t*) pLCDbuffer_temp);
}


void Board_MCSPICycle32(mcspiCfgObj_t *pCfgMcspi, uint32_t *buff, uint32_t len)
{
    //unsigned int length = 1;
    volatile int loop;
    uint32_t itr = 0;
    /* Enable the MCSPI channel for communication.*/
    MCSPIChEnable(pCfgMcspi->instAddr, pCfgMcspi->channelNum, TRUE);
    /* SPIEN line is forced to low state.*/
    MCSPICsAssert(pCfgMcspi->instAddr, pCfgMcspi->channelNum);
    //while(length) {
    for(itr = 0; itr < len; itr++)
    {
        loop = 1000;

        /* Wait for transmit empty */
        while(((MCSPIChStatus(pCfgMcspi->instAddr,
                              pCfgMcspi->channelNum) & MCSPI_CHSTAT_TXS_MASK) == 0)
                && (loop--));    //check if TX buffer is empty

        if(loop == 0)
        {
            return;
        }

        /* Program the data to be transmitted. */
        MCSPITransmitData(pCfgMcspi->instAddr,
                          pCfgMcspi->channelNum,
                          buff[itr]);

        loop = 1000;
        // check if RX buffer

        while(((MCSPIChStatus(pCfgMcspi->instAddr,
                              pCfgMcspi->channelNum) & MCSPI_CHSTAT_RXS_MASK) == 0)
                && (loop--));    // check if RX buffer is full

        if(loop == 0)
        {
            return;
        }
        //DelayOSAL_microSeconds(5);

//        int iiii;
//        for(iiii=0;iiii<1000;iiii++)
//            {
//            ;
//            }

        //rx_data_hvs = (unsigned char)(MCSPIReceiveData(pCfgMcspi->instAddr, pCfgMcspi->channelNum) & 0xff);
        //(pCfgMcspi->pRx[itr]) = (uint8_t)(MCSPIReceiveData(pCfgMcspi->instAddr, pCfgMcspi->channelNum) & 0xff);
        buff[itr] = (uint32_t)(MCSPIReceiveData(pCfgMcspi->instAddr,
                                               pCfgMcspi->channelNum));
    }

    /* Force SPIEN line to the inactive state.*/
    MCSPICsDeAssert(pCfgMcspi->instAddr, pCfgMcspi->channelNum);
    /* Disable the McSPI channel.*/
    MCSPIChEnable(pCfgMcspi->instAddr, pCfgMcspi->channelNum, FALSE);
}

void Board_MCSPICycle16(mcspiCfgObj_t *pCfgMcspi, uint16_t *buff, uint32_t len)
{
    //unsigned int length = 1;
    volatile int loop;
    uint32_t itr = 0;
    /* Enable the MCSPI channel for communication.*/
    MCSPIChEnable(pCfgMcspi->instAddr, pCfgMcspi->channelNum, TRUE);

    /* SPIEN line is forced to low state.*/
    MCSPICsAssert(pCfgMcspi->instAddr, pCfgMcspi->channelNum);

    //while(length) {
    for(itr = 0; itr < len; itr++)
    {
        loop = 1000;

        /* Wait for transmit empty */
        while(((MCSPIChStatus(pCfgMcspi->instAddr,
                              pCfgMcspi->channelNum) & MCSPI_CHSTAT_TXS_MASK) == 0)
                && (loop--));    //check if TX buffer is empty

        if(loop == 0)
        {
            return;
        }

        /* Program the data to be transmitted. */
        MCSPITransmitData(pCfgMcspi->instAddr,
                          pCfgMcspi->channelNum,
                          (uint32_t)buff[itr]);

        loop = 1000;
        // check if RX buffer

        while(((MCSPIChStatus(pCfgMcspi->instAddr,
                              pCfgMcspi->channelNum) & MCSPI_CHSTAT_RXS_MASK) == 0)
                && (loop--));    // check if RX buffer is full

        if(loop == 0)
        {
            return;
        }

        //rx_data_hvs = (unsigned char)(MCSPIReceiveData(pCfgMcspi->instAddr, pCfgMcspi->channelNum) & 0xff);
        //(pCfgMcspi->pRx[itr]) = (uint8_t)(MCSPIReceiveData(pCfgMcspi->instAddr, pCfgMcspi->channelNum) & 0xff);
        buff[itr] = (uint16_t)(MCSPIReceiveData(pCfgMcspi->instAddr,
                                               pCfgMcspi->channelNum));
    }

    /* Force SPIEN line to the inactive state.*/
    MCSPICsDeAssert(pCfgMcspi->instAddr, pCfgMcspi->channelNum);
    /* Disable the McSPI channel.*/
    MCSPIChEnable(pCfgMcspi->instAddr, pCfgMcspi->channelNum, FALSE);
}

int32_t PINMUXPerConfig(uint32_t ctrlModBase, pinmuxPerCfg_t* pInstanceData, uint16_t* pParam1)
{
    int32_t status = E_FAIL;
    uint32_t index = 0;

	for(index = 0; ((uint16_t)PINMUX_INVALID_PIN !=
			pInstanceData[index].pinOffset); index++)
	{
		if(NULL != pParam1)
		{
			if(pInstanceData[index].optParam == (uint16_t*)pParam1)
			{
				HW_WR_REG32((ctrlModBase + pInstanceData[index].pinOffset),
						pInstanceData[index].pinSettings);
				status = S_PASS;
				break;
			}
		}
		else
		{
			HW_WR_REG32((ctrlModBase + pInstanceData[index].pinOffset),
					pInstanceData[index].pinSettings);
		}
	}
	if((NULL != pParam1) && ((uint16_t)PINMUX_INVALID_PIN == pInstanceData[index].pinOffset))
	{
		status = E_FAIL;
	}
	return status;
}
