/**
 * osdrv_pruss.h
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef _OSDRV_PRUSS_H
#define _OSDRV_PRUSS_H

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/BIOS.h>
#include "pruicss.h"
#include "pruicss_v1.h"
#include "chipdb_defs.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/** @brief PRU 0 firmware binary file location in SPI flash - at an offset
   of 480 KB from starting location */
#define SPI_APPL_BIN_OFFSET     0x20000             /* Verify from Starterware Bootloader */

#define QSPI_APPL_BIN_OFFSET    0x80000             /* Verify from Starterware Bootloader */
/** @brief PRU 0 firmware binary file location in SPI flash - at an offset
   of 480 KB from starting location */
#define SPI_PRU0_BINARY_OFFSET      (SPI_APPL_BIN_OFFSET + 0x78000)
/** @brief PRU 1 firmware binary file location in SPI flash - at an offset
   of 9KB from PRU0 binary location */
#define SPI_PRU1_BINARY_OFFSET      (SPI_PRU0_BINARY_OFFSET + 0x4000)

/** @brief Magic number used in application with PRU firmware */
#define MAGIC_NUM_PRU_BIN           0xEE3366AA
/** @brief Magic number used in application   */
#define MAGIC_NUM_APP_BIN           0xEE3355BB

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  @brief PRU Firmware binary header
 *
 */
typedef struct _pru_firmware_bin_header_
{
    //uint32_t magic_number;
    /**< Magic number used to identify binary */
    uint32_t image_size;
    /**< Image size */
    uint32_t load_addr;
    /**< Address to which this binary need to be loaded */
    uint32_t run_addr;
    /**< Address at which the execution should start */
} PRU_FIRMWARE_BIN_HEADER;


/* ========================================================================== */
/*                            Global Variables Declarations                   */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 * @brief   Clock and PRM enable. Populate the memory map holder
 *
 * @param   PRU-ICSS LLD configuration
 *
 * \return  E_FAIL on failure, S_PASS on success
 */

int32_t PRUSSDRVInit(PRUICSS_Handle handle);
#ifdef __cplusplus
}
#endif
#endif  //_OSDRV_PRUSS_H

