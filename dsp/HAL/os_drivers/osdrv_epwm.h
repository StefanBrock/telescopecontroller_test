/**
 * osdrv_epwm.h
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef OSDRV_EPWM_H_
#define OSDRV_EPWM_H_

#include "types.h"
#include "hw_types.h"
#ifdef AM43XX_FAMILY_BUILD
    #include "am437x.h"
    #include "hw_control_am43xx.h"
#elif defined AM335X_FAMILY_BUILD
    #include "soc_am335x.h"
#endif
#include "hw_cm_per.h"

#define PWM_CLK_CNTRL_ENABLE_BIT  0x2
#define PWMSS0_TBCLKEN_ENABLE_BIT 1
#define PWMSS1_TBCLKEN_ENABLE_BIT 1<<1
#define PWMSS2_TBCLKEN_ENABLE_BIT 1<<2
#define PWMSS3_TBCLKEN_ENABLE_BIT 1<<4
#define PWMSS4_TBCLKEN_ENABLE_BIT 1<<5
#define PWMSS5_TBCLKEN_ENABLE_BIT 1<<6
#define PWM3_SYNCI_PIN_ENABLE_BIT 1<<3

void PowerPWM(void);

#endif /* OSDRV_EPWM_H_ */
