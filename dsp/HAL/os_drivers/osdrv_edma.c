/**
 * osdrv_edma.c
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <stdlib.h>
#include "osdrv_edma.h"

#ifdef AM43XX_FAMILY_BUILD
    #include "am437x.h"
#elif defined AM335X_FAMILY_BUILD
    #include "soc_am335x.h"
#endif


/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/** @brief Retry count to process EDMA handler. */
#define EDMA_DRV_HANDLER_RETRY_COUNT                            (10U)


/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */
static int32_t EdmaDrvGetSocInfo(uint32_t instNum, edmaDrvObj_t *pEdmaObj);

static int32_t EDMADrvModuleInit(uint32_t instNum);

static int32_t EDMADrvCtrlInit(uint32_t instNum, edmaDrvObj_t *pEdmaObj);

static int32_t EDMADrvIntrConfigure(uint32_t instNum, edmaDrvObj_t *pEdmaObj);

/**
 *  @brief   A Interrupt Service routine which is invoked when the EDMA completion
 *           interrupt is raised on any channel. The ISR invokes the callback
 *           function assigned to the channel.
 *
 *  @param   userParam  parameter to be passed to the ISR.
 */
static void EdmaDrvChCompletionIsr(UArg pUserParam);

/**
 *  @brief   A Interrupt Service routine which is invoked when the EDMA error
 *           interrupt is raised on any channel. The ISR invokes the callback
 *           function assigned to the channel.
 *
 *  @param   intrNum    interrupt number to which the interrupt event is mapped.
 *  @param   cpuId      id of the CPU to which interrupt is raised.
 *  @param   userParam  parameter to be passed to the ISR.
 */
static void EdmaDrvChErrorIsr(UArg UserParam);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/** @brief EDMA default data configuration */
static const EDMAParamDataConfig_t EDMA_DATA_DEFAULT =
{
    DMA_XFER_DATA_ADDR_MODE_INC,            /* addrMode */
    DMA_XFER_DATA_FIFO_WIDTH_MIN,              /* fifoWidth */
    {
        0U,                                 /* addr */
        0U,                                 /* bCntIdx */
        0U,                                 /* cCntIdx */
    }, /* EDMAParamDataAddrOff_t */
    {
        0U,                                 /* aCnt */
        0U,                                 /* bCnt */
        0U,                                 /* cCnt */
        0U,                                 /* bCntRld */
    }, /* EDMAParamDataSize_t */
    EDMA_PARAM_SYNC_TYPE_A                  /* syncType */
}; /* EDMAParamDataConfig_t */

/** @brief EDMA default configuration */
static const EDMAParamConfig_t EDMA_XFER_DEFAULT =
{
    NULL,                                   /* pSrc */
    NULL,                                   /* pDst */
    EDMA_PARAM_PRIV_LVL_USER,               /* privType */
    0U,                                     /* privId */
    FALSE,                                  /* enableLink */
    FALSE,                                  /* enableStatic */
    0xFFFFU,                                /* linkAddr */
    FALSE,                                  /* enableChain */
    EDMA_PARAM_XFER_TRIGGER_MASK_NONE,      /* chainMask */
    EDMA_PARAM_TCC_MODE_NORMAL,             /* tccMode */
    0U,                                     /* tcc */
    EDMA_PARAM_TCC_MODE_NORMAL              /* intrMask */
}; /* EDMAParamConfig_t */

/** @brief EDMA default configuration */
static const EDMAChConfig_t EDMA_CH_DEFAULT =
{
    0U,                                     /* regionId */
    0U,                                     /* paRamEntry */
    0U,                                     /* queueNum */
    FALSE,                                  /* enableEvt */
    FALSE                                   /* enableIntr */
}; /* EDMAChConfig_t */
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int32_t EDMADrvInit(uint32_t instNum, edmaDrvObj_t *pEdmaObj)

{
    int32_t retStat = S_PASS;

    /* Update SoC info */
    retStat = EdmaDrvGetSocInfo(instNum, pEdmaObj);

    if(S_PASS == retStat)
    {
        /* EDMA clock configuration */
        retStat = EDMADrvModuleInit(instNum);

        if(S_PASS == retStat)
        {
            pEdmaObj->maxIntrCh = EDMAGetNumIntrCh(pEdmaObj->baseChCtrlAddr);
            pEdmaObj->maxQueue = EDMAGetNumQueues(pEdmaObj->baseChCtrlAddr);
            retStat = EDMADrvCtrlInit(instNum, pEdmaObj);

        }
    }

    return retStat;
}

int32_t EDMADrvDataXferReset(uint32_t xferIdx, edmaDrvObj_t *pEdmaObj)
{
    int32_t retStat = S_PASS;

    EDMAParamReset(pEdmaObj->baseChCtrlAddr, xferIdx);

    return retStat;
}

int32_t EDMADrvDataXferConfig(uint32_t xferIdx,
                              dmaDrvXferObj_t *pDmaXferConfig,
                              edmaDrvObj_t *pEdmaObj)
{
    int32_t retStat = E_FAIL;
    EDMAParamDataConfig_t srcData = EDMA_DATA_DEFAULT;
    EDMAParamDataConfig_t dstData = EDMA_DATA_DEFAULT;
    EDMAParamConfig_t xferData = EDMA_XFER_DEFAULT;

    if(NULL != pDmaXferConfig)
    {
        if(NULL != pDmaXferConfig->pSrc)
        {
            if(NULL != pDmaXferConfig->pDst)
            {
                retStat = S_PASS;
            }
        }
    }

    if(S_PASS == retStat)
    {
        /* Configure interrupt mask. */
        if(0U != (pDmaXferConfig->intrConfig & DMA_DRV_DATA_SYNC_MASK_BLOCK))
        {
            xferData.intrMask |= EDMA_PARAM_XFER_TRIGGER_MASK_COMPLETE;
            xferData.tcc = pDmaXferConfig->xferIdx;
        }

        if((0U != (pDmaXferConfig->intrConfig & DMA_DRV_DATA_SYNC_MASK_PACKET)) &&
                (EDMA_PARAM_SYNC_TYPE_A == srcData.syncType))
        {
            xferData.intrMask |= EDMA_PARAM_XFER_TRIGGER_MASK_INTERMEDIATE;
        }

        else if((0U != (pDmaXferConfig->intrConfig & DMA_DRV_DATA_SYNC_MASK_FRAME))
                &&
                (EDMA_PARAM_SYNC_TYPE_AB == srcData.syncType))
        {
            xferData.intrMask |= EDMA_PARAM_XFER_TRIGGER_MASK_INTERMEDIATE;
        }

        else
        {
        }
    }

    if(S_PASS == retStat)
    {
        if(TRUE == pDmaXferConfig->linkEnable)
        {
            xferData.linkAddr = pDmaXferConfig->nxtXferIdx;
            xferData.enableLink = TRUE;
        }

        else
        {
            xferData.linkAddr = 0xFFFF;
            xferData.enableLink = FALSE;
        }
    }

    if(S_PASS == retStat)
    {
        srcData.addrOff.addr = pDmaXferConfig->pSrc->addr;
        srcData.fifoWidth = pDmaXferConfig->pSrc->fifoWidth;

        if((0U == pDmaXferConfig->pSrc->packetActSize) ||
                (0U == pDmaXferConfig->pSrc->frameActSize) ||
                (0U == pDmaXferConfig->pSrc->blockSize))
        {
            retStat = E_FAIL;
        }

        else
        {
            srcData.size.aCnt = pDmaXferConfig->pSrc->packetActSize;
            srcData.size.bCnt = pDmaXferConfig->pSrc->frameActSize;
            srcData.size.cCnt = pDmaXferConfig->pSrc->blockSize;
        }

        if(S_PASS == retStat)
        {
            if(DMA_XFER_DATA_ADDR_MODE_INC == pDmaXferConfig->pSrc->addrMode)
            {
                srcData.addrMode = DMA_XFER_DATA_ADDR_MODE_INC;
            }

            else if(DMA_XFER_DATA_ADDR_MODE_CONST == pDmaXferConfig->pSrc->addrMode)
            {
                srcData.addrMode = DMA_XFER_DATA_ADDR_MODE_CONST;
            }

            else
            {
                retStat = E_FAIL;
            }
        }

        if(S_PASS == retStat)
        {
            if(DMA_DRV_DATA_SYNC_PACKET == pDmaXferConfig->pSrc->syncMode)
            {
                srcData.syncType = EDMA_PARAM_SYNC_TYPE_A;
            }

            else if(DMA_DRV_DATA_SYNC_FRAME == pDmaXferConfig->pSrc->syncMode)
            {
                srcData.syncType = EDMA_PARAM_SYNC_TYPE_AB;
            }

            else if(DMA_DRV_DATA_SYNC_BLOCK == pDmaXferConfig->pSrc->syncMode)
            {
                srcData.syncType = EDMA_PARAM_SYNC_TYPE_A;
            }

            else
            {
                retStat = E_FAIL;
            }
        }

        if(S_PASS == retStat)
        {
            if((1U == pDmaXferConfig->pSrc->frameActSize) &&
                    (1U == pDmaXferConfig->pSrc->blockSize))
            {
                srcData.addrOff.bCntIdx = 0U;
                srcData.addrOff.cCntIdx = 0U;
            }

            else if(1U == pDmaXferConfig->pSrc->blockSize)
            {
                srcData.addrOff.cCntIdx = 0U;
                srcData.addrOff.bCntIdx = pDmaXferConfig->pSrc->packetActSize;
            }

            else
            {
                srcData.addrOff.cCntIdx = pDmaXferConfig->pSrc->frameActSize;
                srcData.addrOff.bCntIdx = pDmaXferConfig->pSrc->packetActSize;
            }
        }

        if(S_PASS == retStat)
        {
            dstData.addrOff.addr = pDmaXferConfig->pDst->addr;

            if(DMA_XFER_DATA_ADDR_MODE_INC == pDmaXferConfig->pDst->addrMode)
            {
                dstData.addrMode = DMA_XFER_DATA_ADDR_MODE_INC;
            }

            else if(DMA_XFER_DATA_ADDR_MODE_CONST == pDmaXferConfig->pDst->addrMode)
            {
                dstData.addrMode = DMA_XFER_DATA_ADDR_MODE_CONST;
            }

            else
            {
                retStat = E_FAIL;
            }
        }

        if(S_PASS == retStat)
        {
            if((1U == pDmaXferConfig->pDst->frameActSize) &&
                    (1U == pDmaXferConfig->pDst->blockSize))
            {
                dstData.addrOff.bCntIdx = 0U;
                dstData.addrOff.cCntIdx = 0U;
            }

            else if(1U == pDmaXferConfig->pDst->blockSize)
            {
                dstData.addrOff.cCntIdx = 0U;
                dstData.addrOff.bCntIdx = pDmaXferConfig->pDst->packetActSize;
            }

            else
            {
                dstData.addrOff.cCntIdx = pDmaXferConfig->pDst->frameActSize;
                dstData.addrOff.bCntIdx = pDmaXferConfig->pDst->packetActSize;
            }
        }
    }

    if(S_PASS == retStat)
    {
        xferData.pSrc = &srcData;
        xferData.pDst = &dstData;
        retStat = EDMAParamConfig(pEdmaObj->baseChCtrlAddr, xferIdx, &xferData);
    }

    return retStat;
}

int32_t EDMADrvChConfig(uint32_t chNum,
                        dmaDrvChObj_t *pDmaChObj,
                        edmaDrvObj_t *pEdmaObj)
{
    int32_t retStat = E_FAIL;
    EDMAChConfig_t chConfig = EDMA_CH_DEFAULT;
    uint32_t chType = EDMA_CH_TYPE_DMA;

    if(NULL != pDmaChObj)
    {
        retStat = S_PASS;
    }

    if(S_PASS == retStat)
    {
        chConfig.region = pEdmaObj->region;
        chConfig.paramIdx = pDmaChObj->xferIdx;
        chConfig.queueNum = pDmaChObj->queueNum;
        chConfig.enableEvt = FALSE;
        chConfig.enableIntr = pDmaChObj->intrEnable;
        pEdmaObj->pFnCallBack[chNum] = pDmaChObj->callBack;

        if((DMA_XFER_TRIGGER_TYPE_MANUAL == pDmaChObj->triggerType) ||
                (DMA_XFER_TRIGGER_TYPE_EVENT == pDmaChObj->triggerType))
        {
            chType = EDMA_CH_TYPE_DMA;
        }

        else if(DMA_XFER_TRIGGER_TYPE_AUTO == pDmaChObj->triggerType)
        {
            chType = EDMA_CH_TYPE_QDMA;
        }

        else
        {
            retStat = E_FAIL;
        }
    }

    if(S_PASS == retStat)
    {
        retStat = EDMAChConfig(pEdmaObj->baseChCtrlAddr, chType, chNum, &chConfig);
    }

    return retStat;
}

int32_t EDMADrvXferStart(uint32_t chNum,
                         uint32_t triggerType,
                         edmaDrvObj_t *pEdmaObj)
{
    int32_t retStat = E_FAIL;

    retStat = EDMATransferStart(pEdmaObj->baseChCtrlAddr, pEdmaObj->region, chNum,
                                triggerType);

    return retStat;
}

int32_t EDMADrvXferStop(uint32_t chNum,
                        uint32_t triggerType,
                        edmaDrvObj_t *pEdmaObj)
{
    int32_t retStat = E_FAIL;

    retStat = EDMATransferStop(pEdmaObj->baseChCtrlAddr, pEdmaObj->region, chNum,
                               triggerType);

    return retStat;
}
/* -------------------------------------------------------------------------- */
/*                 EDMA MemCPY utils                                          */
/* -------------------------------------------------------------------------- */

void EDMAMemCpyInit(edmaDrvObj_t *pEdmaObj)
{
    EDMADrvInit(0, pEdmaObj);

    EDMASetParamDummy(pEdmaObj->baseChCtrlAddr,
                      pEdmaObj->pMemCpyObj->dummyParam/*DUMMY_PARAMSET_ARM*/);
    dmaDrvChObj_t pDmaChObj;
    pDmaChObj.xferIdx = pEdmaObj->pMemCpyObj->transferParam;//ARM_EDMA_CHANNEL;
    pDmaChObj.queueNum = pEdmaObj->pMemCpyObj->queueNum;//0;
    pDmaChObj.intrEnable = TRUE;

    if(NULL == pEdmaObj->pMemCpyObj->pFnCallBack)
    {
        pDmaChObj.callBack = NULL;
    }

    else
    {
        pDmaChObj.callBack = pEdmaObj->pMemCpyObj->pFnCallBack;
    }

    pDmaChObj.triggerType = DMA_XFER_TRIGGER_TYPE_MANUAL;

    EDMADrvChConfig(pEdmaObj->pMemCpyObj->transferParam/*ARM_EDMA_CHANNEL*/,
                    &pDmaChObj, pEdmaObj);

}

void EDMAMemCpyParamSet(dmaDrvXferObj_t *paramSet,
                        edmaMemCpyObj_t *pMemCpyObj)
{
    paramSet->pSrc = (dmaDrvDataObj_t *)malloc(sizeof(dmaDrvDataObj_t));
    paramSet->pDst = (dmaDrvDataObj_t *)malloc(sizeof(dmaDrvDataObj_t));
    paramSet->pSrc->addr = pMemCpyObj->src;
    paramSet->pSrc->fifoWidth = 0;
    paramSet->pSrc->packetActSize = pMemCpyObj->length;   /*ACNT*/
    paramSet->pSrc->frameActSize = 1;  /*BCNT*/
    paramSet->pSrc->blockSize = 1;    /*CCNT*/
    paramSet->pSrc->addrMode = DMA_XFER_DATA_ADDR_MODE_INC;
    paramSet->pSrc->syncMode = DMA_DRV_DATA_SYNC_PACKET;

    paramSet->pDst->addr = pMemCpyObj->dst;
    paramSet->pDst->addrMode = DMA_XFER_DATA_ADDR_MODE_INC;
    paramSet->pDst->frameActSize = 1;  /*BCNT*/
    paramSet->pDst->blockSize = 1;    /*CCNT*/

    paramSet->intrConfig = DMA_DRV_DATA_SYNC_MASK_BLOCK;
    paramSet->linkEnable = TRUE;
    paramSet->nxtXferIdx = pMemCpyObj->dummyParam;//DUMMY_PARAMSET_ARM;
    paramSet->xferIdx = pMemCpyObj->transferParam;
}

int32_t EDMAMemCpy(edmaDrvObj_t *pEdmaObj)
{
    dmaDrvXferObj_t paramDmaXferConfig;

    /* Prepare PaRAM values for edma transfer */
    EDMAMemCpyParamSet(&paramDmaXferConfig, pEdmaObj->pMemCpyObj);

    /* Now write the PaRam Set to EDMA3 */
    EDMADrvDataXferConfig(pEdmaObj->pMemCpyObj->transferParam,
                          &paramDmaXferConfig , pEdmaObj);

    /* Enable EDMA3 Transfer */
    if(S_PASS != EDMADrvXferStart(pEdmaObj->pMemCpyObj->transferParam,
                                  DMA_XFER_TRIGGER_TYPE_MANUAL, pEdmaObj))
    {
        return -1;
    }

    return 0;
}

/* -------------------------------------------------------------------------- */
/*                 Internal Function Definitions                              */
/* -------------------------------------------------------------------------- */

static int32_t EdmaDrvGetSocInfo(uint32_t instNum, edmaDrvObj_t *pEdmaObj)
{
    int32_t status = S_PASS;

    if(TRUE == CHIPDBIsResourcePresent(CHIPDB_MOD_ID_EDMA3CC, instNum))
    {
        pEdmaObj->baseChCtrlAddr =
            CHIPDBBaseAddress(CHIPDB_MOD_ID_EDMA3CC, instNum);
        pEdmaObj->region = 0U;
        pEdmaObj->intrCfg.complIntrLine = SYS_INT_EDMACOMPINT;
        pEdmaObj->intrCfg.errorIntrLine = SYS_INT_EDMAERRINT;
    }

    else
    {
        status = E_FAIL;
    }

    return status;
}

static int32_t EDMADrvModuleInit(uint32_t instNum)
{
    int32_t retStat = S_PASS;

    /* Clock Configuration  */
    retStat = PRCMModuleEnable(CHIPDB_MOD_ID_EDMA3CC, instNum, 0U);
    retStat = PRCMModuleEnable(CHIPDB_MOD_ID_EDMA3TC, 0U, 0U);
    retStat = PRCMModuleEnable(CHIPDB_MOD_ID_EDMA3TC, 1U, 0U);
    retStat = PRCMModuleEnable(CHIPDB_MOD_ID_EDMA3TC, 2U, 0U);

    return retStat;
}

static int32_t EDMADrvCtrlInit(uint32_t instNum, edmaDrvObj_t *pEdmaObj)
{
    int32_t retStat = S_PASS;

    /* Initialization of EDMA */
    EDMAInit(pEdmaObj->baseChCtrlAddr, pEdmaObj->region);

    /* Register EDMA3 Interrupts */
    EDMADrvIntrConfigure(instNum, pEdmaObj);

    return retStat;
}
static int32_t EDMADrvIntrConfigure(uint32_t instNum, edmaDrvObj_t *pEdmaObj)
{
    Hwi_Params cmplHwiParams = pEdmaObj->intrCfg.complIntrParams;
    Hwi_Params errorlHwiParams = pEdmaObj->intrCfg.errorIntrParams;

    Hwi_Params_init(&cmplHwiParams);
    Hwi_Params_init(&errorlHwiParams);

    cmplHwiParams.arg = (UArg)pEdmaObj;
    cmplHwiParams.priority = 0x10;
    Hwi_create(pEdmaObj->intrCfg.complIntrLine,
               (Hwi_FuncPtr) EdmaDrvChCompletionIsr, &cmplHwiParams, NULL);
    Hwi_enableInterrupt(pEdmaObj->intrCfg.complIntrLine);

    errorlHwiParams.arg = (UArg)pEdmaObj;
    errorlHwiParams.priority = 0x10;
    Hwi_create(pEdmaObj->intrCfg.errorIntrLine, (Hwi_FuncPtr) EdmaDrvChErrorIsr,
               &errorlHwiParams, NULL);
    Hwi_enableInterrupt(pEdmaObj->intrCfg.errorIntrLine);
    return 0;
}

static void EdmaDrvChCompletionIsr(UArg pUserParam)
{
    uint32_t cnt = 0U;
    uint32_t chIdx = 0U;
    edmaDrvObj_t *pInstObj = NULL;

    volatile uint32_t intrStatus = 0U;

    if(NULL != (void *)pUserParam)
    {
        pInstObj = (edmaDrvObj_t *) pUserParam;
        {
            intrStatus = EDMAIntrStatus(pInstObj->baseChCtrlAddr,
                                        pInstObj->region, EDMA_CH_SET_0_31);
        }

        while(0U != (intrStatus >> chIdx))
        {
            if(0U != (intrStatus & (1U << chIdx)))
            {
                EDMAIntrClear(pInstObj->baseChCtrlAddr, pInstObj->region, chIdx);

                if(NULL != pInstObj->pFnCallBack[chIdx])
                {
                    pInstObj->pFnCallBack[chIdx](chIdx, DMA_CH_XFER_STATUS_NORMAL);
                }
            }

            chIdx++;
        }

        cnt = 0U;
        chIdx = 0U;
        intrStatus = 0U;

        if(32U < pInstObj->maxIntrCh)
        {
            while((cnt < EDMA_DRV_HANDLER_RETRY_COUNT) && (0U == intrStatus))
            {
                intrStatus = EDMAIntrStatus(pInstObj->baseChCtrlAddr,
                                            pInstObj->region, EDMA_CH_SET_32_63);
                cnt++;
            }

            while(0U != (intrStatus >> chIdx))
            {
                if(0U != (intrStatus & (1U << chIdx)))
                {
                    EDMAIntrClear(pInstObj->baseChCtrlAddr, pInstObj->region, chIdx + 32U);

                    if(NULL != pInstObj->pFnCallBack[chIdx + 32U])
                    {
                        pInstObj->pFnCallBack[chIdx + 32U](chIdx + 32U, DMA_CH_XFER_STATUS_NORMAL);
                    }
                }

                chIdx++;
            }
        }
    }
}

static void EdmaDrvChErrorIsr(UArg pUserParam)
{
    uint32_t cnt = 0U;
    uint32_t chIdx = 0U;
    edmaDrvObj_t *pInstObj = NULL;
    volatile uint32_t intrErrStatus = 0U;

    if(NULL != (void *)pUserParam)
    {
        pInstObj = (edmaDrvObj_t *) pUserParam;

        while((cnt < EDMA_DRV_HANDLER_RETRY_COUNT) && (0U == intrErrStatus))
        {
            intrErrStatus = EDMAEvtMissStatus(pInstObj->baseChCtrlAddr,
                                              EDMA_CH_TYPE_QDMA, EDMA_CH_SET_0_31);
            cnt++;
        }

        while(0U != (intrErrStatus >> chIdx))
        {
            if(0U != (intrErrStatus & (1U << chIdx)))
            {
                EDMAChMissEvtClear(pInstObj->baseChCtrlAddr, pInstObj->region,
                                   EDMA_CH_TYPE_QDMA, chIdx);
            }

            chIdx++;
        }

        cnt = 0U;
        chIdx = 0U;
        intrErrStatus = 0U;

        while((cnt < EDMA_DRV_HANDLER_RETRY_COUNT) && (0U == intrErrStatus))
        {
            intrErrStatus = EDMAEvtMissStatus(pInstObj->baseChCtrlAddr,
                                              EDMA_CH_TYPE_DMA, EDMA_CH_SET_0_31);
            cnt++;
        }

        while(0U != (intrErrStatus >> chIdx))
        {
            if(0U != (intrErrStatus & (1U << chIdx)))
            {
                EDMAChMissEvtClear(pInstObj->baseChCtrlAddr, pInstObj->region,
                                   EDMA_CH_TYPE_DMA, chIdx);
            }

            chIdx++;
        }

        cnt = 0U;
        chIdx = 0U;
        intrErrStatus = 0U;

        if(32U < pInstObj->maxIntrCh)
        {
            while((cnt < EDMA_DRV_HANDLER_RETRY_COUNT) && (0U == intrErrStatus))
            {
                intrErrStatus = EDMAEvtMissStatus(pInstObj->baseChCtrlAddr,
                                                  EDMA_CH_TYPE_DMA, EDMA_CH_SET_32_63);
                cnt++;
            }

            while(0U != (intrErrStatus >> chIdx))
            {
                if(0U != (intrErrStatus & (1U << chIdx)))
                {
                    EDMAChMissEvtClear(pInstObj->baseChCtrlAddr, pInstObj->region,
                                       EDMA_CH_TYPE_DMA, chIdx + 32U);
                }

                chIdx++;
            }
        }

        cnt = 0U;
        chIdx = 0U;
        intrErrStatus = TRUE;

        while((cnt < EDMA_DRV_HANDLER_RETRY_COUNT) && (TRUE == intrErrStatus))
        {
            EDMAComplCodeErrClear(pInstObj->baseChCtrlAddr);
            intrErrStatus = EDMAComplCodeErrStatus(pInstObj->baseChCtrlAddr);
            cnt++;
        }

        for(chIdx = 0U; chIdx < pInstObj->maxQueue; chIdx++)
        {
            cnt = 0U;
            intrErrStatus = TRUE;

            while((cnt < EDMA_DRV_HANDLER_RETRY_COUNT) && (TRUE == intrErrStatus))
            {
                EDMAQueueThresholdErrClear(pInstObj->baseChCtrlAddr, chIdx);
                intrErrStatus = EDMAQueueThresholdErrStatus(pInstObj->baseChCtrlAddr, chIdx);
                cnt++;
            }
        }
    }
}
