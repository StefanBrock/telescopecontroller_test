/**
 * @file osdrv_osal.c
 * @brief Contains API's for RTOS Abstraction. Services like Interrupt generation, registration
 * Task creation and Timer registration are abstracted here for easy portability
 *
 */
/*
* Copyright (c) 2015, Texas Instruments Incorporated
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* *  Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
* *  Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* *  Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/hal/Timer.h>
#include <assert.h>

#include <xdc/cfg/global.h>
#include "chipdb.h"
#include "prcm.h"
#include "error.h"
#include "osdrv_osal.h"
#include "soc_control.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */


void *DMTimerOSAL_create(uint8_t timerID, DMTimerRunOSAL_Mode runMode,
                         DMTimerPeriodOSAL_Type periodType,
                         DMTimerStartOSAL_Mode startMode, uint32_t interval, void *ISRFunc,
                         void *userArg)
{

    /*Timer initialization variables*/
    Timer_Params timerParams;
    Error_Block eb;

    /*Timer handle to be returned*/
    Timer_Handle dmTimerHandle;

    uint32_t status;

#ifdef  AM43XX_FAMILY_BUILD
    /* Clock source selection */
    SOCCtrlTimerClkSrcSelect(timerID, SOC_CTRL_DMTIMER_CLK_SRC_M_OSC_24M);
    /* Clock Configuration  */
    status = PRCMModuleEnable(CHIPDB_MOD_ID_DMTIMER, timerID, FALSE);
#endif

#ifdef AM335X_FAMILY_BUILD
    status = PRCMModuleEnable(CHIPDB_MOD_ID_DMTIMER, timerID + 2, FALSE);
#endif
    /*Check if clock has been enabled*/
    assert(status != E_FAIL);

    /*Timer setup init*/
    Error_init(&eb);
    Timer_Params_init(&timerParams);

    timerParams.period = interval;

    if(DMTimerRunOSAL_Mode_MICROSECS == periodType)
    {
        timerParams.periodType = Timer_PeriodType_MICROSECS;
    }

    else if(DMTimerRunOSAL_Mode_COUNTS == periodType)
    {
        timerParams.periodType = Timer_PeriodType_COUNTS;
    }

    if(DMTimerRunOSAL_Mode_CONTINUOUS == runMode)
    {
        timerParams.runMode = Timer_RunMode_CONTINUOUS;
    }

    else if(DMTimerRunOSAL_Mode_ONESHOT == runMode)
    {
        timerParams.runMode = Timer_RunMode_ONESHOT;
    }

    else if(DMTimerRunOSAL_Mode_DYNAMIC == runMode)
    {
        timerParams.runMode = Timer_RunMode_DYNAMIC;
    }

    timerParams.arg = (UArg)userArg;
    /*24 Mhz, care must be taken to ensure that this value
     * is modified if the source clock frequency is modified */
    timerParams.extFreq.lo = DMTIMER_SRC_CLK_FREQ;
    timerParams.extFreq.hi = 0;

    if(DMTimerRunOSAL_Mode_AUTO == startMode)
    {
        timerParams.startMode = Timer_StartMode_AUTO;
    }

    else if(DMTimerRunOSAL_Mode_MANUAL == startMode)
    {
        timerParams.startMode = Timer_StartMode_USER;
    }

    /*For DM Timer 3 for example, the value 1 must be passed, so 2 is subtracted.
     *  For DM Timer 4 it is 2*/
    dmTimerHandle = Timer_create(timerID,
                                 (ti_sysbios_interfaces_ITimer_FuncPtr)ISRFunc, &timerParams,  &eb);
    /*Check for NULL handle*/
    assert(NULL != dmTimerHandle);

    return (Timer_Handle)dmTimerHandle;

}

Void *TaskOSAL_create(uint8_t priority, uint8_t *taskName, uint32_t stackSize,
                      void *args, void *taskFunc)
{

    Task_Params taskParams;
    Task_Handle taskHandle;

    Task_Params_init(&taskParams);
    taskParams.priority = priority;
    taskParams.instance->name = (char *)taskName;
    taskParams.stackSize = stackSize;
    taskParams.arg0 = (UArg)args;

    taskHandle = Task_create(taskFunc, &taskParams, NULL);

    return (Task_Handle)taskHandle;

}
/**
 *  @brief  Function to delete Task
 *
 *  @param  handle  Task handle to delete
 *
 *  @return Success
 */
int32_t TaskOSAL_delete(Void *handle)
{
    Task_delete(handle);
    return (OSAL_OK);
}

void TaskOSAL_sleep(uint32_t sleepTicks)
{
    Task_sleep(sleepTicks);
}


void *SemOSAL_create(uint32_t count, SemOSAL_Params *params)
{
    Semaphore_Struct *semaphore;
    union
    {
        Error_Block          eb;
        Semaphore_Params     semaphoreParams;
    } createUnion;

    Error_init(&(createUnion.eb));

    semaphore = (Semaphore_Struct *)Memory_alloc(NULL,
                sizeof(Semaphore_Struct),
                0,
                &(createUnion.eb));

    if(params == NULL)
    {
        Semaphore_construct(semaphore, count, NULL);
    }

    else
    {
        Semaphore_Params_init(&(createUnion.semaphoreParams));

        /*
         * The default mode for TI-RTOS is counting, so only change if a binary
         * semaphore is requested.
         */
        if(params->mode == SemOSAL_Mode_BINARY)
        {
            (createUnion.semaphoreParams).mode = Semaphore_Mode_BINARY;
        }

        (createUnion.semaphoreParams).instance->name = params->name;
        Semaphore_construct(semaphore, count, &(createUnion.semaphoreParams));
    }

    return ((SemOSAL_Handle)semaphore);
}


int32_t SemOSAL_delete(Void *handle)
{
    Semaphore_Struct *semaphore = (Semaphore_Struct *) handle;
    Semaphore_destruct((Semaphore_Struct *) &semaphore);
    Memory_free(NULL, (Semaphore_Struct *) handle, sizeof(Semaphore_Struct));

    return (OSAL_OK);
}



void SemOSAL_Params_init(SemOSAL_Params *params)
{
    params->mode = SemOSAL_Mode_COUNTING;
    params->name = NULL;
}


int32_t SemOSAL_pend(Void *handle, uint32_t timeout)
{
    Bool flag;
    flag = Semaphore_pend((Semaphore_Handle)handle, timeout);

    if(FALSE == flag)
    {
        return (OSAL_TIMEOUT);
    }

    return (OSAL_OK);
}


int32_t SemOSAL_post(Void *semHandle)
{
    Semaphore_post(semHandle);
    return (OSAL_OK);
}


int32_t HwiOSAL_enterCritical()
{
    int key;
    key = Hwi_disable();
    return(key);
}


void HwiOSAL_exitCritical(int32_t key)
{

    Hwi_restore(key);

}



int32_t HwiOSAL_deregisterInterrupt(void *hwiPtr)
{
    ti_sysbios_hal_Hwi_Struct *hwi;
    hwi = hwiPtr;
    Hwi_destruct(hwi);

    return (OSAL_OK);

}


void *HwiOSAL_registerInterrupt(int32_t intNum, int32_t eventID,
                                HwiOSAL_EntryFxn entry, void *arg, uint8_t priority)
{
    Error_Block                   eb;
    Hwi_Params                    hwiParams;
    Hwi_Handle hwhandle;
    /* Disable preemption while checking if the UART is open. */
    /* Construct Hwi object for this UART peripheral. */
    Hwi_Params_init(&hwiParams);
    hwiParams.arg = (UArg)arg;
    hwiParams.eventId = eventID;
    hwiParams.enableInt = TRUE;
    hwiParams.maskSetting = Hwi_MaskingOption_SELF;
    Error_init(&eb);
    hwhandle = Hwi_create(intNum, (Hwi_FuncPtr)entry, &hwiParams, &eb);


    return (hwhandle);

}

/* timestamp resolution in ps */
static uint64_t DelayOSAL_getTimestampResolution(void)
{
    static uint64_t resolution_ps;
    Types_FreqHz freq;

    if(resolution_ps)
    {
        return resolution_ps;
    }

    Timestamp_getFreq(&freq);
    /* neglect .hi as chances of it being non-zero doesn't exist for AM437x */
    resolution_ps = 1000000000000 / freq.lo;

    return resolution_ps;
}

static unsigned DelayOSAL_getTimestampTimePrint(void)
{
    unsigned cnt1, cnt2;

    cnt1 = Timestamp_get32();
    cnt2 = Timestamp_get32();

    return (cnt2 - cnt1);
}

void DelayOSAL_microSeconds(uint64_t usDuration)
{
    Bits32 now, end;
    unsigned count = usDuration * 1000000 / DelayOSAL_getTimestampResolution();

    now = Timestamp_get32();

    /*
     * consider also timeprint of timestamp function, so that if now + count is almost equal to ~0,
     * next timestamp after timestamp just before end may wraparound and waiting will go on till
     * one cycle or in worst case may never end (multiply by 2 for safety)
     */
    /* no overflow for this delay */
    if((unsigned)~0 - now > count + 2 * DelayOSAL_getTimestampTimePrint())
    {
        end = now + count;

        while(Timestamp_get32() < end)
            ;

        /* overflow for this delay */
    }

    else
    {
        while(Timestamp_get32() > now)
            ;

        end = (unsigned)~0 - now;
        end = count - end;

        while(Timestamp_get32() < end)
            ;
    }
}

void DelayOSAL_milliSeconds(uint64_t msDuration)
{
    while(msDuration > 0)
    {
        DelayOSAL_microSeconds(1000);
        msDuration--;
    }
}
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
