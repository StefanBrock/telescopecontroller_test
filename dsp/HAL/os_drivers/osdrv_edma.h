/**
 * osdrv_edma.h
 */
/*
* Copyright (c) 2015, Texas Instruments Incorporated
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* *  Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
* *  Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* *  Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/
#ifndef OSDRV_EDMA_H_
#define OSDRV_EDMA_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <xdc/runtime/Error.h>
#include <assert.h>
#include "types.h"
#include "board_platform.h"
#include "dma.h"
#include "edma.h"
#include <ti/sysbios/hal/Hwi.h>
#include "cache.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define DUMMY_PARAM 4

#define EDMA3_NUM_TCC 32

#define EDMA3_XFER_COMPLETE 1

/** @brief Maximum number of EDMA channel supported. */
#define EDMA_DRV_CALLBACK_MAX                                   (64U)

/** @brief Interrupt mapping for AM43XX */
#if defined(AM43XX_FAMILY_BUILD)
    #define SYS_INT_EDMACOMPINT               (32U + 12U)
    #define SYS_INT_EDMAMPERR                 (32U + 13U)
    #define SYS_INT_EDMAERRINT                (32U + 14U)
#else /* if defined(AM43XX_FAMILY_BUILD) */
    #define SYS_INT_EDMACOMPINT               (12U)
    #define SYS_INT_EDMAMPERR                 (13U)
    #define SYS_INT_EDMAERRINT                (14U)
#endif /* if defined(AM43XX_FAMILY_BUILD) */
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/** @brief Call back function definition to handle interrupts. */
typedef void (*EDMACallBackFxn)(uint32_t chNum, uint32_t xferStatus);

/** @brief Call back function definition to handle interrupts. */
typedef void (*DMACallBackFxn)(uint32_t chNum, uint32_t xferStatus);

/**
 *  @brief structure defining the parameters required for interrupt configuration.
 */
typedef struct edmaDrvIntrCfgObj
{
    uint32_t complIntrLine;
    /**< EDMA Completion interrupt line. */
    Hwi_Params complIntrParams;
    /**< EDMA Completion interrupt configuration. */
    uint32_t errorIntrLine;
    /**< EDMA Error interrupt line. */
    Hwi_Params errorIntrParams;
    /**< EDMA Error interrupt configuration. */
} edmaDrvIntrCfgObj_t;

/**
 *  @brief structure defining the parameters required for EDMA configuration.
 */
typedef struct edmaMemCpyObj
{
    uint8_t instance;              /**< EDMA instance. */
    uint8_t dummyParam;            /**< Dummy param channel. */
    uint8_t transferParam;         /**< ARM channel used for memcpy. */
    EDMACallBackFxn
    pFnCallBack;   /**< EDMA call back function. This is the completion ISR*/
    uint8_t queueNum;              /**< EDMA queue number. */
    uint32_t dst;                  /**< Destination address. */
    uint32_t src;                  /**< Source address */
    uint32_t length;               /**< Length to be copied. */
} edmaMemCpyObj_t;
/**
 *  @brief structure defining the parameters required for EDMA configuration.
 */
typedef struct edmaDrvObj
{
    uint32_t baseChCtrlAddr;        /**< Base address of EDMA channel controller. */
    uint32_t region;                /**< Shadow region interfaced with MPU. */
    uint32_t initStatus;            /**< Controller initialization status. */
    edmaDrvIntrCfgObj_t intrCfg;    /**< EDMA interrupt Configuration Structure. */
    EDMACallBackFxn
    pFnCallBack[EDMA_DRV_CALLBACK_MAX]; /**< EDMA call back function. */
    uint32_t maxIntrCh;
    uint32_t maxQueue;
    edmaMemCpyObj_t
    *pMemCpyObj;    /**< If EDMA Memcpy is used, this structure is configure */
} edmaDrvObj_t;



/** @brief Enumerates the types of data synchronization. */
typedef enum dmaDrvDataSync
{
    DMA_DRV_DATA_SYNC_MIN =  0U,                            /**< Min data synchronization. */
    DMA_DRV_DATA_SYNC_PACKET = DMA_DRV_DATA_SYNC_MIN,   /**< Data synchronization on packet. */
    DMA_DRV_DATA_SYNC_FRAME,                                /**< Data synchronization on frame. */
    DMA_DRV_DATA_SYNC_BLOCK,                                /**< Data synchronization on block. */
    DMA_DRV_DATA_SYNC_MAX                                   /**< Invalid data synchronization. */
} dmaDrvDataSync_t;

/** @brief Enumerates the mask of data synchronization types. */
typedef enum dmaDrvDataSyncMask
{
    DMA_DRV_DATA_SYNC_MASK_NONE =  0U,                                  /**< Invalid data synchronization. */
    DMA_DRV_DATA_SYNC_MASK_PACKET = (1U << DMA_DRV_DATA_SYNC_PACKET),   /**< Mask data synchronization on packet. */
    DMA_DRV_DATA_SYNC_MASK_FRAME = (1U << DMA_DRV_DATA_SYNC_FRAME),     /**< Mask data synchronization on frame. */
    DMA_DRV_DATA_SYNC_MASK_BLOCK = (1U << DMA_DRV_DATA_SYNC_BLOCK),     /**< Mask data synchronization on block. */
} dmaDrvDataSyncMask_t;

/**
 *  @brief   Structure defining the parameters required to determine the data
 *           format of source and destination buffer.
 *
 *  \details A packet is defined as the number of bytes. A frame is composition
 *           of packets. Number of such frames to be transferred form a block.
 *           An active packet is the number of contiguous bytes in packet to be
 *           transmitted. The data address shall point to the first active packet
 *           of the block. An active frame is the number of contiguous packets
 *           to be transmitted. A block is number of contiguous frames to be
 *           transmitted.
 *
 *        * - Start address
 *        x - Data not be transferred or inactive size of packet/frame.
 *
 *          t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
 *         k /x x x x x x x x x / x x x x x x /|            /
 *        c /x_x_x_x_x_x_x_x_x_/ x x x x x x //|           /packet
 *       p /                  / x x x x x x ///|          /
 *        /_ _ _ _ _ _ _ _ _ /_x_x_x_x_x_x_////|         /_ _ _ _ _ _ _ _ _
 *      b |*    frame       | x x x x x x |////          |      frame
 *      l |                 | x x x x x x |///           |
 *      o |                 | x x x x x x |//            |
 *      c |                 |_x_x_x_x_x_x_|/             | block
 *      k |_ _ _ _ _ _ _ _ _|<----------->               |
 *         <--------------->   Inactive
 *              Active
 *
 */
typedef struct dmaDrvDataObj
{
    uint32_t              addr;               /**< Data address. */
    uint32_t
    addrMode;           /**< Addressing mode supported by the data. #dmaXferDataAddrMode_t */
    uint32_t
    fifoWidth;          /**< FIFO Width. This is applicable for constant addressing mode. */
    uint32_t
    packetActSize;      /**< Number of bytes in packets to be transferred. packetActSize is FIFO
                                                   width for constant address mode. */
    uint32_t
    frameActSize;       /**< Number of packets in frame to be transferred. */
    uint32_t
    blockSize;          /**< Number of frame to be transferred. */
    uint32_t
    packetInactSize;    /**< Number of trailing bytes not to be transferred in a packets. This is
                                                   not applicable for constant addressing mode. */
    uint32_t
    frameInactSize;     /**< Number of trailing packets not to be transferred in a frame. This is
                                                   not applicable for constant addressing mode. */
    uint32_t              syncMode;           /**< Data sync mode. */
} dmaDrvDataObj_t;

/**
 *  @brief Structure defining the parameters for data, interrupt, chain and link
 *         configuration of a transfer.
 */
typedef struct dmaDrvXferObj
{
    dmaDrvDataObj_t         *pSrc;      /**< Source data buffer configuration. */
    dmaDrvDataObj_t
    *pDst;      /**< Destination data buffer configuration. */
    dmaDrvDataSyncMask_t
    intrConfig;   /**< Interrupt configuration mask takes #dmaDrvDataSyncMask_t. */
    uint32_t
    linkEnable;   /**< Enable linking on completion of this transfer. */
    uint32_t              xferIdx;          /**< Transfer object for the channel. */
    uint32_t
    nxtXferIdx;   /**< Next transfer object configuration. */
} dmaDrvXferObj_t;

/**
 *  @brief Structure defining the parameters for configuration of DMA channel.
 */
typedef struct dmaDrvChObj
{
    uint32_t
    triggerType;      /**< Transfer trigger type #dmaXferTriggerType_t. */
    uint32_t              xferIdx;          /**< Transfer object for the channel. */
    uint32_t              intrEnable;       /**< Enable interrupts. */
    DMACallBackFxn
    callBack;         /**< Call back function to handle interrupts. */
    uint32_t
    queueNum;         /**< Queue number the channel is grouped. */
} dmaDrvChObj_t;

/**
 * @brief Structure to enable automatic Cache flush on ISR completion
 *        This is typically only applicable for DDR copy (applicable for any cached memory)
 */
typedef struct
{
    uint32_t dstAddress;                /**<Destination Address in DDR*/
    uint32_t wbInvSize;                 /**<Size of the address to be flushed*/
    uint8_t enableDstInvalidate;        /**<Whether to Flush the cache or not*/
    uint8_t enableSrcWb;                /**<Whether to Write back contents of Cache or not*/

} dmaCacheStruct_t;
/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
edmaDrvObj_t *pEdmaObj; /**< Pointer to global EDMA Object*/
dmaCacheStruct_t
*dmaCacheObj[EDMA3_NUM_TCC]; /**< Pointer to EDMA Cache Object. The index refers to channel number*/
EDMACallBackFxn(*cb_Fxn[EDMA3_NUM_TCC])(Uint8 tcc,
                                        Uint8 status);  /**< Callback for ISR*/

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */
/**
 * @brief   This function initialize the given instance for the given type of
 *          EDMA controller.
 *
 * @param   instNum        Instance number of EDMA type.
 * @param   pEdmaObj       Configuration for EDMA transfer
 *
 * \retval  S_PASS  EDMA controller initialization successful.
 * \retval  E_FAIL  EDMA controller initialization failed.
 */
int32_t EDMADrvInit(uint32_t instNum, edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function configures the transfer entry required to perform
 *          normal EDMA transfer without chaining and linking.
 *
 * @param   xferIdx Transfer parameter RAM index.
 * @param   pEdmaObj Configuration for EDMA transfer
 *
 * \retval  S_PASS  Transfer reset successful.
 * \retval  E_FAIL  Transfer reset failed.
 */
int32_t EDMADrvDataXferReset(uint32_t xferIdx, edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function configures the transfer entry required to perform
 *          normal EDMA transfer without chaining and linking.
 *
 * @param   xferIdx        Transfer parameter RAM index.
 * @param   pDmaXferConfig EDMA transfer configuration.
 * @param   pEdmaObj Configuration for EDMA transfer
 *
 * \retval  S_PASS         Transfer configuration successful.
 * \retval  E_FAIL         Transfer configuration failed.
 */
int32_t EDMADrvDataXferConfig(uint32_t xferIdx,
                              dmaDrvXferObj_t *pDmaXferConfig,
                              edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function Does a Dummy EDMA configuration, required to be done
 *          once for all transfers
 *
 * @param   none
 * \retval  S_PASS      Configuration successful.
 * \retval  E_FAIL      Configuration failed.
 */
int32_t EDMADrvDummyConfig();

/**
 * @brief   This function Does a Dummy EDMA configuration, required to be done
 *          once for all transfers
 *
 * @param   chNum       Channel Number for which callback is to be registered
 * @param   callBack    callback function which must be called
 * \retval  S_PASS      Configuration successful.
 * \retval  E_FAIL      Configuration failed.
 */
void RegisterEDMACallback(uint8_t chNum, EDMACallBackFxn callBack);

/**
 * @brief   This function configures the parameters entry required to perform
 *          EDMA transfer.
 *
 * @param   chNum        EDMA channel number.
 * @param   pDmaChObj    Channel configuration of EDMA.
 * @param   pEdmaObj Configuration for EDMA transfer
 *
 * \retval  S_PASS       Channel configuration successful.
 * \retval  E_FAIL       Channel configuration failed.
 */
int32_t EDMADrvChConfig(uint32_t chNum,
                        dmaDrvChObj_t *pDmaChObj,
                        edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function enables the EDMA transfer configured for a channel.
 *
 * @param   chNum        EDMA channel number.
 * @param   triggerType  EDMA transfer trigger type takes #dmaXferTriggerType_t.
 * @param   pEdmaObj Configuration for EDMA transfer
 *
 * \retval  S_PASS       Channel transfer enabled.
 * \retval  E_FAIL       Channel transfer enable failed.
 */
int32_t EDMADrvXferStart(uint32_t chNum,
                         uint32_t triggerType,
                         edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function disables the EDMA transfer configured for a channel.
 *
 * @param   chNum        EDMA channel number.
 * @param   triggerType  EDMA transfer trigger type takes #dmaXferTriggerType_t.
 * @param   pEdmaObj Configuration for EDMA transfer
 *
 * \retval  S_PASS       Channel transfer disabled.
 * \retval  E_FAIL       Channel transfer disable failed.
 */
int32_t EDMADrvXferStop(uint32_t chNum,
                        uint32_t triggerType,
                        edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function initializes EDMA, also configure dummy channel, and transfer channel
 *
 * @param   pEdmaObj Configuration for EDMA transfer
 * @param   pEdmaObj Configuration for EDMA Memcpy transfer
 *
 * @pre Configure pEdmaObj->pMemCpyObj
 *
 */
void EDMAMemCpyInit(edmaDrvObj_t *pEdmaObj);

/**
 * @brief   This function initiates the memcpy based on configuration
 *
 * @param   pEdmaObj Configuration for EDMA transfer
 * @param   pEdmaObj Configuration for EDMA Memcpy transfer
 *
 * @pre Configure pEdmaObj->pMemCpyObj
 *
 */
int32_t EDMAMemCpy(edmaDrvObj_t *pEdmaObj);

#endif /* #ifndef OSDRV_EDMA_H_ */
