/**
 * osdrv_lldConfig.c
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <stdlib.h>
#include "types.h"
#include "error.h"
#include "hw_types.h"
#include "hw_icss.h"
#include "osdrv_pruss.h"
#include "prcm.h"
#include "hw_cm_per.h"
#ifdef AM43XX_FAMILY_BUILD
    #include "am437x.h"
#elif defined AM335X_FAMILY_BUILD
    #include "soc_am335x.h"
#endif
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int32_t PRUSSDRVInit(PRUICSS_Handle handle)
{
    uint8_t retVal = E_FAIL;
    PRUICSS_HwAttrs   *hwAttrs;
    PRUICSS_V1_Object *object;
    object = handle->object;
    hwAttrs = handle->hwAttrs;

#ifdef AM335X_FAMILY_BUILD
    volatile uint32_t delayCounter = 0;
    HWREG(SOC_PRM_PER_REGS)        &= 0xFFFFFFFD;

    /*Adding delay*/
    for(delayCounter = 0; delayCounter < 20000; delayCounter++)
    {
        ;
    }

    HWREG(SOC_PRCM_REGS + CM_PER_ICSS_CLKCTRL)        =
        0x2; //PRCM_CM_PER_PRU_ICSS_CLKCTRL

    for(delayCounter = 0; delayCounter < 20000; delayCounter++)
    {
        ;
    }

#elif defined AM43XX_FAMILY_BUILD
    HW_WR_FIELD32((SOC_PRM_PER_REG + PRCM_RM_PER_RSTCTRL),
                  PRCM_RM_PER_RSTCTRL_ICSS_LRST, PRCM_RM_PER_RSTCTRL_ICSS_LRST_CLEAR);

    if(S_PASS == PRCMModuleEnable(CHIPDB_MOD_ID_PRU_ICSS, 0, NULL))
    {
        retVal = S_PASS;
    }

#endif

    /* populate the BaseAddressHolder*/
    if(TRUE == CHIPDBIsResourcePresent(CHIPDB_MOD_ID_PRU_ICSS,
                                       object->instance))
    {
        //pruIcssHandle = (prussMemoryHandle_t*)malloc(sizeof(prussMemoryHandle_t));
        hwAttrs->baseAddr = CHIPDBBaseAddress(CHIPDB_MOD_ID_PRU_ICSS,
                                              object->instance);
        retVal = S_PASS;
    }

    return retVal;
}
