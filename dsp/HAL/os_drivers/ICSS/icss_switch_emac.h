/**
* @file icss_switch_emac.h
*
* @brief Include file for switch EMAC interface
*
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef _ICSS_SWITCH_EMAC_H
#define _ICSS_SWITCH_EMAC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "icss_emacCommon.h"
#include "icss_emacStatistics.h"
#include "icss_ioctl.h"
#include "icss_nimu_eth.h"
#define USE_PHY1

#ifdef USE_PHY1
/**
* @def SWITCH_NUM_PORTS
*      Port Count
*/
#define SWITCH_NUM_PORTS                    3u
/**
* @def SWITCH_NUM_MAC_PORTS
*      MAC Port Count
*/
#define SWITCH_NUM_MAC_PORTS                2u
#else
/**
* @def SWITCH_NUM_PORTS
*      Port Count
*/
#define SWITCH_NUM_PORTS                    2u
/**
* @def SWITCH_NUM_MAC_PORTS
*      MAC Port Count
*/
#define SWITCH_NUM_MAC_PORTS                1u
#endif


/**
* @def EMAC_PKT_FLAGS_SOP
*      Start of packet
*/
#define EMAC_PKT_FLAGS_SOP                  0x80000000u
/**
* @def EMAC_PKT_FLAGS_EOP
*      End of packet
*/
#define EMAC_PKT_FLAGS_EOP                  0x40000000u



/**
* @def EMAC_PKT_FLAGS_HASCRC
*      RxCrc: PKT has 4byte CRC
*/
#define EMAC_PKT_FLAGS_HASCRC               0x04000000u
/**
* @def EMAC_PKT_FLAGS_JABBER
*      RxErr: Jabber
*/
#define EMAC_PKT_FLAGS_JABBER               0x02000000u
/**
* @def EMAC_PKT_FLAGS_OVERSIZE
*      End of  RxErr: Oversize
*/
#define EMAC_PKT_FLAGS_OVERSIZE             0x01000000u
/**
* @def EMAC_PKT_FLAGS_FRAGMENT
*      RxErr: Fragment
*/
#define EMAC_PKT_FLAGS_FRAGMENT             0x00800000u
/**
* @def EMAC_PKT_FLAGS_UNDERSIZED
*      RxErr: Undersized
*/
#define EMAC_PKT_FLAGS_UNDERSIZED           0x00400000u
/**
* @def EMAC_PKT_FLAGS_CONTROL
*      RxCtl: Control Frame
*/
#define EMAC_PKT_FLAGS_CONTROL              0x00200000u
/**
* @def EMAC_PKT_FLAGS_OVERRUN
*      RxErr: Overrun
*/
#define EMAC_PKT_FLAGS_OVERRUN              0x00100000u
/**
* @def EMAC_PKT_FLAGS_CODEERROR
*      RxErr: Code Error
*/
#define EMAC_PKT_FLAGS_CODEERROR            0x00080000u
/**
* @def EMAC_PKT_FLAGS_ALIGNERROR
*       RxErr: Alignment Error
*/
#define EMAC_PKT_FLAGS_ALIGNERROR           0x00040000u
/**
* @def EMAC_PKT_FLAGS_CRCERROR
*      RxErr: Bad CRC
*/
#define EMAC_PKT_FLAGS_CRCERROR             0x00020000u
/**
* @def EMAC_PKT_FLAGS_NOMATCH
*      RxPrm: No Match
*/
#define EMAC_PKT_FLAGS_NOMATCH              0x00010000u




/**
* @def EMAC_RXFILTER_NOTHING
*      Receive filter set to Nothing
*/
#define EMAC_RXFILTER_NOTHING               0
/**
* @def EMAC_RXFILTER_DIRECT
*      Receive filter set to Direct
*/
#define EMAC_RXFILTER_DIRECT                1
/**
* @def EMAC_RXFILTER_BROADCAST
*      Receive filter set to Broadcast
*/
#define EMAC_RXFILTER_BROADCAST             2
/**
* @def EMAC_RXFILTER_MULTICAST
*      Receive filter set to Multicast
*/
#define EMAC_RXFILTER_MULTICAST             3
/**
* @def EMAC_RXFILTER_ALLMULTICAST
*      Receive filter set to All Mcast
*/
#define EMAC_RXFILTER_ALLMULTICAST          4
/**
* @def EMAC_RXFILTER_ALL
*      Receive filter set to All
*/
#define EMAC_RXFILTER_ALL                   5

/** Internal functions return error codes */
/**
* @def SWITCH_INSTANCE_CODE
*      Switch Instance Code
*/
#define SWITCH_INSTANCE_CODE                        0u
/**
* @def SWITCH_ERROR_BASE
*     Switch Error Base
*/
#define SWITCH_ERROR_BASE                           (0x200001Fu)
/**
* @def SWITCH_ERROR_CODE
*      Switch Error Code
*/
#define SWITCH_ERROR_CODE                           ((SWITCH_ERROR_BASE | (SWITCH_INSTANCE_CODE << 16)))
/**
* @def SWITCH_ERROR_INFO
*      Switch Error/Informational
*/
#define SWITCH_ERROR_INFO                           (SWITCH_ERROR_CODE)
/**
* @def SWITCH_ERROR_WARNING
*      Switch Error/Warning
*/
#define SWITCH_ERROR_WARNING                        (SWITCH_ERROR_CODE | 0x10000000u)
/**
* @def SWITCH_ERROR_MINOR
*      Switch Error Minor
*/
#define SWITCH_ERROR_MINOR                          (SWITCH_ERROR_CODE | 0x20000000u)
/**
* @def SWITCH_ERROR_MAJOR
*      Switch Error Major
*/
#define SWITCH_ERROR_MAJOR                          (SWITCH_ERROR_CODE | 0x30000000u)
/**
* @def SWITCH_ERROR_CRITICAL
*      Switch Error Critical
*/
#define SWITCH_ERROR_CRITICAL                       (SWITCH_ERROR_CODE | 0x40000000u)

/**
* @def SWITCH_SUCCESS
*      Success code
*/
#define SWITCH_SUCCESS                              0u
/**
* @def SWITCH_ERR_DEV_ALREADY_INSTANTIATED(instID)
*      Device with same instance ID already created
*/
#define SWITCH_ERR_DEV_ALREADY_INSTANTIATED(instID) (0x30000000u + SWITCH_ERROR_BASE + ((instId) << 16) )
/**
* @def SWITCH_ERR_DEV_NOT_INSTANTIATED
*       Device is not instantiated yet
*/
#define SWITCH_ERR_DEV_NOT_INSTANTIATED             (SWITCH_ERROR_MAJOR + 1u)
/**
* @def SWITCH_INVALID_PARAM
*      Function or calling parameter is invalid
*/
#define SWITCH_INVALID_PARAM                        (SWITCH_ERROR_MAJOR + 2u)
/**
* @def SWITCH_ERR_CH_INVALID
*      Channel number invalid
*/
#define SWITCH_ERR_CH_INVALID                       (SWITCH_ERROR_CRITICAL + 3u)
/**
* @def SWITCH_ERR_CH_ALREADY_INIT
*      Channel already initialized and setup
*/
#define SWITCH_ERR_CH_ALREADY_INIT                  (SWITCH_ERROR_MAJOR + 4u)
/**
* @def SWITCH_ERR_TX_CH_ALREADY_CLOSED
*      STx Channel already  closed. Channel close failed
*/
#define SWITCH_ERR_TX_CH_ALREADY_CLOSED             (SWITCH_ERROR_MAJOR + 5u)
/**
* @def SWITCH_ERR_TX_CH_NOT_OPEN
*      Tx Channel not open.
*/
#define SWITCH_ERR_TX_CH_NOT_OPEN                   (SWITCH_ERROR_MAJOR + 6u)
/**
* @def SWITCH_ERR_TX_NO_LINK
*      Tx Link not up.
*/
#define SWITCH_ERR_TX_NO_LINK                       (SWITCH_ERROR_MAJOR + 7u)
/**
* @def SWITCH_ERR_TX_OUT_OF_BD
*      Tx ran out of Buffer descriptors to use.
*/
#define SWITCH_ERR_TX_OUT_OF_BD                     (SWITCH_ERROR_MAJOR + 8u)
/**
* @def SWITCH_ERR_RX_CH_INVALID
*     Rx Channel invalid number.
*/
#define SWITCH_ERR_RX_CH_INVALID                    (SWITCH_ERROR_CRITICAL + 9u)
/**
* @def SWITCH_ERR_RX_CH_ALREADY_INIT
*      Rx Channel already setup
*/
#define SWITCH_ERR_RX_CH_ALREADY_INIT               (SWITCH_ERROR_MAJOR + 10u)
/**
* @def SWITCH_ERR_RX_CH_ALREADY_CLOSED
*      Rx Channel already closed. Channel close failed
*/
#define SWITCH_ERR_RX_CH_ALREADY_CLOSED             (SWITCH_ERROR_MAJOR + 11u)
/**
* @def SWITCH_ERR_RX_CH_NOT_OPEN
*      Rx Channel not open yet
*/
#define SWITCH_ERR_RX_CH_NOT_OPEN                   (SWITCH_ERROR_MAJOR + 12u)
/**
* @def SWITCH_ERR_DEV_ALREADY_CREATED
*      EMAC device already created.
*/
#define SWITCH_ERR_DEV_ALREADY_CREATED              (SWITCH_ERROR_MAJOR + 13u)
/**
* @def SWITCH_ERR_DEV_NOT_OPEN
*      Device is not open or not ready
*/
#define SWITCH_ERR_DEV_NOT_OPEN                     (SWITCH_ERROR_MAJOR + 14u)
/**
* @def SWITCH_ERR_DEV_ALREADY_CLOSED
*      Device close failed. Device already closed
*/
#define SWITCH_ERR_DEV_ALREADY_CLOSED               (SWITCH_ERROR_MAJOR + 15u)
/**
* @def SWITCH_ERR_DEV_ALREADY_OPEN
*      Device open failed. Device already open.l
*/
#define SWITCH_ERR_DEV_ALREADY_OPEN                 (SWITCH_ERROR_MAJOR + 16u)
/**
* @def SWITCH_ERR_RX_BUFFER_ALLOC_FAIL
*      Rx Buffer Descriptor allocation failed
*/
#define SWITCH_ERR_RX_BUFFER_ALLOC_FAIL             (SWITCH_ERROR_CRITICAL +17u)
/**
* @def SWITCH_INTERNAL_FAILURE
*       EMAC Internal failure
*/
#define SWITCH_INTERNAL_FAILURE                     (SWITCH_ERROR_MAJOR + 18u)
/**
* @def SWITCH_VLAN_UNAWARE_MODE
*      VLAN support not enabled in EMAC
*/
#define SWITCH_VLAN_UNAWARE_MODE                    (SWITCH_ERROR_MAJOR + 19u)
/**
* @def SWITCH_ALE_TABLE_FULL
*      ALE Table full.
*/
#define SWITCH_ALE_TABLE_FULL                       (SWITCH_ERROR_MAJOR + 20u)
/**
* @def SWITCH_ADDR_NOTFOUND
*      Multicast/Unicast/OUI Address not found in ALE
*/
#define SWITCH_ADDR_NOTFOUND                        (SWITCH_ERROR_MAJOR + 21u)
/**
* @def SWITCH_INVALID_VLANID
*      Invalid VLAN Id.
*/
#define SWITCH_INVALID_VLANID                       (SWITCH_ERROR_MAJOR + 22u)
/**
* @def SWITCH_INVALID_PORT
*      Invalid Port Specified
*/
#define SWITCH_INVALID_PORT                         (SWITCH_ERROR_MAJOR + 23u)
/**
* @def SWITCH_BD_ALLOC_FAIL
*      Buffer Descriptor Allocation failure. OOM
*/
#define SWITCH_BD_ALLOC_FAIL                        (SWITCH_ERROR_MAJOR + 24u)
/**
* @def SWITCH_ERR_BADPACKET
*      Supplied packet was invalid
*/
#define SWITCH_ERR_BADPACKET                        (SWITCH_ERROR_MAJOR + 25u)
/**
* @def SWITCH_ERR_COLLISION_FAIL
*     Collision queue was full
*/
#define SWITCH_ERR_COLLISION_FAIL                   (SWITCH_ERROR_MAJOR + 26u)
/**
* @def SWITCH_ERR_MACFATAL
*      Fatal Error - EMACClose() requiredl
*/
#define SWITCH_ERR_MACFATAL                         (SWITCH_ERROR_CRITICAL + 26u)

/**
* @def SWITCH_DEFAULT_MDIOBUSFREQ
*      ICSS MDIO Bus Frequency
*/
#define SWITCH_DEFAULT_MDIOBUSFREQ                           (2200000u)
/**
* @def SWITCH_DEFAULT_MDIOCLOCKFREQ
*      ICSS MDIO Clock Frequency
*/
#define SWITCH_DEFAULT_MDIOCLOCKFREQ                        (200000000)

/** Timeperiod (in ticks) after which Learning Age out function is called to age the table. This divided by 100
 * gives the number of BIOS ticks*/
#define ALE_AGE_OUT_TIME            300000u


/**
 *  @brief  EMAC_Pkt
 *  @details The packet structure defines the basic unit of memory used to hold data
 *  packets for the EMAC device.
 *  A packet is comprised of one or more packet buffers. Each packet buffer
 *  contains a packet buffer header, and a pointer to the buffer data.
 *  The EMAC_Pkt structure defines the packet buffer header.
 *
 *  The pDataBuffer field points to the packet data. This is set when the
 *  buffer is allocated, and is not altered.
 *
 *  BufferLen holds the the total length of the data buffer that is used to
 *  store the packet (or packet fragment). This size is set by the entity
 *  that originally allocates the buffer, and is not altered.
 *
 *  The Flags field contains additional information about the packet
 *
 *  ValidLen holds the length of the valid data currently contained in the
 *  data buffer.
 *
 *  DataOffset is the byte offset from the start of the data buffer to the
 *  first byte of valid data. Thus (ValidLen+DataOffet)<=BufferLen.
 *
 *  Note that for receive buffer packets, the DataOffset field may be
 *  assigned before there is any valid data in the packet buffer. This allows
 *  the application to reserve space at the top of data buffer for private
 *  use. In all instances, the DataOffset field must be valid for all packets
 *  handled by EMAC.
 *
 *  The data portion of the packet buffer represents a packet or a fragment
 *  of a larger packet. This is determined by the Flags parameter. At the
 *  start of every packet, the SOP bit is set in Flags. If the EOP bit is
 *  also set, then the packet is not fragmented. Otherwise; the next packet
 *  structure pointed to by the pNext field will contain the next fragment in
 *  the packet. On either type of buffer, when the SOP bit is set in Flags,
 *  then the PktChannel, PktLength, and PktFrags fields must also be valid.
 *  These fields contain additional information about the packet.
 *
 *  The PktChannel field detetmines what channel the packet has arrived on,
 *  or what channel it should be transmitted on. The EMAC library supports
 *  only a single receive channel, but allows for up to eight transmit
 *  channels. Transmit channels can be treated as round-robin or priority
 *  queues.
 *
 *  The PktLength field holds the size of the entire packet. On single frag
 *  packets (both SOP and EOP set in BufFlags), PktLength and ValidLen will
 *  be equal.
 *
 *  The PktFrags field holds the number of fragments (EMAC_Pkt records) used
 *  to describe the packet. If more than 1 frag is present, the first record
 *  must have EMAC_PKT_FLAGS_SOP flag set, with corresponding fields validated.
 *  Each frag/record must be linked list using the pNext field, and the final
 *  frag/record must have EMAC_PKT_FLAGS_EOP flag set and pNext=0.
 *
 *  In systems where the packet resides in cacheable memory, the data buffer
 *  must start on a cache line boundary and be an even multiple of cache
 *  lines in size. The EMAC_Pkt header must not appear in the same cache line
 *  as the data portion of the packet. On multi-fragment packets, some packet
 *  fragments may reside in cacheable memory where others do not.
 *
 *  It is up to the caller to assure that all packet buffers
 *   residing in cacheable memory are not currently stored in L1 or L2
 *   cache when passed to any EMAC function.
*/
typedef struct _EMAC_Pkt
{
    uint32_t
    AppPrivate;      /**< For use by the application                                         */
    struct _EMAC_Pkt
        *pPrev;            /**< Previous record                                                    */
    struct _EMAC_Pkt
        *pNext;            /**< Next record                                                        */
    uint8_t
    *pDataBuffer;        /**< Pointer to Data Buffer (read only)                                 */
    uint32_t
    BufferLen;           /**< Physical Length of buffer (read only)                              */
    uint32_t
    Flags;               /**< Packet Flags                                                       */
    uint32_t
    ValidLen;            /**< Length of valid data in buffer                                     */
    uint32_t
    DataOffset;      /**< Byte offset to valid data                                          */
    uint32_t
    PktChannel;       /**< Tx/Rx Channel/Priority 0-7 (SOP only)                              */
    uint32_t
    PktLength;           /**< Length of Packet (SOP only) (same as ValidLen on single frag Pkt)  */
    uint32_t
    PktFrags;            /**< No of frags in packet (SOP only) frag is EMAC_Pkt record-normally 1*/
} EMAC_Pkt;

/**
 *  @brief MAC addresses configuration Structure
 *
 */
typedef struct _EMAC_AddrConfig
{
    uint8_t Addr[6];        /** MAC address specific to channel */
} EMAC_AddrConfig;

/**
 *  @brief  EMAC Main Device Instance Structure
 *
 */
typedef struct _NimuEmacDevice
{

    //ICSSEMAC_Handle      icssHandle;                      /**< Base Addresses ICSS module.                                        */
    PDINFO               *nimuPdInfo;
    uint32_t
    aleTicks;                         /**< Ticks for this timer                                               */
    uint32_t
    aleTimerActive;                       /**<ALE ageout timer active?                                            */
    uint32_t
    RxFilter;                          /**<Receive Filter settings    */
    uint32_t
    PktMTU;                               /**< Max physical packet size                                           */
    uint32_t
    FatalError;                           /**< Fatal Error Code                                                   */
    IcssSwitchStatistics
    *nimuStat;       /**< Current running statistics                                         */
    EMAC_Pkt *nimuPktRx;
    EMAC_Pkt *nimuPktTx;
    EMAC_Pkt           *(*pfcbRxPacket)(Handle hApplication,
                                        EMAC_Pkt *pPacket);  /**< Receive packet call back */
} NimuEmacDevice;


/* @} */
/**
* @brief  Opens the EMAC peripheral at the given physical index and initializes
*           it to an embryonic state.
* @details     The calling application must supply a operating configuration that
*           includes a callback function table. Data from this config structure is
*           copied into the device's internal instance structure so the structure
*           may be discarded after EMACOpen() returns. In order to change an item
*           in the configuration, the EMAC device must be closed and then
*           re-opened with the new configuration.
*
*               The application layer may pass in an hApplication callback handle,
*           that will be supplied by the EMAC device when making calls to the
*           application callback functions.
*
*           An EMAC device handle is written to phEMAC. This handle must be saved
*           by the caller and then passed to other EMAC device functions.
*
*           The default receive filter prevents normal packets from being received
*           until the receive filter is specified by calling EMAC_receiveFilter().
*
*           A device reset is achieved by calling EMACClose() followed by EMACOpen().
*
*           The function returns zero on success, or an error code on failure.
*
*           Possible error codes include:
*               SWITCH_ERR_DEV_ALREADY_OPEN   - The device is already open
*               SWITCH_INVALID_PARAM   - A calling parameter is invalid
*
* @internal
*
* @param pi   pointer to ICSS EMAC Handle
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
*           SWITCH_ERR_DEV_ALREADY_OPEN   - The device is already open
*
*/
uint32_t EMACOpen(PDINFO *pi);
/**
* @brief Closed the EMAC peripheral indicated by the supplied instance handle.
*           When called, the EMAC device will shutdown both send and receive
*           operations, and free all pending transmit and receive packets.
* @details    The function returns zero on success, or an error code on failure.
*               Possible error code include:
*               SWITCH_INVALID_PARAM   - A calling parameter is invalid
* @internal
* @param pi   handle to opened the EMAC device
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
* @pre      EMACOpen function must be called before calling this API.
*
*/
uint32_t EMACClose(PDINFO *pi);
/**
* @brief    Called to get the current device statistics. The statistics structure
*           contains a collection of event counts for various packet sent and
*           receive properties. Reading the statistics also clears the current
*           statistic counters, so the values read represent a delta from the last
*           call.
* @details  The statistics information is copied into the structure pointed to
*               by the pStatistics argument.
*               The function returns zero on success, or an error code on failure.
*               Possible error code include:
*               SWITCH_INVALID_PARAM   - A calling parameter is invalid
* @internal
* @param  hEMAC       handle to the opened EMAC device
* @param  pStatistics Pointer to the device statistics
* @param  portNo   port number
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
* @pre      EMAC peripheral instance must be opened before calling this API
*
*/
uint32_t EMACGetStatistics(PDINFO *pi, IcssSwitchStatistics *pStatistics,
                           uint8_t portNo);
/**
* @brief    Called to clear the current device statistics for a particular port
* @internal
* @param  pi       handle to the opened EMAC device
* @param  portNo   port to clear statistics
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
* @pre      EMAC peripheral instance must be opened before calling this API
*
*/
uint32_t EMACClearStatistics(PDINFO *pi, uint8_t portNo);
/**
* @brief    This function is called to perform various get/set operations on the
*           EMAC ALE table, statistics counters etc.
* @internal
* @param  pi       handle to the opened EMAC device
* @param  pBuf        pointer to the buffer that holds the configuration
                      command and any data required.
* @param  size        size of the buffer
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
* @pre      EMAC peripheral instance must be opened before calling this API
*
*/
uint32_t EMACSetConfig(PDINFO *pi, void *pBuf, Uint32 size);
/**

* @brief    Sends a Ethernet data packet out the EMAC device. On a non-error return,
*           the EMAC device takes ownership of the packet. The packet is returned
*           to the application's free pool once it has been transmitted.
* @internal
* @param  hEMAC       handle to the opened EMAC device
* @param  pPkt        EMAC packet structure
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
*           SWITCH_ERR_BADPACKET - The packet structure is invalid
* @pre      EMAC peripheral instance must be opened and get a packet
*            buffer from private queue
*
*/
uint32_t EMACSendPacket(Handle hEMAC, EMAC_Pkt *pPkt);
/**

* @brief    This function should be called every time there is an EMAC device Rx
*           interrupt to process the packet
* @details  Note that the application has the responsibility for mapping the
*           physical device index to the correct EMAC_serviceCheck() function. If
*           more than one EMAC device is on the same interrupt, the function must be
*           called for each device.
* @internal
* @param  hEMAC       handle to the opened EMAC device
* @param  prioQueue   Queue priority
*
* @retval   Success (0)
*           SWITCH_INVALID_PARAM   - A calling parameter is invalid
*           SWITCH_ERR_MACFATAL  - Fatal error in the MAC - Call EMACClose()
* @pre     EMACOpen function must be called before calling this API.
*
*/
uint32_t EMACRxServiceCheck(ICSSEMAC_Handle hEMAC, Int32 prioQueue);
/**
* @brief The function initialize an EMAC_Pkt to receive packets
* @internal
* @param nimuPacket pointer to NDK packet buffer structure
*
* @retval none
*/
void PacketInit(EMAC_Pkt *nimuPacket);


#ifdef __cplusplus
}
#endif
#endif /* _ICSS_SWITCH_EMAC_H_ */
