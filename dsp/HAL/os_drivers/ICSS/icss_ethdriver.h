/**

* @file icss_ethdriver.h
*
* @brief
*
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

#ifndef ICSS_ETHDRIVER_H_
#define ICSS_ETHDRIVER_H_

#include "icss_emacDrv.h"

/**
* @def Ten_Mbps
*      This value in the MDIO Reg means 10 mbps mode is enabled
*/
#define Ten_Mbps  0xa
/**
* @def Hundread_Mbps
*      This value in the MDIO Reg means 100 mbps mode is enabled
*/
#define Hundread_Mbps 0x64

/**
* @def HALF_DUPLEX_TYPE
*      Half Duplex Type
*/
#define HALF_DUPLEX_TYPE            0x1
/**
* @def FULL_DUPLEX_TYPE
*      Full Duplex Type
*/
#define FULL_DUPLEX_TYPE            0x2
/**
* @brief returns the PHY speed status for a port
*
* @param portNum Port Number. 0/1
* @param icssHandle Provides PRUSS memory map
*
* @retval Status. 0 means 10mbps and 1 means 100 mbps. 2 means undefined.
*/
uint8_t getSpeedStatus(uint8_t portNum, ICSSEMAC_Handle icssHandle);

/**
 * @brief returns the PHY duplex status for a port
 *
 * @param  portNum Port Number. 0/1
 * @param icssHandle Provides PRUSS memory map
 *
 * @retval Status. 1 means Half Duplex and 0 means Full Duplex. 2 means not defined
 */
uint8_t getDuplexStatus(uint8_t portNum, ICSSEMAC_Handle icssHandle);
/**
* @brief Function to register callback function to process periodic protocol processing
*
* @param callBack   Callback function pointer
* @param userArg    User defined generic argument
*
* @retval none
*/
void RegisterPeriodicCallback(ICSS_EmacCallBack callBack, void *userArg);
#endif /* ICSS_ETHDRIVER_H_ */
