/**
* @file icss_nimu_eth.h
*
* @brief Include file for NIMU Packet Architecture
*
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#ifndef _ICSS_NIMU_ETH_H_
#define _ICSS_NIMU_ETH_H_

#include <xdc/std.h>
#include "osdrv_ndkdeviceconfig.h"
#include "icss_emacDrv.h"


/**
* @def PKT_PREPAD
*      Pre-Pad Packet Data Offset
*      The TCP/IP stack library requires that every packet device
*      include enough L2 header room for all supported headers. In
*      order to support PPPoE, this requires a 22 byte L2 header.
*      Thus, since standard Ethernet is only 14 bytes, we must add
*      on an additional 8 byte offset, or PPPoE can not function
*      with our driver.
*/
#define     PKT_PREPAD                      0
/**
* @def RAM_MCAST
*   Indicates whether RAM based multicast lists are suported for this
*   EMAC peripheral.
*/
#define     RAM_MCAST                       0
/**
* @def HASH_MCAST
*        Indicates whether HASH based multicasting is suported for this
*        EMAC peripheral.
*/
#define     HASH_MCAST                      0
/**
* @def PKT_MAX_MCAST
*      Multicast Address List Size
*/
#define     PKT_MAX_MCAST                   31
/**
* @def NIMU_DEVICE_NUM
*      number of Driver Devices
*/
#define NIMU_DEVICE_NUM 1


/** Rx queue (one for all PKT devices) */
#ifndef _INCLUDE_NIMU_CODE
    extern PBMQ    PBMQ_rx;
#endif

/**
* @brief NDK Driver Status
*/
enum ndk_stat {NIMU_STAT_UNKNOWN = -1, /**< Unknown status */
               NIMU_STAT_DOWN,       /**< NDK driver is not up */
               NIMU_STAT_UP,         /**< NDK driver is up  */
               NIMU_STAT_PROGRESS
              };  /**< Driver init in progress */

/**
 * @brief
 *  Packet device information
 *
 * @details
 *  This structure caches the device info.
 */
typedef struct _pdinfo
{

    uint            PhysIdx;      /**< Physical index of this device (0 to n-1) */
    HANDLE          hEther;       /**< Handle to logical driver */
    STKEVENT_Handle
    hEvent;         /**< Semaphore handle used by NDK stack and driver to communicate any
                                         pending Rx events that need to be serviced by NDK ethernet stack */
    UINT8           bMacAddr[6];    /**< MAC Address*/
    uint            Filter;         /**< Current RX filter */
    Uint32          OldMCastCnt;    /**< Previous MCast Address Countr */
    Uint8           bOldMCast[6 *
                              PKT_MAX_MCAST]; /**< Previous Multicast list configured by the Application */
    uint            MCastCnt;                   /**< Current MCast Address Countr */
    UINT8           bMCast[6 *
                           PKT_MAX_MCAST];  /**< Multicast list configured by the Application */
    uint            TxFree;                     /**< Transmitter "free" flag */
    PBMQ
    PBMQ_tx;                    /**< Tx queue (one for each PKT device) */
#ifdef _INCLUDE_NIMU_CODE
    PBMQ
    PBMQ_rx;                    /**< Rx queue (one for each PKT device) */
#endif
    ICSSEMAC_Handle
    nimuDrvHandle;                          /**< Stores any PRU sepecific data prussMemoryHandle_t */
} PDINFO;

/**
* @internal
* @brief The EMAC Initialization Function
* @details The function is used to initialize and register the EMAC
*  with the Network Interface Management Unit (NIMU)
*
* @param[in]  hEvent Stack Event Handle.
*
* @retval Success -   0
*         Error   -   <0
*/
int32_t EmacInit(STKEVENT_Handle hEvent);

/**
* @internal
* @brief Initializes the device MAC address to use.Not used Currently
*
* @param none
*
* @retval Whether mac address has been initialized 1 - true 0 - false
*/
extern int32_t HwPktInit();
/**
* @internal
* @brief none
*
* @param none
*
* @retval none
*
*/
extern void HwPktShutdown();
/**
* @internal
* @brief Opens and configures EMAC. Configures Interrupts
*
* @param pi  PDINFO structure pointer.
*
* @retval Success(0) or failure(Error Codes defined)
*/
extern int32_t HwPktOpen(PDINFO *pi);
/**
* @internal
* @brief Closes EMAC and disables interrupts.
*
* @param pi  PDINFO structure pointer.
*
* @retval none
*/
extern void HwPktClose(PDINFO *pi);
/**
* @internal
* @brief Routine to send out a packet.
*
* @param pi  PDINFO structure pointer.
*
* @retval none
*/
void HwPktTxNext(PDINFO *pi);
/**
* @internal
* @brief This function is called at least every 100ms, faster in a
*      polling environment. The fTimerTick flag is set only when
*      called on a 100ms event.
*
* @param pi  PDINFO structure pointer.
* @param fTimerTick Flag for timer, set when called on a 100ms event..
*
* @retval none
*/
extern void _HwPktPoll(PDINFO *pi, uint fTimerTick);
/**
* @internal
* @brief Low level driver Ioctl interface. This interface can be used for
*  ALE configuration,control,statistics
*
* @param pi  PDINFO structure pointer.
* @param cmd Ioctl command.
* @param pBuf Ioctl buffer with commands and params to set/get  configuration from hardware.
* @param size Size of Ioctl buffer
*
* @retval none
*/
extern uint32_t HwPktIoctl(PDINFO *pi, uint cmd, void *param, uint size);
/**
* @brief The function is used to get the status of ndk driver
*
* @param[in]  instID Stack Driver instance(0 in the implementation)
*
* @retval ndk driver status
*/
uint32_t GetNdkStatus(uint8_t instID);

#endif /* _ICSS_NIMU_ETH_H_ */

