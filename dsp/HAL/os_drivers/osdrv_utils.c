/**
 * osdrv_utils.c
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "osdrv_utils.h"
#ifdef AM43XX_FAMILY_BUILD
    #include "am437x.h"
#elif defined AM335X_FAMILY_BUILD
    #include "soc_am335x.h"
#endif
#include "hw_control_am43xx.h"
#include "hw_cm_wkup.h"
#include "osdrv_osal.h"
#include "gpio.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
extern gpioPinObj_t GPIOPINOBJ_DEFAULT;
/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

void UTILsInitCpswIcssPorts(/*unsigned char swicthType*/)
{
    gpioPinObj_t gpioMii1Mux = GPIOPINOBJ_DEFAULT;
    gpioPinObj_t gpioMii2Mux = GPIOPINOBJ_DEFAULT;
    gpioPinObj_t gpioMii3Mux = GPIOPINOBJ_DEFAULT;
    Board_initGPIO(DEVICE_ID_MII_MUX, 7, &gpioMii1Mux);
    Board_initGPIO(DEVICE_ID_MII_MUX, 8, &gpioMii2Mux);
    Board_initGPIO(DEVICE_ID_MII_MUX, 9, &gpioMii3Mux);
    GPIOPinWrite(gpioMii2Mux.instAddr, gpioMii2Mux.pinNum , 1);
    GPIOPinWrite(gpioMii1Mux.instAddr, gpioMii1Mux.pinNum , 0);
    GPIOPinWrite(gpioMii3Mux.instAddr, gpioMii3Mux.pinNum , 0);
}
