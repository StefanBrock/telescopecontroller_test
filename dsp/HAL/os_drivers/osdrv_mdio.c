/**
 * @file   osdrv_mdio.c
 *
 * @brief  MDIO Driver source file
 *
 *
*/

/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <xdc/std.h>

#include "osdrv_mdio.h"
#include "board_phy.h"
#include "mdio.h"
#include "hw_mdio.h"
#include "hw_types.h"
#include "osdrv_osal.h"
#include <ti/sysbios/knl/Mailbox.h>



/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */

/* ========================================================================== */

/**
* @internal
* @brief Task Which writes to MDIO register when Initialized in Thread safe mode
*
*       All the MDIO writes and read are done via MDIO Task when the MDIO is initialized using thread safe
*       method.Whenever a MDIO read/write API is called a mailbox is posted with corresponding details.
*       The task will be pending on the Mailbox and depending on the mailbox message the action is carried out.
*
*       Incase of MDIO PHY reg read, a semaphore handle need to be provided to the MDIO driver so that once the read
*       is done the semaphore will be posted. The application who called the MDIO read function should pend on the semaphore
*       to get an indication that the Read action is completed. Otherwise there is a possibility that the application
*       may get wrong values
*
*
* @param arg0   [IN]
* @param arg1   [IN]
*
*  @retval none
*/
void MDIO_readwriteTask(UArg arg0, UArg arg1);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/** Mail box handle used to implement thread safe MDIO access*/
Mailbox_Handle mdioMailbx;
/** Task handle used to implement thread safe MDIO access*/
void *mdioTask;
/**Variable to indicate the mdio access mode (Direct read/ Read via MDIO Task) */
static uint8_t mdioAccessMode = 0;
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

/**
* @internal
* @brief Task Which writes to MDIO register when Initialized in Thread safe mode
*
*       All the MDIO writes and read are done via MDIO Task when the MDIO is initialized using thread safe
*       method.Whenever a MDIO read/write API is called a mailbox is posted with corresponding details.
*       The task will be pending on the Mailbox and depending on the mailbox message the action is carried out.
*
*       Incase of MDIO PHY reg read, a semaphore handle need to be provided to the MDIO driver so that once the read
*       is done the semaphore will be posted. The application who called the MDIO read function should pend on the semaphore
*       to get an indication that the Read action is completed. Otherwise there is a possibility that the application
*       may get wrong values
*
*
* @param arg0
* @param arg1
*
*  @retval none
*/

/**
* @internal
* @brief Function to write into MDIOUSERPHYSEL0/MDIOUSERPHYSEL1
*
* @param baseAddr   [IN] MDIO Base Address
* @param phyinst    [IN] MDIOUSERPHYSEL0/MDIOUSERPHYSEL1
* @param val        [IN] value to be written
*
*  @retval none
*/
void MDIO_userPhySel(unsigned int baseAddr, unsigned int phyinst,
                     unsigned int val)
{
    HWREG(baseAddr + MDIO_USERPHYSEL(0) + (phyinst * 8)) = val;
}

/**
* @internal
* @brief Task Which writes to MDIO register when Initialized in Thread safe mode
*
*       All the MDIO writes and read are done via MDIO Task when the MDIO is initialized using thread safe
*       method.Whenever a MDIO read/write API is called a mailbox is posted with corresponding details.
*       The task will be pending on the Mailbox and depending on the mailbox message the action is carried out.
*
*       Incase of MDIO PHY reg read, a semaphore handle need to be provided to the MDIO driver so that once the read
*       is done the semaphore will be posted. The application who called the MDIO read function should pend on the semaphore
*       to get an indication that the Read action is completed. Otherwise there is a possibility that the application
*       may get wrong values
*
*
* @param arg0
* @param arg1
*
*  @retval none
*/
void MDIO_readwriteTask(UArg arg0, UArg arg1)
{
    mdio_param_struct mdioParam;

    while(Mailbox_pend(mdioMailbx, &mdioParam, OSAL_SEM_WAIT_FOREVER))
    {
        if(1 == mdioParam.mdioOp)   //Write
        {
            MDIOPhyRegWrite(mdioParam.baseaddress, mdioParam.phyNum, mdioParam.offset,
                            mdioParam.writeVal);
        }

        else if(0 == mdioParam.mdioOp)   //read
        {
            MDIOPhyRegRead(mdioParam.baseaddress, mdioParam.phyNum, mdioParam.offset,
                           mdioParam.readVal);

            if(mdioParam.mdiosem)
            {
                SemOSAL_post(mdioParam.mdiosem);
            }
        }
    }
}


/**
* @brief Does the MDIO module and MDIO driver initialization
*
*       This Function does the MDIO Module init. Uses the starterware API "MDIOInit"
*       The argument mode decides the MDIO driver to enable threadsafe Read/Write option
*       if MDIO_DRV_MODE_TASK is used the MDIO task is created and used for all read and write operations
*       Otherwise starterware APIs will be used
*
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param mdioClockFreq      [IN] MDIO Clock Frequency
* @param mdioBusFreq        [IN] MDIO Bus frequency
* @param mode               [IN] Flag to enable Thread safe/ Non thread safe MDIO read and write
*
*  @retval none
*/
void MDIO_osDrvInit(uint32_t mdioBaseAddress, uint32_t mdioClockFreq,
                    uint32_t mdioBusFreq, uint8_t mode)
{
    uint8_t taskName[] = "mdioTask";
    mdioAccessMode = mode;

    if(mdioAccessMode)
    {
        mdioMailbx = Mailbox_create(sizeof(mdio_param_struct), MDIO_NUM_MSGSIZE,
                                    NULL, NULL);
        TaskOSAL_create(5, taskName, 0x1000, NULL, MDIO_readwriteTask);
    }

    MDIOInit(mdioBaseAddress, mdioClockFreq, mdioBusFreq);

}
/**
* @brief Does the MDIO Driver stop
*
*       Does the deletion of task and Mailbox used in threadsafe MDIO read/write
*
*
*  @retval none
*/
void MDIO_osDrvDeinit()
{
    if(mdioAccessMode)
    {
        TaskOSAL_delete(&mdioTask);
        Mailbox_delete(&mdioMailbx);
    }
}

/**
* @brief Function to read from PHY register
*
*        Use this function to read from a PHY register.This APi checks for the mode of MDIO access
*        rselected by MDIO INit. In case of Normal mode, direct read is done. Otherwise the Mailbox is
*        posted withe details. The MDIO driver task will take the details and the read will be done.
*        The Sem handle need to be passed if the Thread safe MDIO read is used, Pass NULL otherwise
*
*        MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN]    MDIO Base Address
* @param phyNum             [IN]    Phy address
* @param phyOffset          [IN]    Phy register offset where value is to be read
* @param phyRegVal          [OUT]   pointer to read the register value into
* @param mdioSemhandle      [IN]    Semaphore handle if thread safe MDIO access is used
*
*  @retval none
*/
void MDIO_regRead(uint32_t mdioBaseAddress, uint32_t phyNum,
                  uint32_t phyOffset, uint16_t *phyRegVal, MDIOSEM_Handle mdioSemhandle)
{

    if(mdioAccessMode)
    {
        mdio_param_struct mdioReadParam;
        mdioReadParam.baseaddress = mdioBaseAddress;
        mdioReadParam.mdioOp = 0 ;
        mdioReadParam.offset = phyOffset;
        mdioReadParam.phyNum = phyNum;
        mdioReadParam.readVal = phyRegVal;
        mdioReadParam.mdiosem = mdioSemhandle;
        Mailbox_post(mdioMailbx, &mdioReadParam, OSAL_NO_WAIT);
    }

    else
    {
        MDIOPhyRegRead(mdioBaseAddress, phyNum, phyOffset, phyRegVal);
    }
}

/**
* @brief Function to Write to PHY register
*
*        Use this function to write to a PHY register.This APi checks for the mode of MDIO access
*        rselected by MDIO INit. In case of Normal mode, direct write is done. Otherwise the Mailbox is
*        posted with details. The MDIO driver task will take the details and the write is executed
*
*        MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address
* @param phyOffset          [IN] Phy register offset where value is to be written
* @param phyRegVal          [IN] Value to write to register
*
*  @retval none
*/
void MDIO_regWrite(uint32_t mdioBaseAddress, uint32_t phyNum,
                   uint32_t phyOffset, uint16_t phyRegVal)
{
    if(mdioAccessMode)
    {
        mdio_param_struct mdioWriteParam;
        mdioWriteParam.baseaddress = mdioBaseAddress;
        mdioWriteParam.mdioOp = 1    ;
        mdioWriteParam.offset = phyOffset;
        mdioWriteParam.phyNum = phyNum;
        mdioWriteParam.writeVal = phyRegVal;
        Mailbox_post(mdioMailbx, &mdioWriteParam, OSAL_NO_WAIT);
    }

    else
    {
        MDIOPhyRegWrite(mdioBaseAddress, phyNum, phyOffset, phyRegVal);
    }
}

/**
* @brief Function to enable the MDIO Link interrupt
*
*       Used to enable MDIO Link Interrupt.
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyInst            [IN] O/1 to select the User phy select option
* @param phyNum             [IN] Phy address of the port
* @param linkSel            [IN] Flag to select to use MDIO mode or MLINK mode
*
*  @retval none
*/
void MDIO_enableLinkInterrupt(uint32_t mdioBaseAddress, uint32_t phyInst,
                              uint32_t phyNum, uint8_t linkSel)
{
    uint32_t PhySel;

    PhySel = phyNum;
    PhySel |=  0x40;

    if(MDIO_LINKSEL_ENABLE == linkSel)
    {
        PhySel |= 0x80;
    }

    MDIO_userPhySel(mdioBaseAddress, phyInst, PhySel);
}

/**
* @brief Function to disable the MDIO Link interrupt
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyInst            [IN] O/1 to select the User phy select option
* @param phyNum             [IN] Phy address of the port
* @param linkSel            [IN] Flag to select to use MDIO mode or MLINK mode
*
*  @retval none
*/
void MDIO_disableLinkInterrupt(uint32_t mdioBaseAddress, uint32_t phyInst,
                               uint32_t phyNum, uint8_t linkSel)
{
    uint32_t PhySel;
    PhySel = phyNum;
    PhySel &=  ~(0x40);
    MDIO_userPhySel(mdioBaseAddress, phyInst, PhySel);
}

/**
* @brief This function returns the PHY Link status
*
*         Reads Phy Link status from MDIO and returns the value
*         MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
*
*  @retval 1 On Linkup
*          0 On Linkdown
*/
uint8_t MDIO_getPhyLinkStatus(uint32_t mdioBaseAddress, uint32_t phyNum)
{
    return MDIOPhyLinkStatus(mdioBaseAddress, phyNum);
}

/**
* @brief API to Change Phy Configuration
*
*       This function can be used to change the Speed and duplexity of the PHY. The Api changes
*       the PHY config depending on the value of phyMode argument.
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress  [IN]  MDIO Base Address
* @param phyMode          [IN]  Speed/Duplexity mode to set
*                               PHY_CONFIG_AUTONEG
*                               PHY_CONFIG_100FD
*                               PHY_CONFIG_10FD
*                               PHY_CONFIG_100HD
* @param phyNum           [IN]  Phy address of the port
* @param mdioSemhandle    [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void MDIO_setPhyConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                       uint8_t phyMode, MDIOSEM_Handle mdioSemhandle)
{

    uint16_t regStatus;

    MDIO_regRead(mdioBaseAddress, phyNum, PHY_BMCR_REG, &regStatus,
                 mdioSemhandle);

    switch(phyMode)
    {
        case PHY_CONFIG_AUTONEG:
            regStatus |= MII_AUTO_NEGOTIATE_EN;
            break;

        case PHY_CONFIG_100FD:
            regStatus &= ~(MII_AUTO_NEGOTIATE_EN);
            regStatus |= MII_SPEEDSEL_100;
            regStatus |= MII_DUPLEXMODE_FULL;
            break;

        case PHY_CONFIG_10FD:
            regStatus &= ~(MII_AUTO_NEGOTIATE_EN);
            regStatus &= ~(MII_SPEEDSEL_100);
            regStatus |= MII_DUPLEXMODE_FULL;
            break;

        case PHY_CONFIG_100HD:
            regStatus &= ~(MII_AUTO_NEGOTIATE_EN);
            regStatus |= MII_SPEEDSEL_100;
            regStatus &= ~(MII_DUPLEXMODE_FULL);
            break;

        case PHY_CONFIG_10HD:
            regStatus &= ~(MII_AUTO_NEGOTIATE_EN);
            regStatus &= ~(MII_SPEEDSEL_100);
            regStatus &= ~(MII_DUPLEXMODE_FULL);
            break;

        default:
            regStatus |= MII_AUTO_NEGOTIATE_EN;
            break;
    }

    MDIO_regWrite(mdioBaseAddress, phyNum, PHY_BMCR_REG, regStatus);
}

/**
* @brief Function to get the PHY Speed and duplexity
*
*       Function returns the current Speed and duplexity configuration of the PHY.
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress  [IN] MDIO Base Address
* @param phyNum           [IN] Phy address of the port
* @param mdioSemhandle    [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval PHY_CONFIG_10FD 10 Mbps and Full duplex
*          PHY_CONFIG_10HD 10 Mbps and Half duplex
*          PHY_CONFIG_100FD 100 Mbps and Full duplex
*          PHY_CONFIG_100HD 100 Mbps and Half duplex
*/
uint8_t MDIO_getPhyConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                          MDIOSEM_Handle mdioSemhandle)
{
    return Board_getPhyConfig(mdioBaseAddress, phyNum, mdioSemhandle);
}

/**
* @brief Function to get Autoneg/ Forced setting of the PHY
*
*       Function returns whether the PHY is in Auto Neg mode or forced mode.
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress  [IN] MDIO Base Address
* @param phyAddr          [IN] Phy address of the port
* @param mdioSemhandle    [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if Phy is in Autoneg mode
*          FALSE if Phy is in Forced mode
*/
uint8_t MDIO_getAutoNegStat(uint32_t mdioBaseAddress, uint32_t phyAddr,
                            MDIOSEM_Handle mdioSemhandle)
{
    uint16_t regStatus;
    MDIO_regRead(mdioBaseAddress, phyAddr, PHY_BMCR_REG, &regStatus,
                 mdioSemhandle);

    if(regStatus & MII_AUTO_NEGOTIATE_EN)
    {
        return TRUE;
    }

    else
    {
        return FALSE;
    }
}
/**
* @brief Function to get the PHY ready status
*
**      Function returns whether the PHY is identified and ready
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyAddr            [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if Phy is ready
*          FALSE if Phy not ready
*/
uint8_t MDIO_getPhyIdentifyStat(uint32_t mdioBaseAddress, uint32_t phyAddr,
                                MDIOSEM_Handle mdioSemhandle)
{
    uint16_t regStatus;
    MDIO_regRead(mdioBaseAddress, phyAddr, PHY_PHYIDR1_REG, &regStatus,
                 mdioSemhandle);

    if(regStatus == 0x2000)
    {
        return TRUE;
    }

    else
    {
        return FALSE;
    }
}

/**
* @brief Function to get Autoneg complete status
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyAddr            [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if Autoneg is complete
*          FALSE if Autoneg is not complete
*/
uint8_t MDIO_getAutoNegCompleteStat(uint32_t mdioBaseAddress, uint32_t phyAddr,
                                    MDIOSEM_Handle mdioSemhandle)
{
    uint16_t regStatus;
    MDIO_regRead(mdioBaseAddress, phyAddr, PHY_BMSR_REG, &regStatus,
                 mdioSemhandle);

    if(regStatus & PHY_AUTONEG_COMPETE)
    {
        return TRUE;
    }

    else
    {
        return FALSE;
    }
}
/**
* @brief Function to get the Link Partner Autoneg ability
*
**      MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyAddr            [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if parter in Autoneg
*          FALSE if parter in forced mode
*/
uint8_t MDIO_getLinkPartnerAutonegAble(uint32_t mdioBaseAddress,
                                       uint32_t phyAddr, MDIOSEM_Handle mdioSemhandle)
{
    uint16_t regStatus;
    MDIO_regRead(mdioBaseAddress, phyAddr, PHY_ANER_REG, &regStatus,
                 mdioSemhandle);

    if(regStatus & PHY_LNKPTNR_AUTONEG)
    {
        return TRUE;
    }

    else
    {
        return FALSE;
    }
}
