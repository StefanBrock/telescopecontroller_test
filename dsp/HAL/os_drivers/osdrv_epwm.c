/**
 * osdrv_epwm.h
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#include "osdrv_epwm.h"
void PowerPWM(void)
{
    /*Writing to PRCM_CM_PER_PWMSS0-5_CLKCTRL registers*/
    HWREG(SOC_CM_PER_REG + PRCM_CM_PER_EPWMSS0_CLKCTRL)    |=
        PWM_CLK_CNTRL_ENABLE_BIT;        // PWM0
    HWREG(SOC_CM_PER_REG + PRCM_CM_PER_EPWMSS1_CLKCTRL)     |=
        PWM_CLK_CNTRL_ENABLE_BIT;        // PWM1
    HWREG(SOC_CM_PER_REG + PRCM_CM_PER_EPWMSS2_CLKCTRL)     |=
        PWM_CLK_CNTRL_ENABLE_BIT;        // PWM2
    HWREG(SOC_CM_PER_REG + PRCM_CM_PER_EPWMSS3_CLKCTRL)     |=
        PWM_CLK_CNTRL_ENABLE_BIT;        // PWM3
    HWREG(SOC_CM_PER_REG + PRCM_CM_PER_EPWMSS4_CLKCTRL)     |=
        PWM_CLK_CNTRL_ENABLE_BIT;        // PWM4
    HWREG(SOC_CM_PER_REG + PRCM_CM_PER_EPWMSS5_CLKCTRL)     |=
        PWM_CLK_CNTRL_ENABLE_BIT;        // PWM5
    /*CTRL_PWMSS - PWM3 syncin from PWM2 (daisy chain), timebase clock enable for all*/
    HWREG(SOC_CONTROL_MODULE_REG + CTRL_PWMSS) =  PWMSS0_TBCLKEN_ENABLE_BIT |
            PWMSS1_TBCLKEN_ENABLE_BIT | PWMSS2_TBCLKEN_ENABLE_BIT |
            PWMSS3_TBCLKEN_ENABLE_BIT | PWMSS4_TBCLKEN_ENABLE_BIT |
            PWMSS5_TBCLKEN_ENABLE_BIT | PWM3_SYNCI_PIN_ENABLE_BIT;
}

