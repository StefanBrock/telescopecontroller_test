/**
 * @file   osdrv_mdio.h
 *
 *  @brief
 *
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

#ifndef _OSDRV_MDIO_H
#define _OSDRV_MDIO_H

#include "stdint.h"
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/**Normal MDIO read and write will be enabled.*/
#define MDIO_DRV_MODE_NORMAL 0
/**Thread safe MDIO read and write will be enabled*/
#define MDIO_DRV_MODE_TASK   1

/**Phy Basic Mode Control register offset*/
#define PHY_BMCR_REG                            (0x0)
/**Phy Basic Mode Status register offset*/
#define PHY_BMSR_REG                            (0x1)
/**Phy identifier register 1 offset*/
#define PHY_PHYIDR1_REG                         (0x2)
/**Phy Auto neg Expansion register offset*/
#define PHY_ANER_REG                            (0x6)

/**Auto neg Enable bit in PHY_BMCR_REG*/
#define MII_AUTO_NEGOTIATE_EN   (1<<12)
/**Speed Selection bit in PHY_BMCR_REG*/
#define MII_SPEEDSEL_100        (1<<13)
/**Duplexity Selection bit in PHY_BMCR_REG*/
#define MII_DUPLEXMODE_FULL     (1<<8)

/**Autoneg complete bit in PHY_BMSR_REG*/
#define PHY_AUTONEG_COMPETE     (1<<5)
/**Link Partner Autoneg  bit in PHY_ANER_REG*/
#define PHY_LNKPTNR_AUTONEG     (1)

/**MDIO register Bit for Link interrupt enable*/
#define MDIO_USERPHYSEL_LINKSEL             (1 << 7)
/**MDIO register value to select MLINk mode of Link detection*/
#define MDIO_LINKSEL_ENABLE                 1
/**MDIO register value to select MDIO mode of Link detection*/
#define MDIO_LINKSEL_DISABLE                0

/**Maximum Read/Write commands that can be Queued in MDIO driver (Thread safe mode)*/
#define MDIO_NUM_MSGSIZE 5

/**Configure PHY in AutoNeg mode*/
#define  PHY_CONFIG_AUTONEG         0u
/**Force PHY to 100 FullDuplex*/
#define  PHY_CONFIG_100FD           1u
/**Force PHY to 10 FullDuplex*/
#define  PHY_CONFIG_10FD            2u
/**Force PHY to 100 halfDuplex*/
#define  PHY_CONFIG_100HD           3u
/**Force PHY to 100 halfDuplex*/
#define  PHY_CONFIG_10HD            4u

/**
 *  @def  NWAY_AUTOMDIX
 *        Auto MDIX mode
 */
#define NWAY_AUTOMDIX       (1u << 16u)

/**
 *  @def  NWAY_AUTOMDIX
 *        Full Duplex 1Gbps
 */
#define NWAY_FD1000         (1u<<13u)


/**
 *  @def  NWAY_HD1000
 *        Half Duplex 1Gbps
 */
#define NWAY_HD1000         (1u<<12u)

/**
 *  @def  NWAY_NOPHY
 *        No Phy
 */
#define NWAY_NOPHY          (1u<<10u)

/**
 *  @def  NWAY_LPBK
 *        Loopback mode
 */
#define NWAY_LPBK           (1u<<9u)

/**
 *  @def  NWAY_FD100
 *        Full Duplex 100 Mbps
 */
#define NWAY_FD100          (1u<<8u)

/**
 *  @def  NWAY_HD100
 *        Half Duplex 100 Mbps
 */
#define NWAY_HD100          (1u<<7u)


/**
 *  @def  NWAY_FD10
 *        Full Duplex 10 Mbps
 */
#define NWAY_FD10           (1u<<6u)

/**
 *  @def  NWAY_HD10
 *        Half Duplex 10 Mbps
 */
#define NWAY_HD10           (1u<<5u)

/**
 *  @def  NWAY_AUTO
 *        Autonegotiate
 */
#define NWAY_AUTO           (1u<<0u)

/**
 *  @def  NWAY_AUTOMDIX_ENABLE
 *        Auto MDIX Enable
 */
#define NWAY_AUTOMDIX_ENABLE (1u<<15)


/**Collision Queue*/
#define NWAY_FORCEMDIX_ENABLE (1u<<14)
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/** MDIO Semaphore handle type*/
typedef  void       *MDIOSEM_Handle;

/**
 * @brief MDIO super structure containing all MDIO and PHY related values which the MDIO task uses
 *        to read and write Phy registers
 */
typedef struct MDIO_PARAM_STRUCT
{
    /** MDIO operation (Read/Write)*/
    uint8_t mdioOp;
    /** MDIO Base Address*/
    uint32_t baseaddress;
    /** Phy Address*/
    uint32_t phyNum;
    /** Phy register offset whose value is to be read/written*/
    uint32_t offset;
    /**Value to be written. Valid in case of Reg write only*/
    uint16_t writeVal;
    /**Pointer where value is read. Valid in case of Reg read only*/
    uint16_t *readVal;
    /**Semaphore handle which will be posted by MDIO driver once read is complete*/
    MDIOSEM_Handle mdiosem;
} mdio_param_struct;

/* ========================================================================== */
/*                            Global Variables Declarations                   */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/** @defgroup ISDK_OSDRIVER_FUNCTIONS MDIO/PHY Driver OSDriver APIs
 *
 * @section Introduction
 *
 * @subsection xxx Overview
 *
 * Overview here
 */


/**
@defgroup PHY_MDIO_COMMON_FUNCTIONS  PHY Common Register APIs
@ingroup ISDK_OSDRIVER_FUNCTIONS
*/

/**
@defgroup MDIO_CONFIG_FUNCTIONS  MDIO Configuration APIs
@ingroup ISDK_OSDRIVER_FUNCTIONS
*/

/** @addtogroup MDIO_CONFIG_FUNCTIONS
 @{ */

/**
* @brief Does the MDIO module and MDIO driver initialization
*
*       This Function does the MDIO Module init. Uses the starterware API "MDIOInit"
*       The argument mode decides the MDIO driver to enable threadsafe Read/Write option
*       if MDIO_DRV_MODE_TASK is used the MDIO task is created and used for all read and write operations
*       Otherwise starterware APIs will be used
*
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param mdioClockFreq      [IN] MDIO Clock Frequency
* @param mdioBusFreq        [IN] MDIO Bus frequency
* @param mode               [IN] Flag to enable Thread safe/ Non thread safe MDIO read and write
*
*  @retval none
*/
void MDIO_osDrvInit(uint32_t mdioBaseAddress, uint32_t mdioClockFreq,
                    uint32_t mdioBusFreq, uint8_t mode);
/**
* @brief Does the MDIO Driver stop
*
*       Does the deletion of task and Mailbox used in threadsafe MDIO read/write
*
*
*  @retval none
*/
void MDIO_osDrvDeinit();
/**
* @brief Function to read from PHY register
*
*        Use this function to read from a PHY register.This APi checks for the mode of MDIO access
*        rselected by MDIO INit. In case of Normal mode, direct read is done. Otherwise the Mailbox is
*        posted withe details. The MDIO driver task will take the details and the read will be done.
*        The Sem handle need to be passed if the Thread safe MDIO read is used, Pass NULL otherwise
*
*        MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN]    MDIO Base Address
* @param phyNum             [IN]    Phy address
* @param phyOffset          [IN]    Phy register offset where value is to be read
* @param phyRegVal          [OUT]   pointer to read the register value into
* @param mdioSemhandle      [IN]    Semaphore handle if thread safe MDIO access is used
*
*  @retval none
*/
void MDIO_regRead(uint32_t mdioBaseAddress, uint32_t phyNum,
                  uint32_t phyOffset, uint16_t *phyRegVal, MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to Write to PHY register
*
*        Use this function to write to a PHY register.This APi checks for the mode of MDIO access
*        rselected by MDIO INit. In case of Normal mode, direct write is done. Otherwise the Mailbox is
*        posted with details. The MDIO driver task will take the details and the write is executed
*
*        MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address
* @param phyOffset          [IN] Phy register offset where value is to be written
* @param phyRegVal          [IN] Value to write to register
*
*  @retval none
*/
void MDIO_regWrite(uint32_t mdioBaseAddress, uint32_t phyNum,
                   uint32_t phyOffset, uint16_t phyRegVal);
/**
* @brief Function to write into UserPHYSel0/1
*
* @param baseAddr MDIO Base Address
* @param phyinst UserPHYsel0/UserPHYsel1
* @param val value to be written
*
*  @retval none
*/
void MDIO_userPhySel(unsigned int baseAddr, unsigned int phyinst,
                     unsigned int val);
/**
* @brief Function to enable the MDIO Link interrupt
*
*       Used to enable MDIO Link Interrupt.
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyInst            [IN] O/1 to select the User phy select option
* @param phyNum             [IN] Phy address of the port
* @param linkSel            [IN] Flag to select to use MDIO mode or MLINK mode
*
*  @retval none
*/
void MDIO_enableLinkInterrupt(uint32_t mdioBaseAddress, uint32_t phyInst,
                              uint32_t phyNum, uint8_t linkSel);

/**
@}
*/

/** @addtogroup PHY_MDIO_COMMON_FUNCTIONS
 @{ */
/**
* @brief This function returns the PHY Link status
*
*         Reads Phy Link status from MDIO and returns the value
*         MDIO init shall be done before using this function
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyNum             [IN] Phy address of the port
*
*  @retval 1 On Linkup
*          0 On Linkdown
*/
uint8_t MDIO_getPhyLinkStatus(uint32_t mdioBaseAddress, uint32_t phyNum);
/**
* @brief API to Change Phy Configuration
*
*       This function can be used to change the Speed and duplexity of the PHY. The Api changes
*       the PHY config depending on the value of phyMode argument.
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress  [IN]  MDIO Base Address
* @param phyMode          [IN]  Speed/Duplexity mode to set
*                               PHY_CONFIG_AUTONEG
*                               PHY_CONFIG_100FD
*                               PHY_CONFIG_10FD
*                               PHY_CONFIG_100HD
* @param phyNum           [IN]  Phy address of the port
* @param mdioSemhandle    [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval none
*/
void MDIO_setPhyConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                       uint8_t phyMode, MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to get the PHY Speed and duplexity
*
*       Function returns the current Speed and duplexity configuration of the PHY.
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress  [IN] MDIO Base Address
* @param phyNum           [IN] Phy address of the port
* @param mdioSemhandle    [IN] Semaphore handle if thread safe MDIO access is used
*
* @retval PHY_CONFIG_10FD 10 Mbps and Full duplex
*          PHY_CONFIG_10HD 10 Mbps and Half duplex
*          PHY_CONFIG_100FD 100 Mbps and Full duplex
*          PHY_CONFIG_100HD 100 Mbps and Half duplex
*/
uint8_t MDIO_getPhyConfig(uint32_t mdioBaseAddress, uint32_t phyNum,
                          MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to get Autoneg/ Forced setting of the PHY
*
*       Function returns whether the PHY is in Auto Neg mode or forced mode.
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress  [IN] MDIO Base Address
* @param phyAddr          [IN] Phy address of the port
* @param mdioSemhandle    [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if Phy is in Autoneg mode
*          FALSE if Phy is in Forced mode
*/
uint8_t MDIO_getAutoNegStat(uint32_t mdioBaseAddress, uint32_t phyAddr,
                            MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to get the PHY ready status
*
**      Function returns whether the PHY is identified and ready
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyAddr            [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if Phy is ready
*          FALSE if Phy not ready
*/
uint8_t MDIO_getPhyIdentifyStat(uint32_t mdioBaseAddress, uint32_t phyAddr,
                                MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to get Autoneg complete status
*
*       MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyAddr            [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if Autoneg is complete
*          FALSE if Autoneg is not complete
*/
uint8_t MDIO_getAutoNegCompleteStat(uint32_t mdioBaseAddress, uint32_t phyAddr,
                                    MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to get the Link Partner Autoneg ability
*
**      MDIO init shall be done before using this function, The Sem handle need to be passed if the
*       Thread safe MDIO read is used, Pass NULL otherwise
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyAddr            [IN] Phy address of the port
* @param mdioSemhandle      [IN] Semaphore handle if thread safe MDIO access is used
*
*  @retval TRUE if parter in Autoneg
*          FALSE if parter in forced mode
*/
uint8_t MDIO_getLinkPartnerAutonegAble(uint32_t mdioBaseAddress,
                                       uint32_t phyAddr, MDIOSEM_Handle mdioSemhandle);
/**
* @brief Function to disable the MDIO Link interrupt
*
* @param mdioBaseAddress    [IN] MDIO Base Address
* @param phyInst            [IN] O/1 to select the User phy select option
* @param phyNum             [IN] Phy address of the port
* @param linkSel            [IN] Flag to select to use MDIO mode or MLINK mode
*
*  @retval none
*/
void MDIO_disableLinkInterrupt(uint32_t mdioBaseAddress, uint32_t phyInst,
                               uint32_t phyNum, uint8_t linkSel);

/**
@}
*/
#endif

