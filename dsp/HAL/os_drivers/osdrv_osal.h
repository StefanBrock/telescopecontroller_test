/**
 * @file osdrv_osal.h
 * @brief Header file for SysBIOS/RTOS Abstraction
 *
 */
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

#ifndef OSDRV_OSAL_H_
#define OSDRV_OSAL_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <ti/sysbios/BIOS.h>
#include "types.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/**Generic Macro for sucess*/
#define OSAL_OK  0
/**Generic Macro for failure*/
#define OSAL_FAILURE -1
/**Generic Macro for timeout*/
#define OSAL_TIMEOUT 2
/**Generic Macro for BIOS wait (for semaphore pend)*/
#define OSAL_SEM_WAIT_FOREVER   (UInt)~(0)

#define OSAL_NO_WAIT BIOS_NO_WAIT

/**Generic Macro for BIOS wait (for semaphore pend)*/
#define DMTIMER_SRC_CLK_FREQ    24000000

#ifdef AM335X_FAMILY_BUILD
    /**DMTimer 3 ID as used by Starterware API's for AM335x*/
    #define DMTIMER3_ID             1
    /**DMTimer 4 ID as used by Starterware API's for AM335x*/
    #define DMTIMER4_ID             2
    /**DMTimer 5 ID as used by Starterware API's for AM335x*/
    #define DMTIMER5_ID             3
#else
    /**DMTimer 4 ID as used by Starterware API's for AM437x*/
    #define DMTIMER4_ID             4
    /**DMTimer 5 ID as used by Starterware API's for AM437x*/
    #define DMTIMER5_ID             5
#endif

/**
 *  @brief    Opaque client reference to an instance of a SemOSAL
 *
 *  A SemOSAL_Handle returned from the SemOSAL_create represents that instance.
 *  and then is used in the other instance based functions (e.g. SemOSAL_post,
 *  SemOSAL_pend, etc.).
 */
typedef  void *SemOSAL_Handle;

/*!
 *  @brief    Mode of the semaphore
 */
typedef enum SemOSAL_Mode
{
    SemOSAL_Mode_COUNTING = 0x0,
    SemOSAL_Mode_BINARY   = 0x1
} SemOSAL_Mode;

/*!
 *  @brief    Basic SemOSAL Parameters
 *
 *  Structure that contains the parameters are passed into SemOSAL_create
 *  when creating a SemOSAL instance. The SemOSAL_Params_init function should
 *  be used to initialize the fields to default values before the application sets
 *  the fields manually. The SemOSAL default parameters are noted in
 *  SemOSAL_Params_init.
 */
typedef struct SemOSAL_Params
{
    SemOSAL_Mode mode;    /*!< Mode for the semaphore */
    char *name;           /*!< Name of the semaphore instance. Memory must
                               persist for the life of the semaphore instance */
} SemOSAL_Params;


/*!
 *  @brief  Prototype for the entry function for a hardware interrupt
 */
typedef void (*HwiOSAL_EntryFxn)(void *arg);
/**********************************************************************
 ************************* Function Declarations **********************
 **********************************************************************/

/**
* @brief DM Timer Run Mode Type. Values taken from Sys BIOS
*
*/
typedef enum
{
    DMTimerRunOSAL_Mode_CONTINUOUS = 0x0, //Timer_RunMode_CONTINUOUS,
    DMTimerRunOSAL_Mode_ONESHOT = 0x1, //Timer_RunMode_ONESHOT,
    DMTimerRunOSAL_Mode_DYNAMIC = 0x2 //Timer_RunMode_DYNAMIC
} DMTimerRunOSAL_Mode;

/**
* @brief DM Timer Start Mode Type. Values taken from Sys BIOS
*
*/
typedef enum
{
    DMTimerRunOSAL_Mode_AUTO = 0x1, //Timer_StartMode_AUTO,
    DMTimerRunOSAL_Mode_MANUAL = 0x2 //Timer_StartMode_USER

} DMTimerStartOSAL_Mode;

/**
* @brief DM Timer Period Type. Values taken from Sys BIOS
*
*/
typedef enum
{
    DMTimerRunOSAL_Mode_MICROSECS = 0x1, //Timer_PeriodType_MICROSECS,
    DMTimerRunOSAL_Mode_COUNTS = 0x2 //Timer_PeriodType_COUNTS

} DMTimerPeriodOSAL_Type;
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 *  @b Description
 *  @n
 *      The function is used to create a critical section.
 *
 *    *  @param[in]  count
 *      Semaphore count value
 *
 *    *  @param[in]  params
 *
 *  @retval
 *      Semaphore Handle created
 */

void *SemOSAL_create(uint32_t count, SemOSAL_Params *params);

/**
 *  @b Description
 *  @n
 *      The function is used to delete a critical section.
 *
 *  @param[in]  semHandle
 *      Semaphore handle to be deleted
 *
 *  @retval
 *      Not Applicable
 */
int32_t   SemOSAL_delete(void *semHandle);

/*!
 *  @brief  Initialize params structure to default values.
 *
 *  The default parameters are:
 *   - mode: SemOSAL_Mode_COUNTING
 *   - name: NULL
 *
 *  @param params  Pointer to the instance configuration parameters.
 */
void  SemOSAL_Params_init(SemOSAL_Params *params);

/*!
 *  @brief  Function to pend (wait) on a semaphore.
 *
 *  @param  handle  A SemOSAL_Handle returned from ::SemOSAL_create
 *
 *  @param  timeout Timeout (in milliseconds) to wait for the semaphore to
 *                  be posted (signalled).
 *
 *  @return Status of the functions
 *    - OSAL_OK: Obtain the semaphore
 *    - OSAL_TIMEOUT: Timed out. Semaphore was not obtained.
 */
int32_t SemOSAL_pend(void *handle, uint32_t timeout);

/*!
 *  @brief  Function to post (signal) a semaphore.
 *
 *  @param  handle  A SemOSAL_Handle returned from ::SemOSAL_create
 *
 *  @return Status of the functions
 *    - OSAL_OK: Released the semaphore
 */
int32_t   SemOSAL_post(void *semHandle);

/*!
 *  @brief  Function to disable interrupts to enter a critical region
 *
 *  @return key for accessing the critical region
 */
int32_t   HwiOSAL_enterCritical();

/**
 *  @brief  Function to restore interrupts to exit a critical region
 *
 *  @param  key
 *  @return none
 */
void  HwiOSAL_exitCritical(int32_t key);

/**
 *  @brief  Function to deregister a interrupt with BIOS
 *
 *  @param  hwiPtr pointer to interrupt task
 *
 *  @return True if successful
 */
int32_t  HwiOSAL_deregisterInterrupt(void *hwiPtr);

/**
 *  @brief  Function to register an interrupt with the BIOS. This provides an OS abstraction for developer
 *
 *  @param  intNum interrupt number for registration
 *
 *  @param  eventID interrupt number for registration, same as intNum. For compatibility with other API's it's kept like this
 *
 *  @param  entry entry function of the hardware interrupt
 *
 *  @param  arg  argument passed into hte entry function
 *
 *  @param  priority priority of interrupt
 *
 *  @return none
 */
void *HwiOSAL_registerInterrupt(int32_t intNum, int32_t eventID,
                                HwiOSAL_EntryFxn entry, void *arg, uint8_t priority);

/**
 *  @brief  Function to create Task
 *
 *  @param  priority    Task priority
 *
 *  @param  taskName    Name of task
 *
 *  @param  stackSize   Stack size of the task
 *
 *  @param  args        argument passed into the task
 *
 *  @param  taskFunc    function pointer for the task
 *
 *  @return Task_Handle
 */
void *TaskOSAL_create(uint8_t priority, uint8_t *taskName, uint32_t stackSize,
                      void *args, void *taskFunc);

/**
 *  @brief  Function to delete Task
 *
 *  @param  handle  Task handle to delete
 *
 *  @return Success
 */
int32_t TaskOSAL_delete(void *handle);

/**
 *  @brief  Function for Task sleep
 *
 *  @param  sleepTicks  Ticks for task_sleep
 *
 *  @return none
 */
void TaskOSAL_sleep(uint32_t sleepTicks);

/**
* @brief Function to create a DM Timer and Register an interrupt
*
* @param timerID DM TImer ID. Value starts with 1 for DMTimer 3
* @param runMode  Run Mode type. See SYSBIOS user guide
* @param periodType period type. See SYSBIOS user guide
* @param startMode start Mode type. See SYSBIOS user guide
* @param interval Interval in Micro Seconds
* @param ISRFunc ISR Function
* @param userArg user args to the Timer function
*
* @retval Timer_Handle handle to the timer created
*/
void *DMTimerOSAL_create(uint8_t timerID, DMTimerRunOSAL_Mode runMode,
                         DMTimerPeriodOSAL_Type periodType,
                         DMTimerStartOSAL_Mode startMode, uint32_t interval, void *ISRFunc,
                         void *userArg);

/**
 * @brief Tight loop delay in Micro seconds. Reads
 * Timestamp frequency and configures accordingly
 *
 * @param usDuration Delay in us
 *
 */
void DelayOSAL_microSeconds(uint64_t usDuration);

/**
 * @brief Tight loop delay in Milli seconds. Reads
 * Timestamp frequency and configures accordingly
 *
 * @param msDuration Delay in us
 *
 */
void DelayOSAL_milliSeconds(uint64_t msDuration);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */



#endif /* OSDRV_OSAL_H_ */
