/**
 * osdrv_lldConfig.c
*/
/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <stdlib.h>
#include "types.h"
#include "error.h"
#include "hw_types.h"
#include "hw_cm_per.h"
#include "chipdb.h"
#include "hw_icss.h"
#include "icss_emacStatistics.h"
#include "icss_emacStormControl.h"

#ifdef AM335X_FAMILY_BUILD
    #include "soc_am335x.h"
#elif defined AM43XX_FAMILY_BUILD
    #include "am437x.h"
#endif

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

uint8_t ICSSEmacDRVInit(ICSSEMAC_Handle handle, uint8_t instance)
{
    uint8_t retVal = E_FAIL;

    /* LLD attributes mallocs */
    handle->object = (ICSSEMAC_Object *)malloc(sizeof(ICSSEMAC_Object));
    handle->hwAttrs = (ICSSEMAC_HwAttrs *)malloc(sizeof(ICSSEMAC_HwAttrs));

    /* Callback mallocs */
    ICSSEMAC_CallBackObject *callBackObj = (ICSSEMAC_CallBackObject *)malloc(sizeof(
            ICSSEMAC_CallBackObject));

    callBackObj->learningExCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                          ICSSEMAC_CallBackConfig));
    callBackObj->rxCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                  ICSSEMAC_CallBackConfig));

    callBackObj->txCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                  ICSSEMAC_CallBackConfig));

    callBackObj->rxRTCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                    ICSSEMAC_CallBackConfig));
    callBackObj->rxNRTCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                     ICSSEMAC_CallBackConfig));
    callBackObj->port0ISRCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                        ICSSEMAC_CallBackConfig));
    callBackObj->port1ISRCallBack = (ICSSEMAC_CallBackConfig *)malloc(sizeof(
                                        ICSSEMAC_CallBackConfig));
    ((ICSSEMAC_Object *)handle->object)->callBackHandle = callBackObj;

    /*Allocate memory for learning*/
    ((ICSSEMAC_Object *)handle->object)->macTablePtr = (HashTable_t *)malloc(
                NUM_PORTS * sizeof(HashTable_t));

    /*Allocate memory for PRU Statistics*/
    ((ICSSEMAC_Object *)handle->object)->pruStat = (pruStatistics_t *)malloc(
                NUM_PORTS * sizeof(pruStatistics_t));

    /*Allocate memory for Host Statistics*/
    ((ICSSEMAC_Object *)handle->object)->hostStat = (hostStatistics_t *)malloc(
                NUM_PORTS * sizeof(hostStatistics_t));

    /*Allocate memory for Storm Prevention*/
    ((ICSSEMAC_Object *)handle->object)->stormPrevPtr = (stormPrevention_t *)malloc(
                NUM_PORTS * sizeof(stormPrevention_t));

    /* Base address initialization */
    if(NULL == ((ICSSEMAC_HwAttrs *)handle->hwAttrs)->emacBaseAddrCfg)
    {
        ((ICSSEMAC_HwAttrs *)handle->hwAttrs)->emacBaseAddrCfg =
            (ICSS_EmacBaseAddressHandle_T)malloc(sizeof(ICSS_EmacBaseAddrCfgParams));
    }

    ICSS_EmacBaseAddressHandle_T emacBaseAddr = ((ICSSEMAC_HwAttrs *)
            handle->hwAttrs)->emacBaseAddrCfg;

    if(TRUE == CHIPDBIsResourcePresent(CHIPDB_MOD_ID_PRU_ICSS, instance))
    {
        emacBaseAddr->dataRam0BaseAddr = CHIPDBBaseAddress(CHIPDB_MOD_ID_ICSS_DATARAM0,
                                         instance);
        emacBaseAddr->dataRam1BaseAddr = CHIPDBBaseAddress(CHIPDB_MOD_ID_ICSS_DATARAM1,
                                         instance);
        emacBaseAddr->l3OcmcBaseAddr = CHIPDBBaseAddress(CHIPDB_MOD_ID_OCMCRAM, 0);
        emacBaseAddr->prussCfgRegs = CHIPDBBaseAddress(CHIPDB_MOD_ID_ICSS_CFG_REG,
                                     instance);
        emacBaseAddr->prussIepRegs = CHIPDBBaseAddress(CHIPDB_MOD_ID_ICSS_IEP_REG,
                                     instance);
        emacBaseAddr->prussIntcRegs = CHIPDBBaseAddress(CHIPDB_MOD_ID_ICSS_INTC_REG,
                                      instance);
        emacBaseAddr->prussMiiMdioRegs = CHIPDBBaseAddress(CHIPDB_MOD_ID_ICSS_MDIO,
                                         instance);
        emacBaseAddr->prussMiiRtCfgRegsBaseAddr = CHIPDBBaseAddress(
                    CHIPDB_MOD_ID_ICSS_MIIRT_REG, instance);
        emacBaseAddr->prussPru0CtrlRegs = CHIPDBBaseAddress(
                                              CHIPDB_MOD_ID_ICSS_PRU0_CTL_REG, instance);
        emacBaseAddr->prussPru1CtrlRegs = CHIPDBBaseAddress(
                                              CHIPDB_MOD_ID_ICSS_PRU1_CTL_REG, instance);
        emacBaseAddr->sharedDataRamBaseAddr = CHIPDBBaseAddress(
                CHIPDB_MOD_ID_ICSS_SHAREDRAM, instance);
        retVal = S_PASS;
    }

    return retVal;
}
