/*
 * biss_drv.h
 *
 *  Created on: Dec 13, 2016
 *      Author: Dariusz Janiszewski
 */

#ifndef DRIVER_BISS_BISS_DRV_H_
#define DRIVER_BISS_BISS_DRV_H_

#include "HAL.h"

uint32_t* biss_get_angles(mcspiCfgObj_t *pCfgMcspi);

#endif /* DRIVER_BISS_BISS_DRV_H_ */
