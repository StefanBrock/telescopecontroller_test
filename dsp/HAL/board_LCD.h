/*
 * board_LCD.h
 *
 *  Created on: Aug 29, 2016
 *      Author: darjan
 */

#ifndef BOARD_LCD_H_
#define BOARD_LCD_H_
/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "board_platform.h"
#include "board_gpio.h"
#include "i2c_utils.h"

//#include "osdrv_osal.h" //for DelayOSAL_

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */
/* Red  */
#define TRI_COLOR0_RED      0
/* Green  */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/** \brief Structure holding i2c data transfer parameters. */
typedef struct i2cTxRxParams
{
    uint32_t            baseAddr;
    /**< i2c instance  base address. */
    uint32_t            slaveAddr;
    /**< i2c device slave address */
    uint32_t            bufLen;
    /**< Length of the buffer passed */
    uint8_t             *pBuf;
    /**< pointer to a buffer to hold the data */
    uint32_t            txThreshold;
    /**< Transmit FIFO threshold configuration. */
    uint32_t            rxThreshold;
    /**< Receive FIFO threshold configuration. */
    int32_t             statusFlag;
    /**< Status flag to indicate the Transfer Status */
} i2cTxRxParams_t;

/**
 *  \brief Enumerates the types of Operational modes supported
 *         for I2C Operations.
 */
typedef enum i2cOpMode
{
    I2C_OPMODE_MIN =  0U,
    /**< Min Operational mode type of the I2C device */
    I2C_OPMODE_POLLING = I2C_OPMODE_MIN,
    /**< Polling mode of the I2C device */
    I2C_OPMODE_INTERRUPT,
    /**< Interrupt mode of I2C device */
    I2C_OPMODE_DMA,
    /**< DMA mode of I2C device */
    I2C_OPMODE_MAX = I2C_OPMODE_DMA,
    /**< Max Operational mode type of the I2C device */
} i2cOpMode_t;

/**
 *  \brief I2C intr config
 */
typedef struct i2cIntrCfgObj
{
    intcTrigType_t intrTrigType;
    /**< Type of interrupt Edge/Level. */
    uint32_t intrLine;
    /**< Interrupt line number. */
    uint32_t intrPriority;
    /**< Interrupt priority level. */
    uint32_t isIntrSecure;
    /**< Secure Interrupt or not */
    void (*pFnIntrHandler)(uint32_t intrId, uint32_t cpuId, void *pUserParam);
    /**< Function pointer to Interrupt handler which needs to be
     *  registered with the interrupt Controller.- Application needs
     *  to initialize this. If set to null then polling Mode is assumed
     */
} i2cIntrCfgObj_t;

/** \brief Structure holding I2C device configuration parameters. */
typedef struct i2cDevCfg
{
    uint32_t            busSpeed;
    /**< I2C Bus Speed in kHz. */
    i2cAddrMode_t       addrMode;
    /**< Addressing mode to be configured for the device. */
    i2cOpMode_t      opMode;
    /**< Operational mode to be configured for the device. */
    uint32_t            txThreshold;
    /**< Transmit FIFO threshold configuration. */
    uint32_t            rxThreshold;
    /**< Receive FIFO threshold configuration. */
} i2cDevCfg_t;

/** \brief Structure holding I2C device Object parameters. */
typedef struct i2cCfgObj
{
    uint32_t       i2cInstNum;
    /**< i2c instance number. */
    uint32_t       i2cBaseAddr;
    /**< i2c instance base address. */
    uint32_t       slaveAddr;
    /**< Slave Address of the Part to be accessed */
    i2cDevCfg_t i2cDevCfg;
    /**< I2C device configuration params structure */
    i2cIntrCfgObj_t i2cIntrCfg;
    /**< I2C interrupt Configuration Structure */
} i2cCfgObj_t;

int32_t Board_initLCDscreen(i2cCfgObj_t *pI2cObj);
void Board_setLCDscreenOut(i2cCfgObj_t *pI2cCtrlCfg, uint8_t LCDscreen_val);
void Board_setLCDscreenCmd(i2cCfgObj_t *pI2cCtrlCfg, uint8_t LCDscreen_char);

void Board_setLCDscreenChar(i2cCfgObj_t *pI2cCtrlCfg, uint8_t LCDscreen_char);
void Board_setLCDscreenPrintf(i2cCfgObj_t *pI2cCtrlCfg, uint8_t *LCDscreen_buff);

void Board_setLCDscreenClear(i2cCfgObj_t *pI2cCtrlCfg);
void Board_setLCDscreenHome(i2cCfgObj_t *pI2cCtrlCfg);

void Board_setLCDscreenInit(i2cCfgObj_t *pI2cCtrlCfg);

#endif /* BOARD_LCD_H_ */
