/*
 * adc.c
 *
 * Copyright (c) 2015, Texas Instruments Incorporated
 * Copyright (c) 2016-2017 Dariusz Janiszewski
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */

/**
 *  /File adc.c
 *
 *  /Description: This file contains the ADC driver implementations
 ============================================================================ */

/*
 * modified by dariusz . janiszewski (at) put . poznan . pl
 *
 */

/* Include files */
#include "adc.h"
#include "stdio.h"
#include "am437x.h"
#include "hw_control_am43xx.h" //dj


#define PRCM_CLKSEL_MAG_ADC 0x44DF424C
#define PRCM_CLKCTRL_MAG_ADC 0x44DF8A30

#define ADC_HW_SYNC    1
#define ADC_FOC_HW    1

#ifdef    ADC_SDDF
signed short ADCInitialization(void)
{
    return 0;
}
#else /* ADC_SDDF */
signed short ADCInitialization (void)
{
    uint32_t adc_base;
#ifndef MINIMAL_ADC_CONFIG
    unsigned char fifo_level = 4; // Convert 3 currents + 1 voltage for debugging
#else
    unsigned char fifo_level = 1; // Convert 1 current  (2 ADCs) for optimal speed
#endif

    adc_base = CHIPDBBaseAddress(CHIPDB_MOD_ID_ADC1, 0);

    HWREG (PRCM_CLKSEL_MAG_ADC) = 0; //Select OSC clock input //dj?
    HWREG (PRCM_CLKCTRL_MAG_ADC) = 2; //Enable MAG ADC clocks	//dj?
    {
        volatile int i=0;
        for (;i < 100000; i++);
    }
    HWREG(adc_base + ADC0_ADC_CLKDIV) = 0x00000001; //ADC clock divider

    /* Configure the TS ADC controller by configuring the CNTL Register
     * Set the HW Preempt Enable
     * Set HW event mapping
     * Disable Touch Screen
     * ADC_Bias_Select is internal (0)
     * Set Step configuration write Not protected
     * Disable the step ID tag in the FIFO data
     * Disable the ADC conversion (FSM needs to be configured before getting enabled)
     * */
    HWREG(adc_base + ADC0_CTRL) = 0 | \
        ((ADC0_CTRL_HW_PREEMPT_NOPREEMPT << ADC0_CTRL_HW_PREEMPT_SHIFT) | \
        ((ADC0_CTRL_HW_EVT_MAPPING_HWEVTINPUT << ADC0_CTRL_HW_EVT_MAPPING_SHIFT)) | \
        (ADC0_CTRL_TOUCH_SCREEN_EN_DISABLE << ADC0_CTRL_TOUCH_SCREEN_EN_SHIFT) | \
        (ADC0_CTRL_STEPCONFIG_WRITEPROTECT_N_NOTPROTECTED  << ADC0_CTRL_STEPCONFIG_WRITEPROTECT_N_SHIFT) | \
        (ADC0_CTRL_EN_DISABLE << ADC0_CTRL_EN_SHIFT) | \
        (ADC0_CTRL_STEP_ID_TAG_WRZERO << ADC0_CTRL_STEP_ID_TAG_SHIFT));



    /* Configure the IDLE Step register to Default values */
    HWREG(adc_base + ADC0_IDLECONFIG) = 0;

    /* Configure Step register 1 to 8 for channel 0 and 1 only
     * Enable the Single ended Mode
     * Select Averaging sample as 1
     * Select FIFO 0 for storage
     * Select Single shot mode
     * */
    // Step 1: AIN1 --> IS_IHB1 MAG_ADC.AIN1 --> IS_MIHB2
    HWREG(adc_base + ADC0_STEPCONFIG(0)) =
        ((ADC0_STEPCONFIG_AVERAGING_NOAVG << ADC0_STEPCONFIG_AVERAGING_SHIFT) |
        (ADC0_STEPCONFIG_FIFO_SELECT_0 << ADC0_STEPCONFIG_FIFO_SELECT_SHIFT) |
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFP_SWC_SHIFT) |    /* VREFP: 111 = VREFP */
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFM_SWC_SHIFT) |    /* VREFN: 11 = VREFN */
//    (ADC0_STEPCONFIG_MODE_HW_SYNC_CONTINUOUS << ADC0_STEPCONFIG_MODE_SHIFT) |
    (ADC0_STEPCONFIG_MODE_HW_SYNC_ONESHOT<< ADC0_STEPCONFIG_MODE_SHIFT) |
        (0 << ADC0_STEPCONFIG_DIFF_CNTRL_SHIFT) |
        ((1) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INP_SWC_SHIFT)  |
        ((8) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INM_SWM_SHIFT) );

#ifndef MINIMAL_ADC_CONFIG
    /* Step Delay register configuration  */
    HWREG(adc_base + ADC0_STEPDELAY(0)) =
            ((3 << ADC0_STEPDELAY_OPENDELAY_SHIFT) |
            (5 << ADC0_STEPDELAY_SAMPLEDELAY_SHIFT));
    HWREG(adc_base + ADC0_STEPCONFIG(1)) =
        ((ADC0_STEPCONFIG_AVERAGING_NOAVG << ADC0_STEPCONFIG_AVERAGING_SHIFT) |
        (ADC0_STEPCONFIG_FIFO_SELECT_0 << ADC0_STEPCONFIG_FIFO_SELECT_SHIFT) |
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFP_SWC_SHIFT) |    /* VREFP: 111 = VREFP */
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFM_SWC_SHIFT) |    /* VREFN: 11 = VREFN */
    (ADC0_STEPCONFIG_MODE_HW_SYNC_CONTINUOUS << ADC0_STEPCONFIG_MODE_SHIFT) |
        (0 << ADC0_STEPCONFIG_DIFF_CNTRL_SHIFT) |
        ((3) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INP_SWC_SHIFT) |
        ((8) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INM_SWM_SHIFT) );
    /* Step Delay register configuration  */
    HWREG(adc_base + ADC0_STEPDELAY(1)) =
            ((3 << ADC0_STEPDELAY_OPENDELAY_SHIFT) |
            (5 << ADC0_STEPDELAY_SAMPLEDELAY_SHIFT));
    // Step 3: AIN0 --> MOTVM_ADC MAG_ADC.AIN0 --> Not connected
    HWREG(adc_base + ADC0_STEPCONFIG(2)) =
        ((ADC0_STEPCONFIG_AVERAGING_NOAVG << ADC0_STEPCONFIG_AVERAGING_SHIFT) |
        (ADC0_STEPCONFIG_FIFO_SELECT_0 << ADC0_STEPCONFIG_FIFO_SELECT_SHIFT) |
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFP_SWC_SHIFT) |    /* VREFP: 111 = VREFP */
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFM_SWC_SHIFT) |    /* VREFN: 11 = VREFN */
    (ADC0_STEPCONFIG_MODE_HW_SYNC_CONTINUOUS << ADC0_STEPCONFIG_MODE_SHIFT) |
        (0 << ADC0_STEPCONFIG_DIFF_CNTRL_SHIFT) |
/*        ((1) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INP_SWC_SHIFT)| */
        ((0) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INP_SWC_SHIFT) |
        ((8) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INM_SWM_SHIFT) );

    /* Step Delay register configuration  */
    HWREG(adc_base + ADC0_STEPDELAY(2)) =
            ((3 << ADC0_STEPDELAY_OPENDELAY_SHIFT) |
            (5 << ADC0_STEPDELAY_SAMPLEDELAY_SHIFT));
    HWREG(adc_base + ADC0_STEPCONFIG(3)) =
        ((ADC0_STEPCONFIG_AVERAGING_NOAVG << ADC0_STEPCONFIG_AVERAGING_SHIFT) |
        (ADC0_STEPCONFIG_FIFO_SELECT_0 << ADC0_STEPCONFIG_FIFO_SELECT_SHIFT) |
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFP_SWC_SHIFT) |    /* VREFP: 111 = VREFP */
        (3 << ADC0_TS_CHARGE_STEPCONFIG_SEL_RFM_SWC_SHIFT) |    /* VREFN: 11 = VREFN */
    (ADC0_STEPCONFIG_MODE_HW_SYNC_CONTINUOUS << ADC0_STEPCONFIG_MODE_SHIFT) |
        (0 << ADC0_STEPCONFIG_DIFF_CNTRL_SHIFT) |
        ((5) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INP_SWC_SHIFT) |
        ((8) << ADC0_TS_CHARGE_STEPCONFIG_SEL_INM_SWM_SHIFT) );
    /* Step Delay register configuration  */
    HWREG(adc_base + ADC0_STEPDELAY(3)) =
            ((0 << ADC0_STEPDELAY_OPENDELAY_SHIFT) |
            (0 << ADC0_STEPDELAY_SAMPLEDELAY_SHIFT));

#endif //#ifndef MINIMAL_ADC_CONFIG
    // configure pru_host_event0 (control module, pr1_host0_event)
    //HWREG(0x44E10000 + 0xFDC) = 0;
    HWREG(SOC_CONTROL_MODULE_REG+CTRL_MAG_EVT_CAPT)=0x05; //dj ADC1 event capture via ext_hw_trig
    // configure fifo threshold event generation
    // disable all IRQs
    HWREG(adc_base + ADC0_IRQEN_CLR) = 0x7FF;
    // clear all pending events
    HWREG(adc_base + ADC0_IRQSTS) = HWREG(adc_base + ADC0_IRQSTS);
   // HWREG(adc_base + ADC0_IRQSTS) = 0x04;
    // set threshold to xxx entries
    HWREG(adc_base + ADC0_FIFOTHR(0)) = (fifo_level-1);
    // enable threshold event
    HWREG(adc_base + ADC0_IRQEN_SET) = \
//            (1 << ADC0_IRQSTS_RAW_FIFO0_THR_SHIFT); //dj: IRQ source definition TODO: make interrupt after first conversion!!!
    		(1 << ADC0_IRQSTS_RAW_END_OF_SEQUENCE_SHIFT);


    /* Configure the TS ADC controller by configuring the CNTL Register
     * Set Step configuration write  protected
     *  Enable the ADC conversion
     **/
    HWREG(adc_base + ADC0_CTRL) &=
        ~(1 << ADC0_CTRL_STEPCONFIG_WRITEPROTECT_N_SHIFT);

    HWREG(adc_base + ADC0_STEPEN) = 0;

#if ADC_FOC_HW
    /* Enable the Step1 to 3 in the step enable register */
    // For HW triggered event only
    HWREG(adc_base + ADC0_STEPEN) =
        ( ADC0_STEPEN_STEP1_MASK
#ifndef MINIMAL_ADC_CONFIG
          | ADC0_STEPEN_STEP2_MASK |
            ADC0_STEPEN_STEP3_MASK |
          ADC0_STEPEN_STEP4_MASK
#endif //MINIMAL_ADC_CONFIG
         );
    HWREG(adc_base + ADC0_CTRL) |=
        (ADC0_CTRL_EN_ENABLE
#ifdef ADC_SIMULTANEOUS_SAMPLING
        | (1 << 2) | (1 << 6) /* Simultaneous sampling and Preamp Bypass */
#endif
        );
#else
#if ADC_HW_SYNC
    /* Enable the Step1 to 8 in the step enable register */
    // For HW triggered event only
    HWREG(adc_base + ADC0_STEPEN) =
         (ADC0_STEPEN_STEP1_MASK |
                 ADC0_STEPEN_STEP2_MASK |
                 ADC0_STEPEN_STEP3_MASK |
                 ADC0_STEPEN_STEP4_MASK |
                 ADC0_STEPEN_STEP5_MASK |
                 ADC0_STEPEN_STEP6_MASK |
                 ADC0_STEPEN_STEP7_MASK |
                 ADC0_STEPEN_STEP8_MASK );

    HWREG(adc_base + ADC0_CTRL) |=
            ADC0_CTRL_EN_ENABLE;
#endif
#endif
    return 0;
}
#endif /* ADC_SDDF */
